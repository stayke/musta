<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $helpers = array(
        'Session',
        'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form' => array('className' => 'BoostCake.BoostCakeForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
    );

    public $components = array(
        'Session',
        'RequestHandler',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish',
                    'fields' => array('username' => 'email')
                )
            )
        )
    );


    public function beforeFilter()
    {
        $this->Auth->allow();
    }

    public $pay = array(
        'minFee' => 1.50,
        'percentFee' => 2.5
    );

    protected function addProfile($user, $id, $wipefirst = false)
    {
        if (!$id) return false;

        $this->loadModel('Userinfo');
        $this->loadModel('Tempimage');

        if ($wipefirst) {
            debug($this->Userinfo->deleteAll(array('user_id' => $id)));
        }

        if (isset($user['firstaid'])) $saves[] = array('user_id' => $id, 'meta_key' => 'firstaid', 'meta_value' => $user['firstaid']);
        if (isset($user['insurance'])) $saves[] = array('user_id' => $id, 'meta_key' => 'insurance', 'meta_value' => $user['insurance']);
        if (isset($user['cpr'])) $saves[] = array('user_id' => $id, 'meta_key' => 'cpr', 'meta_value' => $user['cpr']);
        if (!isset($user['long_desc'])) $user['long_desc'] = '';
        if (!isset($user['short_desc'])) $user['short_desc'] = '';
        $saves[] = array('user_id' => $id, 'meta_key' => 'short_desc', 'meta_value' => $user['short_desc']);
        $saves[] = array('user_id' => $id, 'meta_key' => 'long_desc', 'meta_value' => $user['long_desc']);

        if (isset($user['qualifications'])) {
            foreach ($user['qualifications'] as $q) {
                $saves[] = array('user_id' => $id, 'meta_key' => 'qualification', 'meta_value' => json_encode($q));
            }
        }


        if (isset($user['speciality'])) {
            foreach ($user['speciality'] as $key => $s) {
                $saves[] = array('user_id' => $id, 'meta_key' => 'speciality', 'meta_value' => $key);
            }
        }
        if (isset($user['profile_pic']) && $user['profile_pic'] !== 'false' && $user['profile_pic']) {
            $saves[] = array('user_id' => $id, 'meta_key' => 'profile_pic', 'meta_value' => $user['profile_pic']);
        } else if ($user['pic']['id']) {
            $saves[] = array('user_id' => $id, 'meta_key' => 'profile_pic', 'meta_value' => $this->attachImageToUser($user['pic']['id']));
        }

        $this->Userinfo->saveAll($saves);
    }

    protected function addUserTerms($user, $id)
    {
        if (!$id || !isset($user['tcs'])) return false;

        foreach ($user['tcs'] as $q) {
            $saves[] = array('user_id' => $id, 'meta_key' => $q['show'], 'meta_value' => (isset($q['input'])) ? $q['input'] : true);
        }

        if ($saves) {
            $this->Userinfo->saveAll($saves);
        }

    }

    protected function addBankDetails($user, $id)
    {
        if (!$id) return false;

        $this->loadModel('Bankdeet');
        $this->Bankdeet->deleteAll(array('user_id' => $id));
        $save = $this->Bankdeet->save(array(
            'user_id' => $id,
            'acc_num' => $user['acc'],
            'acc_name' => $user['accname'],
            'address1' => $user['address1'],
            'address2' => (isset($user['address2'])) ? $user['address2'] : '',
            'bsb' => $user['bsb'],
            'postcode' => $user['postcode'],
            'state' => $user['state'],
            'suburb' => $user['suburb'],
            'acceptaud' => (isset($user['acceptaud'])) ? $user['acceptaud'] : '0'
        ));

        return $save;
    }

    protected function getUserDetail($userId, $detail)
    {
        $this->loadModel('Userinfo');
        return $this->Userinfo->find('all', array('conditions' => array('meta_key' => $detail, 'user_id' => $userId)));
    }

    // NDJ - sooo much of this was never designed to allow you to edit existing locations, tried to fix that
    //       $user - an array of locations.... love the naming convention here
    //       $id - The user id
    protected function addLocations($user, $id)
    {
        if (!$id) return false;
        $this->loadModel('Locationdb');
        $this->loadModel('LocationsLink');
        $this->loadModel('Location');
        $this->layout = 'ajax';
        $response = array();

        foreach ($user as $u) {

            $locationid = false;

            // NDJ - this part right here is not working nfi why, got pulled off before I could fix it
            if (isset($u["id"]) && $u["id"]) {
                $this->Location->id = $u["id"];
                $locationid = $u["id"];
            } else {
                $this->Location->create();
            }
            // NDJ - end
            $l = $this->Location->save(array(
                'name' => $u['name'],
                'notes' => (isset($u['notes'])) ? $u['notes'] : '',
//                'deals' => (isset($u['deals'])) ? $u['deals'] : '', // moved to it's own table
                'color' => (isset($u['Location']['color'])) ? $u['Location']['color'] : '#000',
                'user' => $id,
                'type' => $u['type'],
                'max_participants' => (isset($u['participants'])) ? $u['participants'] : 0,
                'image' => (isset($u['image']['id'])) ? $this->attachImageToUser($u['image']['id']) : null,
                'gst' => (isset($u['gst'])) ? $u['gst'] : false,
                'event' => (isset($u['event'])) ? $u['event'] : false
            ));


            if (!$locationid) {
                $locationid = $l['Location']['id'];
            }


//            echo "<pre>";
//            print_r($u["prices"]);
//            echo "</pre>";
//            die;
            $this->saveDeals($u['Deals'], $locationid, $id);

            $sgt = false;
            if (isset($u['event']) && $u["event"]) {
                $sgt = $this->saveBcPrice($u['prices'], $locationid, $id);
            } else {
                $sgt = $this->savePtPrice($u['prices'], $locationid, $id);
            }

            $this->Location->save(array('Location' => array('id' => $locationid, 'sgt' => ($sgt) ? 1 : 0)));

            foreach ($u['locations'] as $loc) {

                if ($record = $this->Locationdb->find('first', array('conditions' => array('placesid' => $loc['place_id'])))) {
                    $locationdbid = $record['Locationdb']['id'];
                } else {
                    $this->Locationdb->create();
                    $l = $this->Locationdb->save(array(
                        'Locationdb' => array(
                            'lat' => $loc['geometry']['location']['lat'],
                            'lng' => $loc['geometry']['location']['lng'],
                            'placesid' => $loc['place_id'],
                            'address' => $loc['formatted_address']
                        )
                    ));
                    $locationdbid = $l['Locationdb']['id'];
                }

                $this->LocationsLink->deleteAll(array("locations_id" => $locationid));

                $this->LocationsLink->create();
                $this->LocationsLink->save(array(
                    'locationdbs_id' => $locationdbid,
                    'locations_id' => $locationid
                ));

            }
        }

        $this->set('response', $response);
    }

    protected function savePtPrice($prices, $locationid, $userid)
    {

        $this->loadModel('ptPricing');
        $sgt = false;

        $price_ids = array();

        foreach ($prices as $key => $price) {

            if (!isset($price['active']) || $price['active'] == 0) continue;

            switch ($key) {
                case 'firstSession':
                    $length = 0;
                    break;

                case 'thirtymin':
                    $length = 30;
                    break;

                case 'fourtyfivemin':
                    $length = 45;
                    break;

                case 'sixtymin':
                    $length = 60;
                    break;

                default:
                    $length = 9999;
                    break;
            }

            if (isset($price["id"]) && $price["id"]) {
                $price_ids[] = $price["id"];
                $this->ptPricing->id = $price["id"];
            } else {
                $this->ptPricing->create();
            }

            $save = array(
                'user_id' => $userid,
                'session_length' => $length,
                'price' => $price['fee'],
                'first_only' => ($length == 0) ? true : false,
                'sgt' => (isset($price['sgtactive'])) ? ($price['sgtactive']) ? true : false : false,
                'sgt_fee' => (isset($price['sgtfee'])) ? $price['sgtfee'] : null,
                'locationid' => $locationid
            );
            if (isset($price['sgtactive']) && $price['sgtactive']) {
                $sgt = true;
            }
            $result = $this->ptPricing->save($save);
            if(isset($result["ptPricing"]["id"])){
                $price_ids[] = $result["ptPricing"]["id"];
            }
        }

        // Delete any old pricings that aren't there anymore
        $this->ptPricing->deleteAll(array(
            "locationid" => $locationid,
            "NOT" => array( "id" => $price_ids)
        ));

        return $sgt;
    }

    protected function saveBcPrice($prices, $locationid, $userid)
    {

        $this->loadModel('bcPricing');
        $sgt = false;

        $price_ids = array();

        foreach ($prices as $key => $price) {
            $save = array(
                'user_id' => $userid,
                'locations_id' => $locationid,
                'name' => $price['name'],
                'price' => $price['fee']
            );

            if (isset($price["id"]) && $price["id"]) {
                $price_ids[] = $price["id"];
                $this->bcPricing->id = $price["id"];
            } else {
                $this->bcPricing->create();
            }

            $result = $this->bcPricing->save($save);
            if(isset($result["bcPricing"]["id"])){
                $price_ids[] = $result["bcPricing"]["id"];
            }
        }

        // Delete any old pricings that aren't there anymore
        $this->bcPricing->deleteAll(array(
            "locations_id" => $locationid,
            "NOT" => array( "id" => $price_ids)
        ));

        return $sgt;
    }

    protected function saveDeals($deals, $locationid, $userid)
    {

        $this->loadModel('LocationDeal');
        $sgt = false;

        $deal_ids = array();

        foreach ($deals as $key => $deal) {
            $save = array(
                'location' => $locationid,
                'deal_text' => $deal['deal_text']
            );

            if (isset($deal["id"]) && $deal["id"]) {
                $deal_ids[] = $deal["id"];
                $this->LocationDeal->id = $deal["id"];
            } else {
                $this->LocationDeal->create();
            }

            $result = $this->LocationDeal->save($save);
            if(isset($result["LocationDeal"]["id"])){
                $deal_ids[] = $result["LocationDeal"]["id"];
            }
        }

        // Delete any old pricings that aren't there anymore
        $this->LocationDeal->deleteAll(array(
            "location" => $locationid,
            "NOT" => array( "id" => $deal_ids)
        ));

        return $sgt;
    }

    protected function acceptTerms($user, $id)
    {
        if (!$id || !isset($user['acceptterms'])) return false;
        $this->User->Save(array('id' => $id, 'accepted_terms' => ($user['acceptterms']) ? 1 : 0));
    }

    protected function attachImageToUser($id)
    {

        $this->loadModel('Tempimage');

        $pic = $this->Tempimage->find('first', array('conditions' => array('id' => $id)));

        if (!$pic) return false;

        rename(APP . 'Uploads/Pending/' . $pic['Tempimage']['secure'] . '.' . $pic['Tempimage']['extension'], APP . 'Uploads/' . $pic['Tempimage']['secure'] . '.' . $pic['Tempimage']['extension']);
//        $this->Tempimage->delete(array('id' => $id));

        return $pic['Tempimage']['secure'] . '.' . $pic['Tempimage']['extension'];
    }

    protected function getFee($type, $total)
    {
        $this->loadModel('PaymentFee');
        $this->layout = 'ajax';
        $this->view = 'pay';
        $options = $this->PaymentFee->find('list', array('fields' => array('meta_key', 'meta_value')));

        $fee = 0;

        switch ($type):

            case 'card_fee':
                $testFee = ($total / 100) * $options['card_fee'];
                $fee = ($testFee > $options['card_fee_min']) ? $testFee : (float)$options['card_fee_min'];
                break;

            case 'dd_fee':
                $testFee = ($total / 100) * $options['dd_fee'];
                $fee = ($testFee > $options['dd_fee_min']) ? $testFee : (float)$options['dd_fee_min'];
                break;

        endswitch;

        return $fee;
    }

    public function getFeeFront($type, $total)
    {
        $this->layout = 'ajax';
        $this->view = 'pay';
        $this->set('response', $this->getFee($type, $total));
    }

    public function getFavourites($additional = false)
    {
        $this->loadModel('Favourite');
        $this->loadModel('User');
        if (!$this->Auth->user('id')) {
            $this->set('response', array());
            return array();
        }

        $faves = $this->Favourite->find('all', array('conditions' => array('user_id' => $this->Auth->user('id'))));
        if (!$faves) $faves = array();
        if ($additional) {
            foreach ($faves as &$fave) {
                $fave['deets'] = $this->User->find('first', array('conditions' => array('id' => $fave['Favourite']['trainer_id'])));
                $fave['deets']['profile_pic'] = ($pic = $this->getUserDetail($fave['Favourite']['trainer_id'], 'profile_pic')) ? $pic['0']['Userinfo']['meta_value'] : 'defaultUser.jpg';
                $fave['deets']['specialities'] = $this->getUserDetail($fave['Favourite']['trainer_id'], 'speciality');
            }
        }

        return $faves;
    }

    protected function addNotification($notification)
    {
        $this->loadModel('Notification');
        $this->loadModel('User');

        $user = $this->User->find('first', array('conditions' => array('id' => $notification['user_id'])));

        if (!$user) return false;

        $this->Notification->save($notification);

        $Email = new CakeEmail();
        $Email->from(array('admin@musta.global' => 'Musta Global'));
        $Email->to($user['User']['email']);
        $Email->subject('New Notification');
        $Email->send($notification['text']);
    }

    public function saveDefaultTerms()
    {
        $this->layout = 'ajax';
        $this->view = 'add_client';

        if (!$this->Auth->user('id')) return false;

        $this->loadModel('Userinfo');
        $this->Userinfo->deleteAll(array('meta_key' => 'defaultTerms', 'user_id' => $this->Auth->user('id')));
        $this->Userinfo->save(array('meta_key' => 'defaultTerms', 'meta_value' => json_encode($this->request->data('terms')), 'user_id' => $this->Auth->user('id')));
    }

    public function getDefaultTerms()
    {
        $this->layout = 'ajax';
        $this->view = 'add_client';
        $this->loadModel('Userinfo');

        if (!$this->Auth->user('id')) return false;
        $terms = $this->Userinfo->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'), 'meta_key' => 'defaultTerms')));
        if (!$terms) {
            $this->set('return', array('Userinfo' => array()));
        } else {
            $this->set('return', json_decode($terms['Userinfo']['meta_value']));
        }
    }

    public function sitemap()
    {

    }

    protected function addUser($user, $id = false)
    {

        if (!$id && isset($user["id"])) {
            $id = $user["id"];
        }

        $this->loadModel('User');
        if ($this->User->find('count', array('conditions' => array('email' => $user['email']))) > 0 && !$id) return false;
        if ($this->User->find('count', array('conditions' => array('email' => $user['email'], 'NOT' => array('status' => 'unregistered')))) > 0 && $id && $user['email'] != $this->Auth->user('email')) return array('error' => 'Email already in use.');

        $postcode = "";
        if(isset($user['postcode'])){
            $postcode = $user['postcode'];
        }

        $save = array('User' => array(
            'abn' => (isset($user['abn'])) ? $user['abn'] : NULL,
            'address1' => $user['add1'],
            'address2' => (isset($user['add2'])) ? $user['add2'] : '',
            'businessname' => (isset($user['bname'])) ? $user['bname'] : NULL,
            'dob' => date('Y-m-d'),
            'email' => $user['email'],
            'first_name' => $user['firstName'],
            'fregisternumber' => (isset($user['freg'])) ? $user['freg'] : NULL,
            'fregistertype' => (isset($user['fregtype'])) ? $user['fregtype'] : NULL,
            'second_name' => $user['lastName'],
            'phone' => $user['phone'],
            'state' => $user['state'],
            'suburb' => $user['suburb'],
            'postcode' => $postcode,
            'role' => (isset($user['role'])) ? $user['role'] : false,
            'password' => (isset($user['password'])) ? $user['password'] : false,
            'gender' => $user['gender']
        ));

        if (!$save['User']['role']) unset($save['User']['role']);
        if (!$save['User']['password']) unset($save['User']['password']);

        if ($id) {
            $save['User']['id'] = $id;
        }

        if ($user['licence']['back']['id']) {
            $save['User']['licence_back'] = $this->attachImageToUser($user['licence']['back']['id']);
        }

        if ($user['licence']['front']['id']) {
            $save['User']['licence_front'] = $this->attachImageToUser($user['licence']['front']['id']);
        }

        if ($user['clientpic']['id']) {
            $save['User']['clientpic'] = $this->attachImageToUser($user['clientpic']['id']);
        }

        if ($user['bankstatement_pic']['id']) {
            $save['User']['bankstatement_pic'] = $this->attachImageToUser($user['bankstatement_pic']['id']);
        }

        $response = $this->User->save($save);
        if ($id) {
//            $this->User->addCreateNotification($id);
        } else {
            $this->User->addCreateNotification($response["User"]["id"]);
        }

        $response["test a"] = $save;
        $response["test b"] = $user;

        return $response;

    }


}
