<?php

class CalendarController extends AppController
{
    public $components = array('Integra');

    function index()
    {

    }

    // DEPRECATED
    // Saves the current calendar week
    //   currently it saves all adhoc hours and events on the current week when pressing done
    // This seems to be called at many times when individual things change not just when done is clicked and the whole week is saved
    function add()
    {
        //if(!$this->Auth->user){die();}
        $this->layout = 'ajax';

        // NDJ - this is slightly worrying
        $data = $this->Calendar->deleteAll(array(
            'date >=' => date('Y-m-d', strtotime($this->request->data['days']['Monday']['cleanDate'])),
            'date <' => date('Y-m-d', strtotime($this->request->data['days']['Monday']['cleanDate'] . ' +1 week')),
            'user' => $this->Auth->user('id')
        ));


        $save = array();

        foreach ($this->request->data['calendar'] as $key => $hour) {
            foreach ($hour as $k => $d) {
                if ($k == 'order' || !$d) continue;

                if ($hour[$k]['event']) {

                    $this->loadModel('LinkedSession');

                    if (isset($hour[$k]['additional']['repeat']) && $hour[$k]['additional']['repeat'] && !isset($hour[$k]['id'])) {
                        $this->LinkedSession->create();
                        $linkedid = $this->LinkedSession->save(array(
                            'nothing' => 1
                        ));
                        $hour[$k]['linked_id'] = $linkedid['LinkedSession']['id'];
                    }

                    if (isset($hour[$k]['additional']['colour'])) {
                        $color = $hour[$k]['additional']['colour'];
                    } else if (isset($hour[$k]['timings']['colour'])) {
                        $color = $hour[$k]['timings']['colour'];
                    } else {
                        $color = NULL;
                    }


                    // NDJ - #$#$# this is currently saving events twice when you click done...
                    if (isset($hour[$k]['additional']['repeat']) && $hour[$k]['additional']['repeat']) {

                        if ($hour[$k]['additional']['every'] == 'daily') {
                            $every = 'Days';
                        } else {
                            $every = 'Weeks';
                        }
                        $gap = $hour[$k]['additional']['frequency'];
                        $times = $hour[$k]['additional']['repeatduration'];
                        $count = 1;

                        $time_start = $key . ':00:00';
                        if(isset($hour[$k]['time_start'])) {
                            $time_start = $key . ":" . $hour[$k]['time_start'] . ":00";
                        }
                        while ($count <= $times) {
                            $changedate = date('Y-m-d', strtotime($this->request->data['days'][$k]['cleanDate'] . ' +' . ($gap * $count) . ' ' . $every));
                            $arr = array(
                                'location' => (is_array($hour[$k]['location'])) ? $hour[$k]['location']['Location']['id'] : $hour[$k]['location'],
                                'time_start' => $time_start,
                                'duration' => $hour[$k]['length'],
                                'date' => $changedate,
                                'user' => $this->Auth->user('id'),
                                'type' => $hour[$k]['type'],
                                'title' => $hour[$k]['title'],
                                'event' => $hour[$k]['event'],
                                'linked_id' => $hour[$k]['linked_id'],
                                'colour' => $color,
                                'notes' => isset($hour[$k]['additional']['notes']) ? $hour[$k]['additional']['notes'] : NULL
                            );
                            $save[] = $arr;
                            $count++;
                        }

                    }

                    $time_start = $key . ':00:00';
                    if(isset($hour[$k]['time_start'])) {
                        $time_start = $key . ":" . $hour[$k]['time_start'] . ":00";
                    }
                    if($hour[$k]['time_type'] != "rule") {
                        $arr = array(
                            'location' => (is_array($hour[$k]['location'])) ? $hour[$k]['location']['Location']['id'] : $hour[$k]['location'],
                            'time_start' => $time_start,
                            'duration' => $hour[$k]['length'],
                            'date' => date('Y-m-d', strtotime($this->request->data['days'][$k]['cleanDate'])),
                            'user' => $this->Auth->user('id'),
                            'type' => $hour[$k]['type'],
                            'title' => $hour[$k]['title'],
                            'event' => $hour[$k]['event'],
                            'linked_id' => (isset($hour[$k]['linked_id']) ? $hour[$k]['linked_id'] : NULL),
                            'colour' => $color,
                            'notes' => isset($hour[$k]['additional']['notes']) ? $hour[$k]['additional']['notes'] : NULL
                        );
                        if (isset($hour[$k]['id'])) {
                            $arr['id'] = $hour[$k]['id'];
                        }
                        $save[] = $arr;
                    }

                    if ($hour[$k]['additional']['sendemail']) {
                        foreach ($hour[$k]['additional']['selectedClients'] as $cl):
                            $c = $cl['TrainerClient'];
                            if ($c['id'] > 0):
                                $notification = array(
                                    'user_id' => $c['id'],
                                    'text' => $this->Auth->user('first_name') . ' has added you to an event',
                                    'target' => 0,
                                    'type' => 'information'
                                );
                                $this->addNotification($notification);
                            endif;
                        endforeach;
                    }
                } else {
                    if($hour[$k]['time_type'] != "rule") {
                        $arr = array(
                            'location' => $hour[$k]['location']['Location']['id'],
                            'time_start' => $key . ':00:00',
                            'duration' => $hour[$k]['length'],
                            'date' => date('Y-m-d', strtotime($this->request->data['days'][$k]['cleanDate'])),
                            'user' => $this->Auth->user('id'),
                            'type' => $hour[$k]['type'],
                            'title' => $hour[$k]['title'],
                            'event' => $hour[$k]['event']
                        );
                        if (isset($hour[$k]['id'])) $arr['id'] = $hour[$k]['id'];
                        $save[] = $arr;
                    }
                };
            }

        }
        if (count($save) > 0) {
            $response['error'] = ($this->Calendar->saveAll($save)) ? false : true;
        } else {
            $response['error'] = false;
        }

        $this->set('response', $response);
    }

    // Used to view a week in calendar/trainer view
    function view($id = false)
    {
        $this->layout = 'ajax';
        $this->loadModel('Location');
        $this->loadModel('Holiday');
        $this->loadModel('PtPricing');
        $this->loadModel('BcPricing');
        $this->loadModel('User');
        $this->loadModel('TrainerClient');
        $this->loadModel('BcBooking');
        $this->loadModel('PtBooking');
        $this->loadModel('Payment');
        $this->loadModel("CalendarRule");
        $this->loadModel("CalendarEvent");

        $days = array(
            0 => "Monday",
            1 => "Tuesday",
            2 => "Wednsday",
            3 => "Thursday",
            4 => "Friday",
            5 => "Saturday",
            6 => "Sunday",
        );

        $times = array(
            0 => "00",
            1 => "01",
            2 => "02",
            3 => "03",
            4 => "04",
            5 => "05",
            6 => "06",
            7 => "07",
            8 => "08",
            9 => "09",
            10 => "10",
            11 => "11",
            12 => "12",
            13 => "13",
            14 => "14",
            15 => "15",
            16 => "16",
            17 => "17",
            18 => "18",
            19 => "19",
            20 => "20",
            21 => "21",
            22 => "22",
            23 => "23"
        );

        if (!$id || $id == 'false') {
            $id = $this->Auth->user('id');
        }

        $cur_user_id = $this->Auth->user('id');

        $start = $this->request->data['dateStart'];
        $start = date('Y-m-d', strtotime($start));
        $finish = date('Y-m-d', strtotime($start . " +6 days"));
        $response["test start"] = $start;
        $response["test finish"] = $finish;

        // Loop through start to finish time to find what dates are equal to what days
        // #$#$# NDJ - potential to extend if more than a week long calendar
        $day_to_date = array();
        $i = 0;
        while($i < 7) {
            $cur_date = date('Y-m-d', strtotime($start . " +" . $i . " days"));
            $cur_day = date('l', strtotime($cur_date));
            $day_to_date[$cur_day] = $cur_date;
            $i++;
        }

        // Get all locations for this user
        $response['locations'] = $this->Location->find('all', array('conditions' => array('user' => $id)));

        // Get all the users and all the Trainers clients
        $clients = $this->User->find('all', array(
            "joins" => array(
                array(
                    "table" => "trainer_clients",
                    "alias" => "TrainerClient",
                    "type" => "LEFT",
                    "conditions" => array(
                        "User.id = TrainerClient.user_id"
//                        "TrainerClient.trainer_id = " . $id
                    )
                )
            ),
            "conditions" => array(
                "TrainerClient.trainer_id" => $id
            )
        ));
//        $clients = $this->TrainerClient->find('all', array('conditions' => array('trainer_id' => $id)));
        $clients_by_id = array();
        foreach($clients as $client){
            // Only pass info that we want sent to the front end (eg. not the full thing with password/email fields)
            // $clients_by_id[$client["User"]["id"]] = $client["User"];
            $clients_by_id[$client["User"]["id"]] = array(
                "first_name" => $client["User"]["first_name"],
                "second_name" => $client["User"]["second_name"]
            );
            // $clients_by_id[$client["User"]["id"]]["Userinfo"] = $client["Userinfo"]; // if we need userinfo add this
        }
//        echo "<pre>";
//        echo count($clients_by_id);
//        echo "<br/>";
//        print_r($clients_by_id);
//        echo "</pre>";
//        die;

        // Store locations via id for easier access later
        $id_locations = array();
        foreach ($response['locations'] as $loc) {
            $id_locations[$loc["Location"]["id"]] = $loc;
        }

//        $start_date = date("Y-m-d", strtotime($start . " +1 days"));
        $start_date = date("Y-m-d", strtotime($start));
        $finish_date = date("Y-m-d", strtotime($finish));

        // NDJ - yeah there should be a better way of doing multiple condition or statements than this.
        $data_rules = $this->CalendarRule->find('all', array(
            'conditions' => array(
                'user' => $id,
//                "('CalendarRule'.'date_start' <= " . $start_date . " OR 'CalendarRule'.'date_start' = null)"
                "OR" => array(
                    'date_start' => null,
                    "date_start <=" =>  $finish_date
                ),
                "OR " => array(
                    'date_end' => null,
                    "date_end >=" =>  $start_date
                )
//                "date_end >=" => $start_date
            )
        ));

        // Loops through all rules and adds them to calendar
        foreach ($data_rules as $d) {

            $time_start = explode(':', $d['CalendarRule']['time_start']);
            $time_end = explode(":", $d['CalendarRule']['time_end']);
            $time_start_hour = $time_start[0];
            $start_time = $time_start[1];
            $first_duration = "60" - $start_time;
            $final_hour = $time_end[0];
            $final_duration = $time_end[1];

            $location = $id_locations[$d['CalendarRule']['location']];

            // Get all days this rule applies, also check which days fit in by date encase the rule ends/starts this week
            $days_all = explode(",",$d['CalendarRule']['day']);
            $days = array();
            foreach($days_all as $day){
                $day_date = $day_to_date[$day];
                if((!$d['CalendarRule']["date_start"] || $day_date >= $d['CalendarRule']["date_start"])
                && (!$d['CalendarRule']["date_end"] || $day_date <= $d['CalendarRule']["date_end"])){
                    $days[] = $day;
                }
            }

            // Loop through all times and add sessions for all times between start and end time
            $start_reached = false;
            $duration = "60";
            $start_hour = "00";
            $bookable = array();
            foreach($times as $time) {
                if($time == $time_start_hour){
                    $start_reached = true;
                    $start_hour = $start_time;
                    $duration = $first_duration;
                    $bookable = array(
                        "00" => false,
                        "15" => false,
                        "30" => false,
                        "45" => false
                    );
                    foreach($bookable as $key => $val){
                        if($key >= $start_time){
                            $bookable[$key] = true;
                        }
                    }
                } else {
                    $duration = "60";
                    $start_hour = "00";
                    $bookable = array(
                        "00" => true,
                        "15" => true,
                        "30" => true,
                        "45" => true
                    );
                }
                if($time == $final_hour){
                    $duration = $final_duration;
                    if($final_duration == "00"){
                        break;
                    }
                    $bookable = array(
                        "00" => false,
                        "15" => false,
                        "30" => false,
                        "45" => false
                    );
                    foreach($bookable as $key => $val){
                        if(($key + 15) <= $duration){
                            $bookable[$key] = true;
                        }
                    }
                }

                if($start_reached) {
                    foreach($days as $day) {
                        $d['CalendarRule']["date"] = $day_to_date[$day]; // NDJ - need to load date and time of this hour into timings variable as much of the original code uses that timings object
                        $d['CalendarRule']["time_start"] = $time . ":" . $start_hour . ":00";
                        $response['sessions'][$time][$day] = array(
                            'id' => $d['CalendarRule']['id'],
                            'timings' => $d['CalendarRule'],
                            'start' => $start_hour,
                            'length' => $duration,
                            'location' => $location,
                            'type' => $d['CalendarRule']['type'],
                            'time_type' => "rule",
                            'title' => $d['CalendarRule']['title'],
                            'event' => false,
                            'pt_bookings' => array(),
                            'bc_bookings' => array(),
                            'bookable' => $bookable
                        );
                    }
                }
                if($time == $final_hour){
                    break;
                }
            }

        }


        $holiday = $this->Holiday->find('list', array(
            'conditions' => array(
                'user' => $id,
                'date >=' => $start,
                'date <=' => $finish
            ),
            'fields' => array('id', 'date')
        ));

        foreach ($holiday as $h) {
            $response['holiday'][date('l', strtotime($h))] = true;
        }

//        echo $finish_date . ", " . $start_date;
//        die;

        // NDJ - Get all events then loop through getting instances for each event and plotting them on calendar
        $data_events = $this->CalendarEvent->find('all', array(
            'conditions' => array(
                'user' => $id,
//                "status" => 2, // event may be unpublished while some children are still published
                "OR" => array(
                    'date_start' => null,
                    "date_start <=" =>  $finish_date
                ),
                "OR " => array(
                    'date_end' => null,
                    "date_end >=" =>  $start_date
                )
            )
        ));
        $data_events[] = array("CalendarEvent" => array("id" => 0)); // also search for events which are not based off any rule


        foreach($data_events as $event){

            $event_instances = $this->CalendarEvent->getInstances($start_date, $finish_date, $event["CalendarEvent"]["id"], 2, $id);

//            if(!$event["CalendarEvent"]["id"]){
//                var_dump($event_instances);
//                die;
//                continue;
//            }
            if(!$event_instances){
                continue;
            }

            foreach($event_instances as $inst){
                $time_start_array = explode(":", $inst["CalendarEventInstance"]["time_start"]);
                $time_start_hour = str_pad($time_start_array[0], 2, "0", STR_PAD_LEFT);
                $day = date("l", strtotime($inst["CalendarEventInstance"]["date"]));

                if($event["CalendarEvent"]["id"]) {
                    $inst["CalendarEventInstance"]["rule"] = $event["CalendarEvent"];
                    // instances now have colour so only copy colour if needed
                    if (!isset($inst["CalendarEventInstance"]["colour"])) {
                        $inst["CalendarEventInstance"]["colour"] = false;
                    }
                    if (!$inst["CalendarEventInstance"]["colour"]) {
                        $inst["CalendarEventInstance"]["colour"] = $event["CalendarEvent"]["colour"];
                    }
                    if (!isset($inst["CalendarEventInstance"]["title"])) {
                        $inst["CalendarEventInstance"]["title"] = false;
                    }
                    if (!$inst["CalendarEventInstance"]["title"]) {
                        $inst["CalendarEventInstance"]["title"] = $event["CalendarEvent"]["title"];
                    }
                } else {
                    $inst["CalendarEventInstance"]["rule"] = $inst["CalendarEventInstance"];
                }

                if(isset($inst["Pricing"])) {
                    $inst["CalendarEventInstance"]["prices"] = $inst["Pricing"];
                    if($inst["Pricing"]["name"]) {
                        $inst["CalendarEventInstance"]["title"] = $inst["Pricing"]["name"];
                    }
                }

                $inst["CalendarEventInstance"]["full"] = false;
                if(isset($inst[0]) && isset($inst[0]["attendance_count"])) {
                    $inst["CalendarEventInstance"]["attendance_count"] = $inst[0]["attendance_count"];
                    $spaces_left = $inst["CalendarEventInstance"]["spaces"] - $inst["CalendarEventInstance"]["spaces_unusable"] - $inst["CalendarEventInstance"]["attendance_count"];
                    if($spaces_left > 0){
                        $inst["CalendarEventInstance"]["full"] = false;
                    } else {
                        $inst["CalendarEventInstance"]["full"] = true;
                    }
                }

                $inst['CalendarEventInstance']["start"] = $time_start_array[1];
                if(!isset($response['sessions'][$time_start_hour][$day])){
                    $response['sessions'][$time_start_hour][$day] = array();
                }
                if(!isset($response['sessions'][$time_start_hour][$day]["events"])){
                    $response['sessions'][$time_start_hour][$day]["events"] = array();
                }
                if(!isset($response['sessions'][$time_start_hour][$day]["bookable"])){
                    $response['sessions'][$time_start_hour][$day]["bookable"] = array(
                        "00" => false,
                        "15" => false,
                        "30" => false,
                        "45" => false
                    );
                }

                $start_block = 0; // starting time rounded down to nearest 15 minutes
                if(($time_start_array[1] % 15) == 0){
                    $start_block = $time_start_array[1];
                } else {
                    $start_block = $time_start_array[1] - ($time_start_array[1] % 15);
                }

                $duration_left = intval($inst["CalendarEventInstance"]["duration"]);
                $hour_offset = 0;
                while($duration_left > 0) { // loop through this hour and future hours
                    $hour_string = str_pad($time_start_array[0] + $hour_offset, 2, "0", STR_PAD_LEFT);
                    if (isset($response['sessions'][$hour_string][$day])) {
                        if(isset($response['sessions'][$hour_string][$day]['bookable'])) {
                            $response['sessions'][$hour_string][$day]['bookable'][str_pad($start_block, 2, "0", STR_PAD_LEFT)] = false;
                        }
                    }
                    $start_block += 15;
                    $duration_left -= 15;
                    if($start_block >= 60){
                        $start_block -= 60;
                        $hour_offset++;
                    }
                    if($time_start_array[0] + $hour_offset >= 24){
                        break;
                    }
                }

                $response['sessions'][$time_start_hour][$day]['event'] = true;
                if($inst["CalendarEventInstance"]["bookable"] || $cur_user_id == $id) {
                    $response['sessions'][$time_start_hour][$day]['events'][] = $inst['CalendarEventInstance'];
                }
                $response['sessions'][$time_start_hour][$day]['pt_bookings'] = array();
                $response['sessions'][$time_start_hour][$day]['bc_bookings'] = array();

            }
        }

        $finish_midnight = date("Y-m-d", strtotime($finish . " +1 day"));

        // Loop through all private trainer bookings and add them to the calendar
        $data_pt_book = $this->PtBooking->find('all', array(
            'conditions' => array(
                'time >=' => $start,
                'time <=' => $finish_midnight,
                'to_user' => $id,
                'cancelled' => false,
                "pay_status IN ('complete', 'pending')"
            )
        ));

        $response["test bookings"] = $data_pt_book;

        foreach($data_pt_book as $pt_book) {
            $str_time = strtotime($pt_book["PtBooking"]["time"]);
            $day = date("l", $str_time);
            $time = date("H:i:s", $str_time);

            $time_array = explode(":", $time);

            $pt_book["PtBooking"]["start"] = $time_array[1];
//            $pt_book["PtBooking"]["user_detils"] = "";
            $pt_book["PtBooking"]["user_info"] = array();
            if (isset($clients_by_id[$pt_book["PtBooking"]["user"]])) {
                $pt_book["PtBooking"]["user_info"] = $clients_by_id[$pt_book["PtBooking"]["user"]];
            }

            if(!isset($response['sessions'][$time_array[0]][$day])){
                $response['sessions'][$time_array[0]][$day] = array();
            }
            if(!isset($response['sessions'][$time_array[0]][$day]["pt_bookings"])){
                $response['sessions'][$time_array[0]][$day]["pt_bookings"] = array();
            }
            // NDJ - #$#$# only load this data in if you have access to it
            $response['sessions'][$time_array[0]][$day]["pt_bookings"][] = $pt_book["PtBooking"];

            // remove bookable times (15 min blocks of time that are bookable and used by original system)
            if(!isset($response['sessions'][$time_array[0]][$day]['bookable'])){
                $response['sessions'][$time_array[0]][$day]['bookable'] = array(
                    "00" => false,
                    "15" => false,
                    "30" => false,
                    "45" => false
                );
            } else { // if already created by rules/adhoc remove only ones that you block
                $start_block = 0; // starting time rounded down to nearest 15 minutes
                if(($time_array[1] % 15) == 0){
                    $start_block = $time_array[1];
                } else {
                    $start_block = $time_array[1] - ($time_array[1] % 15);
                }
                $duration_left = intval($pt_book["PtBooking"]["length"]);
                $hour_offset = 0;
                while($duration_left > 0){ // loop through this hour and future hours
                    $hour_string = str_pad($time_array[0] + $hour_offset,2,"0",STR_PAD_LEFT);
                    if(isset($response['sessions'][$hour_string][$day])) {
                        $response['sessions'][$hour_string][$day]['bookable'][str_pad($start_block, 2, "0", STR_PAD_LEFT)] = false;
                    }
                    $start_block += 15;
                    $duration_left -= 15;
                    if($start_block >= 60){
                        $start_block -= 60;
                        $hour_offset++;
                    }
                    if($time_array[0] + $hour_offset >= 24){
                        break;
                    }
                }
            }
        }

        // Loop through locations and add prices to them
        foreach ($response['locations'] as &$l) {
            $viewing = $this->User->find('first', array('conditions' => array('id' => $id)));

            if (!$l["Location"]["event"]) {
                $l['pricing'] = $this->PtPricing->find('all', array('conditions' => array('user_id' => $id, 'locationid' => $l['Location']['id'])));
                if ($this->Auth->user('id') && $id != $this->Auth->user('id')) {
                    $firstTimeCheck = $this->Payment->find('count', array('conditions' => array('user' => $this->Auth->user('id'), 'to_user' => $id)));
                    if (!$firstTimeCheck > 0) {
                        $discount = false;
                        $amount = 0;
                        foreach ($l['pricing'] as $p) {
                            if ($p['PtPricing']['first_only']) {
                                $amount = $p['PtPricing']['price'];
                                $discount = true;
                            }
                        }
                        foreach ($l['pricing'] as &$p) {
                            $p['PtPricing']['price'] = ($discount) ? $amount : $p['PtPricing']['price'];
                        }
                    }
                }

            } else {
                $l['pricing'] = $this->BcPricing->find('all', array('conditions' => array('user_id' => $id, 'locations_id' => $l['Location']['id'])));
            }

        }


        $response['usertype'] = $this->User->find('first', array('conditions' => array('id' => $id), 'fields' => array('role')));

        $this->set('response', $response);
    }

    public function getRegister($id)
    {
        $this->layout = 'ajax';
        $this->loadModel('BcBooking');
        $this->loadModel('User');
        $this->loadModel('Userinfo');
        $this->loadModel('BcBoughtsession');
        $this->loadModel('BcUsedsessions');

        if (!$this->Auth->user('id')) {
            $this->set('response', '');
            return false;
        }

        $people = $this->BcBooking->find('list', array('conditions' => array('session_id' => $id, 'to_user' => $this->Auth->user('id')), 'fields' => array('user')));
        $responses = $this->User->find('all', array('conditions' => array('id' => $people)));

        foreach ($responses as &$response) {

            $response['Sessions']['bought'] = 0;

            $bought = $this->BcBoughtsession->find('all',
                array(
                    'conditions' => array(
                        'user' => $response['User']['id'],
                        'to_user' => $this->Auth->user('id')
                    ),
                    'fields' => 'amount'
                )
            );

            $used = $this->BcUsedsessions->find('count',
                array(
                    'conditions' => array(
                        'user' => $response['User']['id'],
                        'to_user' => $this->Auth->user('id')
                    )
                )
            );

            $thisused = $this->BcUsedsessions->find('count',
                array(
                    'conditions' => array(
                        'user' => $response['User']['id'],
                        'to_user' => $this->Auth->user('id'),
                        'session_id' => $id
                    )
                )
            );

            $response['Sessions']['thisused'] = ($thisused > 0) ? true : false;

            foreach ($bought as $s) $response['Sessions']['bought'] += $s['BcBoughtsession']['amount'];
            $response['Sessions']['used'] = $used;
            $response['Sessions']['remaining'] = $response['Sessions']['bought'] - $used;

        }

        $this->set('response', $responses);

    }


    // Deprecated function that I don't think ever worked.
    public function saveRegister($session_id)
    {
        $this->loadModel('BcUsedsession');
        $used = $this->request->data('used');
        $user = $this->request->data('user');

        if (!$used) {
            $this->BcUsedsession->deleteAll(array('session_id' => $session_id, 'user' => $user, 'to_user' => $this->Auth->user('id')));
        } else {
            $this->BcUsedsession->create();
            $this->BcUsedsession->save(array('session_id' => $session_id, 'user' => $user, 'to_user' => $this->Auth->user('id')));

            $bought = $this->BcBoughtsession->find('all',
                array(
                    'conditions' => array(
                        'user' => $response['User']['id'],
                        'to_user' => $this->Auth->user('id')
                    ),
                    'fields' => 'amount'
                )
            );

            $used = $this->BcUsedsessions->find('count',
                array(
                    'conditions' => array(
                        'user' => $response['User']['id'],
                        'to_user' => $this->Auth->user('id')
                    )
                )
            );
            $amount = 0;
            foreach ($bought as $s) $amount += $s['BcBoughtsession']['amount'];

            if ($bought - $used == 2) {
                $notification = array(
                    'user_id' => $user,
                    'text' => 'You have 1 session remaining',
                    'target' => 0,
                    'type' => 'information'
                );
                $this->addNotification($notification);
            }


        }
        $this->set('response', '');
    }


    // NDJ - some calendar rule functions #$#$# todo refactor these into their own controller
    // $id of the rule you want to view
    public function getRule($id)
    {
        $this->layout = 'ajax';

        $this->loadModel("CalendarRule");

        $calendar_rule = $this->CalendarRule->find('first', array(
            'conditions' => array(
                'id' => $id
            )
        ));

        if (isset($calendar_rule["CalendarRule"])) {
            $days = array();
            foreach(explode(",", $calendar_rule["CalendarRule"]["day"]) as $day){
                $days[$day] = true;
            }

            $calendar_rule["CalendarRule"]["day"] = $days;

            $response['rule'] = $calendar_rule["CalendarRule"];
        } else {
            $response["error"] = "could not find the calendar rule you requested";
        }

        $this->set('response', $response);
    }

    public function saveRule() {

        $this->layout = 'ajax';
        $response = array();

        $this->loadModel("CalendarRule");
        $this->loadModel("Location");

        $cur_user_id = $this->Auth->user('id');

        $rawRule = $this->request->data['newRule'];

        // Process and data check the rule before saving #$#$# todo more data and security checks here, user authorization etc.
        $newRule = array(); // #$#$# ideally check data and copy to new array rather than just pass everything from post into db

        if(isset($rawRule["user"]) && $cur_user_id != $rawRule["user"]){
            $response["error_messages"][] = "You are trying to create a rule for a user that is not yourself";
            $this->set('response', $response);
            return false;
        }

        if((strtotime($rawRule["time_start"]) > strtotime($rawRule["time_end"])) && $rawRule["time_end"]){
            $response["error_messages"][] = "The end time must be later than the start time" . $rawRule["time_start"] . ", " . $rawRule["time_end"];
            $this->set('response', $response);
            return false;
        }

        if((strtotime($rawRule["date_start"]) > strtotime($rawRule["date_end"])) && $rawRule["date_end"]){
            $response["error_messages"][] = "The end date must be later than the start date";
//            $response["error_messages"][] = "The end date must be later than the start date" . strtotime($rawRule["date_start"]) . " " . strtotime($rawRule["date_end"]);
//            $response["error_messages"][] = "The end date must be later than the start date" . $rawRule["date_start"] . " " . $rawRule["date_end"];
            $this->set('response', $response);
            return false;
        }

        $location = $this->Location->find('first', array(
            'conditions' => array(
                "id" => $rawRule["location"]
            )
        ));

        if(!$location){
            $response["error_messages"][] = "Please enter a valid location";
            $this->set('response', $response);
            return false;
        }

        if(!$rawRule["frequency"]){
            $rawRule["frequency"] = "weekly";
        }

        $rawRule["time_start"] = date("H:i:s", strtotime($rawRule["time_start"]));
        $rawRule["time_end"] = date("H:i:s", strtotime($rawRule["time_end"]));

        $new_days = array();
        foreach($rawRule["day"] as $day => $val){
            if($val){
                $new_days[] = $day;
            }
        }

        $rawRule["day"] = implode(",",$new_days);

        $id = false;
        if(isset($rawRule["id"])) {
            $id = $rawRule["id"];
        }

        if($id) {
            $this->CalendarRule->id = $id;
        } else {
            $rawRule["type"] = "pt";
            $rawRule["user"] = $this->Auth->user('id');
        }

        $response['result'] = $this->CalendarRule->save($rawRule);
//        $response['error'] = $this->CalendarRule->save($newRule);

        $this->set('response', $response);
    }

    // Used when I want to test code directly in cake no actual functionality in here
    public function testEvent(){
//        $this->CalendarEventInstance = ClassRegistry::init("CalendarEventInstance");
//        $this->loadModel("CalendarEvent");
//        $this->loadModel("CalendarEventInstance");
        $this->loadModel("PtBooking");
        $this->loadModel("EventAttendee");
        $this->loadModel("EventRegistration");
        $this->loadModel("Payment");
        $this->loadModel("CalendarEvent");
        $this->loadModel("CalendarEventInstance");

//        $this->CalendarEventInstance->bindPrices();
//        $response = $this->CalendarEventInstance->find('all', array(
//            "conditions" => array(
//                "CalendarEventInstance.id" => "146"
//            )
//        ));

        $response = $this->CalendarEvent->getInstances("2016-08-15", "2016-08-21", 0, 2, 136);


//        $location_id = 43;
//        $user_id = 136;
//        $prices = array(
//            array(
//                "name" => "test x",
//                "fee" => 13.00
//            )
//        );
//
//        $response = $this->saveBcPrice($prices, $location_id, $user_id);



//        $response = $this->CalendarEvent->getInstances("2016-07-04", "2016-07-10", 2);
//        $response = $this->Integra->addRecurringPayment(20);
//        $response = $this->Integra->addRecurringPayment(23);
//        $response = $this->Integra->addRecurringPayment(24);
//        $response = $this->Integra->addUser(137);

//        $response = $this->Integra->addRecurringPayment(1);

//        $response = $this->PtBooking->sendEmails(19, array("return" => true));
//        $response = $this->EventAttendee->sendEmails(24, array("return" => true));
//        $response = $this->EventRegistration->sendEmails(1, array("return" => true));
//        $response = $this->Payment->sendEmails(106, array("return" => true));

//        $response = $this->PtBooking->addNotification(20);
//        $response = $this->EventAttendee->addNotification(44);
//        $response = $this->EventRegistration->addNotification(7);
//        $response = $this->Payment->addNotification(105);

        echo "<pre>";
//        print_r($response);
        var_dump($response);
        echo "</pre>";

        die;
    }


}