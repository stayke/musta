<?php
class NotificationsController extends AppController {
    public function get(){
        $this->layout = 'ajax';
        
        if(!$this->Auth->user('id')) {$this->set('response',array()); return false;}
        
        $f = $this->Notification->find('all', array('conditions' => array('user_id' => $this->Auth->user('id'))));
        if(!$f){$f = array();}
        
        $this->set('response', $f);
    }
    
    public function read($id){
        $this->layout = 'ajax';
        $this->view = 'get';
        
        if(!$this->Auth->user('id')) {$this->set('response',false); return false;}
        
        
        
        $this->set('response', $this->Notification->deleteAll(array('user_id' => $this->Auth->user('id'), 'id' => $id)));
    }
    
    
}