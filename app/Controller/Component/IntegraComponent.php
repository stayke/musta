<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 2/06/2016
 * Time: 8:59 AM
 */
App::uses('Component', 'Controller');

class IntegraComponent extends Component {

    private $test_mode = true;

    // Corporate account details
//    private $test_api_user = "1553:1554";
    private $test_api_user = "1553:1528";
    private $test_api_pass = "D?g53E%k+yK";

    // Originally supplied details - has been moved to be a business under the above corporate
//    private $test_api_user = "1528";
//    private $test_api_pass = "Eb3%8Z{gQw2";
    private $test_api_url = "https://testpayments.integrapay.com.au/API/API.ashx";
    private $test_api_soap_url = "https://apitest.integrapay.com.au/PayLinkService.svc?WSDL";

    private $live_api_user = "1553";
    private $live_api_pass = "D?g53E%k+yK";
    private $live_api_url = "https://testpayments.integrapay.com.au/API/API.ashx";
    private $live_api_soap_url = "https://api.integrapay.com.au/PayLinkService.svc?WSDL";

    // Get the token for a remote hosted payment method
    public function getToken($id){
        $response = array();

        $this->Payment = ClassRegistry::init("Payment");
        $this->PaymentIntegra = ClassRegistry::init("PaymentIntegra");
        $this->User = ClassRegistry::init("User");

//        $this->loadModel("Payment");
//        $this->loadModel("PaymentIntegra");
//        $this->loadModel("User");

//        $cur_user_id = $this->Auth->user('id');

        if(!$id){
            $response["result"] = "FAIL 1";
            $response["error_messages"][] = "You need to specify a payment";
            return $response;
        }

        $payment = $this->Payment->find('first', array(
            'conditions' => array(
                'id' => $id
            )
        ));

        if(!$payment){
            $response["result"] = "FAIL 2";
            $response["error_messages"][] = "Can not find the required payment";
            return $response;
        }

        if($payment["Payment"]["status"] == "complete"){
            $response["result"] = "FAIL 3";
            $response["error_messages"][] = "You have already paid this payment";
            return $response;
        }

        if($payment["Payment"]["status"] == "failed"){
            $response["result"] = "FAIL 4";
            $response["error_messages"][] = "The payment failed please create a new payment";
            return $response;
        }

//        if($payment["Payment"]["user"] != $cur_user_id){
//            $response["result"] = "FAIL";
//            $response["error_messages"][] = "You do not have access to the specified payment";
//            return $response;
//        }

        $user = $this->User->find('first', array(
            'conditions' => array(
                'id' => $payment["Payment"]["user"]
            )
        ));

        // If the user doesn't exist
        if(!$user){
            $response["result"] = "FAIL 5";
            $response["error_messages"][] = "The user this payment is attached to doesn't exist";
            return $response;
        }

        $added_user = $this->addUser($user["User"]["id"]);

        $payer_id = "";

        if(!$added_user["error_messages"]){
            $payer_id = $user["User"]["id"];
        }

        $integra = $this->PaymentIntegra->find('first', array(
            'conditions' => array(
                'payment' => $id,
                'method' => "HostedRealTimePayment",
                'response' => null
            )
        ));

        // If the token already exists
        if($integra){
            $response["result"] = "OK";
            $response["webPageToken"] = $integra["PaymentIntegra"]["token"];
            return $response;
        }

//        $data_request = $this->request->data['payload'];

        // should get data from

        if($this->test_mode) {
            $api_user = $this->test_api_user;
            $api_pass = $this->test_api_pass;
            $api_url = $this->test_api_url;
        } else {
            $api_user = $this->live_api_user;
            $api_pass = $this->live_api_pass;
            $api_url = $this->live_api_url;
        }

        // round total to nearest cent
        $amount = round(($payment['Payment']['total'] * 100));

        $return_url = 'http://musta.ifactory.dev/payments/handleResponse';

        $xml_data = '
<request>'.
    '<username>' . $api_user . '</username>'.
    '<password>' . $api_pass . '</password>'.
    '<command>PreHostedRealTimePayment</command>'.
    '<returnUrl>' . $return_url . '</returnUrl>'.
    '<transactionID>'. 'OT' . $payment["Payment"]["id"] . '</transactionID>'.
    '<transactionAmountInCents>' . $amount . '</transactionAmountInCents>'.
    '<transparentRedirect>1</transparentRedirect>'.
//    '<payerUniqueID>' . $payer_id . '</payerUniqueID>'.
    '<payerFirstName>' . $user["User"]["first_name"] . '</payerFirstName>'.
    '<payerLastName>' . $user["User"]["second_name"] . '</payerLastName>'.
    '<payerAddressStreet>' . $user["User"]["address1"] . '</payerAddressStreet>'.
    '<payerAddressSuburb>' . $user["User"]["suburb"] . '</payerAddressSuburb>'.
    '<payerAddressState>' . strtoupper($user["User"]["state"]) . '</payerAddressState>'.
    '<payerAddressPostCode>' . "1234". '</payerAddressPostCode>'.
    '<payerAddressCountry>AUSTRALIA</payerAddressCountry>'.
    '<payerEmail>' . $user["User"]["email"] . '</payerEmail>'.
    '<payerPhone>' . $user["User"]["phone"] . '</payerPhone>'.
    '<payerMobile>' . $user["User"]["phone"] . '</payerMobile>'.
'</request>
';

//        echo $xml_data . "<br/><br/>";

        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache"
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $data_result = curl_exec($ch);
        curl_close($ch);

        $data = simplexml_load_string($data_result);

        $response["result"] = $data->result;
        if($data->result == "OK") {

            $new_integra = array(
                "payment" => $payment["Payment"]["id"],
                "method" => "HostedRealTimePayment",
                "token" => $data->webPageToken
            );

            $this->PaymentIntegra->save($new_integra);

            $response["result"] = "OK";
            $response["webPageToken"] = (string)$data->webPageToken;
        } else if($data->result == "ERROR"){
            $response["result"] = "FAIL 6";
            $response["testa"] = $data;
//            $response["testb"] = $xml_data; // dangerous do not leave active in live
            $response["error_messages"][] = $data->errormessage;
        }

        return $response;
    }

    public function fetchResponse($webPageToken, $method){

        $response = array();

        if(!$method){
            $method = "HostedRealTimePayment";
        }

        if($this->test_mode) {
            $api_user = $this->test_api_user;
            $api_pass = $this->test_api_pass;
            $api_url = $this->test_api_url;
        } else {
            $api_user = $this->live_api_user;
            $api_pass = $this->live_api_pass;
            $api_url = $this->live_api_url;
        }

        $xml_data = '<request>'.
'<username>'.$api_user.'</username>'.
'<password>'.$api_pass.'</password>'.
'<command>Post'.$method.'</command>'.
'<webPageToken>'.$webPageToken.'</webPageToken>'.
'</request>';

        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache"
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $data_result = curl_exec($ch);
        curl_close($ch);

        $data = simplexml_load_string($data_result);

        // Remove credit card info as it's unneeded
        $data->creditCardNumberMasked = "";
        $data->creditCardName = "";
        $data->creditCardType = "";

        $response["data"] = $data;

        $method_response = array();

        switch($method){
            case "AddToVault":
                $method_response = $this->handleVaultResponse($webPageToken, $data);
                break;
            case "HostedRealTimePayment":
            default:
                $method_response = $this->handleRealTimeResponse($webPageToken, $data);
        }

        $response = $response + $method_response;

        return $response;

    }

    public function handleRealTimeResponse($webPageToken, $data)
    {

        $response = array();
        $response["error_messages"] = array();

        $this->Payment = ClassRegistry::init("Payment");
        $this->PaymentIntegra = ClassRegistry::init("PaymentIntegra");
        $this->User = ClassRegistry::init("User");
        $this->PtBooking = ClassRegistry::init("PtBooking");
        $this->EventAttendee = ClassRegistry::init("EventAttendee");
        $this->EventRegistration = ClassRegistry::init("EventRegistration");
        $this->CalendarEvent = ClassRegistry::init("CalendarEvent");
        $this->CalendarEventInstance = ClassRegistry::init("CalendarEventInstance");
        $this->Location = ClassRegistry::init("Location");
        $this->Message = ClassRegistry::init("Message");
        $this->Notification = ClassRegistry::init("Notification");
        $this->TrainerClient = ClassRegistry::init("TrainerClient");

        // Find and update the record in the database
        $old_integra_data = $this->PaymentIntegra->find('first', array(
            'conditions' => array(
                'token' => $webPageToken
            )
        ));

        if ($old_integra_data) {
            $this->PaymentIntegra->id = $old_integra_data["PaymentIntegra"]["id"];
        }

        $new_integra_response = array(
            "response" => $data->asXML()
        );

        $this->PaymentIntegra->save($new_integra_response);

        // If not a successful payment
        if($data->resultID != "S"){
            // Error Codes and descriptions taken from Integra API documentation
            switch($data->resultID){
                case "R":
                    $response["error_messages"][] = "Retry Later";
                    break;
                case "F":
                    $response["error_messages"][] = "Payment Failed";
                    break;
                case "C":
                    $response["error_messages"][] = "Payment Cancelled";
                    break;
                case "N":
                    $response["error_messages"][] = "Not Processed";
                    break;
                case "E":
                    $response["error_messages"][] = "Form Error";
                    break;
                default:
                    $response["error_messages"][] = "An error has occurred";
            }

            switch($data->resultRejectionTypeID){
                case 1:
                    $response["error_messages"][] = "Insufficient Funds";
                    break;
                case 3:
                    $response["error_messages"][] = "Invalid Credit Card";
                    break;
                case 4:
                    $response["error_messages"][] = "Expired Credit Card";
                    break;
                case 5:
                    $response["error_messages"][] = "Technical Failure";
                    break;
                case 6:
                    $response["error_messages"][] = "Transaction Declined";
                    break;
                case 7:
                    $response["error_messages"][] = "Authority Revoked By Payer";
                    break;
                case 8:
                    $response["error_messages"][] = "Payer Deceased";
                    break;
                case 11:
                    $response["error_messages"][] = "Invalid Payer Contact Details";
                    break;
                default:
            }

            if($data->resultDescription) {
                $response["error_messages"][] = $data->resultDescription;
            }

            if($data->formErrorType) {
                $response["error_messages"][] = $data->formErrorType;
            }

            if($data->formErrorMessage) {
                $response["error_messages"][] = $data->formErrorMessage;
            }


            $transaction_code = $data->transactionID;

            $transaction_id = str_replace("OT", "",$transaction_code);

            $payment = $this->Payment->find('first', array(
                'conditions' => array(
                    'id' => $transaction_id
                )
            ));

            $payment["Payment"]["status"] = "failed";

            $this->Payment->save($payment);

            $response["payment"] = $payment;

        // If successful update payment
        } else if($data->resultID == "S"){
            $response["result"] = "OK";

            $transaction_code = $data->transactionID;

            // if the transactionID has "OT" at start then process it as a One Time PtBooking/attendance
            if(strpos($transaction_code, "OT") !== false){
                $transaction_id = str_replace("OT", "",$transaction_code);

                // Find any notifications about this payment and remove them
                $notification = $this->Notification->find('first', array(
                    'conditions' => array(
                        "type" => "paymentRequest",
                        'target' => $transaction_id
                    )
                ));

                if($notification){
                    $this->Notification->delete($notification["Notification"]["id"]);
                }

                $update_payment = array(
                    "status" => "complete"
                );

                $this->Payment->id = $transaction_id;

                $this->Payment->save($update_payment);

                $response["payment"] = $this->Payment->find('first', array(
                    'conditions' => array(
                        'id' => $transaction_id
                    )
                ));

                $trainer_id = 0;
                $location_id = 0;
                $user_id = 0;

                if($response["payment"]){
                    $user_id = $response["payment"]["Payment"]["user"];
                    $trainer_id = $response["payment"]["Payment"]["to_user"];

                    // Make sure user is registered as a client for said trainer
                    $trainer_client = $this->TrainerClient->find("first", array(
                        "conditions" => array(
                            "user_id" => $user_id,
                            "trainer_id" => $trainer_id
                        )
                    ));
                    if(!$trainer_client){
                        $this->TrainerClient->create();
                        $this->TrainerClient->save(array(
                            "user_id" => $user_id,
                            "trainer_id" => $trainer_id
                        ));
                    }
                }

                // Check if there was a ptBooking for this payment and
                $pt_booking = $this->PtBooking->find('first', array(
                    'conditions' => array(
                        'payment_id' => $transaction_id
                    )
                ));

                if($pt_booking) {

                    $this->PtBooking->id = $pt_booking["PtBooking"]["id"];

                    $update_booking = array(
                        "pay_status" => "complete"
                    );

                    $this->PtBooking->save($update_booking);

                    $response["booking"] = $pt_booking;

                    $trainer_id = $pt_booking["PtBooking"]["to_user"];
                    $location_id = $pt_booking["PtBooking"]["location"];
                    $user_id = $pt_booking["PtBooking"]["user"];

                    $this->PtBooking->sendEmails($pt_booking["PtBooking"]["id"], array("return" => true));
                    $this->PtBooking->addNotification($pt_booking["PtBooking"]["id"]);
                }


                // Check if the payment was for a registration
                $registration = $this->EventRegistration->find('first', array(
                    'conditions' => array(
                        'payment' => $transaction_id
                    )
                ));

                if($registration){

                    $update_registration = array(
                        "id" => $registration["EventRegistration"]["id"],
                        "status" => "complete"
                    );

                    $this->EventRegistration->save($update_registration);

                    // Update all attendances for that registration payment
                    $update_attendance_fields = array(
                        "status" => "'complete'"
                    );

                    $update_attendance_where = array(
                        "payment_id" => $transaction_id
                    );

                    $this->EventAttendee->updateAll($update_attendance_fields, $update_attendance_where);

                    $trainer_id = $registration["EventRegistration"]["to_user"];
                    $event_id = $registration["EventRegistration"]["event"];
                    $user_id = $registration["EventRegistration"]["user"];

                    $event = $this->CalendarEvent->find('first', array(
                        'conditions' => array(
                            'id' => $event_id
                        )
                    ));

                    $response["event"] = $event;

                    $location_id = $event["CalendarEvent"]["location"];

                    $this->EventRegistration->sendEmails($registration["EventRegistration"]["id"], array("return" => true));
                    $this->EventRegistration->addNotification($registration["EventRegistration"]["id"]);

                    // If payment was not for a registration check if it was for a single event
                } else {

                    $attendance = $this->EventAttendee->find('first', array(
                        'conditions' => array(
                            'payment_id' => $transaction_id
                        )
                    ));

                    if ($attendance) {

                        $update_attendance = array(
                            "id" => $attendance["EventAttendee"]["id"],
                            "status" => "complete"
                        );

                        $this->EventAttendee->save($update_attendance);

                        $trainer_id = $attendance["EventAttendee"]["to_user"];
                        $instance_id = $attendance["EventAttendee"]["event_instance"];
                        $user_id = $attendance["EventAttendee"]["user"];

                        $instance = $this->CalendarEventInstance->find('first', array(
                            'conditions' => array(
                                'id' => $instance_id
                            )
                        ));

                        $response["instance"] = $instance;

                        $location_id = $instance["CalendarEventInstance"]["location"];

                        $this->EventAttendee->sendEmails($attendance["EventAttendee"]["id"], array("return" => true));
                        $this->EventAttendee->addNotification($attendance["EventAttendee"]["id"]);

                    } else { // Not any of the known uses just send default payment emails
                        $this->Payment->sendEmails($transaction_id, array("return" => true));
                        $this->Payment->addNotification($transaction_id);
                    }
                }

                // Find trainer and location for displaying invoice
                $trainer = $this->User->find('first', array(
                    'conditions' => array(
                        'id' => $trainer_id
                    )
                ));

                $response["trainer"] = $trainer;

                $location = $this->Location->find('first', array(
                    'conditions' => array(
                        'id' => $location_id
                    )
                ));

                $response["location"] = $location;

            }

        }

        return $response;

    }

    public function handleVaultResponse($webPageToken, $data){

        $this->IntegraVault = ClassRegistry::init("IntegraVault");
        $this->User = ClassRegistry::init("User");
        $this->Message = ClassRegistry::init("Message");
        $this->Notification = ClassRegistry::init("Notification");
        $this->Location = ClassRegistry::init("Location");
        $this->Payment = ClassRegistry::init("Payment");
        $this->PtBooking = ClassRegistry::init("PtBooking");
        $this->RecurringPayment = ClassRegistry::init("RecurringPayment");
        $this->EventRegistration = ClassRegistry::init("EventRegistration");
        $this->EventAttendee = ClassRegistry::init("EventAttendee");
        $this->CalendarEvent = ClassRegistry::init("CalendarEvent");
        $this->TrainerClient = ClassRegistry::init("TrainerClient");

        $response = array();
        $response["error_messages"] = array();

        // Find and update the record in the database
        $old_integra_data = $this->IntegraVault->find('first', array(
            'conditions' => array(
                'token' => $webPageToken
            )
        ));

        if($old_integra_data){
            $this->IntegraVault->id = $old_integra_data["IntegraVault"]["id"];
        }

        $new_integra_response = array(
            "response" => $data->asXML()
        );

        $this->IntegraVault->save($new_integra_response);

        if($old_integra_data){
            if($old_integra_data["IntegraVault"]["rec_payment"]){

                // Find any notifications about this payment and remove them
                $notification = $this->Notification->find('first', array(
                    'conditions' => array(
                        "type" => "repeatPaymentRequest",
                        'target' => $old_integra_data["IntegraVault"]["rec_payment"]
                    )
                ));

                if($notification){
                    $this->Notification->delete($notification["Notification"]["id"]);
                }

                $recurring_payment = $this->RecurringPayment->find('first', array(
                    'conditions' => array(
                        'id' => $old_integra_data["IntegraVault"]["rec_payment"]
                    )
                ));

                if($recurring_payment){

                    // If the payment hasn't been successfully added to the Integra system try again
                    if(true){ // because Integra recurring payment is acting screwy again
//                    if($recurring_payment["RecurringPayment"]["status"] != "approved"){
                        $rec_add_response = $this->addRecurringPayment($recurring_payment["RecurringPayment"]["id"]);
                        if($rec_add_response["error_messages"]){
                            $response = $response + $rec_add_response;
                            return $response;
                        }
                    }

                    // Make sure user is registered as a client for said trainer
                    $trainer_client = $this->TrainerClient->find("first", array(
                        "conditions" => array(
                            "user_id" => $recurring_payment["RecurringPayment"]["user"],
                            "trainer_id" => $recurring_payment["RecurringPayment"]["to_user"]
                        )
                    ));
                    if(!$trainer_client){
                        $this->TrainerClient->create();
                        $this->TrainerClient->save(array(
                            "user_id" => $recurring_payment["RecurringPayment"]["user"],
                            "trainer_id" => $recurring_payment["RecurringPayment"]["to_user"]
                        ));
                    }

                    // Check if the payment was for a registration
                    $registration = $this->EventRegistration->find('first', array(
                        'conditions' => array(
                            'rec_payment' => $old_integra_data["IntegraVault"]["rec_payment"]
                        )
                    ));

                    $trainer_id = 0;
                    $location_id = 0;
                    $user_id = 0;

                    if($registration){

                        $update_registration = array(
                            "id" => $registration["EventRegistration"]["id"],
                            "status" => "complete"
                        );

                        $this->EventRegistration->save($update_registration);

                        // Update all attendances for that registration payment
                        $update_attendance_fields = array(
                            "status" => "'complete'"
                        );

                        $update_attendance_where = array(
//                            "payment_id" => $old_integra_data["IntegraVault"]["rec_payment"]
                            "registration" => $registration["EventRegistration"]["id"]
                        );

                        $this->EventAttendee->updateAll($update_attendance_fields, $update_attendance_where);

                        $trainer_id = $registration["EventRegistration"]["to_user"];
                        $event_id = $registration["EventRegistration"]["event"];
                        $user_id = $registration["EventRegistration"]["user"];

                        $event = $this->CalendarEvent->find('first', array(
                            'conditions' => array(
                                'id' => $event_id
                            )
                        ));

                        $response["event"] = $event;

                        $location_id = $event["CalendarEvent"]["location"];

                        $this->EventRegistration->sendEmails($registration["EventRegistration"]["id"], array("return" => true, "extra_message" => "Your payment details have been updated"));
                        $this->EventRegistration->addNotification($registration["EventRegistration"]["id"]);

                    } else { // Not a registration just send default payment emails

                        $this->RecurringPayment->sendEmails($old_integra_data["IntegraVault"]["rec_payment"], array("return" => true));
                        $this->RecurringPayment->addNotification($old_integra_data["IntegraVault"]["rec_payment"]);

                    }

                    // Find trainer and location for displaying invoice
                    $trainer = $this->User->find('first', array(
                        'conditions' => array(
                            'id' => $trainer_id
                        )
                    ));

                    $response["trainer"] = $trainer;

                    $location = $this->Location->find('first', array(
                        'conditions' => array(
                            'id' => $location_id
                        )
                    ));

                    $response["location"] = $location;

                }

            } else if($old_integra_data["IntegraVault"]["payment"]) {

                $payment_id = $old_integra_data["IntegraVault"]["payment"];

                $payment = $this->Payment->find("first", array(
                    "conditions" => array(
                        "id" => $payment_id
                    )
                ));

                if($payment) {
                    $update_payment = array(
                        "status" => "complete"
                    );

                    $this->Payment->id = $payment_id;

                    $this->Payment->save($update_payment);

                    $response["payment"] = $this->Payment->find('first', array(
                        'conditions' => array(
                            'id' => $payment_id
                        )
                    ));

                    $trainer_id = 0;
                    $location_id = 0;
                    $user_id = 0;

                    if ($response["payment"]) {
                        $user_id = $response["payment"]["Payment"]["user"];
                        $trainer_id = $response["payment"]["Payment"]["to_user"];

                        // Make sure user is registered as a client for said trainer
                        $trainer_client = $this->TrainerClient->find("first", array(
                            "conditions" => array(
                                "user_id" => $user_id,
                                "trainer_id" => $trainer_id
                            )
                        ));
                        if (!$trainer_client) {
                            $this->TrainerClient->create();
                            $this->TrainerClient->save(array(
                                "user_id" => $user_id,
                                "trainer_id" => $trainer_id
                            ));
                        }
                    }

                    // Check if there was a ptBooking for this payment and
                    $pt_booking = $this->PtBooking->find('first', array(
                        'conditions' => array(
                            'payment_id' => $payment_id
                        )
                    ));

                    if ($pt_booking) {

                        $this->PtBooking->id = $pt_booking["PtBooking"]["id"];

                        $update_booking = array(
                            "pay_status" => "complete"
                        );

                        $this->PtBooking->save($update_booking);

                        $response["booking"] = $pt_booking;

                        $trainer_id = $pt_booking["PtBooking"]["to_user"];
                        $location_id = $pt_booking["PtBooking"]["location"];
                        $user_id = $pt_booking["PtBooking"]["user"];

                        $this->PtBooking->sendEmails($pt_booking["PtBooking"]["id"], array("return" => true));
                        $this->PtBooking->addNotification($pt_booking["PtBooking"]["id"]);
                    }

                    $trainer = $this->User->find('first', array(
                        'conditions' => array(
                            'id' => $trainer_id
                        )
                    ));

                    $response["trainer"] = $trainer;

                    $user = $this->User->find('first', array(
                        'conditions' => array(
                            'id' => $user_id
                        )
                    ));

                    $response["user"] = $user;

                    $location = $this->Location->find('first', array(
                        'conditions' => array(
                            'id' => $location_id
                        )
                    ));

                    $response["location"] = $location;
                }

            }
        }

        return $response;

    }

    public function addUser($user_id){
        $this->User = ClassRegistry::init("User");

        $response = array();

        $response["error_messages"] = array();

        if($this->test_mode) {
            $api_user = $this->test_api_user;
//            $api_user = "1553"; // planned to add users to corporate instead of each individual trainer but this doesn't work with vault tokens (unless they've fixed it by now)
            $api_pass = $this->test_api_pass;
            $api_url = $this->test_api_soap_url;
        } else {
            $api_user = $this->live_api_user;
//            $api_user = "1553";
            $api_pass = $this->live_api_pass;
            $api_url = $this->live_api_soap_url;
        }

        $user = $this->User->find('first', array(
            'conditions' => array(
                'id' => $user_id
            )
        ));

        $api_url = "https://apitest.integrapay.com.au/basic/PayLinkService.svc?WSDL";

        $client = new SoapClient($api_url);

        $user_info = array(
            "username" => $api_user,
            "password" => $api_pass,
            "payerUniqueID" => $user["User"]["id"],
            "payerDisplayID" => $user["User"]["id"],
            "payerFirstName" => $user["User"]["first_name"],
            "payerLastName" => $user["User"]["second_name"],
            "dateOfBirth" => date("Ymd", strtotime($user["User"]["dob"])),
            "payerAddressLine1" => $user["User"]["address1"],
            "payerAddressLine2" => $user["User"]["address2"],
            "payerAddressSuburb" => $user["User"]["suburb"],
            "payerAddressState" => $user["User"]["state"],
            "payerAddressPostCode" => $user["User"]["id"],
            "payerAddressCountry" => "Australia",
            "payerEmail" => $user["User"]["email"],
            "payerPhone" => $user["User"]["phone"],
            "payerMobileNumber" => $user["User"]["phone"],
            "contractStartDate" => date("Ymd", strtotime($user["User"]["created"])),
            "sendRejectionNotificationSms" => false,// Service not implemented yet by Integra
            "sendRejectionNotificationEmail" => false,// Service not implemented yet by Integra
//            "salespersonUsername" => $api_pass,
//            "identificationType" => $api_pass,
//            "identificationNumber" => $api_pass,
//            "extraInfo" => $api_pass,
//            "auditUsername" => $api_pass
        );

        try {
            $user_result = $client->PayerSave($user_info);

            if($user_result->PayerSaveResult){
                $update_user = array(
                    "id" => $user_id,
                    "providers_payer_id" => $user_result->PayerSaveResult
                );
                $this->User->save($update_user);
            }

            $response["user_result"] = $user_result;
        } catch (Exception $e){
            $response["error_messages"][] = $e->getMessage();
        }

        return $response;
    }

    public function addRecurringPayment($recurring_payment_id){
        $this->RecurringPayment = ClassRegistry::init("RecurringPayment");

        $response = array();

        $response["error_messages"] = array();

        if($this->test_mode) {
            $api_user = $this->test_api_user;
            $api_pass = $this->test_api_pass;
            $api_url = $this->test_api_soap_url;
        } else {
            $api_user = $this->live_api_user;
            $api_pass = $this->live_api_pass;
            $api_url = $this->live_api_soap_url;
        }

        $api_url = "https://apitest.integrapay.com.au/basic/PayLinkService.svc?WSDL";

        $client = new SoapClient($api_url);

        $recurring_payment = $this->RecurringPayment->find('first', array(
            'conditions' => array(
                'id' => $recurring_payment_id
            )
        ));

        if(!$recurring_payment){
            $response["error_messages"][] = "Could not find the recurring payment you were attempting to pay";
            return $response;
        }

        // If you are trying to add a recurring payment that starts in the past ignore all previous dates.
        $recurring_date_start = $recurring_payment["RecurringPayment"]["date_start"];
        $num_repeat = $recurring_payment["RecurringPayment"]["num_repeat"];
        $cur_date = date("Y-m-d");
        if($recurring_date_start < $cur_date){
            $time_diff = strtotime($cur_date) - strtotime($recurring_date_start);
            $weeks_ahead = ceil($time_diff/(60*60*24*7));
            if($weeks_ahead >= $num_repeat){
                $response["error_messages"][] = "This payment is scheduled to end in the past and could not be created";
                return $response;
            } else {
                $recurring_date_start = date("Y-m-d", strtotime($recurring_date_start . " +" . $weeks_ahead . " weeks"));
                $num_repeat = $num_repeat - $weeks_ahead;
            }
        }

        // Now make the recurring payment
        $recurring_amount = round($recurring_payment["RecurringPayment"]["total"] * 100);

        $recurring_info = array(
            "username" => $api_user,
            "password" => $api_pass,
            "payerUniqueID" => $recurring_payment["RecurringPayment"]["user"],
            "scheduleType" => "LIMITED",
            "transactionAmountInCents" => $recurring_amount,
            "frequency" => "WEEKLY",
            "frequencyDay" => 6,
            "limitToTransactionCount" => $num_repeat,
            "limitToTransactionTotalInCents" => 0,
//            "scheduleTemplateID" => 1,
            "startDate" => date("Ymd", strtotime($recurring_date_start)),
            "removeAllPaymentsBeforeStartDate" => false,
            "scheduleID" => "REC" . $recurring_payment_id,
//            "auditUsername" => "admin"
        );

        $recurring_save = array(
            'id' => $recurring_payment_id
        );

        try {
            $response["recurring_result"] = $client->ScheduleGenerate($recurring_info);
            $recurring_save["integra_response"] = print_r($recurring_info, true) . print_r($response["recurring_result"], true); // should be empty if successful
            $recurring_save["status"] = "approved";
        } catch (Exception $e){
            $response["error_messages"][] = $e->getMessage();
            $recurring_save["integra_response"] = $e->getMessage();
            $recurring_save["failed"] = "failed";
        }

        $this->RecurringPayment->save($recurring_save);

        return $response;

    }

    public function getVaultToken($user_id, $recurring_payment_id = 0, $payment_id = 0){
        $response = array();

        $this->IntegraVault = ClassRegistry::init("IntegraVault");
        $this->User = ClassRegistry::init("User");

//        $this->loadModel("Payment");
//        $this->loadModel("PaymentIntegra");
//        $this->loadModel("User");
//        $cur_user_id = $this->Auth->user('id');

        $user = $this->User->find('first', array(
            'conditions' => array(
                'id' => $user_id
            )
        ));

        // If the user doesn't exist
        if(!$user){
            $response["result"] = "FAIL 7";
            $response["error_messages"][] = "The user could not be found";
            return $response;
        }

        // Create a new one regardless of if token already exists
//        $integra = $this->IntegraVault->find('first', array(
//            'conditions' => array(
//                'user' => $user_id,
//                'method' => "AddToVault"
//            )
//        ));

        // If the token already exists
//        if($integra){
//            $response["result"] = "OK";
//            $response["webPageToken"] = $integra["PaymentIntegra"]["token"];
//            return $response;
//        }

        // should get data from

        if($this->test_mode) {
            $api_user = $this->test_api_user;
//            $api_user = "1553";
            $api_pass = $this->test_api_pass;
            $api_url = $this->test_api_url;
        } else {
            $api_user = $this->live_api_user;
//            $api_user = "1553";
            $api_pass = $this->live_api_pass;
            $api_url = $this->live_api_url;
        }

        $return_url = 'http://musta.ifactory.dev/payments/handleResponse?method=AddToVault';

        $xml_data = '
<request>'.
            '<username>' . $api_user . '</username>'.
            '<password>' . $api_pass . '</password>'.
            '<command>PreAddToVault</command>'.
            '<returnUrl>' . $return_url . '</returnUrl>'.
            '<payerUniqueId>' . $user["User"]["id"]  . '</payerUniqueId>'.
            '<transparentRedirect>1</transparentRedirect>'.
            '</request>
';

        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache"
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $data_result = curl_exec($ch);
        curl_close($ch);

        $data = simplexml_load_string($data_result);

        $response["result"] = $data->result;
        if($data->result == "OK") {

            $new_integra = array(
                "user" => $user_id,
                "method" => "AddToVault",
                "token" => $data->webPageToken,
                "rec_payment" => $recurring_payment_id,
                "payment" => $payment_id
            );

            $this->IntegraVault->save($new_integra);

            $response["result"] = "OK";
            $response["vault_save"] = true;
            $response["webPageToken"] = (string)$data->webPageToken;
        } else if($data->result == "ERROR"){
            $response["result"] = "FAIL 8";
//            $response["test x"][] = $data;
//            $response["test y"][] = $xml_data;
            $response["error_messages"][] = $data->errormessage;
        }

        return $response;
    }
}