<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 29/06/2016
 * Time: 4:18 PM
 */
App::uses('AppController', 'Controller');


// Deprecated - use the admin controller instead
class UsermanagerController extends AppController
{

    // Default view which lets you see all trainers and clients
    public function index() {
        die;
        $this->loadModel('User');

        if($this->Auth->user('role') != 0 || !$this->Auth->user('id')){
            echo "You do not have access to this page";
            die;
        }

        $cur_user_id = $this->Auth->user('id');

        echo $cur_user_id;
        echo $this->Auth->user('role');


        $trainers = $this->User->find('all', array(
            'conditions' => array(
                'role' => 1
            )
        ));

        $this->set('trainers', $trainers);

        $participants = $this->User->find('all', array(
            'conditions' => array(
                'role' => 3
            )
        ));

        $this->set('participants', $participants);

    }

}