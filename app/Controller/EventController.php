<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 6/06/2016
 * Time: 1:50 PM
 */
class EventController extends AppController
{
    public $components = array('Integra');

    function index()
    {

    }

    // NDJ - Get a single event by ID
    // $id of the event you want to view
    public function getEvent($id)
    {
        $this->layout = 'ajax';

        $response = $this->retrieveEvent($id);

        $this->set('response', $response);
    }

    // NDJ - Get a single event by ID
    // $id of the event instance you want to view
    public function getEventInstance($id)
    {
        $this->layout = 'ajax';

        $response = $this->retrieveEventInstance($id);

        if(isset($response["instance"])) {
            $event_response = $this->retrieveEvent($response["instance"]["event"]);

            $response = $response + $event_response;
        }

        $this->set('response', $response);
    }

    public function getEventInstanceAllAttendees($id){
        $this->layout = 'ajax';

        $response = $this->retrieveEventInstance($id);

        if(isset($response["instance"])) {
            $event_response = $this->retrieveEvent($response["instance"]["event"]);

            $response = $response + $event_response;

            $attendee_response = $this->retrieveEventAllAttendees($response["instance"]["id"]);

            $response = $response + $attendee_response;

        }

        $this->set('response', $response);
    }

    // NDJ - Get a single event by ID
    // $id of the event instance and if you are attending you want to view
    public function getEventInstanceAttendee($id)
    {
        $this->layout = 'ajax';

        $response = $this->retrieveEventInstance($id);

        if(isset($response["instance"])) {
            $attendance_count = $this->retrieveEventInstanceAttendanceCount($id);
            $response["instance"]["attendance_count"] = intval($attendance_count);

            $event_response = $this->retrieveEvent($response["instance"]["event"]);

            $response = $response + $event_response;

            $cur_user_id = $this->Auth->user('id');

            $attendee_response = $this->retrieveEventAttendance($response["instance"]["id"], $cur_user_id);

            $response = $response + $attendee_response;

            if(isset($attendee_response["attendance"])){
                if($attendee_response["attendance"]["registration"]){
                    $registration_response = $this->retrieveEventRegistration($attendee_response["attendance"]["registration"], $response["instance"]["original_day"]);
                    $response = $response + $registration_response;
                }
            }
        }

        $this->set('response', $response);
    }

    private function retrieveEvent($id){
        $this->loadModel("CalendarEvent");

        $this->CalendarEvent->bindPrices();
        $calendar_event = $this->CalendarEvent->find('first', array(
            'conditions' => array(
                'CalendarEvent.id' => $id
            )
        ));

        if (isset($calendar_event["CalendarEvent"])) {
            $days = array();
            foreach(explode(",", $calendar_event["CalendarEvent"]["day"]) as $day){
                $days[$day] = true;
            }

            $calendar_event["CalendarEvent"]["day"] = $days;
            $time_start = explode(":",$calendar_event["CalendarEvent"]["time_start"]);

            $response['event'] = $calendar_event["CalendarEvent"];
        } else {
            $response["error"] = "could not find the calendar event you requested";
        }

        if(isset($calendar_event["Pricing"])){
            $response["event_pricing"] = $calendar_event["Pricing"];
        }

        return $response;

    }

    private function retrieveEventInstance($id){
        $this->loadModel("CalendarEventInstance");

        $this->CalendarEventInstance->bindPrices();
        $calendar_event = $this->CalendarEventInstance->find('first', array(
            'conditions' => array(
                'CalendarEventInstance.id' => $id
            )
        ));

        if (isset($calendar_event["CalendarEventInstance"])) {
           $response['instance'] = $calendar_event["CalendarEventInstance"];
        } else {
            $response["error"] = "could not find the calendar event instance you requested";
        }

        if(isset($calendar_event["Pricing"])){
            $response["inst_pricing"] = $calendar_event["Pricing"];
        }

        return $response;

    }

    // Get's the number of people who are attending an event instance
    // $id the event instance id
    private function retrieveEventInstanceAttendanceCount($id){
        $this->loadModel("EventAttendees");

        $attendance_count = $this->EventAttendees->find('count', array(
            'conditions' => array(
                'event_instance' => $id,
                'status' => 'complete'
            )
        ));

        return $attendance_count;

    }

    private function retrieveEventAttendance($instance_id, $user){
        $this->loadModel("EventAttendee");

        $response = array();

        $event_attendee = $this->EventAttendee->find('first', array(
            'conditions' => array(
                'event_instance' => $instance_id,
                'user' => $user,
                'status' => 'complete'
            )
        ));

        if (isset($event_attendee["EventAttendee"])) {
           $response['attendance'] = $event_attendee["EventAttendee"];
        } else {
//            $response["error"][] = "could not find the calendar event instance you requested";
        }

        return $response;

    }

    private function retrieveEventAllAttendees($instance_id){
        $this->loadModel("EventAttendee");

        $response = array();

        $event_attendee = $this->EventAttendee->find('all', array(
            'conditions' => array(
                'event_instance' => $instance_id
            )
        ));

        if ($event_attendee) {
           $response['attendees'] = $event_attendee;
        } else {
//            $response["error"][] = "could not find the calendar event instance you requested";
        }

        return $response;

    }

    private function retrieveEventRegistration($registration_id){
        $this->loadModel("EventRegistration");

        $response = array();

        $event_attendee = $this->EventRegistration->find('first', array(
            'conditions' => array(
                'EventRegistration.id' => $registration_id
            )
        ));

        if (isset($event_attendee["EventRegistration"])) {
           $response['registration'] = $event_attendee["EventRegistration"];
        } else {
//            $response["error"][] = "could not find the calendar event instance you requested";
        }

        return $response;

    }

    // Save an event
    public function saveEvent() {

        $this->layout = 'ajax';

        $this->loadModel("CalendarEvent");

        $rawEvent = $this->request->data['newEvent'];

        $start_date = $this->request->data["startDate"];

        // Process and data check the event before saving #$#$# todo more data and security checks here, user authorization etc.
        $newEvent = array(); // ideally check data and copy to new array rather than just pass everything from post into db

        $rawEvent["time_start"] = date("H:i:s", strtotime($rawEvent["time_start"]));

        $rawEvent["pricing"] = $rawEvent["class"];

        if($rawEvent["id"] == 0){
            $rawEvent["status"] = 2;
        }

        if(isset($rawEvent["delete"]) && $rawEvent["delete"]){
            $rawEvent["status"] = 0;
        }

        $new_days = array();
        foreach($rawEvent["day"] as $day => $val){
            if($val){
                $new_days[] = $day;
            }
        }

        $rawEvent["day"] = implode(",",$new_days);

        if(!(isset($rawEvent["colour"]) && $rawEvent["colour"])){
            $rawEvent["colour"] = "#000";
        }

        $id = $rawEvent["id"];
        if($id) {
            $this->CalendarEvent->id = $id;
        } else {
            $rawEvent["type"] = "pt";
            $rawEvent["user"] = $this->Auth->user('id');
        }

        $original_event = $this->CalendarEvent->find('first', array(
            'conditions' => array (
                'id' => $id
            )
        ));

        $response['result'] = $this->CalendarEvent->save($rawEvent);
//        $response['error'] = $this->CalendarEvent->save($newEvent);

        // Update all instances of this event from start date
        $response["instance result"] = $this->CalendarEvent->updateInstances($id, $start_date, $original_event);

        $this->set('response', $response);
    }

    public function saveEventInstance()
    {

        $this->layout = 'ajax';

        $this->loadModel("CalendarEventInstance");
        $this->loadModel("EventAttendee");

        $rawEvent = $this->request->data['newEvent'];

        // Process and data check the event before saving #$#$# todo more data and security checks here, user authorization etc.
        $newEvent = array(); // ideally check data and copy to new array rather than just pass everything from post into db

        $rawEvent["time_start"] = date("H:i:s", strtotime($rawEvent["time_start"]));

        $rawEvent["pricing"] = $rawEvent["class"];

        $id = false;
        if (isset($rawEvent["id"])) {
            $id = $rawEvent["id"];
        }
        if($id) {
            $this->CalendarEventInstance->id = $id;
        } else {
            $rawEvent["type"] = "pt";
            $rawEvent["status"] = 2;
            $rawEvent["user"] = $this->Auth->user('id');
            $rawEvent["original_day"] = date("l", strtotime($rawEvent["date"]));
        }

        if(isset($rawEvent["delete"]) && $rawEvent["delete"]){
            $rawEvent["status"] = 0;
        }

        if(!(isset($rawEvent["colour"]) && $rawEvent["colour"])){
            $rawEvent["colour"] = "#000";
        }

        $response['result'] = $this->CalendarEventInstance->save($rawEvent);
//        $response['error'] = $this->CalendarEventInstance->save($newEvent);

        // Save event attendee info
        foreach($rawEvent["selectedClients"] as $client){

            $attendance = $this->EventAttendee->find('first', array(
                "conditions" => array(
    //                "id" => $client["attendance_id"]
                    'user' => $client["TrainerClient"]["user_id"],
                    'event_instance' => $response["result"]["CalendarEventInstance"]["id"]
                )
            ));

            // if an old attendance save the checked off value
            if($attendance) {
                $checked_off = false;
                if(isset($client["checked_off"])){
                    $checked_off = $client["checked_off"];
                }
                $attendance_updates = array(
                    'id' => $attendance["EventAttendee"]["id"],
                    'checked_off' => $checked_off
                );

                $this->EventAttendee->save($attendance_updates);
            } else {
                // new attendance - not able to do these yet
            }
        }

        $this->set('response', $response);
    }

    // Save an event attendance

    public function saveAttendee(){
        $this->loadModel("CalendarEvent");
        $this->loadModel("CalendarEventInstance");
        $this->loadModel("EventAttendee");
        $this->loadModel("EventRegistration");
        $this->loadModel("Payment");
        $this->loadModel("RecurringPayment");
        $this->loadModel("User");
        $this->loadModel("Location");
        $this->loadModel("TrainerClient");

        $response = array();
        $response["error_messages"] = array();

        $rawInstance = $this->request->data['instance'];
        $rawAttendance = $this->request->data['attendance'];
        $rawRegistration = $this->request->data['registration'];

        $this->CalendarEventInstance->bindPrices();
        $instance = $this->CalendarEventInstance->find('first', array(
            'conditions' => array(
                'CalendarEventInstance.id' => $rawInstance["id"]
            )
        ));

        if(!$instance){
            $response["error_messages"][] = "could not find event instance";
            $this->set('response', $response);
            return false;
        }

        $trainer_id = $instance["CalendarEventInstance"]["user"];
        $payment_id = false;
        $recurring_payment_id = false;

        // If editing an existing registration
        if(isset($rawRegistration["id"])){
            $originalRegistration = $this->EventRegistration->find('first', array(
                'conditions' => array(
                    'id' => $rawRegistration["id"]
                )
            ));

            if(!$originalRegistration){
                $response["error_messages"][] = "could not find event registration specified";
                $this->set('response', $response);
                return false;
            }

            // Check if they are trying to pay for an event registration
            if($originalRegistration["EventRegistration"]["status"] != "complete"){

                if($originalRegistration["EventRegistration"]["rec_payment"]){
                    $recurring_payment_id = $originalRegistration["EventRegistration"]["rec_payment"];

                    $rec_payment = $this->RecurringPayment->find('first', array(
                        'conditions' => array(
                            'id' => $originalRegistration["EventRegistration"]["rec_payment"]
                        )
                    ));

                    $response["recurring_payment"] = $rec_payment["RecurringPayment"];

                    $vault_response = $this->Integra->getVaultToken($originalRegistration["EventRegistration"]["user"], $originalRegistration["EventRegistration"]["rec_payment"]);

                    $response = $response + $vault_response;

                } else if($originalRegistration["EventRegistration"]["payment"]) {
                    $payment_id = $originalRegistration["EventRegistration"]["payment"];

                    $payment = $this->Payment->find('first', array(
                        'conditions' => array(
                            'id' => $originalRegistration["EventRegistration"]["payment"]
                        )
                    ));

                    $response["payment"] = $payment["Payment"];

                    $token_response = $this->Integra->getToken();

                    $response = $response + $token_response;

                }

            }

        // if editing an existing attendance
        } else if(isset($rawAttendance["id"])) {

            $originalAttendance = $this->EventAttendee->find('first', array(
                'conditions' => array(
                    'id' => $rawAttendance["id"]
                )
            ));

            if (!$originalAttendance) {
                $response["error_messages"][] = "could not find the attendance";
                $this->set('response', $response);
                return false;
            }

            // If they haven't paid then pay for this attendance
            if ($originalAttendance["EventAttendee"]["status"] != "complete") {
                if ($originalAttendance["EventAttendee"]["payment_id"]) {
                    $payment = $this->Payment->find('first', array(
                        'conditions' => array(
                            'id' => $originalAttendance["EventAttendee"]["payment_id"]
                        )
                    ));

                    $token_array = $this->Integra->getToken($payment["Payment"]["id"]);

                    $payment_id = $payment["Payment"]["id"];

                    $response = $response + $token_array;

                } else {

                    $cur_user_id = $this->Auth->user('id');

                    $price = $instance["CalendarEventInstance"]["price"];

                    if($instance["Pricing"]["id"]) {
                        $price = $instance["Pricing"]["price"];
                    }

                    $new_payment = array(
                        "amount" => $price,
                        "total" => $price,
                        "datestamp" => date("Y-m-d H:i:s"),
                        "status" => "unpaid",
                        "user" => $cur_user_id,
                        "to_user" => $instance["CalendarEventInstance"]["user"]
                    );

                    $payment = $this->Payment->save($new_payment);

                    $token_array = $this->Integra->getToken($payment["Payment"]["id"]);

                    $payment_id = $payment["Payment"]["id"];

                    $response = $response + $token_array;
                }
            }

        // if creating a new attendance
        } else {

            // If new attendance or registration check to make sure user is marked as one of the trainers clients
            $client = array(
                "user_id" => $this->Auth->user('id'),
                "trainer_id" => $instance["CalendarEventInstance"]["user"]
            );
            $existing_client = $this->TrainerClient->find('first', array(
                'conditions' => $client
            ));

            if(!$existing_client){
                $this->TrainerClient->save($client);
            }

            // if for one session
            if($rawRegistration["num_sessions"] == 1) {

                $cur_user_id = $this->Auth->user('id');

                $price = $instance["CalendarEventInstance"]["price"];

                if($instance["Pricing"]["id"]) {
                    $price = $instance["Pricing"]["price"];
                }

                $new_payment = array(
                    "amount" => $price,
                    "total" => $price,
                    "datestamp" => date("Y-m-d H:i:s"),
                    "status" => "unpaid",
                    "user" => $cur_user_id,
                    "to_user" => $instance["CalendarEventInstance"]["user"]
                );

                $payment = $this->Payment->save($new_payment);

                $payment_id = $payment["Payment"]["id"];

                $new_attendance = array(
                    "event" => $instance["CalendarEventInstance"]["event"],
                    "event_instance" => $instance["CalendarEventInstance"]["id"],
                    "user" => $cur_user_id,
                    "to_user" => $instance["CalendarEventInstance"]["user"],
                    "payment_id" => $payment["Payment"]["id"],
                    "status" => "unpaid",
                );

                $attendance = $this->EventAttendee->save($new_attendance);

                $response["payment"] = $payment;
                $response["attendance"] = $attendance;

                $token_array = $this->Integra->getToken($payment["Payment"]["id"]);

                $response = $response + $token_array;

            // if for repeat sessions
            } else if($rawRegistration["num_sessions"] > 1) {

                if($rawRegistration["frequency_pay"] == "one_time") {
                    $cur_user_id = $this->Auth->user('id');

                    $price = $instance["CalendarEventInstance"]["price"];

                    if($instance["Pricing"]["id"]) {
                        $price = $instance["Pricing"]["price"];
                    }

                    $amount = $price * intval($rawRegistration["num_sessions"]);

                    $new_payment = array(
                        "amount" => $price,
                        "total" => $amount,
//                        "total" => $instance["CalendarEventInstance"]["price"],
                        "datestamp" => date("Y-m-d H:i:s"),
                        "status" => "unpaid",
                        "user" => $cur_user_id,
                        "to_user" => $instance["CalendarEventInstance"]["user"]
                    );

                    $payment = $this->Payment->save($new_payment);

                    $payment_id = $payment["Payment"]["id"];

                    $date_end = date("Y-m-d", strtotime($instance["CalendarEventInstance"]["date"] . " +" . ($rawRegistration["num_sessions"] -1) . " weeks"));

                    $new_registration = array(
                        "event" => $instance["CalendarEventInstance"]["event"],
                        "user" => $cur_user_id,
                        "to_user" => $instance["CalendarEventInstance"]["user"],
                        "payment" => $payment["Payment"]["id"],
                        "date_start" => $instance["CalendarEventInstance"]["date"],
                        "date_end" => $date_end,
                        "day" => date("l", strtotime($instance["CalendarEventInstance"]["date"])),
                        "num_sessions" => $rawRegistration["num_sessions"],
                        "status" => "unpaid",
                    );

                    $registration = $this->EventRegistration->save($new_registration);

                    $this->CalendarEvent->attend_event($instance["CalendarEventInstance"]["event"], $registration["EventRegistration"]["id"]);

                    $response["payment"] = $payment;
                    $response["registration"] = $registration;

                    $token_array = $this->Integra->getToken($payment["Payment"]["id"]);

                    $response = $response + $token_array;
                } else {
                    // Create a recurring payment
                    $cur_user_id = $this->Auth->user('id');

                    $price = $instance["CalendarEventInstance"]["price"];

                    if($instance["Pricing"]["id"]) {
                        $price = $instance["Pricing"]["price"];
                    }

                    $new_recurring_payment = array(
                        "user" => $cur_user_id,
                        "to_user" => $instance["CalendarEventInstance"]["user"],
                        "create_date" => date("Y-m-d H:i:s"),
                        "date_start" => $instance["CalendarEventInstance"]["date"],
                        "num_repeat" => $rawRegistration["num_sessions"],
                        "amount" => $price,
                        "total" => $price,
                        "frequency" => "weekly",
                        "day" => $instance["CalendarEventInstance"]["original_day"],
                        "status" => "pending"
                    );

                    $rec_payment = $this->RecurringPayment->save($new_recurring_payment);

                    $recurring_payment_id = $rec_payment["RecurringPayment"]["id"];

                    $date_end = date("Y-m-d", strtotime($instance["CalendarEventInstance"]["date"] . " +" . ($rawRegistration["num_sessions"] -1) . " weeks"));

                    $new_registration = array(
                        "event" => $instance["CalendarEventInstance"]["event"],
                        "user" => $cur_user_id,
                        "to_user" => $instance["CalendarEventInstance"]["user"],
                        "rec_payment" => $rec_payment["RecurringPayment"]["id"],
                        "date_start" => $instance["CalendarEventInstance"]["date"],
                        "date_end" => $date_end,
                        "day" => date("l", strtotime($instance["CalendarEventInstance"]["date"])),
                        "num_sessions" => $rawRegistration["num_sessions"],
                        "status" => "unpaid",
                    );

                    $registration = $this->EventRegistration->save($new_registration);

                    $this->CalendarEvent->attend_event($instance["CalendarEventInstance"]["event"], $registration["EventRegistration"]["id"]);

                    $response["rec_payment"] = $rec_payment;
                    $response["registration"] = $registration;

                    $user = $this->User->find("first", array(
                        "conditions" => array(
                            "id" => $cur_user_id
                        )
                    ));

                    $integra_user_response = $this->Integra->addUser($cur_user_id);
                    // Don't try to add recurring payment until we have done the vault stuff.
//                    $integra_rec_response = $this->Integra->addRecurringPayment($rec_payment["RecurringPayment"]["id"]);

                    $response = $response + $integra_user_response;
//                    $response = $response + $integra_rec_response;

                    if($response["error_messages"]){
                        $this->set('response', $response);
                        return false;
                    }

                    // If the user has stored payment details show that todo make this work
//                    if($user["User"]["payment_method_stored"]){

                    // If the user dose not have stored payment details prepare to make some
//                    } else {
                        $token_array = $this->Integra->getVaultToken($cur_user_id, $rec_payment["RecurringPayment"]["id"]);

                        $response = $response + $token_array;
                        if(isset($token_array["webPageToken"]) && $token_array["webPageToken"]){
                            $response["vault_save"] = true;
                        }
//                    }


                }
            }
        }

        // Get extra information that is now needed on the payment form.
        $trainer = $this->User->find("first", array(
            "conditions" => array(
                "id" => $trainer_id
            )
        ));

        if($trainer) {
            $response["trainer_name"] = $trainer["User"]["first_name"];
        }

        $location_name = false;

        if($payment_id) {
            $location_name = $this->getLocationFromPayment($payment_id);
        } else if($recurring_payment_id){
            $location_name = $this->getLocationFromRecurringPayment($recurring_payment_id);
        }

        $response["location_name"] = $location_name;


        $this->set('response', $response);

    }

    public function getLocationFromRecurringPayment($rec_id){
        $this->loadModel("EventRegistration");
        $this->loadModel("CalendarEvent");
        $this->loadModel("Location");

        $event_reg = $this->EventRegistration->find("first", array(
            "conditions" => array(
                "rec_payment" => $rec_id
            )
        ));

        if($event_reg) {
            $event_id = $event_reg["EventRegistration"]["event"];

            $event = $this->CalendarEvent->find("first", array(
                "conditions" => array(
                    "id" => $event_id
                )
            ));

            if($event){
                $location_id = $event["CalendarEvent"]["location"];

                $location = $this->Location->find("first", array(
                    "conditions" => array(
                        "id" => $location_id
                    )
                ));

                if($location){
                    return $location["Location"]["name"];
                }
            }
        }
        return false;

    }

    public function getLocationFromPayment($payment_id){
        $this->loadModel("EventRegistration");
        $this->loadModel("EventAttendee");
        $this->loadModel("CalendarEvent");
        $this->loadModel("PtBooking");
        $this->loadModel("Location");

        // First check event registrations
        $event_reg = $this->EventRegistration->find("first", array(
            "conditions" => array(
                "payment" => $payment_id
            )
        ));

        if($event_reg) {
            $event_id = $event_reg["EventRegistration"]["event"];

            $event = $this->CalendarEvent->find("first", array(
                "conditions" => array(
                    "id" => $event_id
                )
            ));

            if($event){
                $location_id = $event["CalendarEvent"]["location"];

                $location = $this->Location->find("first", array(
                    "conditions" => array(
                        "id" => $location_id
                    )
                ));

                if($location){
                    return $location["Location"]["name"];
                }
            }
        }

        // Now check event attendances
        $event_att = $this->EventAttendee->find("first", array(
            "conditions" => array(
                "payment_id" => $payment_id
            )
        ));

        if($event_att) {
            $event_id = $event_att["EventAttendee"]["event"];

            $event = $this->CalendarEvent->find("first", array(
                "conditions" => array(
                    "id" => $event_id
                )
            ));

            if($event){
                $location_id = $event["CalendarEvent"]["location"];

                $location = $this->Location->find("first", array(
                    "conditions" => array(
                        "id" => $location_id
                    )
                ));

                if($location){
                    return $location["Location"]["name"];
                }
            }
        }

        // Now check PT Bookings
        $book = $this->PtBooking->find("first", array(
            "conditions" => array(
                "payment_id" => $payment_id
            )
        ));

        if($book) {
            $location_id = $book["PtBooking"]["location"];

            $location = $this->Location->find("first", array(
                "conditions" => array(
                    "id" => $location_id
                )
            ));

            if($location){
                return $location["Location"]["name"];
            }
        }

        return false;

    }

}