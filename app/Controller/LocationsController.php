<?php
class LocationsController extends AppController {
	public function view($id) {
		$this->layout = 'ajax';
        $this->Location->bindModel(
            array(
                'hasMany' => array(
                    'PtPricing' => array(
                        'className' => 'PtPricing',
                        'foreignKey' => 'locationid'
                    ),
                    'BcPricing' => array(
                        'className' => 'BcPricing',
                        'foreignKey' => 'locations_id'
                    )
                )
            )
        );
		$locations = $this->Location->find('all', array('conditions' => array('user' => $id)));
	
		$this->set('response', $locations);
	}

    // NDJ - All the functions for saving the location from the popups are in the AppController...
	public function add(){
		$this->addLocations(array($this->request->data),$this->Auth->user('id'));
	}
	
	public function delete($id) {
		$this->loadModel('Calendar');
		$this->loadModel('CalendarRule');
		$this->layout = 'ajax';
		$candelete = $this->Location->find('all', array('conditions' => array('user' => $this->Auth->user('id'), 'Location.id' => $id)));
		if(count($candelete) > 0) {
			$this->Location->delete($id);
			$this->Calendar->deleteAll(array('location' => $id));
			$this->CalendarRule->deleteAll(array('location' => $id));
		}
		$response = '';
		$this->set('response', $response);
	}
	public function index(){
        
    }
	public function search(){
		$this->layout = 'ajax';
		$this->loadModel('Calendar');
		$response = array();
		
		$lat = $this->request->data['lat'];
		$lng = $this->request->data['lon'];
		$distance = $this->request->data['distance'];
		$filters = $this->request->data('filters');
        $check_times = $this->request->data("time");
        $gender = $this->request->data("gender");
        $showAll = $this->request->data("showAll");

        /*
		$filter = ' 1=1 AND (';
		$first = true;
		$sgt = 0;
		if(isset($filters['sgt']) && $filters['sgt'] == 1) {  $sgt = true; }
		if(isset($filters['gym']) && $filters['gym'] == 1) {  $filter .= ($first) ? ' ' : ' OR '; $first = false; $filter .= "(locations.type = 'gym' OR  locations.type = 'studio')"; }
		if(isset($filters['outdoor']) && $filters['outdoor'] == 1) { $filter .= ($first) ? ' ' : ' OR '; $first = false; $filter .= "(locations.type = 'outdoor')"; }
		if(isset($filters['mobile']) && $filters['mobile'] == 1) { $filter .= ($first) ? ' ' : ' OR '; $first = false; $filter .= "(locations.type = 'mobile')"; }
		if(isset($filters['bootcamp']) && $filters['bootcamp'] == 1) {  $filter .= ($first) ? ' ' : ' OR '; $first = false; $filter .= "(locations.type = 'outdoor' OR users.role = 2 )"; }
		if($sgt && !$first){
			$filter .=  ') AND '; 
			$filter .= " ( locations.sgt = true ) "; 
		} else if($sgt && $first){
			$filter .=  ' 1=1 ) AND '; 
			$filter .= " ( locations.sgt = true ) "; 
		} else if($first) {
			$filter .=  ' 1=1 ) '; 
		} else {
			$filter .=  ' ) '; 
		}

        // Only show approved trainers
        $filter .= " AND users.status = 'approved'";

        $query_string = "
			SELECT
			locationdbs.id, (
			  6371 * acos (
				cos ( radians($lat) )
				* cos( radians( lat ) )
				* cos( radians( lng ) - radians($lng) )
				+ sin ( radians($lat) )
				* sin( radians( lat ) )
			  )
			) AS distance,
			locationdbs.lat,
			locationdbs.lng,
			locationdbs.placesid,
			locationdbs.address,
			locations.user,
			locations.id,
			locations.type,
			locations.sgt,
			users.first_name,
			users.id,
			users.role
		  FROM locationdbs
		  INNER JOIN locations_links
		  ON locationdbs.id = locations_links.locationdbs_id
		  INNER JOIN locations
		  ON locations.id = locations_links.locations_id
		  INNER JOIN users
		  on locations.user = users.id
		  WHERE $filter
		  HAVING distance < $distance
		  ORDER BY distance
		";

		$response = $this->Location->query($query_string);
        */

        $query_select = array("
			SELECT
			locationdbs.id, (
			  6371 * acos (
				cos ( radians(" . $lat . ") )
				* cos( radians( lat ) )
				* cos( radians( lng ) - radians(" . $lng . ") )
				+ sin ( radians(" . $lat . ") )
				* sin( radians( lat ) )
			  )
			) AS distance,
			locationdbs.lat,
			locationdbs.lng,
			locationdbs.placesid,
			locationdbs.address,
			locations.user,
			locations.id,
			locations.type,
			locations.sgt,
			users.first_name,
			users.id,
			users.role");

        $query_joins = array(
            "INNER JOIN locations_links ON locationdbs.id = locations_links.locationdbs_id ",
            "INNER JOIN locations ON locations.id = locations_links.locations_id ",
            "INNER JOIN users ON locations.user = users.id "
        );

        $query_where = array(
            "users.status = 'approved'"
        );

        if(isset($filters['sgt']) && $filters['sgt'] == 1) {
            $query_where[] = "locations.sgt = true";
        }

        $query_locations = array();

        if(isset($filters['gym']) && $filters['gym'] == 1) {
            $query_locations[] = "locations.type = 'gym' OR  locations.type = 'studio'";
        }
        if(isset($filters['outdoor']) && $filters['outdoor'] == 1) {
            $query_locations[] = "locations.type = 'outdoor'";
        }
        if(isset($filters['mobile']) && $filters['mobile'] == 1) {
            $query_locations[] = "locations.type = 'mobile'";
        }

        if($query_locations){
            $query_where[] = "(" . implode(" OR ", $query_locations) . ") ";
        }

        $query_having = array();
        if(!$showAll) {
            $query_having = array(
                "HAVING distance < $distance"
            );
        }

        $query_order = array(
            "distance ASC"
        );

        // Optional sections only used if filled out

        if($gender){
            $query_where[] = "users.gender = '" . $gender ."'";
        }


        $time_start = false;
        $time_end = false;
        $date_start = false;
        $date_end = false;
        if($check_times) {
            // Use these both events and availability
            if (isset($check_times["time_start"]) && $check_times["time_start"]) {
                $time_start = date("H:i:s", strtotime($check_times["time_start"]));
            }
            if (isset($check_times["time_end"]) && $check_times["time_end"]) {
                $time_end = date("H:i:s", strtotime($check_times["time_end"]));
            }

            if (isset($check_times["date_start"]) && $check_times["date_start"]) {
                $date_start = date("Y-m-d", strtotime($check_times["date_start"]));
            } else { // always have a start date default to today
                $date_start = date("Y-m-d");
            }
            if (isset($check_times["date_end"]) && $check_times["date_end"]) {
                $date_end = date("Y-m-d", strtotime($check_times["date_end"]));
            }
        }

        // Check if any events are available during the time/date they asked for.
        if(isset($filters['event']) && $filters['event'] == 1) {
            $query_select[] = "COUNT(calendar_event_instances.id) as event_count";
            $events_where = array();

            $events_where[] = "calendar_event_instances.bookable = 1";
            $events_where[] = "calendar_event_instances.status = 2";

            if ($time_start && $time_end) { // if both time start and end
                $events_where[] = "calendar_event_instances.time_start > '" . $time_start . "'";
                $events_where[] = "calendar_event_instances.time_start < '" . $time_end . "'";
            } else if ($time_start) { // If only time start make sure it starts after then
                $events_where[] = "calendar_event_instances.time_start > '" . $time_start . "'";
            } else if ($time_end) { // If only time end make sure time starts before the end time
                $events_where[] = "calendar_event_instances.time_start < '" . $time_end . "'";
            }

            if ($date_start && $date_end) { // if both date start and end
                $events_where[] = "calendar_event_instances.date > '" . $date_start . "'";
                $events_where[] = "calendar_event_instances.date < '" . $date_end . "'";
            } else if ($date_start) { // If only date start make sure date is after then
                $events_where[] = "calendar_event_instances.date > '" . $date_start . "'";
            } else if ($date_end) { // If only date end make sure date is before end date
                $events_where[] = "calendar_event_instances.date < '" . $date_end . "'";
            }

            // Check if the rules is available on any of the days you searched for
            if(isset($check_times["day"]) && $check_times["day"]){
                $days = $check_times["day"];
//                $days = explode(",", $check_times["day"]);
                $day_or = array();
                foreach($days as $key => $day){
                    if($day) {
                        $day_or[] = " FIND_IN_SET('" . $key . "', calendar_event_instances.original_day) ";
                    }
                }
                if($day_or) {
                    $events_where[] = "(" . implode(" OR ", $day_or) . ")";
                }
            }

            $join_string = "LEFT JOIN calendar_event_instances ON locations.id = calendar_event_instances.location";
            if ($events_where) {
                $join_string .= " AND " . implode(" \r\nAND ", $events_where);
            }
            $query_joins[] = $join_string;

            // For now just do a having #$#$# todo upgrade to order by instead
            $query_having[] = " event_count > 0 ";
        } else if($check_times){

            // If checking times check that there is an available calendar rule that overlaps the times/dates/days that they have requested
            $query_select[] = "COUNT(calendar_rules.id) as rule_count";
            $rules_where = array();

            if($time_start && $time_end){ // if both time start and end
                $rules_where[] = "(calendar_rules.time_start > '" . $time_start . "' OR calendar_rules.time_end > '" . $time_start . "')";
                $rules_where[] = "(calendar_rules.time_start < '" . $time_end . "' OR calendar_rules.time_end < '" . $time_end . "')";
            } else if($time_start){ // If only time start make sure it doesn't end before then
//                $query_where[] = "calendar_rules.time_start < '" . $time_start . "'";
                $rules_where[] = "calendar_rules.time_end > '" . $time_start . "'";
            } else if($time_end){ // If only time end make sure time starts before the end time
                $rules_where[] = "calendar_rules.time_start < '" . $time_end . "'";
//                $query_where[] = "calendar_rules.time_end < '" . $time_end . "'";
            }

            // Check if the rules is available on any of the days you searched for
            if(isset($check_times["day"]) && $check_times["day"]){
                $days = $check_times["day"];
//                $days = explode(",", $check_times["day"]);
                $day_or = array();
                foreach($days as $key => $day){
                    if($day) {
                        $day_or[] = " FIND_IN_SET('" . $key . "', calendar_rules.day) ";
                    }
                }
                if($day_or) {
                    $rules_where[] = "(" . implode(" OR ", $day_or) . ")";
                }
            }

            // Use these for events as well below

            if($date_start && $date_end){
                $rules_where[] = "(calendar_rules.date_start > '" . $date_start . "' OR calendar_rules.date_end > '" . $date_start . "' OR calendar_rules.date_end IS NULL)";
                $rules_where[] = "(calendar_rules.date_start < '" . $date_end . "' OR calendar_rules.date_end < '" . $date_end . "' OR calendar_rules.date_start IS NULL)";
            } else if($date_start){ // If only date start make sure it doesn't end before then
                $rules_where[] =  "(calendar_rules.date_end > '" . $date_start . "' OR calendar_rules.date_end IS NULL)";
            } else if($date_end){ // If only date end make sure date starts before the end date
                $rules_where[] =  "(calendar_rules.date_start < '" . $date_end . "' OR calendar_rules.date_start IS NULL)";
            }

            $join_string = "LEFT JOIN calendar_rules ON locations.id = calendar_rules.location ";
            if($rules_where){
                $join_string .= " AND " . implode(" \r\nAND ", $rules_where);
            }
            $query_joins[] = $join_string;

            // For now just do a having #$#$# todo upgrade to order by instead
            $query_having[] = " rule_count > 0 ";

        }

        // Line breaks are only in there so it's easier to debug
        $full_query = implode(", \r\n", $query_select) .
            "\r\n FROM locationdbs \r\n" .
            implode(" \r\n", $query_joins) .
            "\r\n WHERE " . implode("\r\n AND ", $query_where) . "\r\n " .
             " GROUP BY CONCAT(locationdbs.id, locations.id) \r\n" .
            implode("\r\n AND ", $query_having) .
            "\r\n ORDER BY " . implode("\r\n ", $query_order);

        $response["results"] = $this->Location->query($full_query);
        $response["test a"] = $full_query;
//        $response["test b"] = $gender;

        /*
		// NDJ - deprecated method for checking that there are available times when only calendar system had available days
		foreach($response as $key => &$r){
			if($cal = $this->Calendar->find('all', array('conditions' => array('location' => $r['locations']['id'], 'user' => $r['users']['id'], 'date >=' => date('Y-m-d'))))){
				$r['days'] = array(
					0 => false,
					1 => false,
					2 => false,
					3 => false,
					4 => false,
					5 => false,
					6 => false
				);
				foreach($cal as $c):
					$r['days'][date('N', strtotime($c['Calendar']['date']))] = true;
				endforeach;
			} else {
				unset($response[$key]);
			}
		}
		
		$response = array_values($response);
        */

		$this->set('response',$response);
	}
	
	public function reportsearch(){
		if(!$this->Auth->user('role') == 0) return false;
		$this->layout = 'ajax';
		$this->view = 'search';
		$response = '';
		
		$lat = $this->request->data['lat'];
		$lng = $this->request->data['lon'];
		$distance = $this->request->data['distance'];
		$dateFrom = date('Y-m-d', strtotime($this->request->data['dateFrom']));
		$dateTo = date('Y-m-d', strtotime($this->request->data['dateTo']));
		
		$filters = $this->request->data['filters'];
		
		$filter = ' 1=1 ';
		$first = true;
		if(isset($filters['gym']) && $filters['gym'] == 1) {  $filter .= ($first) ? 'AND ' : 'OR '; $first = false; $filter .= "((locations.type = 'gym' OR  locations.type = 'studio')  AND locations.sgt = 0)"; }
		if(isset($filters['outdoor']) && $filters['outdoor'] == 1) { $filter .= ($first) ? 'AND ' : 'OR '; $first = false; $filter .= "(locations.type = 'outdoor'  AND locations.sgt = 0)"; }
		if(isset($filters['mobile']) && $filters['mobile'] == 1) { $filter .= ($first) ? 'AND ' : 'OR '; $first = false; $filter .= "(locations.type = 'mobile')"; }
		if(isset($filters['bootcamp']) && $filters['bootcamp'] == 1) {  $filter .= ($first) ? 'AND ' : 'OR '; $first = false; $filter .= "(locations.type = 'outdoor' AND users.role = 2)"; }
		if(isset($filters['sgt']) && $filters['sgt'] == 1) {  $filter .= ($first) ? 'AND ' : 'OR '; $first = false; $filter .= "((locations.type = 'gym' OR locations.type = 'studio') AND locations.sgt = 1)"; }
		
		
		$response['trainerCount'] = $this->reportTrainerCount($lat, $lng, $distance, $dateFrom, $dateTo, $filter);
		$response['locationsByArea'] = $this->reportLocationsByArea($lat, $lng, $distance, $dateFrom, $dateTo, $filter);
		$response['uniqueLocationsByArea'] = $this->reportUniqueLocationsByArea($lat, $lng, $distance, $dateFrom, $dateTo, $filter);
		$response['PTSessions'] = $this->reportPTSessions($lat, $lng, $distance, $dateFrom, $dateTo, $filter);
		$response['BCSessions'] = $this->reportBCSessions($lat, $lng, $distance, $dateFrom, $dateTo, $filter);
		$response['profileViews'] = $this->reportProfileViews($lat, $lng, $distance, $dateFrom, $dateTo, $filter);
		$response['bookingsByTrainer'] = $this->reportBookingsByTrainer($lat, $lng, $distance, $dateFrom, $dateTo, $filter);
		$response['bookingsMoreThanOneTrainer'] = $this->reportBookingsMoreThanOneTrainer($lat, $lng, $distance, $dateFrom, $dateTo, $filter);
		
		//$response = array_values($response);
		
		$this->set('response', $response);
	}
	
	protected function reportBCSessions($lat, $lng, $distance, $dateFrom, $dateTo, $filter){
		$classes = "
			SELECT
			locationdbs.id, (
			  6371 * acos (
				cos ( radians($lat) )
				* cos( radians( lat ) )
				* cos( radians( lng ) - radians($lng) )
				+ sin ( radians($lat) )
				* sin( radians( lat ) )
			  )
			) AS distance,
			bc_bookings.id,
			calendars.date
		  FROM bc_bookings
		  INNER JOIN calendars
		  on bc_bookings.session_id = calendars.id
		  AND calendars.date >= '$dateFrom'
		  AND calendars.date <= '$dateTo'
		  INNER JOIN locations
		  on locations.id = calendars.location
		  INNER JOIN locations_links
		  on locations_links.locations_id = locations.id
		  INNER JOIN locationdbs
		  on locations_links.locationdbs_id = locationdbs.id
		  INNER JOIN users
		  on bc_bookings.to_user = users.id
		  WHERE $filter
		  HAVING distance < $distance
		";
		
		
		$query = $this->Location->query($classes);
		return count($query);
	}
	
	protected function reportPTSessions($lat, $lng, $distance, $dateFrom, $dateTo, $filter){
		$classes = "
			SELECT
			locationdbs.id, (
			  6371 * acos (
				cos ( radians($lat) )
				* cos( radians( lat ) )
				* cos( radians( lng ) - radians($lng) )
				+ sin ( radians($lat) )
				* sin( radians( lat ) )
			  )
			) AS distance,
			pt_bookings.id,
			calendars.date
		  FROM pt_bookings
		  INNER JOIN calendars
		  on pt_bookings.session_id = calendars.id
		  AND calendars.date >= '$dateFrom'
		  AND calendars.date <= '$dateTo'
		  INNER JOIN locations
		  on locations.id = calendars.location
		  INNER JOIN locations_links
		  on locations_links.locations_id = locations.id
		  INNER JOIN locationdbs
		  on locations_links.locationdbs_id = locationdbs.id
		  INNER JOIN users
		  on pt_bookings.to_user = users.id
		  WHERE $filter
		  HAVING distance < $distance
		";
		
		
		$query = $this->Location->query($classes);
		return count($query);
	}
	
	
	protected function reportLocationsByArea($lat, $lng, $distance, $dateFrom, $dateTo, $filter){
		$classes = "
			SELECT
			locationdbs.id, (
			  6371 * acos (
				cos ( radians($lat) )
				* cos( radians( lat ) )
				* cos( radians( lng ) - radians($lng) )
				+ sin ( radians($lat) )
				* sin( radians( lat ) )
			  )
			) AS distance,
			locationdbs.lat,
			locationdbs.lng,
			locationdbs.placesid,
			locationdbs.address,
			locations.user,
			locations.id,
			locations.type,
			locations.sgt,
			users.first_name,
			users.id,
			users.role	
		  FROM locationdbs
		  INNER JOIN locations_links
		  ON locationdbs.id = locations_links.locationdbs_id
		  INNER JOIN locations
		  ON locations.id = locations_links.locations_id
		  INNER JOIN users
		  on locations.user = users.id
		  WHERE $filter
		  HAVING distance < $distance
		  ORDER BY distance
		";
		
		$query = $this->Location->query($classes);
		
		$locations = [];
		foreach($query as $q){
			if(!in_array($q['locations']['id'], $locations)) $locations[] = $q['locations']['id'];
		}
		
		return count($locations);
	}
	
	protected function reportUniqueLocationsByArea($lat, $lng, $distance, $dateFrom, $dateTo, $filter){
		$classes = "
			SELECT
			locationdbs.id, (
			  6371 * acos (
				cos ( radians($lat) )
				* cos( radians( lat ) )
				* cos( radians( lng ) - radians($lng) )
				+ sin ( radians($lat) )
				* sin( radians( lat ) )
			  )
			) AS distance,
			locationdbs.lat,
			locationdbs.lng,
			locationdbs.placesid,
			locationdbs.address,
			locations.user,
			locations.id,
			locations.type,
			locations.sgt,
			users.first_name,
			users.id,
			users.role	
		  FROM locationdbs
		  INNER JOIN locations_links
		  ON locationdbs.id = locations_links.locationdbs_id
		  INNER JOIN locations
		  ON locations.id = locations_links.locations_id
		  INNER JOIN users
		  on locations.user = users.id
		  WHERE $filter
		  HAVING distance < $distance
		  ORDER BY distance
		";
		
		$query = $this->Location->query($classes);
		
		$locations = [];
		foreach($query as $q){
			if(!in_array($q['locationdbs']['id'], $locations)) $locations[] = $q['locationdbs']['id'];
		}
		
		return count($locations);
	}
	
	protected function reportTrainerCount($lat, $lng, $distance, $dateFrom, $dateTo, $filter){
		$classes = "
			SELECT
			locationdbs.id, (
			  6371 * acos (
				cos ( radians($lat) )
				* cos( radians( lat ) )
				* cos( radians( lng ) - radians($lng) )
				+ sin ( radians($lat) )
				* sin( radians( lat ) )
			  )
			) AS distance,
			locationdbs.lat,
			locationdbs.lng,
			locationdbs.placesid,
			locationdbs.address,
			locations.user,
			locations.id,
			locations.type,
			locations.sgt,
			users.first_name,
			users.id,
			users.role	
		  FROM locationdbs
		  INNER JOIN locations_links
		  ON locationdbs.id = locations_links.locationdbs_id
		  INNER JOIN locations
		  ON locations.id = locations_links.locations_id
		  INNER JOIN users
		  on locations.user = users.id
		  WHERE $filter
		  HAVING distance < $distance
		  ORDER BY distance
		";
		
		$query = $this->Location->query($classes);
		
		$trainers = [];
		foreach($query as $q){
			if(!in_array($q['users']['id'], $trainers)) $trainers[] = $q['users']['id'];
		}
		
		return count($trainers);
	}
	
	protected function reportProfileViews($lat, $lng, $distance, $dateFrom, $dateTo, $filter){
		$classes = "
			SELECT
			locationdbs.id, (
			  6371 * acos (
				cos ( radians($lat) )
				* cos( radians( lat ) )
				* cos( radians( lng ) - radians($lng) )
				+ sin ( radians($lat) )
				* sin( radians( lat ) )
			  )
			) AS distance,
			users.first_name,
			users.second_name,
			users.id,
			users.role,
			users.email,
			users.profile_views
		  FROM users
		  INNER JOIN locations
		  ON locations.user = users.id
		  INNER JOIN locations_links
		  ON locations.id = locations_links.locations_id
		  INNER JOIN locationdbs
		  ON locationdbs.id = locations_links.locationdbs_id
		  WHERE $filter
		  HAVING distance < $distance
		  ORDER BY distance
		";
		$query = $this->Location->query($classes);
		
		$addedList = [];
		$return = [];
		foreach($query as $q){
			if(!in_array($q['users']['id'], $addedList)){
				$addedList[] = $q['users']['id'];
				$return[] = $q;
			}
		}
		
		return $return;
	}
	
	protected function reportBookingsMoreThanOneTrainer($lat, $lng, $distance, $dateFrom, $dateTo, $filter){
		$this->loadModel('BcBooking');
		$this->loadModel('PtBooking');
		
		$classes = "
			SELECT
			users.first_name,
			users.second_name,
			users.id,
			users.role,
			users.email,
			users.profile_views
		  FROM users
		  WHERE users.role = 3
		";
		$query = $this->Location->query($classes);
		
		$addedList = [];
		$return = [];
		foreach($query as $q){
			if(!in_array($q['users']['id'], $addedList)){
				$addedList[] = $q['users']['id'];
				$return[] = $q;
			}
		}
		
		foreach($return as $key => &$r){
			$pt = $this->PtBooking->find('all', array('conditions' => array('user' => $r['users']['id'])));
			$bc = $this->BcBooking->find('all', array('conditions' => array('user' => $r['users']['id'])));
			$trains = [];
			foreach($pt as $p){
				if(!in_array($p['PtBooking']['to_user'], $trains)){
					$trains[] = $p['PtBooking']['to_user'];
				}
			}
			
			foreach($bc as $p){
				if(!in_array($p['BcBooking']['to_user'], $trains)){
					$trains[] = $p['BcBooking']['to_user'];
				}
			}
			
			if(count($trains) < 2){ unset($return[$key]); } else { $r['overbook'] = count($trains); }
		}
		
		return $return;
	}
	
	protected function reportBookingsByTrainer($lat, $lng, $distance, $dateFrom, $dateTo, $filter){
		$this->loadModel('BcBooking');
		$this->loadModel('PtBooking');
		
		$classes = "
			SELECT
			locationdbs.id, (
			  6371 * acos (
				cos ( radians($lat) )
				* cos( radians( lat ) )
				* cos( radians( lng ) - radians($lng) )
				+ sin ( radians($lat) )
				* sin( radians( lat ) )
			  )
			) AS distance,
			users.first_name,
			users.second_name,
			users.id,
			users.role,
			users.email,
			users.profile_views
		  FROM users
		  INNER JOIN locations
		  ON locations.user = users.id
		  INNER JOIN locations_links
		  ON locations.id = locations_links.locations_id
		  INNER JOIN locationdbs
		  ON locationdbs.id = locations_links.locationdbs_id
		  WHERE $filter
		  HAVING distance < $distance
		  ORDER BY distance
		";
		$query = $this->Location->query($classes);
		
		$addedList = [];
		$return = [];
		foreach($query as $q){
			if(!in_array($q['users']['id'], $addedList)){
				$addedList[] = $q['users']['id'];
				$return[] = $q;
			}
		}
		
		foreach($return as &$r){
			$r['bookings'] = $this->BcBooking->find('count', array('conditions' => array('to_user' => $r['users']['id'], 'datestamp >=' => $dateFrom, 'datestamp <=' => $dateTo . '00:00:00'))) + $this->PtBooking->find('count', array('conditions' => array('to_user' => $r['users']['id'], 'time >=' => $dateFrom, 'time <=' => $dateTo . ' 00:00:00')));
			
		}
		
		return $return;
	}

    public function getLocation(){
        $id = $this->request->data("id");

        $this->layout = 'ajax';
        $this->view = 'ajaxresponse';

        $response = array();

        if($id){

            $this->Location->bindModel(
                array(
                    'hasMany' => array(
                        'PtPricing' => array(
                            'className' => 'PtPricing',
                            'foreignKey' => 'locationid'
                        ),
                        'bcPricing' => array(
                            'className' => 'BcPricing',
                            'foreignKey' => 'locations_id'
                        )
                    )
                )
            );
            $cur_location = $this->Location->find("first", array(
                "conditions" => array(
                    "id" => $id
                )
            ));

            $response["data"] = $cur_location;
        }

        $this->set("response", $response);

    }
	
}