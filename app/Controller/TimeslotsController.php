<?php

App::uses('AppController', 'Controller');

class TimeslotsController extends AppController {

    public function add(){
		$this->layout = 'ajax';		
		$this->request->data['Timeslot']['user'] = $this->Auth->user('id');
		if($this->Timeslot->save($this->request->data)){
			$response = 'true';
		} else {
			$response = 'false';
		}
		
		$this->set('response', $response);
    }
	
	public function view($id){
		$this->layout = 'ajax';
		$response = false;
		
		$month =  $this->request->data['month'];
		$year =  $this->request->data['year'];
		
		$dateFrom = new DateTime($year . '/' . $month . '/' . '1');
		
		$dateFrom->modify('-2 month');
		
		$conditions = array(
			'conditions' => array(
				'date >=' => $dateFrom->format('Y/m/d')
			)
		);
		
		$conditions['conditions']['date <'] = $dateFrom->modify('+4 month')->format('Y/m/d');
		$conditions['conditions']['user'] = $id;
		
		$responses = $this->Timeslot->find('all', $conditions);
		
		foreach($responses as $r){
			$response[] = $r['Timeslot'];
		}
		
		$this->set('response', $response);
	}

}