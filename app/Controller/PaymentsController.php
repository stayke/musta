<?php
// NDJ - This controller handles the payments section for the trainer (terms and conditions)
//
//       I have moved payment API functionality to the Integra Component so these aren't all mixed together


App::uses('AppController', 'Controller');

class PaymentsController extends AppController {

    public $components = array('Integra');
    
    public function index(){
        
    }
    
    public function currentBalance(){
        $this->loadModel('User');
        
        if(!$this->Auth->user('id')){
            $this->set('response', array('error' => 'Not logged in'));
            return true;
        }
        
        $response['balance'] = $this->calculateBalance($this->Auth->user('id'));
        $response['taxbalance'] = $this->calculateTaxBalance($this->Auth->user('id'));
        
        $this->set('response', $response);
    }
    
    public function getCharts($period){
        $this->loadModel('User');
        
        if(!$this->Auth->user('id')){
            $this->set('response', array('error' => 'Not logged in'));
            return true;
        }
        
        $response['views'] = $this->getViews($period);
        $response['bookings'] = $this->getBookings($period);
        
        $this->set('response', $response);
    }
    
    public function getViews($period){
        if($period ==7) return array('labels' => array($this->Auth->user('type'),$period,'Wednesday','Thursday','Friday','Saturday','Sunday'), 'data' => array(array(13,2,22,64,23,43,22)));
        if($period == 30) return array('labels' => array('1','2','3','4','5','6','7','7','7','7','7','7','7','7','7','7','7','7','7','7','7','7','7','7','7','7','7','7','7'), 'data' => array(array(13,2,22,64,23,43,22)));
    }
    
    public function getBookings($period){
        
        $data = array();
        $labels = array();

        if($this->Auth->user('role') == 2){ // BC
            $this->loadModel('BcBooking');
            if($period == 7 || $period ==  30){
                $start = $period;
                while($start>=0){
                    $date = date("Y-m-d", strtotime(date("Y-m-d") . ' -' . $start . ' days'));
                    
                    $data[0][] = $this->BcBooking->find('count', array('conditions' => array('to_user' => $this->Auth->user('id'), 'datestamp >=' => $date . ' 00:00:00', 'datestamp <= ' => $date . '23:59:59')));
                    $labels[] = date("d/m", strtotime($date));
                    
                    $start--;
                }
            }
        }
        
        if($this->Auth->user('role') == 1){ // PT

            $this->loadModel('PtBooking');
            if($period == 7 || $period ==  30){
                $start = $period;
                while($start>=0){
                    $date = date("Y-m-d", strtotime(date("Y-m-d") . ' -' . $start . ' days'));
                    
                    $data[0][] = $this->PtBooking->find('count', array('conditions' => array('to_user' => $this->Auth->user('id'), 'time >=' => $date . ' 00:00:00', 'time <= ' => $date . '23:59:59')));
                    $labels[] = date("d/m", strtotime($date));
                    
                    $start--;
                }
                
            }
            
            
            
        }
        
        return array('data' => $data,'labels' => $labels);

    }
    
    protected function calculateBalance($id){
        $unclaimed = $this->Payment->find('all', array('conditions' => array('to_user' => $id, 'trainer_paid' => 0, 'success' => 1, 'error' => 0)));
        
        $up = 0.00;
        
        foreach($unclaimed as $u){
            $up = $up + $u['Payment']['amount'];
        }
        
        return $up;
    }
    
    protected function calculateTaxBalance($id){
        $unclaimedTax = $this->Payment->find('all', array('conditions' => array('to_user' => $id, 'trainer_paid' => 0, 'success' => 1, 'error' => 0, 'inc_gst' => 1)));
        
        $up = 0.00;
        
        foreach($unclaimedTax as $u){
            $up = $up + $u['Payment']['gst'];
        }
        
        return $up;
    }
    
    public function viewCardPayments(){
        $this->loadModel('User');
        $this->loadModel('TrainerClient');
        
        if(!$this->Auth->user('id')){
            $this->set('response', array('error' => 'Not logged in'));
            return true;
        }
        
        $response = $this->Payment->find('all',
            array(
                'conditions' => array(
                    'to_user' => $this->Auth->user('id')
                ),
                'order' => 'datestamp DESC')
            );
        
        foreach($response as &$r){
            $customer = $this->User->find('first', array('conditions' => array('id' => $r['Payment']['user'])));
            
            if($customer){
                $r['Customer'] = $customer;
            } else {
                $tempcust = $this->TrainerClient->find('first', array('conditions' => array('email' => $r['Payment']['useremail'])));
                $r['Customer']['User'] = array(
                    'first_name' => $tempcust['TrainerClient']['name'],
                    'second_name' => ''
                );
            }
            
            
        }
        
        $this->set('response', $response);
    }
    
     public function viewRepeatPayments(){
        
        $this->layout = 'ajax';
        $this->view = 'pay';
        
        $this->loadModel('User');
        $this->loadModel('TrainerClient');
        $this->loadModel('RepeatPayment');
        
        if(!$this->Auth->user('id')){
            $this->set('response', array('error' => 'Not logged in'));
            return true;
        }
        
        $response = $this->RepeatPayment->find('all',
            array(
                'conditions' => array(
                    'trainer_id' => $this->Auth->user('id')
                ))
            );
        
        foreach($response as &$r){
            $customer = $this->User->find('first', array('conditions' => array('id' => $r['RepeatPayment']['user_id'])));
            
            if($customer){
                $r['Customer'] = $customer;
            } else {
                $tempcust = $this->TrainerClient->find('first', array('conditions' => array('email' => $r['RepeatPayment']['user_email'])));
                $r['Customer']['User'] = array(
                    'first_name' => $tempcust['TrainerClient']['name'],
                    'second_name' => ''
                );
            }
            
            
        }
        
        $this->set('response', $response);
    }

     public function viewRecurringPayments(){

        $this->layout = 'ajax';
        $this->view = 'pay';

        $this->loadModel('User');
        $this->loadModel('TrainerClient');
        $this->loadModel('RecurringPayment');

        if(!$this->Auth->user('id')){
            $this->set('response', array('error' => 'Not logged in'));
            return true;
        }

        $response = $this->RecurringPayment->find('all',
            array(
                'conditions' => array(
                    'to_user' => $this->Auth->user('id')
                ))
            );

        foreach($response as &$r){
            $customer = $this->User->find('first', array('conditions' => array('id' => $r['RecurringPayment']['user'])));

            if($customer){
                $customer["User"]["access_token"] = null;
                $customer["User"]["password"] = null;
                $r['Customer'] = $customer;
            }

            if($r["RecurringPayment"]["date_start"]) {
                $r["RecurringPayment"]["date_start"] = date("d-m-Y", strtotime($r["RecurringPayment"]["date_start"]));
            }

            if($r["RecurringPayment"]["date_end"]) {
                $r["RecurringPayment"]["date_end"] = date("d-m-Y", strtotime($r["RecurringPayment"]["date_end"]));
            }


        }

        $this->set('response', $response);
    }

    public function pay(){
        $this->loadModel('Location');
        $this->loadModel('TrainerClient');
        $this->loadModel('Notification');
        $this->loadModel('PendingPayment');
        $this->loadModel('RepeatPayment');
        if(!$id = $this->Auth->user('id')) return false;
        
        $data = $this->request->data['payload'];
        
        $response = array();
        
        if($data['paymentData']['type'] == 'pending_payment'){
        
            if(!$payment = $this->tryPay($data['paymentData']['itemTotal'], $data['paymentData']['bookingFee'], $data['paymentData']['additional']['viewing'], $data['paymentData']['gst'])){
                $response['error'] = 'Invalid Details';
                $this->set('response', $response);
                return;
            } else {
                $response['error'] = false;
                $this->Notification->delete(array('id' => $data['paymentData']['additional']['notification_id']));
                $this->PendingPayment->delete(array('id' => $data['paymentData']['additional']['pending_payment_id']));
                $this->set('response', $response);
                return;
            }
            
        }
        
        if($data['paymentData']['type'] == 'recurring_payment'){
        
            if(!$payment = $this->tryPay($data['paymentData']['itemTotal'], $data['paymentData']['bookingFee'], $data['paymentData']['additional']['viewing'], $data['paymentData']['gst'])){
                $response['error'] = 'Invalid Details';
                $this->set('response', $response);
                return;
            } else {
                $response['error'] = false;
                $this->Notification->delete(array('id' => $data['paymentData']['additional']['notification_id']));
                $this->RepeatPayment->save(array('id' => $data['paymentData']['target'], 'last_payment' => date('Y-m-d'), 'approved' => 1));
                $this->set('response', $response);
                return;
            }
            
        }
        
        
        if($data['paymentData']['type'] == 'pt'){ $filler = 'PtPricing'; } else { $filler = 'BcPricing'; }
        
        if(!$this->getPrice($data['paymentData']['additional']['viewing'], $data['paymentData']['items'][0]['id'], $data['paymentData']['type']) == $data['paymentData']['total'] ){
            $response['error'] = 'Internal Error';
            $this->set('response', $response);
            return;
        }
        

        $location = (isset($data['paymentData']['items'][0]['additional']['locations_id'])) ? $data['paymentData']['items'][0]['additional']['locations_id'] : $data['paymentData']['items'][0]['additional']['locationid'];
        $userQ = $this->Location->find('first', array('conditions' => array('id' => $location)));
        if(!$payment = $this->tryPay($data['paymentData']['items'][0]['additional']['price'], $data['paymentData']['bookingFee'], $data['paymentData']['additional']['viewing'], $userQ['Location']['gst'])){
            $response['error'] = 'Invalid Details';
            $this->set('response', $response);
            return;
        }
        
        
        
        // Amount is correct
        // Payment went through
        // Add booking to calendar
        
        if($data['paymentData']['type'] == 'pt'){ $this->addPt($data, $payment); }else{ $this->addBc($data, $payment); }
        
        $count = $this->TrainerClient->find('count', array('conditions' => array('user_id' => $this->Auth->user('id'),'trainer_id' => $data['paymentData']['additional']['viewing'])));
        if(!$count){
            $this->TrainerClient->create();
            $this->TrainerClient->save(array('user_id' => $this->Auth->user('id'),'trainer_id' => $data['paymentData']['additional']['viewing']));
        }
        
        $response['error'] = false;
        $this->set('response', $response);
    }

    protected function tryPay($amount, $fee, $to, $incgst, $useremail = false, $user = false){
        
        if($user === false) { $user = $this->Auth->user('id'); }
        if($useremail === false) { $useremail = NULL; }

        
        $total = $amount + $fee;
        if($incgst){ $total += $amount/10; $gst = $amount/10; } else { $gst = 0; }
        
        $this->Payment->create();
        $pay = $this->Payment->save(array(
            'user' => $user,
            'amount' => $amount,
            'fee' => $fee,
            'inc_gst' => $incgst,
            'gst' => $gst,
            'to_user' => $to,
            'success' => false,
            'datestamp' => date('Y-m-d H:i:s'),
            'total' => $total,
            'useremail' => $useremail,
            'termsId' => $termsId
        ));
        
        if($pay){
            $notification = array(
                'user_id' => $user,    
                'text' => 'You have succesfully paid for an event',
                'target' => 0,
                'type' => 'information'
            );
            $this->addNotification($notification);
            
            $notification = array(
                'user_id' => $to,    
                'text' => 'A user has purchased a session',
                'target' => 0,
                'type' => 'information'
            );
            $this->addNotification($notification);
        }
        
        return $pay['Payment']['id'];
    }
    
    protected function getPrice($user, $id, $type){
        $this->loadModel('PtPricing');
        $this->loadModel('BcPricing');
        if($type == 'pt'){
            $r = $this->PtPricing->find('first', array('conditions' => array('id' => $id, 'user_id' => $user)));
            $price = $r['PtPricing']['price'];
            $location = $r['PtPricing']['locationid'];
        } else {
            $r = $this->BcPricing->find('first', array('conditions' => array('id' => $id, 'user_id' => $user)));
            $price = $r['BcPricing']['price'];
            $location = $r['BcPricing']['locations_id'];
        }
        
        $gst = $this->Location->find('first', array('conditions' => array('id' => $location)));
        
        if($gst['Location']['gst']){
            $price += $price/10;
        }
        
        $final = ($price/100)*$this->pay['percentFee'];
        if($final<$this->pay['minFee']){ $final = $this->pay['minFee']; }
        $final = $final + $price;
        return $final;
    }
    
    protected function addBc($data, $payment){
        $this->loadModel('BcBoughtsession');
        $this->loadModel('BcBooking');
        
        // Add prepay bookings to db
        $this->BcBoughtsession->create();
        $this->BcBoughtsession->save(array(
            'user' => $this->Auth->user('id'),
            'to_user' => $data['paymentData']['additional']['viewing'],
            'amount' => $data['paymentData']['items'][0]['additional']['included_sessions'],
            'payment_id' => $payment
        ));
        
        // Add initial booking for session
        $this->BcBooking->create();
        $this->BcBooking->save(array(
            'user' => $this->Auth->user('id'),
            'to_user' => $data['paymentData']['additional']['viewing'],
            'session_id' => $data['paymentData']['additional']['session']
        ));
    
    }
    
    protected function addPt($data, $payment){
        $this->loadModel('PtBooking');
        
        // Add initial booking for session
        $this->PtBooking->create();
        $this->PtBooking->save(array(
            'user' => $this->Auth->user('id'),
            'to_user' => $data['paymentData']['additional']['viewing'],
            'session_id' => $data['paymentData']['additional']['session'],
            'time' => $data['paymentData']['items'][0]['date'],
            'length' => $data['paymentData']['items'][0]['additional']['session_length'],
            'payment_id' => $payment
        ));
    
    }
    
    public function addCreditCardPayment() {
        if(!$this->Auth->user('id')) { $this->set('return', array('error' => 'Not logged in')); return false;}
        
        
        $gst = ($this->request->data['addGST']) ? true : false;
        
        $clientinfo = explode(',',$this->request->data['client']);
        
        $clientemail = ($clientinfo[0]>0) ? NULL : $clientinfo[1];
        $client = ($clientinfo[0]>0) ? $clientinfo[0] : 0;
        
        
        //$this->tryPay($this->request->data['price'], 1.5, $this->Auth->user('id'), NULL, NULL, $gst, $clientemail, $client);
        $this->set('return', false);
	}
    
    public function requestPayment() {
        $this->loadModel('PendingPayment');
        $this->loadModel('Payment');
        $this->loadModel('Notification');
        $this->loadModel('RepeatPayment');
        $this->loadModel('RecurringPayment');
        if(!$this->Auth->user('id')) { $this->set('return', array('error' => 'Not logged in')); return false;}
        
        $termsId = $this->saveTermsForPayment($this->request->data['terms']);
        
        $gst = (isset($this->request->data['card']['addGST']) && $this->request->data['card']['addGST']) ? true : false;
        
        $clientinfo = explode(',',$this->request->data['card']['client']);
        
        $clientemail = ($clientinfo[0]>0) ? NULL : $clientinfo[1];
        $client = ($clientinfo[0]>0) ? $clientinfo[0] : 0;

        // We no longer calculate the fee on our end
        $total = ($gst) ? ($this->request->data['card']['price']*1.1) : $this->request->data['card']['price'];
//        $total = ($gst) ? ($this->request->data['card']['price']*1.1) + $this->getFee('card_fee', $this->request->data['card']['price']) : $this->request->data['card']['price'] + $this->getFee('card_fee', $this->request->data['card']['price']);

        $pic = $this->getUserDetail($this->Auth->user('id'),'profile_pic');
        $repeat = false;
        if(isset($this->request->data['card']['repeat'])){
            $repeat = $this->request->data['card']['repeat'];
        }
        if($repeat){
            $notes = "";
            if(isset($this->request->data['card']['notes'])){
                $notes = $this->request->data['card']['notes'];
            }

            $date_start = $this->request->data['card']['date_start'];

            $num_repeat = 0;

            if(isset($this->request->data['card']['num_repeat'])) {
                $num_repeat = $this->request->data['card']['num_repeat'];
            }

            $date_end = date("Y-m-d", strtotime($date_start . " + " . ($num_repeat - 1) . " weeks"));

            $save = array(
                'status' => "pending",
                'user' => $client,
                'to_user' =>  $this->Auth->user('id'),
                'create_date' => date("Y-m-d"),
                'date_start' => date("Y-m-d", strtotime($date_start)),
                'num_repeat' => $num_repeat,
                'date_end' => $date_end,
                'amount' => $this->request->data['card']['price'],
//                'fee' => $this->getFee('card_fee', $this->request->data['card']['price']),
                'gst' => $gst,
                'total' => $total,
                'frequency' => "weekly",
                'day' => date("l", strtotime($date_start)),
                'notes' => $notes,
            );
            
            $pendingpay = $this->RecurringPayment->save($save);

            $this->RecurringPayment->sendEmails($pendingpay["RecurringPayment"]["id"],
                array(
                    "extra_message" => $this->Auth->user('first_name') . ' has set up a payment request for you with this message "' . $this->request->data['card']['notes'] . '"',
                ));
            $this->RecurringPayment->addNotification($pendingpay["RecurringPayment"]["id"]);

        } else {
            $notes = "";
            if(isset($this->request->data['card']['notes'])){
                $notes = $this->request->data['card']['notes'];
            }
            $save = array(
                'user' => $client,
                'to_user' =>  $this->Auth->user('id'),
                'amount' => $this->request->data['card']['price'],
                'gst' => $gst,    
                'fee' => $this->getFee('card_fee', $this->request->data['card']['price']),
                'total' => $total,
                'notes' => $notes,
                'user_email' => $clientemail,
                'termsId' => $termsId,
                "status" => "pending",
                "datestamp" => date("Y-m-d H:i:s")
            );

            $pendingpay = $this->Payment->save($save);

            $this->Payment->sendEmails($pendingpay["Payment"]["id"],
                array(
                    "extra_message" => $this->Auth->user('first_name') . ' has set up a payment request for you with this message "' . $this->request->data['card']['notes'] . '"',
                ));
            $this->Payment->addNotification($pendingpay["Payment"]["id"]);
        }
        
        $this->set('response', false);
        return true;
	}

    // deprecated used the pending payment system
    public function oldrequestPayment() {
        $this->loadModel('PendingPayment');
        $this->loadModel('Notification');
        $this->loadModel('RepeatPayment');
        if(!$this->Auth->user('id')) { $this->set('return', array('error' => 'Not logged in')); return false;}

        $termsId = $this->saveTermsForPayment($this->request->data['terms']);

        $gst = (isset($this->request->data['card']['addGST']) && $this->request->data['card']['addGST']) ? true : false;

        $clientinfo = explode(',',$this->request->data['card']['client']);

        $clientemail = ($clientinfo[0]>0) ? NULL : $clientinfo[1];
        $client = ($clientinfo[0]>0) ? $clientinfo[0] : 0;

        $total = ($gst) ? ($this->request->data['card']['price']*1.1) + $this->getFee('card_fee', $this->request->data['card']['price']) : $this->request->data['card']['price'] + $this->getFee('card_fee', $this->request->data['card']['price']);

        $pic = $this->getUserDetail($this->Auth->user('id'),'profile_pic');
        if(isset($this->request->data['repeat']) && $this->request->data['card']['repeat']){
            $save = array(
                'user_id' => $client,
                'trainer_id' =>  $this->Auth->user('id'),
                'price' => $this->request->data['card']['price'],
                'gst' => $gst,
                'notes' => $this->request->data['card']['notes'],
                'fee' => $this->getFee('card_fee', $this->request->data['card']['price']),
                'total' => $total,
                'approved' => 0,
                'paused' => 0,
                'repeating' => $this->request->data['card']['repeat'],
                'frequency' => $this->request->data['card']['frequency'],
                'termsId' => $termsId
            );

            $pendingpay = $this->RepeatPayment->save($save);

            if($client>0){
                $notification = array(
                    'user_id' => $client,
                    'text' => $this->Auth->user('first_name') . ' has set up a payment request for you with this message "' . $this->request->data['card']['notes'] . "'",
                    'image' => '/users/viewUserImage/' . $pic[0]['Userinfo']['meta_value'],
                    'target' => $pendingpay['RepeatPayment']['id'],
                    'type' => 'repeatPaymentRequest'
                );

                $this->addNotification($notification);
            } else {
                $Email = new CakeEmail();
                $Email->from(array('admin@musta.global' => 'Musta Global'));
                $Email->to($clientemail);
                $Email->subject('Your trainer wants you to sign up to Musta Global!');
                $Email->send('A user has requested you make a payment via http://musta.global . ');
            }
        } else {
            $notes = "";
            if(isset($this->request->data['card']['notes'])){
                $notes = $this->request->data['card']['notes'];
            }
            $save = array(
                'user_id' => $client,
                'trainer_id' =>  $this->Auth->user('id'),
                'price' => $this->request->data['card']['price'],
                'gst' => $gst,
                'notes' => $notes,
                'fee' => $this->getFee('card_fee', $this->request->data['card']['price']),
                'total' => $total,
                'user_email' => $clientemail,
                'termsId' => $termsId
            );

            $pendingpay = $this->PendingPayment->save($save);


            if($client>0){
            $notification = array(
                    'user_id' => $client,
                    'text' => $this->Auth->user('first_name') . ' has set up a payment request for you with this message "' . $this->request->data['card']['notes'] . "'",
                    'image' => '/users/viewUserImage/' . $pic[0]['Userinfo']['meta_value'],
                    'target' => $pendingpay['PendingPayment']['id'],
                    'type' => 'paymentRequest'
                );

                $this->addNotification($notification);
            } else {
                $Email = new CakeEmail();
                $Email->from(array('admin@musta.global' => 'Musta Global'));
                $Email->to($clientemail);
                $Email->subject('Your trainer wants you to sign up to Musta Global!');
                $Email->send('A user has requested you make a payment via http://musta.global . ');
            }

        }




        $this->set('response', false);
	}
    
    
    public function getRequest($id){
        $this->layout = 'ajax';
        $this->view = 'pay';
        $this->loadModel('PendingPayment');
        $this->loadModel('Term');
        if(!$this->Auth->user('id')) { $this->set('return', array('error' => 'Not logged in')); return false;}
        
        $response = $this->PendingPayment->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'), 'id' => $id)));
        $tandc = $this->Term->find('first', array('conditions' => array('id' => $response['PendingPayment']['termsId'])));
        $response['terms'] = $tandc['Term']['terms'];
        $this->set('response', $response);
    }
    
    public function getRepeatRequest($id){
        $this->layout = 'ajax';
        $this->view = 'pay';
        $this->loadModel('RepeatPayment');
        if(!$this->Auth->user('id')) { $this->set('return', array('error' => 'Not logged in')); return false;}
        
        $response = $this->RepeatPayment->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'), 'id' => $id)));
        $this->set('response', $response);
    }
    
    protected function saveTermsForPayment($terms){
        $this->loadModel('Term');
        $t = $terms;
  
        if(isset($terms['paystop']['active']) && $terms['paystop']['active']) $string .= ' Payments will stop after the minimum number has been settled';
        if(isset($terms['minpay']['active']) && $terms['minpay']['active']) $string .= ' After the minimum number of payments the trainee will continue on the recurring payments with no end date.';
        if(isset($terms['payfull']['active']) && $terms['payfull']['active']) $string .= ' Payment for all sessions must be made in full prior to the session commencing.';
        if(isset($terms['failedpayment']['active']) && $terms['failedpayment']['active']) $string .= ' Failed payments will incur a fee which will be added to the next successful payment.';
        if(isset($terms['ifcancel']['active']) && $terms['ifcancel']['active']) $string .= ' If training is cancelled due to unavoidable reasons (ie. pregnancy, health issues) and this falls before the next paid session, the trainee will be charged for times they have trained only';
        if(isset($terms['custompaymentpolicy']['active']) && $terms['custompaymentpolicy']['active']) $string .= $terms['custompaymentpolicy']['text'];
        $string .= '<h2>Cancelation & Rescheduling sessions</h2>';
        if(isset($terms['sessioncancelled']['refunded']) && $terms['sessioncancelled']['refunded']) $string .= ' A session cancelled or missed by the trainee will not be refunded under any circumstances and full payment taken.';
        if(isset($terms['sessioncancelled']['minof']) && $terms['sessioncancelled']['minof']){
            $string .= ' A session cancelled by the trainee with a minimum of ' . $terms['sessioncancelled']['min'] . ' ' . $terms['cancel']['frequency'] . ' notice will be - <br />';
            if(isset($terms['cancel']['makeup']) && $terms['cancel']['makeup']) $string .= ' Offered as a make up session at a time the trainer is available';
            if(isset($terms['cancel']['makeupweek']) && $terms['cancel']['makeupweek']) $string .= ' in that week. Failure to do so will incur the full cost of your session';
            if(isset($terms['cancel']['fullyrefund']) && $terms['cancel']['fullyrefund']) $string .= ' Fully refunded by the trainer';
            if(isset($terms['cancel']['norefunded']) && $terms['cancel']['norefunded']) $string .= ' Sessions cancelled with less than this notice period will not be refunded or offered as a make up session';
        }
        if(isset($terms['cancel']['addcancelation']['active']) && $terms['cancel']['addcancelation']['active']) $string .= $terms['cancel']['addcancelation']['text'];
        
        if(isset($terms['cancelledbytrainer']['active']) && $terms['cancelledbytrainer']['active']){
            $string .= ' A session cancelled by the trainer will be … <br />';
            if(isset($terms['cancelledbytrainer']['makeup']) && $terms['cancelledbytrainer']['makeup']) $string .= ' offered as a make up session at a time agreed between the trainer and trainee';
            if(isset($terms['cancelledbytrainer']['or']) && $terms['cancelledbytrainer']['or']) $string .= ' Or';
            if(isset($terms['cancelledbytrainer']['fullyrefund']) && $terms['cancelledbytrainer']['fullyrefund']) $string .= ' Fully refunded';
            if(isset($terms['cancelledbytrainer']['bootcampattendance']) && $terms['cancelledbytrainer']['bootcampattendance']) $string .= ' Bootflaps, freezing membership and Public holidays';
            if(isset($terms['cancelledbytrainer']['entitled']) && $terms['cancelledbytrainer']['entitled']) $string .= ' Members are entitled to attend all sessions they have signed up for and are encouraged not to miss any sessions.';
        }
        
        if(isset($terms['cancelledbytrainer']['brokendown']) && $terms['cancelledbytrainer']['brokendown']) $string .= ' As memberships are broken down into ' . $terms['breakdown']['period'] . ' payments, [the Bootcamp] will not compensate members who do not show up to sessions they have paid for. However members have the opportunity to place their membership on hold in advance if they have a medical condition.';
        if(isset($terms['cancelledbytrainer']['publicholiday']) && $terms['cancelledbytrainer']['publicholiday']) $string .= '  [The Bootcamp] is closed on Public Holidays, hence training sessions will not be run on Public holidays and will also not be compensated for.';
        if(isset($terms['cancel']['addcancelation']['active']) && $terms['cancel']['addcancelation']['active']) $string .= $terms['cancel']['addcancelation']['text'];
        
        if(isset($terms['cancel']['addcancelationandreschedle']['active']) && $terms['cancel']['addcancelationandreschedle']['active']) $string .= $terms['cancel']['addcancelationandreschedle']['text'];
        
        if(isset($terms['cancel']['addcancelation']['lateness']) && $terms['cancel']['addcancelation']['lateness']) $string .= ' Lateness';
        if(isset($terms['cancel']['addcancelation']['ifyouarelate']) && $terms['cancel']['addcancelation']['ifyouarelate']) $string .= ' If you are late for a session your training will still finish at the end of the scheduled time';
        if(isset($terms['cancel']['addcancelation']['weather']) && $terms['cancel']['addcancelation']['weather']) $string .= ' Weather';
        if(isset($terms['cancel']['addcancelation']['assessionsareheld']) && $terms['cancel']['addcancelation']['assessionsareheld'])  $string .= ' As sessions are held outdoors, weather is uncontrollable, and training sessions will run in rain unless the weather is deemed dangerous due to hail or lightning.';
        if(isset($terms['cancel']['addcancelation']['washedout']) && $terms['cancel']['addcancelation']['washedout'])  $string .= ' Sessions that are washed out due to rain will not be compensated for';
        if(isset($terms['cancel']['addcancelation']['unlessawholeweek']) && $terms['cancel']['addcancelation']['unlessawholeweek'])  $string .= ' unless a whole week of training has been cancelled';
        if(isset($terms['cancel']['addcancelation']['ifsessionscancelled']) && $terms['cancel']['addcancelation']['ifsessionscancelled'])  $string .= ' If sessions are cancelled due to dangerous weather conditions they will be';
        if(isset($terms['cancel']['addcancelation']['offeredasamakeup']) && $terms['cancel']['addcancelation']['offeredasamakeup'])  $string .= ' Offered as a make up session at a time the trainer is available';
        if(isset($terms['cancel']['addcancelation']['inthatweek']) && $terms['cancel']['addcancelation']['inthatweek'])  $string .= ' in that week. Failure to do so will incur the full cost of your session';
        if(isset($terms['cancel']['addcancelation']['fullyrefunded']) && $terms['cancel']['addcancelation']['fullyrefunded'])  $string .= ' Fully refunded by the trainer';
        
        $this->Term->create();
        $save = $this->Term->save(array(
            'terms' => $string
        ));
        return $save['Term']['id'];
    }

    public function newPayment(){
        $this->layout = 'ajax';

        $response = array();

        $this->loadModel("Payment");
        $this->loadModel("User");
        $this->loadModel("Location");
        $this->loadModel("PtBooking");
        $this->loadModel("PtPricing");
        $this->loadModel("TrainerClients");

        $cur_user_id = $this->Auth->user('id');

        $rawBooking = $this->request->data['newBooking'];

        if($rawBooking["user"] != $cur_user_id){
            $response["result"] = "FAIL";
            $response["errorMsg"] = "You do not have access to create a booking for this user";
            $this->set('response', $response);
            return false;
        }

        $pricing_id = str_replace("SGT-", "", $rawBooking["pricing"]);

        $pricing = $this->PtPricing->find('first', array(
            'conditions' => array(
                'id' => $pricing_id
            )
        ));

        if(!$pricing){
            $response["result"] = "FAIL";
            $response["errorMsg"] = "Could not find the requested pricing option";
            $this->set('response', $response);
            return false;
        }

        // Check if they are an existing client or first time
        $relationship = $this->TrainerClients->find('first', array(
            'conditions' => array(
                'user_id' => $cur_user_id,
                'trainer_id' => $pricing["PtPricing"]["user_id"]
            )
        ));

        $first_time = false;
        if(!$relationship) {
            $first_time = $this->PtPricing->find('first', array(
                'conditions' => array(
                    'user_id' => $pricing["PtPricing"]["user_id"],
                    'locationid' => $pricing["PtPricing"]["locationid"],
                    'first_only' => 1,
                )
            ));
        }

        // #$#$# todo check availability of hours/time
        $time = date("Y-m-d H:i:s", strtotime($rawBooking["time"]));
        $length = $pricing["PtPricing"]["session_length"];

        $user = $rawBooking["user"];
        $amount = 0;
        if(strpos($rawBooking["pricing"], "SGT-") !== false){
            $amount = $pricing["PtPricing"]["sgt_fee"];
        } else {
            $amount = $pricing["PtPricing"]["price"];
        }
        // if first time
        if($first_time){
            $amount = $first_time["PtPricing"]["price"];
        }

        $to_user = $rawBooking["to_user"];
        $location_id = $pricing["PtPricing"]["locationid"];

        $datestamp = date("Y-m-d H:i:s");
        $status = "unpaid";

        $location = $this->Location->find('first', array(
            'conditions' => array(
                'id' => $location_id
            )
        ));

        if($location){
            $response["location_name"] = $location["Location"]["name"];
        }

        $trainer = $this->User->find("first", array(
            "conditions" => array(
                "id" => $to_user
            )
        ));

        if($trainer) {
            $response["trainer_name"] = $trainer["User"]["first_name"];
        }

        $new_payment = array(
            "user" => $user,
            "amount" => $amount,
            "to_user" => $to_user,
            "datestamp" => $datestamp,
            "status" => $status,
            "total" => $amount
        );

        $payment_result = $this->Payment->save($new_payment);
        $response["payment"] = $payment_result;

        $participant_notes = "";
        if(isset($rawBooking["participant_notes"])){
            $participant_notes = $rawBooking["participant_notes"];
        }

        $new_booking = array(
            "user" => $user,
            "to_user" => $to_user,
            "time" => $time,
            "length" => $length,
            "payment_id" => $payment_result["Payment"]["id"],
            "pay_status" => "unpaid",
            "participant_notes" => $participant_notes,
            "location" => $location_id
        );

        $booking_result = $this->PtBooking->save($new_booking);

        $response["booking"] = $booking_result;

        $new_payment_id = $payment_result["Payment"]["id"];

        // If the first time fee is $0 they want it to instead just ask them submit their details to Integra
        if($first_time && $amount == 0){
            $token_array = $this->Integra->getVaultToken($cur_user_id, 0, $new_payment_id);
            $response = $response + $token_array;
        } else {
            $token_array = $this->Integra->getToken($new_payment_id);
            $response = $response + $token_array;
        }

        $this->set('response', $response);
        return true;
    }

    // The handler for responses coming from Integra
    public function handleResponse(){

        $webPageToken = $this->request->query('webPageToken');
        $method = $this->request->query('method');

        $response = $this->Integra->fetchResponse($webPageToken, $method);

        $this->set('response', $response);

        return true;

    }

    // Landing page for paying invoices
    public function makePayment($id){
        $this->layout = 'ajax';
        $this->loadModel("User");

        $response = array();

        $payment = $this->Payment->find('first', array(
            'conditions' => array(
                'id' => $id
            )
        ));

        if(!$payment){
            $response["error_messages"][] = "Could not find the requested payment";
            $this->set("response", $response);
            return;
        } else {
            $response["payment"] = $payment["Payment"];
        }

        $trainer = $this->User->find("first", array(
            "conditions" => array(
                "id" => $payment["Payment"]["to_user"]
            )
        ));

        if($trainer) {
            $response["trainer_name"] = $trainer["User"]["first_name"];
        }

        $location_name = $this->getLocationFromRecurringPayment($payment["Payment"]["id"]);

        $response["location_name"] = $location_name;

        $token_response = $this->Integra->getToken($id);

        $response = $response + $token_response;

        $response["test a"] = "test x";

        $this->set("response", $response);

    }

    public function makeRecurringPayment($id){

        $this->loadModel("RecurringPayment");
        $this->loadModel("EventRegistration");
        $this->loadModel("User");

        $response = array();

        $rec_payment = $this->RecurringPayment->find('first', array(
            'conditions' => array(
                'id' => $id
            )
        ));

        if(!$rec_payment){
            $response["error_messages"][] = "Could not find the requested recurring payment";
            $this->set("response", $response);
            return;
        } else {
            $rec_payment["RecurringPayment"]["integra_response"] = "";
            $response["rec_payment"] = $rec_payment["RecurringPayment"];
        }

        $trainer = $this->User->find("first", array(
            "conditions" => array(
                "id" => $rec_payment["RecurringPayment"]["to_user"]
            )
        ));

        if($trainer) {
            $response["trainer_name"] = $trainer["User"]["first_name"];
        }

        $location_name = $this->getLocationFromRecurringPayment($rec_payment["RecurringPayment"]["id"]);

        $response["location_name"] = $location_name;

        $cur_user_id = $rec_payment["RecurringPayment"]["user"];

        $integra_user_response = $this->Integra->addUser($cur_user_id);
//        $integra_rec_response = $this->Integra->addRecurringPayment($id); // shouldn't make recurring payments until we get the vault response

        $response = $response + $integra_user_response;
//        $response = $response + $integra_rec_response;

        $token_array = $this->Integra->getVaultToken($cur_user_id, $id);

        $response = $response + $token_array;
        if(isset($token_array["webPageToken"]) && $token_array["webPageToken"]){
            $response["vault_save"] = true;
        }

        $this->set("response", $response);

    }

    public function getLocationFromRecurringPayment($rec_id){
        $this->loadModel("EventRegistration");
        $this->loadModel("CalendarEvent");
        $this->loadModel("Location");

        $event_reg = $this->EventRegistration->find("first", array(
            "conditions" => array(
                "rec_payment" => $rec_id
            )
        ));

        if($event_reg) {
            $event_id = $event_reg["EventRegistration"]["event"];

            $event = $this->CalendarEvent->find("first", array(
                "conditions" => array(
                    "id" => $event_id
                )
            ));

            if($event){
                $location_id = $event["CalendarEvent"]["location"];

                $location = $this->Location->find("first", array(
                    "conditions" => array(
                        "id" => $location_id
                    )
                ));

                if($location){
                    return $location["Location"]["name"];
                }
            }
        }
        return false;

    }

    public function getLocationFromPayment($payment_id){
        $this->loadModel("EventRegistration");
        $this->loadModel("EventAttendee");
        $this->loadModel("CalendarEvent");
        $this->loadModel("PtBooking");
        $this->loadModel("Location");

        // First check event registrations
        $event_reg = $this->EventRegistration->find("first", array(
            "conditions" => array(
                "payment" => $payment_id
            )
        ));

        if($event_reg) {
            $event_id = $event_reg["EventRegistration"]["event"];

            $event = $this->CalendarEvent->find("first", array(
                "conditions" => array(
                    "id" => $event_id
                )
            ));

            if($event){
                $location_id = $event["CalendarEvent"]["location"];

                $location = $this->Location->find("first", array(
                    "conditions" => array(
                        "id" => $location_id
                    )
                ));

                if($location){
                    return $location["Location"]["name"];
                }
            }
        }

        // Now check event attendances
        $event_att = $this->EventAttendee->find("first", array(
            "conditions" => array(
                "payment_id" => $payment_id
            )
        ));

        if($event_att) {
            $event_id = $event_att["EventAttendee"]["event"];

            $event = $this->CalendarEvent->find("first", array(
                "conditions" => array(
                    "id" => $event_id
                )
            ));

            if($event){
                $location_id = $event["CalendarEvent"]["location"];

                $location = $this->Location->find("first", array(
                    "conditions" => array(
                        "id" => $location_id
                    )
                ));

                if($location){
                    return $location["Location"]["name"];
                }
            }
        }

        // Now check PT Bookings
        $book = $this->PtBooking->find("first", array(
            "conditions" => array(
                "payment_id" => $payment_id
            )
        ));

        if($book) {
            $location_id = $book["PtBooking"]["location"];

            $location = $this->Location->find("first", array(
                "conditions" => array(
                    "id" => $location_id
                )
            ));

            if($location){
                return $location["Location"]["name"];
            }
        }

        return false;

    }

    public function getPayment($id){
        $this->layout = 'ajax';
        $response = array();
        if(strpos($id, "OT") !== false){
            $payment_id = str_replace("OT", "", $id);
            $this->makePayment($payment_id);
        } else if(strpos($id, "REC") !== false){
            $id = str_replace("REC", "", $id);
            $this->makeRecurringPayment($id);
        }
    }
}