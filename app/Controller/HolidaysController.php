<?php
class HolidaysController extends AppController {
    
    public function add() {
        $this->layout = 'ajax';
        $date = date('Y-m-d',strtotime($this->request->data['date']));
        $duration = $this->request->data['duration'];
        $x = 0;
        
        while($x<$duration){
            $this->Holiday->create();
            $this->Holiday->save(array(
              'user' => $this->Auth->user('id'),
                'date' => $date
            ));
            $date =  date('Y-m-d',strtotime($date . '+1 day'));
            $x++;
        }
        
        $response['error'] = false;
        
        $this->set('response', $response);
	}
    
}