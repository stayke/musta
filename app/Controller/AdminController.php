<?php

App::uses('Controller', 'Controller');

class AdminController extends AppController {
	public $components = array('DebugKit.Toolbar');
	public function beforeFilter() {
        if(!$this->Auth->user('role') == 0){
			$this->redirect('/');
		}
    }
	
	function index(){
		$this->redirect('/admin/reports');
	}
	
	public function reports(){
        
	}
	
	public function users() {

        $this->loadModel('User');

        if($this->Auth->user('role') != 0 || !$this->Auth->user('id')){
            echo "You do not have access to this page";
            die;
        }

        $cur_user_id = $this->Auth->user('id');


        $trainers = $this->User->find('all', array(
            'conditions' => array(
                'role' => 1
            )
        ));

        $this->set('trainers', $trainers);

        $participants = $this->User->find('all', array(
            'conditions' => array(
                'role' => 3
            )
        ));

        $this->set('participants', $participants);
	}
	
	public function approvals() {
		
	}

    public function trainerdisable($id)
    {
        $this->layout = 'ajax';
        $this->view = "ajaxresponse";
        $this->loadModel('User');

        $response = array();
        $response["error_messages"] = array();

        if ($this->Auth->user('role') != 0 || !$this->Auth->user('id')) {
            $response["error_messages"][] = "You do not have access to this page";
            $this->set("response", $response);
            return;
        }

        $this->User->id = $id;

        $save_data = array(
            "status" => "disabled"
        );

        $response = $this->User->save($save_data);

        $this->set("response", $response);
    }


    public function trainerapprove($id){
        $this->layout = 'ajax';
        $this->view = "ajaxresponse";
        $this->loadModel('User');

        $response = array();
        $response["error_messages"] = array();

        if($this->Auth->user('role') != 0 || !$this->Auth->user('id')){
            $response["error_messages"][] = "You do not have access to this page";
            $this->set("response", $response);
            return;
        }

        $this->User->id = $id;

        $save_data = array(
            "status" => "approved"
        );

        $response = $this->User->save($save_data);

        $this->set("response", $response);

    }
	
	public function getapprovals(){
		$this->layout = 'ajax';
		$this->loadModel('User');
		
		$items = $this->User->find('all', array('conditions' => array('OR' => array('approved_licence' => false, 'approved_freg' => false), 'role' => array(2,3))));
		$this->set('response', $items);
	}

    public function sendDetails($id){
        $this->layout = 'ajax';
        $this->view = 'ajaxresponse';
        $this->loadModel('User');

        $response = array();

        if($this->Auth->user('role') != 0 || !$this->Auth->user('id')){
            echo "You do not have access to this page";
            die;
        }

        $send_user = $this->User->find("first", array(
            "conditions" => array(
                "id" => $id
            )
        ));

        if(!$send_user){
            echo "cannot find specified user";
            die;
        }

        $message = "Here are the details for salesforce user creation for user: " . $send_user["User"]["id"];

        $email = new CakeEmail();
        $email->from(array("support@ifactory.com.au" => "Musta Email Sender"));
        $email->to(array("sales@integrapay.com.au" => "Musta Email Receiver"));
//        $email->from(array("nicholas.jephcott@ifactory.com.au" => "Musta Email Sender"));
//        $email->to(array("typhado@hotmail.com" => "Musta Email Receiver"));
        $email->subject("Musta - User Details Attachments");

        $email->attachments(array(
            'licence_front.png' => '/home/mustadev/public_html/app/Uploads/' . $send_user["User"]["licence_front"],
            'licence_back.png' => '/home/mustadev/public_html/app/Uploads/' . $send_user["User"]["licence_back"],
            'bankstatement_pic.png' => '/home/mustadev/public_html/app/Uploads/' . $send_user["User"]["bankstatement_pic"]
        ));

        $email->send($message);

        $this->set("response", $response);

    }
	
}