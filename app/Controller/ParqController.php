<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 7/07/2016
 * Time: 10:10 AM
 */

App::uses('AppController', 'Controller');

class ParqController extends AppController
{

    private $sections = array("condition", "experienced", "misc", "result");

    public function view($id){
        $this->layout = "ajax";
        $this->view = "ajaxresponse";

        $this->loadModel("TrainerClient");
        $this->loadModel("ParqOption");

        $response = array();
        $response["error_messages"] = array();

        $cur_user = $this->Auth->user('id');

        // If you are not trying to access your own
        if($id != $cur_user){
            // Check if you are trying to access a clients
            $relationship = $this->TrainerClient->find('first', array(
                'conditions' => array(
                    'user_id' => $id,
                    'trainer_id' => $cur_user
                )
            ));
            if($relationship){
                $response["trainer_access"] = true;
            } else {
                $response["error_messages"][] = "You do not have access to this information";
                $this->set("response", $response);
                return false;
            }
        }

        $parq = $this->Parq->find("first", array(
            "conditions" => array(
                "user" => $id
            )
        ));
        if(isset($parq["Parq"])) {
            $response["parq"] = $parq["Parq"];

            // Go through options and add them to response
            if(isset($parq["ParqOption"])){
                foreach($parq["ParqOption"] as $opt) {
                    $response["parq"][$opt["section"]][$opt["field"]] = $opt["value"];
                }
            }
        }

        $this->set("response", $response);

    }

    public function save(){
        $this->layout = "ajax";
        $this->view = "ajaxresponse";

        $this->loadModel("TrainerClient");
        $this->loadModel("ParqOption");

        $response = array();
        $response["error_messages"] = array();

        $cur_user = $this->Auth->user('id');
        $id = $this->request->data['id'];
        $data = $this->request->data['data'];
        $response["data"] = $data;

        // If you are not trying to access your own
        if($id != $cur_user){
            // Check if you are trying to access a clients
            $relationship = $this->TrainerClient->find('first', array(
                'conditions' => array(
                    'user_id' => $id,
                    'trainer_id' => $cur_user
                )
            ));
            if($relationship){
                $response["trainer_access"] = true;
            } else {
                $response["error_messages"][] = "You do not have access to this information";
                $this->set("response", $response);
                return false;
            }
        }

        $parq = $this->Parq->find("first", array(
            "conditions" => array(
                "user" => $id
            )
        ));

        $parq_id = false;
        if($parq){
            $this->Parq->id = $parq["Parq"]["id"];
            $parq_id = $parq["Parq"]["id"];
        } else {
            $this->Parq->create();
        }

        $parq_data = $data;
        $parq_data["user"] = $id;

        $result = $this->Parq->save($parq_data);
        $response["result"] = $result;

        $insert_parq_options = array();
        if(!$parq_id) {
            $parq_id = $result["Parq"]["id"];
        }

        // delete all old Parq Options
        $this->ParqOption->deleteAll(array(
            "parq" => $parq_id
        ));

        foreach($this->sections as $section){
            if($data[$section]){
                foreach($data[$section] as $field => $value){
                    $insert_row = array(
                        "parq" => $parq_id,
                        "section" => $section,
                        "field" => $field,
                        "value" => $value
                    );
                    $insert_parq_options[] = $insert_row;
                }
            }
        }

        $option_result = $this->ParqOption->saveMany($insert_parq_options);
        $response["option_result"] = $option_result;

        $this->set("response", $response);

    }

}