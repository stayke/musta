<?php
class BookingController extends AppController {
	function index(){
		
	}
    
    function view($id = false){
		$this->layout = 'ajax';
        $this->loadModel('PtBookings');
		if(!$id){
            $id = $this->Auth->user('id');
        }
        
        $response = $this->PtBookings->find('all', array('conditions' => array('to_user' => $id), 'fields' => array('id','session_id','time','length')));
		
		$this->set('response', $response);
	}

    // Function to get a specific booking by ID
    // $id of the booking you want to view
    public function getPtbooking($id)
    {
        $this->layout = 'ajax';

        $this->loadModel("Location");
        $this->loadModel("PtBooking");
        $this->loadModel("PtPricing");
        $this->loadModel("Payment");
        $this->loadModel("User");

        $cur_user_id = $this->Auth->user('id');

        $booking = $this->PtBooking->find('first', array(
            'conditions' => array(
                'id' => $id
            )
        ));

        if(!isset($booking["PtBooking"])){
            $response["error"] = "Could not find the booking you requested.";
            $this->set('response', $response);
            return false;
        }

        // NDJ - #$#$# todo also check admin access
        if($booking["PtBooking"]["user"] != $cur_user_id && $booking["PtBooking"]["to_user"] != $cur_user_id){
            $response["error"] = "You do not have access to view this booking";
            $this->set('response', $response);
            return false;
        }

        // Calculate some convenience values out.
        $booking["PtBooking"]["date_start"] = date("Y-m-d", strtotime($booking["PtBooking"]["time"]));
        $booking["PtBooking"]["time_start"] = date("H:i:s", strtotime($booking["PtBooking"]["time"]));

        // Load the booking into the response
        $response['booking'] = $booking["PtBooking"];

        // Load the location into the response
        $location = $this->Location->find('first', array(
            'conditions' => array(
                'id' => $booking["PtBooking"]["location"]
            )
        ));
        $response["booking"]["location"] = $location;

        // Load the Payment into the response
        $payment = $this->Payment->find('first', array(
            'conditions' => array(
                'id' => $booking["PtBooking"]["payment_id"]
            )
        ));
        $response["booking"]["payment"] = $payment;

        // Load the trainer into the response
        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $booking["PtBooking"]["to_user"]
            )
        ));
        $trainer_data = array( // Only load certain fields (password and such are store in this table don't want to send them to the browser)
            "first_name" => $trainer["User"]["first_name"],
            "second_name" => $trainer["User"]["second_name"]
        );
        $response["booking"]["trainer"] = $trainer_data;

        // Load the participant into the response
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $booking["PtBooking"]["user"]
            )
        ));
        $participant_data = array( // Only load certain fields (password and such are store in this table don't want to send them to the browser)
            "first_name" => $participant["User"]["first_name"],
            "second_name" => $participant["User"]["second_name"]
        );
        $response["booking"]["participant"] = $participant_data;

        // Load the location prices into the response
        $pricing = $this->PtPricing->find('all', array(
            'conditions' => array(
                'locationid' => $booking["PtBooking"]["location"]
            )
        ));
        $response["booking"]["pricing"] = $pricing;

        $this->set('response', $response);
    }

    // Function to save a ptbooking
    //  #$#$# currently does not handle new bookings or bc bookings
    public function savePtbooking(){

        $this->layout = 'ajax';

        $this->loadModel("PtBooking");
        $this->loadModel("User");
        $this->loadModel("Message");
        $this->loadModel("Location");

        $rawBooking = $this->request->data['newBooking'];

        $booking_id = $rawBooking["id"];

        $cur_user_id = $this->Auth->user('id');

        $original_booking = $this->PtBooking->find('first', array(
            'conditions' => array(
                'id' => $booking_id
            )
        ));

        if(!$original_booking){
            if($rawBooking["id"]){
                $response["error"] = "could not find the booking you were trying to save";
                $response["booking"] = $rawBooking;
                $this->set('response', $response);
                return false;
            } else {
                // It's a new booking
            }
        } else { // if an existing booking is found make sure user id's match up
            if($cur_user_id != $original_booking["PtBooking"]["to_user"]){
                $response["error"] = "You are not the owner of this booking";
                $response["booking"] = $rawBooking;
                $this->set('response', $response);
                return false;
            }
        }

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $original_booking["PtBooking"]["to_user"]
            )
        ));

        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $original_booking["PtBooking"]["user"]
            )
        ));


        // Process and data check the event before saving #$#$# todo more data and security checks here, user authorization etc.

        $newBooking = array();
        $newBooking["id"] = $booking_id;
        $newBooking["session_notes"] = $rawBooking["session_notes"];

        // Actions if you are editing more than just notes
        if($rawBooking["editBooking"]){
            $newBooking["time"] = date("Y-m-d", strtotime($rawBooking["date_start"])) . " " . date("H:i:s", strtotime($rawBooking["time_start"]));
        }

        if($rawBooking["cancelBooking"]){
            $newBooking["cancelled"] = true;
        }

        // Save the booking
        $response['result'] = $this->PtBooking->save($newBooking);

        // Actions if you want to notify the client
        if($rawBooking["notifyClient"] || $rawBooking["editBooking"] || $rawBooking["cancelBooking"]){
            // Create the message
            $message = array();
            if(isset($rawBooking["msgToClient"])) {
                $message[] = $rawBooking["msgToClient"];
                $message[] = "";
            }

//            $message .= print_r($response['result'], true);
            if($rawBooking["cancelBooking"]){
                $message[] = "The booking has been cancelled.";
            }

            if($rawBooking["editBooking"]){
                $message[] = "Date/Time: " . $newBooking["time"];
                if($original_booking["PtBooking"]["time"] != $newBooking["time"]){
                    $message[] = "Date/Time Was: " . $original_booking["PtBooking"]["time"];
                }
            } else {
                $message[] = "Date/Time: " . $original_booking["PtBooking"]["time"];
            }

            $message[] = "Duration: " . $original_booking["PtBooking"]["length"];

            $message[] = "Trainer Notes: " . $rawBooking["session_notes"];

            $message_str = implode("\r\n", $message);

            // Save the message to the database
            $new_message = array(
                "from_user" => $trainer["User"]["id"],
                "to_user" => $participant["User"]["id"],
                "from_email" => $trainer["User"]["email"],
                "to_email" => $participant["User"]["email"],
                "subject" => "Your booking has been updated",
                "message" => $message_str
            );

            $this->Message->save($new_message);

            // Send the email
            $email = new CakeEmail();
            $email->from(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
            $email->to(array($participant["User"]["email"] => $participant["User"]["first_name"]));
            $email->subject("Your booking has been updated");
            $email->send($message);
        }

        $this->set('response', $response);
    }

    // Used to view all the bookings for the currently logged in user
    public function mybookings() {

        $this->loadModel("User");
        $this->loadModel("Location");
        $this->loadModel("PtBooking");
        $this->loadModel("EventAttendee");
        $this->loadModel("EventRegistration");
        $this->loadModel("CalendarEventInstance");
        $this->loadModel("CalendarEvent");

        $error_messages = array();
        $bookings = array();
        $events = array();
        $event_attendances = array();

        $cur_user_id = $this->Auth->user('id');
        $cur_date = date("d-m-Y");

        $cur_user = $this->User->find('first', array(
            'conditions' => array(
                'id' => $cur_user_id
            )
        ));

        if(!$cur_user || !$cur_user["User"]["id"]){
            $error_messages[] = "Cannot find the requested user";
        }

        // Makes the bookings load all user info
        $this->PtBooking->getUserInfo();

        // if trainer
        if($cur_user["User"]["role"] == 1) {
            $this->set("userview", "trainer");
            $bookings = $this->PtBooking->find('all', array(
                'conditions' => array(
                    "to_user" => $cur_user_id,
//                    'time >= ' => date("Y-m-d H:i:s")
                ),
                'order' => array('time ASC')
            ));

//            echo "<pre>";
//            print_r($bookings);
//            echo "</pre>";

            foreach($bookings as &$booking){
                $location = $this->Location->find("first", array(
                    'conditions' => array(
                        'id' => $booking["PtBooking"]["location"]
                    )
                ));
                $booking["Location"] = $location;
            }

            $events = $this->CalendarEventInstance->find('all', array(
                'conditions' => array(
                    'user' => $cur_user_id,
                    'date >= ' => date("Y-m-d")
                ),
                'order' => array('date ASC')
            ));

            foreach($events as &$event){
                $location = $this->Location->find("first", array(
                    'conditions' => array(
                        'id' => $event["CalendarEventInstance"]["location"]
                    )
                ));
                $event["Location"] = $location;
            }

        // if participant
        } else if($cur_user["User"]["role"] == 3) {
            $this->set("userview", "participant");
            $bookings = $this->PtBooking->find('all', array(
                'conditions' => array(
                    "user" => $cur_user_id,
                    'time >= ' => date("Y-m-d H:i:s")
                )
            ));

            foreach($bookings as &$booking){
                $location = $this->Location->find("first", array(
                    'conditions' => array(
                        'id' => $booking["PtBooking"]["location"]
                    )
                ));
                $booking["Location"] = $location;
            }

            $this->EventAttendee->bindModel(
                array('belongsTo' => array(
                    'CalendarEvent' => array(
                        'className' => 'CalendarEvent',
                        'foreignKey' => 'event'
                    ),
                    'CalendarEventInstance' => array(
                        'className' => 'CalendarEventInstance',
                        'foreignKey' => 'event_instance'
                    ),
                    'EventRegistration' => array(
                        'className' => 'EventRegistration',
                        'foreignKey' => 'registration'
                    )
                ))
            );

            $event_attendances = $this->EventAttendee->find('all', array(
                'conditions' => array(
                    'EventAttendee.user' => $cur_user_id,
                    'CalendarEventInstance.date >=' => date("Y-m-d")
                )
            ));

            foreach($event_attendances as &$event_attendance){
                $location = $this->Location->find("first", array(
                    'conditions' => array(
                        'id' => $event_attendance["CalendarEventInstance"]["location"]
                    )
                ));
                $event_attendance["Location"] = $location;
            }

        }

        $this->set('error_messages', $error_messages);
        $this->set('bookings', $bookings);
        $this->set('event_attendances', $event_attendances);
        $this->set('events', $events);

    }
}