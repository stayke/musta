<?php
class TrainerController extends AppController {
	public function index(){
		$this->layout = 'trainer';
		if(!in_array($this->Auth->user('role'), array(1,2)) ){
            $this->redirect('/');
        }
	}
}