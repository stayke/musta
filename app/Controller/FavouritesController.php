<?php
class FavouritesController extends AppController {
    public function index(){
        
        $this->set('favourites', $this->getFavourites(true));
    }
    
    
    public function updateFave($id, $add){
        $this->layout = 'ajax';
        if(!$this->Auth->user('id')) {$this->set('response',false); return false;}
        
        if($add){
            $this->Favourite->save(array('user_id' => $this->Auth->user('id'), 'trainer_id' => $id));
        } else {
            $this->Favourite->deleteAll(array('user_id' => $this->Auth->user('id'), 'trainer_id' => $id));
        }
        
        $this->set('response', true);
    }
    
    public function count() {
        $this->layout = 'ajax';
        $this->view = 'update_fave';
        if(!$this->Auth->user('id')) {$this->set('response',false); return false;}
        
        $response = $this->Favourite->find('count', array('conditions' => array('trainer_id' => $this->Auth->user('id'))));
        
        $this->set('response', $response);
	}
    
}