<?php
class SignupController extends AppController {
    
   public function tempImage($type){
        
        $this->layout = 'ajax';
        
        $this->loadModel('Tempimage');

        $this->Tempimage->deleteAll(array(
            'added <' => date('Y-m-d h:i:s', strtotime('-1 day'))
        ));
        
        $response['date'] = date('Y-m-d h:i:s', strtotime('-1 day'));
        
        $check = 1;
        
        while($check>0){
            $md5 = md5(uniqid(mt_rand(), true));
            $check = $this->Tempimage->find('count', array('conditions' => array('secure' => $md5)));
        }
        
        $ext = pathinfo($this->request->params['form']['file']['name'], PATHINFO_EXTENSION);
        
        $id = $this->Tempimage->save(array('extension' => $ext, 'added' => date("Y-m-d h:i:s"), 'secure' => $md5));
        
        if($id){
            move_uploaded_file($this->request->params['form']['file']['tmp_name'], APP . 'Uploads/Pending/' . $id['Tempimage']['secure'] . '.' . $ext);
            $response['error'] = false;
            $response['data']['id'] = $id['Tempimage']['id'];
            $response['data']['ext'] = $ext;
            $response['data']['secure'] = $md5;
            
        } else {
            $response['error'] = true;
        }
        
        $this->set('response', $response);
   }
   
   public function viewTempImage($img){
        $this->response->file(APP . 'Uploads/Pending/' . $img);
        // Return response object to prevent controller from trying to render
        // a view
        return $this->response;
   }
   
   public function signup(){
      $this->layout = 'ajax';
      
      $response['data'] = $this->request->data;
      $response['error'] = false;
      
      foreach($this->request->data['steps'] as $step){
         $steps[$step['name']] = $step;
      }
      $steps['personal']['data']['role'] = $this->request->data['role'];
      if(!$user = $this->addUser($steps['personal']['data'])){
         $response['error'] = array('code' => 10, 'text' => 'Email Already In Use');
         $this->set('response', $response);
         return false;
      };
      
      if($user){
         $id = $user['User']['id'];
       } else {
         $id = false;
       }
      
      foreach($steps as $step):
         switch($step['name']):
            case 'profile':
               $this->addProfile($step['data'], $id);
            break;
         
            case 'terms':
               $this->addUserTerms($step['data'], $id);
            break;
         
            case 'bankdetails':
               $this->addBankDetails($step['data'], $id);
            break;
         
            case 'mustaterms':
               $this->acceptTerms($step['data'], $id);
            break;
         
            case 'locations':
               $this->addLocations($step['data'], $id);
            break;
         
         endswitch;
      endforeach;
      
      $response['user']['email'] = $steps['personal']['data']['email'];
      $response['user']['password'] = $steps['personal']['data']['password'];
      

      
      if($steps['personal']['data']['role'] == 3){
         $this->loadModel('TrainerClient');
         
         $previous = $this->TrainerClient->find('all', array('conditions' => array('email' => $steps['personal']['data']['email'])));
         
         
         foreach($previous as &$p){
            $p['TrainerClient']['user_id'] = $id;
            $this->TrainerClient->save($p);
            
            $notification = array(
                'user_id' => $p['TrainerClient']['trainer_id'],    
                'text' => 'a Client has just joined the site',
                'type' => 'information'
            );
            
            $this->addNotification($notification);
            
         }
         
         $this->loadModel('PendingPayments');
         
         $previous = $this->PendingPayments->find('all', array('conditions' => array('user_email' => $steps['personal']['data']['email'])));
         foreach($previous as &$p){
            $p['PendingPayments']['user_id'] = $id;
            $this->PendingPayments->save($p);
            
            $notification = array(
                'user_id' => $id,    
                'text' => 'a trainer has set up a payment request for you with this message',
                'target' => $p['PendingPayment']['id'],
                'type' => 'paymentRequest'
            );
            
            $this->addNotification($notification);
         }

         
      }
      
      $this->set('response', $response);
   }
   
   public function verify(){
      $this->layout = 'ajax';
      
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, 
         http_build_query(array(
            'secret' => '6LfuPCETAAAAAIljXPpY732tBFzKiRnZtCIcmrLO',
            'response' => $this->request->data['g-recaptcha-response'],
            'ip' => $this->request->clientIp()
         ))
      );
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $server_output = curl_exec ($ch);
      curl_close ($ch);
      $this->set('repsonse', $server_output);
   }
   
}