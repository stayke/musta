<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController
{

    public $components = array('RequestHandler');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('add');
    }

    public function index()
    {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function view($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->findById($id));
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The user could not be saved. Please, try again.')
            );
        }
    }

    public function edit($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The user could not be saved. Please, try again.')
            );
        } else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete($id = null)
    {
        // Prior to 2.5 use
        // $this->request->onlyAllow('post');

        $this->request->allowMethod('post');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Session->setFlash(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

    public function login()
    {
        $this->layout = 'ajax';

        if ($this->request->is('ajax')) {
            $this->Auth->logout();
            $rest_json = file_get_contents("php://input");
            $this->request->data['User'] = json_decode($rest_json, true);

            if ($this->Auth->login()) {
                if($this->Auth->user('status') == "disabled"){
                    $this->Auth->logout();
                    $return = array(
                        'error' => array(
                            'text' => 'This user has been disabled'
                        )
                    );
                } else {
                    $access_token = bin2hex(openssl_random_pseudo_bytes(16));
                    $this->User->save(array('id' => $this->Auth->user('id'), 'access_token' => $access_token));
                    $return = array(
                        'user' => $this->Auth->user(),
                        'error' => false
                    );
                }

            } else {
                $return = array(
                    'error' => array(
                        'text' => 'Invalid Username/Password'
                    )
                );
            }

        } else {
            $this->redirect('/');
        }

        $this->set('return', $return);

    }

    public function logout()
    {
        $this->User->save(array('id' => $this->Auth->user('id'), 'auth_token' => NULL));
        $this->Auth->logout();
    }

    public function get_token()
    {
        $this->layout = 'ajax';
        $this->loadModel('Userinfo');
        if (!$this->Auth->user('id')) $return = false;
        else $return = $this->Auth->user();


        $profilePage = $this->Userinfo->find('all', array(
            'conditions' => array(
                'user_id' => $this->Auth->user('id')
            ),
            'fields' => array('meta_key', 'meta_value')
        ));
        $return['favourites'] = $this->getFavourites();
        $return['profilePage'] = array();

        foreach ($profilePage as $pp) {
            switch ($pp['Userinfo']['meta_key']):

                case 'speciality':
                    $return['profilePage']['speciality'][$pp['Userinfo']['meta_value']] = true;
                    break;

                case 'qualification':
                    $return['profilePage']['qualifications'][] = json_decode($pp['Userinfo']['meta_value']);
                    break;

                default:
                    $return['profilePage'][$pp['Userinfo']['meta_key']] = $pp['Userinfo']['meta_value'];
                    break;
            endswitch;
        }

        $this->set('return', $return);
    }

    public function profile($id)
    {
        $this->layout = 'ajax';
        $this->loadModel('Locations');
        $this->User->recursive = 3;
        $response = $this->User->find('first', array('conditions' => array('id' => $id), 'contain' => array('Timerange.Locations')));

        $locations = $this->Locations->find('list', array('conditions' => array('user' => $id)));

        if (isset($response['Timeslots'])) {
            foreach ($response['Timeslots'] as $r) {
                $r['type'] = 'slot';
                $r['location'] = $locations[$r['location']];
                $response['Combinedslots'][] = $r;
            }
        }

        if (isset($response['Timeranges'])) {
            foreach ($response['Timeranges'] as $r) {
                $r['type'] = 'range';
                $r['location'] = $locations[$r['location']];
                $response['Combinedslots'][] = $r;
            }
        }
        $this->set('response', $response);
    }

    public function search()
    {
        if (in_array($this->Auth->user('role'), array(1, 2))) {
            $this->redirect('/trainer/');
        }
    }

    public function signup()
    {

    }

    public function getUsers()
    {
        $this->layout = 'ajax';
        $this->view = 'saveprofile';
        $users = $this->request->data('users');
        $response = [];
        foreach ($users as $user) {
            $response[] = $this->viewuser($user);
        }

        foreach ($response as &$r) {
            foreach ($r['meta']['qualifications'] as &$info) {
                $info['Userinfo']['meta_value'] = json_decode($info['Userinfo']['meta_value']);
            }
        }

        $this->set('response', $response);

    }


    public function saveProfile()
    {
        $this->layout = 'ajax';
        $response = array();
        if (!$this->Auth->user('id') || !$this->request->is('post')) {
            $this->set('response', 'Moo');
            return false;
        }


        $this->addProfile($this->request->data, $this->Auth->user('id'), true);

        $this->set('response', $this->request->data);
    }

    public function savepersonal()
    {
        $this->layout = 'ajax';
        $response = array();
        if (!$this->Auth->user('id') || !$this->request->is('post')) {
            $this->set('response', 'Moo');
            return false;
        }
        $save = $this->addUser($this->request->data, $this->Auth->user('id'));
        $this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));
        $this->set('response', $save);
    }

    public function savebankdeets()
    {
        $this->layout = 'ajax';
        $response = array();
        if (!$this->Auth->user('id') || !$this->request->is('post')) {
            $this->set('response', 'Moo');
            return false;
        }
        $save = $this->addBankDetails($this->request->data, $this->Auth->user('id'));
        $this->set('response', $save);
    }

    public function getbankdeets()
    {
        $this->layout = 'ajax';
        $this->loadModel('Bankdeet');
        $response = array();
        if (!$this->Auth->user('id') || !$this->request->is('post')) {
            $this->set('response', 'Moo');
            return false;
        }
        $save = $this->Bankdeet->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'))));
        $this->set('response', $save);
    }


    public function viewUserImage($img)
    {
        if (file_exists(APP . 'Uploads/' . $img)) {
            $this->response->file(APP . 'Uploads/' . $img);
        } else {
            $this->response->file(APP . 'Uploads/defaultUser.jpg');
        }
        // Return response object to prevent controller from trying to render
        // a view
        return $this->response;
    }

    public function review()
    {
        $results = explode(',', $this->request->query['trainers']);

        $trainers = $this->User->find('all', array('conditions' => array('id' => $results)));

        // NDJ - JESUS CHRIST WTF??? #$#$# todo move getUserDetail out of AppController and make one database for all users not 4 calls for each user
        foreach ($trainers as &$t) {
            $t['profile_pic'] = ($pic = $this->getUserDetail($t['User']['id'], 'profile_pic')) ? $pic['0']['Userinfo']['meta_value'] : 'defaultUser.jpg';
            $t['specialities'] = $this->getUserDetail($t['User']['id'], 'speciality');
            $t['short_desc'] = $this->getUserDetail($t['User']['id'], 'short_desc');
            $t['long_desc'] = $this->getUserDetail($t['User']['id'], 'long_desc');
        }

        $this->set('results', implode(',', $results));
        $this->set('trainers', $trainers);
    }

    public function reviewobj(){

        $this->layout = "ajax";

        $this->loadModel('Location');
        $this->loadModel('PtPricing');

        $results = explode(',', $this->request->query['trainers']);

        // Only show these locations for the selected trainers
        $locations = explode(',', $this->request->query["locations"]);

        $trainers = $this->User->find('all', array('conditions' => array('id' => $results)));

//        // NDJ - JESUS CHRIST WTF??? #$#$# todo move getUserDetail out of AppController and make one database call for all users not 4 calls for each user
//        foreach ($trainers as &$t) {
//            $t['profile_pic'] = ($pic = $this->getUserDetail($t['User']['id'], 'profile_pic')) ? $pic['0']['Userinfo']['meta_value'] : 'defaultUser.jpg';
//            $t['specialities'] = $this->getUserDetail($t['User']['id'], 'speciality');
//            $t['short_desc'] = $this->getUserDetail($t['User']['id'], 'short_desc');
//            $t['long_desc'] = $this->getUserDetail($t['User']['id'], 'long_desc');
//        }

        // Organise meta data and only transfer data that's needed, otherwise you transfer everything including encrypted password and email.
        $filtered_trainers = array();
        foreach($trainers as $t){

            // New function to organise meta into keyed array rather than spam the database
            $user_meta = $this->User->sortMeta($t["Userinfo"]);

            $short_desc = isset($user_meta["short_desc"]) ? $user_meta["short_desc"] : "";
            $long_desc = isset($user_meta["long_desc"]) ? $user_meta["long_desc"] : "";
            $specialities = isset($user_meta["speciality"]) ? $user_meta["speciality"] : array();

            // Bind location model temporarily to get Both pricing options as well.
            $this->Location->bindModel(
                array(
                    'hasMany' => array(
                        'PtPricing' => array(
                            'className' => 'PtPricing',
                            'foreignKey' => 'locationid'
                        ),
                        'BcPricing' => array(
                            'className' => 'BcPricing',
                            'foreignKey' => 'locations_id'
                        )
                    )
                )
            );
            $trainer = array(
                "first_name" => $t["User"]["first_name"],
                "trainer_id" => $t["User"]["id"],
                "short_desc" => $short_desc,
                "long_desc" => $long_desc,
                "profile_pic" => ($pic = $this->getUserDetail($t['User']['id'], 'profile_pic')) ? $pic['0']['Userinfo']['meta_value'] : 'defaultUser.jpg',
                "specialities" => $specialities,
                "locations" => $this->Location->find('all', array('conditions' => array('user' => $t['User']['id'], "id" => $locations)))
            );

//            foreach($trainer["locations"] as &$l){
//                $l['pricing'] = $this->PtPricing->find('all', array('conditions' => array('user_id' => $t["User"]["id"], 'locationid' => $l['Location']['id'])));
//            }
            $filtered_trainers[] = $trainer;
        }

        $response = array();
        $response["results"] = implode(',', $results);
//        $response["trainers"] = $trainers;
        $response["trainers"] = $filtered_trainers;

        $this->set('response', $response);
    }

    public function viewuser($id)
    {
        $this->loadModel('Location');

        $user = $this->User->find('first', array('conditions' => array('id' => $id)));
        if(!$user){
            echo "Could not find the requested user";
            die;
        }

        if($user["User"]["role"] != 1){
            echo "Could not find the requested user";
            die;
        }

        if($user["User"]["status"] != "approved"){
            if($this->Auth->user('id') && $this->Auth->user('role') == '0') {
                // let super user view this
            } else {
                echo "The requested user is not yet approved to go live, please check back again later.";
                die;
            }
        }

        $user['meta'] = array(
            'profile_pic' => ($pic = $this->getUserDetail($user['User']['id'], 'profile_pic')) ? $pic['0']['Userinfo']['meta_value'] : 'defaultUser.jpg',
            'specialities' => $this->getUserDetail($user['User']['id'], 'speciality'),
            'qualifications' => $this->getUserDetail($user['User']['id'], 'qualification'),
            'insurance' => $this->getUserDetail($user['User']['id'], 'insurance'),
            'cpr' => $this->getUserDetail($user['User']['id'], 'cpr'),
            'short_desc' => $this->getUserDetail($user['User']['id'], 'short_desc'),
            'long_desc' => $this->getUserDetail($user['User']['id'], 'long_desc'),
            'locations' => $this->Location->find('all', array('conditions' => array('user' => $id)))
        );

        $this->set('user', $user);
        if ($this->Auth->user('id')) {
            $authed = true;
        } else {
            $authed = false;
        }

        $title = $user['User']['first_name'] . ' - ';
        $title .= ($user['User']['role'] == 1) ? 'Personal Trainer ' : 'Boot Camp ';
        $title .= 'in Australia';

        $this->set('authed', $authed);
        if (isset($user['meta']['long_desc']) && count($user['meta']['long_desc']) > 0) {
            $this->set('meta_description', $user['meta']['long_desc'][0]['Userinfo']['meta_value']);
        }

        $this->set('meta_title', $title);

        return $user;
    }

    // NDJ - this can fail if you just delete clients from client list without deleting references.
    //       It's also rather inefficient and we should be using joins here.
    // but then we get no time so things end up broken like this and we just will have to do them later/never. todo fix this
    public function getClientList()
    {
        $this->layout = 'ajax';
        $this->loadModel('TrainerClient');

        if (!$this->Auth->user('id')) {
            $this->set('return', array('error' => 'Not logged in'));
            return false;
        }

        $clientLists = $this->TrainerClient->find('all', array('conditions' => array('trainer_id' => $this->Auth->user('id'))));

        foreach ($clientLists as &$client):
            if ($client['TrainerClient']['user_id'] > 0) {
                $cl = $this->User->find('first', array('conditions' => array('id' => $client['TrainerClient']['user_id'])));

                $client['TrainerClient']['name'] = $cl['User']['first_name'] . ' ' . $cl['User']['second_name'];
                $client['TrainerClient']['email'] = $cl['User']['email'];
                $client['TrainerClient']['mobile'] = $cl['User']['phone'];
                $client['TrainerClient']['clientpic'] = $cl['User']['clientpic'];

            }
        endforeach;

        $this->set('return', $clientLists);
    }

    public function addClient()
    {
        $this->loadModel('TrainerClient');
        if (!$this->Auth->user('id')) {
            $this->set('return', array('error' => 'Not logged in'));
            return false;
        }

        $found = $this->TrainerClient->find('first', array('conditions' => array('email' => $this->request->data['email'])));
        if ($found) {
            $this->set('return', array('error' => 'User already in list'));
            return false;
        }

        $user = array(
            'user_id' => NULL,
            'name' => $this->request->data['name'],
            'mobile' => $this->request->data['mobile'],
            'email' => $this->request->data['email'],
            'trainer_id' => $this->Auth->user('id')
        );

        $found = $this->User->find('first', array('conditions' => array('email' => $this->request->data['email'])));
        if ($found) {
            $user['user_id'] = $found['User']['id'];
        } else {
            $new_user = array(
                'first_name' => $this->request->data['name'],
                "email" => $this->request->data['email'],
                "role" => 3,
                "created" => date("Y-m-d"),
                "modified" => date("Y-m-d"),
                "status" => "unregistered"
            );
            $this->User->create();
            $result = $this->User->save($new_user);
            $user["user_id"] = $result["User"]["id"];
            $this->User->addCreateNotification($user["user_id"]);
        }

        $this->TrainerClient->create();
        $this->TrainerClient->save($user);

        $this->set('return', true);
    }

    public function reset()
    {
        $this->view = 'get_client_list';
        $this->layout = 'ajax';

        if ($u = $this->User->find('first', array('conditions' => array('email' => $this->request->data['email'])))) {
            $pw = $this->randomString(8);
            $u['User']['password'] = $pw;
            $this->User->save($u);
            $r['message']['text'] = 'Password has been reset, please check your email.';

            $Email = new CakeEmail();
            $Email->from(array('admin@musta.global' => 'Musta Global'));
            $Email->to($u['User']['email']);
            $Email->subject('Password Reset');
            $Email->send('Your new password is ' . $pw);

        } else {
            $r['message']['text'] = 'Email does not exist';
        }

        $this->set('return', $r);
    }

    private function randomString($length = 6)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function checkemail()
    {
        $this->view = 'get_client_list';
        $this->layout = 'ajax';

        $return = array();

        $user = $this->User->find('first', array('conditions' => array('email' => $this->request->data['email'])));

        $return["user_id"] = false;

        if ($user) {
            $return["exists"] = true;
            // If unregistered send id as well
            if($user["User"]["status"] == "unregistered") {
                $return["user_id"] = $user["User"]["id"];
            }
        } else {
            $return["exists"] = false;
        }

        $this->set('return', $return);
//        $this->set('return', array('exists' => $return, 'user_id' => $user_id));
    }

    public function checkpw()
    {
        $this->layout = 'ajax';
        $this->view = 'get_client_list';

        $pw = $this->User->find('first', array('conditions' => array('id' => $this->Auth->user('id'))));
        $check = new BlowfishPasswordHasher;
        if (!$this->Auth->user('id')) return false;
        if ($check->check($this->request->data('password'), $pw['User']['password'])) {
            $return = array('entry' => true);
        } else {
            $return = array('entry' => false);
        }


        $this->set('return', $return);
    }

    public function pageview()
    {
        $this->layout = 'ajax';
        $this->view = 'get_client_list';
        $uid = $this->request->data('uid');

        $pre = $this->User->find('first', array('conditions' => array('id' => $uid)));

        $x = (int)$pre['User']['profile_views'];
        $this->User->id = $pre['User']['id'];

        $x++;
        $this->set('return', $this->User->save(array('id' => $pre['User']['id'], 'profile_views' => $x)));
    }

    public function saveVisibleTimes()
    {
        $this->layout = 'ajax';
        $visible_times = $this->request->data('visibleTimes');

        $update_times = array(
            'id' => $this->Auth->user('id'),
            "visible_times" => json_encode($visible_times)
        );

        $response = $this->User->save($update_times);

        $this->set("response", $response);
    }

    public function loadVisibleTimes($id)
    {
        $this->layout = 'ajax';
        $this->view = 'ajaxresponse';

        $response = $this->User->find('first', array(
            'conditions' => array(
                'id' => $id
            )
        ));

        $this->set("response", $response);
    }

    public function getCurUser(){
        $this->layout = 'ajax';
        $this->view = 'ajaxresponse';

        $cur_user_id = $this->Auth->user('id');
        $cur_user = $this->User->find('first', array(
            'conditions' => array(
                "id" => $cur_user_id
            )
        ));

        $this->set("response", $cur_user);
    }

}