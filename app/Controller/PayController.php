<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 31/05/2016
 * Time: 4:26 PM
 *
 *
 * DEPRECATED
 * DEPRECATED
 * DEPRECATED
 * DEPRECATED
 * DEPRECATED
 * DEPRECATED
 *
 * The functions this controller was designed for have been moved to the integra component or the payments controller
 *
 *
 * This Controller holds the functions needed to talk to the integra payment API
 */


App::uses('AppController', 'Controller');

class PayController extends AppController {

    public $components = array('Integra');

    private $test_mode = true;

    private $test_api_user = "1528";
    private $test_api_pass = "Eb3%8Z{gQw2";
    private $test_api_url = "https://testpayments.integrapay.com.au/API/API.ashx";

    public function index(){

    }

    // Create a new payment and booking and return details and then get a token
    public function newPayment(){
        $this->layout = 'ajax';

        $this->loadModel("Payment");
        $this->loadModel("User");

        $user = 118;
        $amount = 100;

        $to_user = 66;

        $datestamp = date("Y-m-d H:i:s");

        $status = "unpaid";

        $new_payment = array(
            "user" => $user,
            "amount" => $amount,
            "to_user" => $to_user,
            "datestamp" => $datestamp,
            "status" => $status,
            "total" => $amount
        );

        $result = $this->Payment->save($new_payment);

        $new_payment_id = $result["Payment"]["id"];

        $this->getToken($new_payment_id);
        return true;

    }

    // Get the token for a remote hosted payment method
    public function getToken($id){
        $this->layout = 'ajax';
        $response = array();

        $this->loadModel("Payment");
        $this->loadModel("PaymentIntegra");
        $this->loadModel("User");

        $cur_user_id = $this->Auth->user('id');

        if(!$id){
            $response["result"] = "FAIL";
            $response["errorMsg"] = "You need to specify a payment";
            $this->set('response', $response);
            return false;
        }

        $payment = $this->Payment->find('first', array(
                'conditions' => array(
                'id' => $id
            )
        ));

        if(!$payment){
            $response["result"] = "FAIL";
            $response["errorMsg"] = "Can not find the required payment";
            $this->set('response', $response);
            return false;
        }

        if($payment["Payment"]["user"] != $cur_user_id){
            $response["result"] = "FAIL";
            $response["errorMsg"] = "You do not have access to the specified payment";
            $this->set('response', $response);
            return false;
        }

        $user = $this->User->find('first', array(
            'conditions' => array(
                'id' => $cur_user_id
            )
        ));

        // If the user doesn't exist
        if(!$user){
            $response["result"] = "FAIL";
            $response["errorMsg"] = "The user this payment is attached to doesn't exist";
            $this->set('response', $response);
            return false;
        }

        $integra = $this->PaymentIntegra->find('first', array(
            'conditions' => array(
                'payment' => $id,
                'method' => "HostedRealTimePayment"
            )
        ));

        // If the token already exists
        if($integra){
            $response["result"] = "OK";
            $response["webPageToken"] = $integra["PaymentIntegra"]["token"];
            $this->set('response', $response);
            return true;
        }

//        $data_request = $this->request->data['payload'];

        // should get data from

        $api_user = $this->test_api_user;
        $api_pass = $this->test_api_pass;
        $api_url = $this->test_api_url;

        // round total to nearest cent
        $amount = round(($payment['Payment']['total'] * 100));

        $return_url = 'http://musta.ifactory.test/pay/handleResponse';

        $xml_data = '
<request>'.
    '<username>' . $api_user . '</username>'.
    '<password>' . $api_pass . '</password>'.
    '<command>PreHostedRealTimePayment</command>'.
    '<returnUrl>' . $return_url . '</returnUrl>'.
    '<transactionID>'. 'OT' . $payment["Payment"]["id"] . '</transactionID>'.
    '<transactionAmountInCents>' . $amount . '</transactionAmountInCents>'.
//    '<payerUniqueID>' . $user["User"]["id"] . '</payerUniqueID>'.
    '<payerFirstName>' . $user["User"]["first_name"] . '</payerFirstName>'.
    '<payerLastName>' . $user["User"]["second_name"] . '</payerLastName>'.
    '<payerAddressStreet>' . $user["User"]["address1"] . '</payerAddressStreet>'.
    '<payerAddressSuburb>' . $user["User"]["suburb"] . '</payerAddressSuburb>'.
    '<payerAddressState>' . strtoupper($user["User"]["state"]) . '</payerAddressState>'.
    '<payerAddressPostCode>' . "1234". '</payerAddressPostCode>'.
    '<payerAddressCountry>AUSTRALIA</payerAddressCountry>'.
    '<payerEmail>' . $user["User"]["email"] . '</payerEmail>'.
    '<payerPhone>' . $user["User"]["phone"] . '</payerPhone>'.
    '<payerMobile>' . $user["User"]["phone"] . '</payerMobile>'.
'</request>
';

//        echo $xml_data . "<br/><br/>";

        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache"
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $data_result = curl_exec($ch);
        curl_close($ch);

        $data = simplexml_load_string($data_result);

        $response["result"] = $data->result;
        if($data->result == "OK") {

            $new_integra = array(
                "payment" => $payment["Payment"]["id"],
                "method" => "HostedRealTimePayment",
                "token" => $data->webPageToken
            );

            $this->PaymentIntegra->save($new_integra);

            $response["result"] = "OK";
            $response["webPageToken"] = $data->webPageToken;
        } else if($data->result == "ERROR"){
            $response["result"] = "FAIL";
            $response["errorMsg"] = $data->errormessage;
        }

        $this->set('response', $response);

    }

    public function view (){
        $this->layout = 'ajax';
        $response = array();
        $this->set('response', $response);
    }
}


