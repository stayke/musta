<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 4/07/2016
 * Time: 4:47 PM
 */
App::uses('AppModel', 'Model');

// Status:
//        pending - A recurring payment has been setup but has not yet been sent to Integra System
//        approved - Recurring payment has been submitted to Integra system
//        failed - Have tried to submit recurring payment and failed

class RecurringPayment extends AppModel {

    // Sends the confirmation emails for a booking
    public function sendEmails($payment_id, $options = array()){
        $default_options = array(
            "return" => false,
            "extra_message" => ""
        );

        $options = $options + $default_options;

        $this->User = ClassRegistry::init("User");
        $this->Location = ClassRegistry::init("Location");
        $this->Message = ClassRegistry::init("Message");

        $recurring_payment = $this->find('first', array(
            'conditions' => array(
                'id' => $payment_id
            )
        ));

        if(!$recurring_payment){
            return false;
        }

        // Get Participant and Trainer
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $recurring_payment["RecurringPayment"]["user"]
            )
        ));

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $recurring_payment["RecurringPayment"]["to_user"]
            )
        ));

        if(!$participant || !$trainer){
            return false;
        }

        $message_array = array();

        if($options["extra_message"]){
            $message_array[] = $options["extra_message"];
            $message_array[] = "";
        }

        if($recurring_payment) {
            $message_array[] = "Recurring Payment Details";
            $message_array[] = "Amount: " . $recurring_payment["RecurringPayment"]["total"];
            $message_array[] = "Num Repeats: " . $recurring_payment["RecurringPayment"]["num_repeat"];
            $message_array[] = "Day: " . $recurring_payment["RecurringPayment"]["day"];
            $message_array[] = "Date Start: " . $recurring_payment["RecurringPayment"]["date_start"];
            $message_array[] = "Status: " . $recurring_payment["RecurringPayment"]["status"];
            $message_array[] = "Notes: " . $recurring_payment["RecurringPayment"]["notes"];
            $message_array[] = "";

        }

        $message = implode("\r\n", $message_array);

        // Save the message to the database
        $new_message = array(
            "from_user" => $trainer["User"]["id"],
            "to_user" => $participant["User"]["id"],
            "from_email" => $trainer["User"]["email"],
            "to_email" => $participant["User"]["email"],
            "subject" => "Musta - Recurring Payment Details",
            "message" => $message,
            "return" => $options["return"]
        );

        $this->Message->save($new_message);

        // Send the email
        $email = new CakeEmail();
        $email->from(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
        $email->to(array($participant["User"]["email"] => $participant["User"]["first_name"]));
        $email->subject("Musta - Recurring Payment Details");
        $email->send($message);

        if($options["return"]){
            // Send the email
            $email = new CakeEmail();
            $email->to(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
            $email->from(array($participant["User"]["email"] => $participant["User"]["first_name"]));
            $email->subject("Musta - Participants - Recurring Payment Details");
            $email->send($message);
        }

        return true;

    }

    // Add a notification about a payment, if unpaid it will link to the payment
    public function addNotification($payment_id){

        $this->User = ClassRegistry::init("User");
        $this->Notification = ClassRegistry::init("Notification");

        $recurring_payment = $this->find('first', array(
            'conditions' => array(
                'id' => $payment_id
            )
        ));

        if(!$recurring_payment){
            return false;
        }

        // Get Participant and Trainer
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $recurring_payment["RecurringPayment"]["user"]
            )
        ));

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $recurring_payment["RecurringPayment"]["to_user"]
            )
        ));

        if(!$participant || !$trainer){
            return false;
        }

        if($recurring_payment["RecurringPayment"]["status"] == "approved") {

            $notification = array(
                'user_id' => $participant["User"]["id"],
                'text' => 'You have successfully approved a recurring payment',
                'target' => 0,
                'type' => 'information'
            );

            $this->Notification->create();
            $this->Notification->save($notification);

            $notification = array(
                'user_id' => $trainer["User"]["id"],
                'text' => 'A user has made a payment',
                'target' => 0,
                'type' => 'information'
            );

            $this->Notification->create();
            $this->Notification->save($notification);

        } else if($recurring_payment["RecurringPayment"]["status"] == "pending") {

            $notification = array(
                'user_id' => $participant["User"]["id"],
                'text' => 'You have a pending payment for ' . $trainer["User"]["first_name"] . ' with message ' . $recurring_payment["RecurringPayment"]["notes"],
                'target' => $recurring_payment["RecurringPayment"]["id"],
                'type' => 'repeatPaymentRequest'
            );

            $this->Notification->create();
            $this->Notification->save($notification);
        }

        return true;

    }

}