<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 4/07/2016
 * Time: 3:28 PM
 */
App::uses('AppModel', 'Model');

// Status:
//        unpaid - default state for payments that have just been created but not actioned (may need to add expired after certain time)
//        pending - A payment has been setup (by trainer) but has not yet been sent to Integra System
//        complete - Payment has been submitted to Integra system successfully
//        failed - Have tried to submit payment and failed, Integra system does not allow multiple attempts to pay with same payment ID

class Payment extends AppModel {

    // Sends the confirmation emails for a booking
    public function sendEmails($payment_id, $options = array()){
        $default_options = array(
            "return" => false,
            "extra_message" => ""
        );

        $options = $options + $default_options;

        $this->User = ClassRegistry::init("User");
        $this->Location = ClassRegistry::init("Location");
        $this->Message = ClassRegistry::init("Message");

        $payment = $this->find('first', array(
            'conditions' => array(
                'id' => $payment_id
            )
        ));

        if(!$payment){
            return false;
        }

        // Get Participant and Trainer
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $payment["Payment"]["user"]
            )
        ));

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $payment["Payment"]["to_user"]
            )
        ));

        if(!$participant || !$trainer){
            return false;
        }

        $message_array = array();

        if($options["extra_message"]){
            $message_array[] = $options["extra_message"];
            $message_array[] = "";
        }

        if($payment) {
            $message_array[] = "Payment Details";
            $message_array[] = "Amount: " . $payment["Payment"]["total"];
            $message_array[] = "Date: " . $payment["Payment"]["datestamp"];
            $message_array[] = "Status: " . $payment["Payment"]["status"];
            $message_array[] = "";

        }

        $message = implode("\r\n", $message_array);

        // Save the message to the database
        $new_message = array(
            "from_user" => $trainer["User"]["id"],
            "to_user" => $participant["User"]["id"],
            "from_email" => $trainer["User"]["email"],
            "to_email" => $participant["User"]["email"],
            "subject" => "Musta - Payment Details",
            "message" => $message,
            "return" => $options["return"]
        );

        $this->Message->save($new_message);

        // Send the email
        $email = new CakeEmail();
        $email->from(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
        $email->to(array($participant["User"]["email"] => $participant["User"]["first_name"]));
        $email->subject("Musta - Payment Details");
        $email->send($message);

        if($options["return"]){
            // Send the email
            $email = new CakeEmail();
            $email->to(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
            $email->from(array($participant["User"]["email"] => $participant["User"]["first_name"]));
            $email->subject("Musta - Participants - Payment Details");
            $email->send($message);
        }

        return true;

    }

    // Add a notification about a payment, if unpaid it will link to the payment
    public function addNotification($payment_id){

        $this->User = ClassRegistry::init("User");
        $this->Notification = ClassRegistry::init("Notification");

        $payment = $this->find('first', array(
            'conditions' => array(
                'id' => $payment_id
            )
        ));

        if(!$payment){
            return false;
        }

        // Get Participant and Trainer
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $payment["Payment"]["user"]
            )
        ));

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $payment["Payment"]["to_user"]
            )
        ));

        if(!$participant || !$trainer){
            return false;
        }

        if($payment["Payment"]["status"] == "complete") {

            $notification = array(
                'user_id' => $participant["User"]["id"],
                'text' => 'You have successfully made a payment',
                'target' => 0,
                'type' => 'information'
            );

            $this->Notification->create();
            $this->Notification->save($notification);

            $notification = array(
                'user_id' => $trainer["User"]["id"],
                'text' => 'A user has made a payment',
                'target' => 0,
                'type' => 'information'
            );

            $this->Notification->create();
            $this->Notification->save($notification);

        } else if($payment["Payment"]["status"] == "pending") {

            $notification = array(
                'user_id' => $participant["User"]["id"],
                'text' => 'You have a pending payment for ' . $trainer["User"]["first_name"] . ' with message ' . $payment["Payment"]["notes"],
                'target' => $payment["Payment"]["id"],
                'type' => 'paymentRequest'
            );

            $this->Notification->create();
            $this->Notification->save($notification);
        }

        return true;

    }

}