<?php
App::uses('AppModel', 'Model');

class Location extends AppModel {

public $hasAndBelongsToMany = array(
        'Addresses' =>
            array(
                'className' => 'Locationdb',
                'joinTable' => 'locations_links',
                'foreignKey' => 'locations_id',
                'associationForeignKey' => 'locationdbs_id'
            )
    );

    public $hasMany = array(
        'Deals' =>
            array(
                'className' => 'LocationDeals',
                'foreignKey' => 'location'
            )
    );
}