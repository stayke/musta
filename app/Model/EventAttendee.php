<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 1/07/2016
 * Time: 10:40 AM
 */
App::uses('AppModel', 'Model');

class EventAttendee extends AppModel
{
    // Sends the confirmation emails for an attendance
    public function sendEmails($attendance_id, $options = array()){
        $default_options = array(
            "return" => false,
            "extra_message" => ""
        );

        $options = $options + $default_options;

        $this->User = ClassRegistry::init("User");
        $this->Payment = ClassRegistry::init("Payment");
        $this->CalendarEventInstance = ClassRegistry::init("CalendarEventInstance");
        $this->Location = ClassRegistry::init("Location");
        $this->Message = ClassRegistry::init("Message");

        $attendance = $this->find('first', array(
            'conditions' => array(
                'id' => $attendance_id
            )
        ));

        if(!$attendance){
            return false;
        }

        $event_instance = $this->CalendarEventInstance->find('first', array(
            'conditions' => array(
                'id' => $attendance["EventAttendee"]["event_instance"]
            )
        ));

        if(!$event_instance){
            return false;
        }

        // Get Participant and Trainer
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $attendance["EventAttendee"]["user"]
            )
        ));

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $attendance["EventAttendee"]["to_user"]
            )
        ));

        if(!$participant || !$trainer){
            return false;
        }

        $payment = $this->Payment->find('first', array(
            'conditions' => array(
                'id' => $attendance["EventAttendee"]["payment_id"]
            )
        ));

        $location = $this->Location->find('first', array(
            'conditions' => array(
                'id' => $event_instance["CalendarEventInstance"]["location"]
            )
        ));

        $message_array = array();

//        $message_array[] = "Booking Confirmation";
//        $message_array[] = "";

        if($options["extra_message"]){
            $message_array[] = $options["extra_message"];
            $message_array[] = "";
        }

        $message_array[] = "Booking Details";
        $message_array[] = "Date: " . $event_instance["CalendarEventInstance"]["date"];
        $message_array[] = "Time: " . $event_instance["CalendarEventInstance"]["time_start"];
        $message_array[] = "Duration: " . $event_instance["CalendarEventInstance"]["duration"];
        $message_array[] = "Status: " . $attendance["EventAttendee"]["status"];
        $message_array[] = "Trainers Notes: " . $event_instance["CalendarEventInstance"]["notes"];
        $message_array[] = "Trainer Phone: " . $trainer["User"]["phone"];
        $message_array[] = "";


        $message_array[] = "Location Details";
        $message_array[] = "Name: " . $location["Location"]["name"];
        $message_array[] = "Notes: " . $location["Location"]["notes"];
        if($location["Addresses"] && $location["Location"]["type"] != "mobile") {
            $message_array[] = "Address: " . $location["Addresses"][0]["address"];
        }
        $message_array[] = "";


        if($payment) {
            $message_array[] = "Payment Details";
            $message_array[] = "Amount: " . $payment["Payment"]["total"];
            $message_array[] = "Date: " . $payment["Payment"]["datestamp"];
            $message_array[] = "Status: " . $payment["Payment"]["status"];
            $message_array[] = "";

        }

        if($attendance["EventAttendee"]["registration"]) {
            $registration = $this->EventRegistration->find('first', array(
                'conditions' => array(
                    'id' => $event_instance["CalendarEventInstance"]["registration"]
                )
            ));
            if($registration){
                $message_array[] = "Registration Details";
                $message_array[] = "Date Start: " . $registration["EventRegistration"]["date_start"];
                $message_array[] = "Date End: " . $registration["EventRegistration"]["date_end"];
                $message_array[] = "Day: " . $registration["EventRegistration"]["day"];
                $message_array[] = "Num Sessions: " . $registration["EventRegistration"]["num_sessions"];
                $message_array[] = "";
            }
        }

        $message = implode("\r\n", $message_array);

        // Save the message to the database
        $new_message = array(
            "from_user" => $trainer["User"]["id"],
            "to_user" => $participant["User"]["id"],
            "from_email" => $trainer["User"]["email"],
            "to_email" => $participant["User"]["email"],
            "subject" => "Musta - Event Details",
            "message" => $message,
            "return" => $options["return"]
        );

        $this->Message->save($new_message);

        // Send the email
        $email = new CakeEmail();
        $email->from(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
        $email->to(array($participant["User"]["email"] => $participant["User"]["first_name"]));
        $email->subject("Musta - Event Details");
        $email->send($message);

        if($options["return"]){
            // Send the email
            $email = new CakeEmail();
            $email->to(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
            $email->from(array($participant["User"]["email"] => $participant["User"]["first_name"]));
            $email->subject("Musta - Participants - Event Details");
            $email->send($message);
        }

        return true;

    }

    public function addNotification($attendance_id){

        $this->User = ClassRegistry::init("User");
        $this->Notification = ClassRegistry::init("Notification");

        $attendance = $this->find('first', array(
            'conditions' => array(
                'id' => $attendance_id
            )
        ));

        if(!$attendance){
            return false;
        }

        // Get Participant and Trainer
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $attendance["EventAttendee"]["user"]
            )
        ));

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $attendance["EventAttendee"]["to_user"]
            )
        ));

        if(!$participant || !$trainer){
            return false;
        }

        if($attendance["EventAttendee"]["status"] == "complete") {
            $notification = array(
                'user_id' => $participant["User"]["id"],
                'text' => 'You have successfully paid for an event session',
                'target' => 0,
                'type' => 'information'
            );

            $this->Notification->create();
            $this->Notification->save($notification);

            $notification = array(
                'user_id' => $trainer["User"]["id"],
                'text' => 'A user has purchased an event session',
                'target' => 0,
                'type' => 'information'
            );

            $this->Notification->create();
            $this->Notification->save($notification);
        }

        return true;

    }

}