<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 1/07/2016
 * Time: 12:06 PM
 */
App::uses('AppModel', 'Model');

class EventRegistration extends AppModel
{
    // Sends the confirmation emails for an attendance
    public function sendEmails($registration_id, $options = array())
    {
        $default_options = array(
            "return" => false,
            "extra_message" => ""
        );

        $options = $options + $default_options;

        $this->User = ClassRegistry::init("User");
        $this->Payment = ClassRegistry::init("Payment");
        $this->RecurringPayment = ClassRegistry::init("RecurringPayment");
        $this->CalendarEvent = ClassRegistry::init("CalendarEvent");
        $this->Location = ClassRegistry::init("Location");
        $this->Message = ClassRegistry::init("Message");

        $registration = $this->find('first', array(
            'conditions' => array(
                'id' => $registration_id
            )
        ));

        if (!$registration) {
            return false;
        }

        $event = $this->CalendarEvent->find('first', array(
            'conditions' => array(
                'id' => $registration["EventRegistration"]["event"]
            )
        ));

        if (!$event) {
            return false;
        }

        // Get Participant and Trainer
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $registration["EventRegistration"]["user"]
            )
        ));

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $registration["EventRegistration"]["to_user"]
            )
        ));

        if (!$participant || !$trainer) {
            return false;
        }

        $location = $this->Location->find('first', array(
            'conditions' => array(
                'id' => $event["CalendarEvent"]["location"]
            )
        ));

        $message_array = array();

//        $message_array[] = "Booking Confirmation";
//        $message_array[] = "";

        if ($options["extra_message"]) {
            $message_array[] = $options["extra_message"];
            $message_array[] = "";
        }

        $message_array[] = "Registration Details";
        $message_array[] = "Date Start: " . $registration["EventRegistration"]["date_start"];
        $message_array[] = "Date End: " . $registration["EventRegistration"]["date_end"];
        $message_array[] = "Day: " . $registration["EventRegistration"]["day"];
        $message_array[] = "Num Sessions: " . $registration["EventRegistration"]["num_sessions"];
        $message_array[] = "";

        $message_array[] = "Event Details";
        $message_array[] = "Title: " . $event["CalendarEvent"]["title"];
        $message_array[] = "Date Start: " . $event["CalendarEvent"]["date_start"];
        $message_array[] = "Date End: " . $event["CalendarEvent"]["date_end"];
        $message_array[] = "Time Start: " . $event["CalendarEvent"]["time_start"];
        $message_array[] = "Duration: " . $event["CalendarEvent"]["duration"];
        $message_array[] = "Status: " . $registration["EventRegistration"]["status"];
        $message_array[] = "Trainers Notes: " . $event["CalendarEvent"]["notes"];
        $message_array[] = "Trainer Phone: " . $trainer["User"]["phone"];
        $message_array[] = "";

        $message_array[] = "Location Details";
        $message_array[] = "Name: " . $location["Location"]["name"];
        $message_array[] = "Notes: " . $location["Location"]["notes"];
        if ($location["Addresses"] && $location["Location"]["type"] != "mobile") {
            $message_array[] = "Address: " . $location["Addresses"][0]["address"];
        }
        $message_array[] = "";

        if ($registration["EventRegistration"]["payment"]) {
            $payment = $this->Payment->find('first', array(
                'conditions' => array(
                    'id' => $registration["EventRegistration"]["payment"]
                )
            ));

            if ($payment) {
                $message_array[] = "Payment Details";
                $message_array[] = "Amount: " . $payment["Payment"]["total"];
                $message_array[] = "Date: " . $payment["Payment"]["datestamp"];
                $message_array[] = "Status: " . $payment["Payment"]["status"];
                $message_array[] = "";
            }
        }

        if ($registration["EventRegistration"]["rec_payment"]) {
            $rec_payment = $this->RecurringPayment->find('first', array(
                'conditions' => array(
                    'id' => $registration["EventRegistration"]["rec_payment"]
                )
            ));

            if ($rec_payment) {
                $message_array[] = "Recurring Payment Details";
                $message_array[] = "Amount: " . $rec_payment["RecurringPayment"]["total"];
                $message_array[] = "Date Start: " . $rec_payment["RecurringPayment"]["date_start"];
                $message_array[] = "Date End: " . $rec_payment["RecurringPayment"]["date_end"];
                $message_array[] = "Day: " . $rec_payment["RecurringPayment"]["day"];
                $message_array[] = "Frequency: " . $rec_payment["RecurringPayment"]["frequency"];
                $message_array[] = "Number Times: " . $rec_payment["RecurringPayment"]["num_repeat"];
                $message_array[] = "Status: " . $rec_payment["RecurringPayment"]["status"];
                $message_array[] = "";
            }
        }

        $message = implode("\r\n", $message_array);

        // Save the message to the database
        $new_message = array(
            "from_user" => $trainer["User"]["id"],
            "to_user" => $participant["User"]["id"],
            "from_email" => $trainer["User"]["email"],
            "to_email" => $participant["User"]["email"],
            "subject" => "Musta - Registration Details",
            "message" => $message,
            "return" => $options["return"]
        );

        $this->Message->save($new_message);

        // Send the email
        $email = new CakeEmail();
        $email->from(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
        $email->to(array($participant["User"]["email"] => $participant["User"]["first_name"]));
        $email->subject("Musta - Registration Details");
        $email->send($message);

        if($options["return"]){
            // Send the email
            $email = new CakeEmail();
            $email->to(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
            $email->from(array($participant["User"]["email"] => $participant["User"]["first_name"]));
            $email->subject("Musta - Participants - Registration Details");
            $email->send($message);
        }

        return true;

    }

    public function addNotification($registration_id){

        $this->User = ClassRegistry::init("User");
        $this->Notification = ClassRegistry::init("Notification");

        $registration = $this->find('first', array(
            'conditions' => array(
                'id' => $registration_id
            )
        ));

        if(!$registration){
            return false;
        }

        // Get Participant and Trainer
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $registration["EventRegistration"]["user"]
            )
        ));

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $registration["EventRegistration"]["to_user"]
            )
        ));

        if(!$participant || !$trainer){
            return false;
        }

        $notification = array(
            'user_id' => $participant["User"]["id"],
            'text' => 'You have successfully created a new registration',
            'target' => 0,
            'type' => 'information'
        );

        $this->Notification->create();
        $this->Notification->save($notification);

        $notification = array(
            'user_id' => $trainer["User"]["id"],
            'text' => 'A user has signed up for a registration',
            'target' => 0,
            'type' => 'information'
        );

        $this->Notification->create();
        $this->Notification->save($notification);

        return true;

    }

}