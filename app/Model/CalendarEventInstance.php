<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 17/08/2016
 * Time: 2:47 PM
 */
App::uses('AppModel', 'Model');

class CalendarEventInstance extends AppModel
{
    public function bindPrices()
    {
        $this->bindModel(array(
            "hasOne" => array(
                "Pricing" => array(
                    "className" => "BcPricing",
                    "foreignKey" => false,
                    "conditions" => array("Pricing.id = CalendarEventInstance.pricing")
                )
            )
        ));
    }
}