<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 27/06/2016
 * Time: 10:51 AM
 */
App::uses('AppModel', 'Model');

// Status:
//        unpaid - Temporary state while waiting for payment, if no payment is made this will not block time
//        complete - Has been paid for or otherwise approved

class PtBooking extends AppModel {

//    public $belongsTo = array(
//        'Trainer' => array(
//            'className' => 'User',
//            'foreignKey' => 'to_user'
//        ),
//        'Participant' => array(
//            'className' => 'User',
//            'foreignKey' => 'user'
//        ),
//        'Location' => array(
//            'className' => 'Location',
//            'foreignKey' => 'location'
//        )
//    );

    public function getUserInfo(){
        $this->bindModel(
            array(
                'belongsTo' => array(
                    'Trainer' => array(
                        'className' => 'User',
                        'foreignKey' => 'to_user'
                    ),
                    'Participant' => array(
                        'className' => 'User',
                        'foreignKey' => 'user'
                    )
                )
            )
        );
    }

//    public $hasAndBelongsToMany = array(
//        'Addresses' =>
//            array(
//                'className' => 'Locationdb',
//                'joinTable' => 'locations_links',
//                'foreignKey' => 'locations_id',
//                'associationForeignKey' => 'locationdbs_id'
//            )
//    );

    // Sends the confirmation emails for a booking
    public function sendEmails($booking_id, $options = array()){
        $default_options = array(
            "return" => false,
            "extra_message" => ""
        );

        $options = $options + $default_options;

        $this->User = ClassRegistry::init("User");
        $this->Payment = ClassRegistry::init("Payment");
        $this->Location = ClassRegistry::init("Location");
        $this->Message = ClassRegistry::init("Message");

        $booking = $this->find('first', array(
            'conditions' => array(
                'id' => $booking_id
            )
        ));

        if(!$booking){
            return false;
        }

        // Get Participant and Trainer
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $booking["PtBooking"]["user"]
            )
        ));

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $booking["PtBooking"]["to_user"]
            )
        ));

        if(!$participant || !$trainer){
            return false;
        }

        $payment = $this->Payment->find('first', array(
            'conditions' => array(
                'id' => $booking["PtBooking"]["payment_id"]
            )
        ));

        $location = $this->Location->find('first', array(
            'conditions' => array(
                'id' => $booking["PtBooking"]["location"]
            )
        ));

        $message_array = array();

//        $message_array[] = "Booking Confirmation";
//        $message_array[] = "";

        if($options["extra_message"]){
            $message_array[] = $options["extra_message"];
            $message_array[] = "";
        }

        $message_array[] = "Booking Details";
        $message_array[] = "Time: " . $booking["PtBooking"]["time"];
        $message_array[] = "Duration: " . $booking["PtBooking"]["length"];
        $message_array[] = "Status: " . $booking["PtBooking"]["pay_status"];
        $message_array[] = "Participant Notes: " . $booking["PtBooking"]["participant_notes"];
        $message_array[] = "Trainers Notes: " . $booking["PtBooking"]["session_notes"];
        $message_array[] = "Trainer Phone: " . $trainer["User"]["phone"];
        $message_array[] = "";


        $message_array[] = "Location Details";
        $message_array[] = "Name: " . $location["Location"]["name"];
        $message_array[] = "Notes: " . $location["Location"]["notes"];
        if($location["Addresses"] && $location["Location"]["type"] != "mobile") {
            $message_array[] = "Address: " . $location["Addresses"][0]["address"];
        }
        $message_array[] = "";


        if($payment) {
            $message_array[] = "Payment Details";
            $message_array[] = "Amount: " . $payment["Payment"]["total"];
            $message_array[] = "Date: " . $payment["Payment"]["datestamp"];
            $message_array[] = "Status: " . $payment["Payment"]["status"];
            $message_array[] = "";

        }

        $message = implode("\r\n", $message_array);

        // Save the message to the database
        $new_message = array(
            "from_user" => $trainer["User"]["id"],
            "to_user" => $participant["User"]["id"],
            "from_email" => $trainer["User"]["email"],
            "to_email" => $participant["User"]["email"],
            "subject" => "Musta - Booking Details",
            "message" => $message,
            "return" => $options["return"]
        );

        $this->Message->save($new_message);

        // Send the email
        $email = new CakeEmail();
        $email->from(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
        $email->to(array($participant["User"]["email"] => $participant["User"]["first_name"]));
        $email->subject("Musta - Booking Details");
        $email->send($message);

        if($options["return"]){
            // Send the email
            $email = new CakeEmail();
            $email->to(array($trainer["User"]["email"] => $trainer["User"]["first_name"]));
            $email->from(array($participant["User"]["email"] => $participant["User"]["first_name"]));
            $email->subject("Musta Participants - Booking Details");
            $email->send($message);
        }

        return true;

    }

    public function addNotification($booking_id){

        $this->User = ClassRegistry::init("User");
        $this->Notification = ClassRegistry::init("Notification");

        $booking = $this->find('first', array(
            'conditions' => array(
                'id' => $booking_id
            )
        ));

        if(!$booking){
            return false;
        }

        // Get Participant and Trainer
        $participant = $this->User->find('first', array(
            'conditions' => array(
                'id' => $booking["PtBooking"]["user"]
            )
        ));

        $trainer = $this->User->find('first', array(
            'conditions' => array(
                'id' => $booking["PtBooking"]["to_user"]
            )
        ));

        if(!$participant || !$trainer){
            return false;
        }
        if($booking["PtBooking"]["pay_status"] == "complete") {

            $notification = array(
                'user_id' => $participant["User"]["id"],
                'text' => 'You have successfully paid for a booking',
                'target' => 0,
                'type' => 'information'
            );

            $this->Notification->create();
            $this->Notification->save($notification);

            $notification = array(
                'user_id' => $trainer["User"]["id"],
                'text' => 'A user has purchased a session',
                'target' => 0,
                'type' => 'information'
            );

            $this->Notification->create();
            $this->Notification->save($notification);
        }

        return true;

    }
}