<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

// Variable options

// Status:
//        disabled - user has been disabled by admin, cannot login
//        unregistered - user has been created by trainer/admin but needs to go through registration process to enter details
//        approved - Trainer has approved by Musta/Integra and can receive payments etc.
//        blank/null - default participant enabled status

class User extends AppModel
{
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A username is required'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A password is required'
            )
        )
    );

    public $hasMany = array(
        'Userinfo',
//        'BookingsMade' => array(
//            'className' => 'PtBooking',
//            'foreignKey' => 'user'
//        ),
//        'BookingsTo' => array(
//            'className' => 'PtBooking',
//            'foreignKey' => 'to_user'
//        )
    );


    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    // Sorts the meta values into a keyed array
    //     pass the Userinfo you get from a database call
    public function sortMeta($user_info){
        $sorted_meta = array();
        foreach($user_info as $ui){
            // If never before seen meta add to sorted meta
            if(!isset($sorted_meta[$ui["meta_key"]])){
                $sorted_meta[$ui["meta_key"]] = $ui["meta_value"];
            // If multiple of this type of meta info already exist add to array
            } else if(is_array($sorted_meta[$ui["meta_key"]])){
                $sorted_meta[$ui["meta_key"]][] = $ui["meta_value"];
            // if this is second type of this meta convert to array and add both
            } else {
                $sorted_meta[$ui["meta_key"]] = array($sorted_meta[$ui["meta_key"]], $ui["meta_value"]);
            }
        }
        return $sorted_meta;
    }

    public function addCreateNotification($user_id)
    {
        $this->Notification = ClassRegistry::init("Notification");

        $user = $this->find('first', array(
            'conditions' => array(
                'id' => $user_id
            )
        ));

        if(!$user){
            return false;
        }

        if($user["User"]["role"] == 3) { // participant create notification
            $notification = array(
                'user_id' => $user["User"]["id"],
                'text' => 'Please complete your PARQ & Goals for your trainer to see before your training session',
                'target' => 0,
                'type' => 'information'
            );

            $this->Notification->create();
            $this->Notification->save($notification);
        } else if($user["User"]["role"] == 1) { // participant create notification
            $notification = array(
                'user_id' => $user["User"]["id"],
                'text' => 'Please submit your bank details',
                'target' => 0,
                'type' => 'information'
            );

            $this->Notification->create();
            $this->Notification->save($notification);
        }

    }
}