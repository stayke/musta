<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 6/06/2016
 * Time: 4:51 PM
 */
App::uses('AppModel', 'Model');

class CalendarEvent extends AppModel {

//    public $hasOne = array(
//      "Pricing" => array(
//          "className" => "BcPricing",
//          "foreignKey" => false,
//          "conditions" => array("Pricing.id" => "CalendarEvent.pricing")
//      )
//    );

    public function bindPrices(){
        $this->bindModel( array(
            "hasOne" => array(
              "Pricing" => array(
                  "className" => "BcPricing",
                  "foreignKey" => false,
                  "conditions" => array("Pricing.id = CalendarEvent.pricing")
              )
            )
        ) );
    }

    // Get all instances between two date ranges (usually week/month/day as called from calendar)
    public function getInstances($start_date, $end_date, $event_id, $status = 2, $user_id = false){
        $this->CalendarEventInstance = ClassRegistry::init("CalendarEventInstance");

        $event_rule = $this->find('first', array(
            'conditions' => array(
                'CalendarEvent.id' => $event_id
            )
        ));

        if(!$event_rule && $event_id != 0){
            return false;
        }

        // If based off a rule check for instancing
        if($event_id != 0) {

            $instanced_date = $event_rule["CalendarEvent"]["instanced_up_to"];

            // Only create instances that would be current/future (current = this week)
            $cur_date = date("Y-m-d", strtotime("-1 week"));

            $dates = array($cur_date, $instanced_date);

            $event_start_date = $event_rule["CalendarEvent"]["date_start"];

            if($event_start_date){
                $dates[] = $event_start_date;
            }

            $upto_date = max($dates);

            $end_dates = array($end_date);

            $last_date = $event_rule["CalendarEvent"]["date_end"];

            if($last_date){
                $end_dates[] = $last_date;
            }

            $final_date = min($end_dates);

            // Check if we have created the instances for the dates requested
            if ($upto_date > $end_date) {
                // already got em
            } else {
                // will create them now
                if (!$this->createInstances($upto_date, $final_date, $event_rule)) {
                    // creating instances failed
                    return false;
                }
            }
        }

        // Base options to always get
        $options = array(
            'conditions' => array(
                'CalendarEventInstance.status' => $status,
                'date >=' => $start_date,
                'date <=' => $end_date,
                'CalendarEventInstance.event' => $event_id
            )
        );

        if($user_id){
            $options["conditions"]["CalendarEventInstance.user"] = $user_id;
        }

        // Extra options to get the count of people attending (may make optional)
        $options['fields'] = array(
            'CalendarEventInstance.*',
            'Pricing.*',
            'count(EventAttendee.id) AS attendance_count'
        );

        $options["group"] = array(
            'CalendarEventInstance.id'
        );

        $options["joins"] = array(
            array(
                'table' => 'event_attendees',
                'alias' => 'EventAttendee',
                'type' => 'Left',
                'conditions' => array(
                    'CalendarEventInstance.id = EventAttendee.event_instance',
                    'EventAttendee.status = "complete"'
                )
            )
        );

        $this->CalendarEventInstance->bindPrices();
        $event_instances = $this->CalendarEventInstance->find('all', $options);

        return $event_instances;
    }

    // NDJ - would be nice to call this via cron instead of on view calendar
    // Create all instances between the two asked for dates (can be a wide date range)
    // $days = array of days to create instances on if you are only creating instances for certain days
    private function createInstances($start_date, $end_date, $event_rule, $days = false ){
        $this->CalendarEventInstance = ClassRegistry::init("CalendarEventInstance");
        $this->EventRegistration = ClassRegistry::init("EventRegistration");
        $this->EventAttendee = ClassRegistry::init("EventAttendee");

        $registrations = $this->EventRegistration->find('all', array(
            'conditions' => array(
                'event' => $event_rule["CalendarEvent"]["id"],
                'date_end >= ' => $start_date
            )
        ));

        // Do some processing on the rule first
        $days = explode(",", $event_rule["CalendarEvent"]["day"]);

        $instances_to_save = array();

        $days_to_add = -1;
        while($days_to_add < 1000){
            $days_to_add++;

            $cur_time = strtotime($start_date . " +" . $days_to_add . " days");

            $cur_date = date("Y-m-d", $cur_time);
            if($cur_date > $end_date){
                break;
            }

            $cur_day = date("l", $cur_time);
            if(!in_array($cur_day, $days)){
                continue;
            }

            if($days){
                if(in_array($cur_day, $days)){

                }
            }

            $new_event_instance = array(
                "id" => NULL,
                "event" => $event_rule["CalendarEvent"]["id"],
                "location" => $event_rule["CalendarEvent"]["location"],
                "pricing" => $event_rule["CalendarEvent"]["pricing"],
                "bookable" => $event_rule["CalendarEvent"]["bookable"],
                "spaces" => $event_rule["CalendarEvent"]["spaces"],
                "time_start" => $event_rule["CalendarEvent"]["time_start"],
                "date" => $cur_date,
                "status" => $event_rule["CalendarEvent"]["status"],
                "original_day" => $cur_day,
                "duration" => $event_rule["CalendarEvent"]["duration"],
                "price" => $event_rule["CalendarEvent"]["price"],
                "user" => $event_rule["CalendarEvent"]["user"],
                "title" => $event_rule["CalendarEvent"]["title"],
                "colour" => $event_rule["CalendarEvent"]["colour"],
                "location_db" => $event_rule["CalendarEvent"]["location_db"],
                "type" => $event_rule["CalendarEvent"]["type"],
                "notes" => $event_rule["CalendarEvent"]["notes"]
            );

            $saved_instance = $this->CalendarEventInstance->save($new_event_instance);

            // just brute force our way through registrations, couldn't find better way to do this than this.
            foreach($registrations as $key => $registration){
                if($registration["EventRegistration"]["day"] == $cur_day){
                    if($registration["EventRegistration"]["date_end"] <= $cur_date){
                        unset($registrations[$key]);
                    } else {
                        $new_event_attendance = array(
                            "id" => NULL,
                            "event" => $event_rule["CalendarEvent"]["id"],
                            "event_instance" => $saved_instance["CalendarEventInstance"]["id"],
                            "user" => $registration["EventRegistration"]["user"],
                            "to_user" => $registration["EventRegistration"]["to_user"],
                            "status" => $registration["EventRegistration"]["status"],
                            "registration" => $registration["EventRegistration"]["id"]
                        );
                        $saved_attendance = $this->EventAttendee->save($new_event_attendance);
                    }
                }
            }

//            $instances_to_save[] = $new_event_instance;

        }

        // NDJ - this was more effecient but has been disabled so we can just brute force add event attendances
        // $this->CalendarEventInstance->saveMany($instances_to_save);

        $new_upto_date = date("Y-m-d", strtotime($start_date . " +" . $days_to_add . " days"));

        $updated_event = array(
            "id" => $event_rule["CalendarEvent"]["id"],
            "instanced_up_to" => $new_upto_date
        );

        $this->save($updated_event);

        // was looping for to long and stopped to prevent infinite loops/timeouts
        if($days_to_add >= 1000){
            return false;
        }

        return true;

    }

    public function updateInstances($event_id, $start_date, $original_event){
        $this->CalendarEventInstance = ClassRegistry::init("CalendarEventInstance");

        $response = array();

        $event = $this->find("first", array(
            "conditions" => array(
                "CalendarEvent.id" => $event_id
            )
        ));

        if(!$event){
            return false;
        }

        // First update basic data
        $fields = array(
            'location' => $event["CalendarEvent"]["location"],
            'pricing' => $event["CalendarEvent"]["pricing"],
            'time_start' => "'" . $event["CalendarEvent"]["time_start"] . "'",
            'duration' => $event["CalendarEvent"]["duration"],
            'price' => "'" . $event["CalendarEvent"]["price"] . "'",
            'bookable' => "'" . $event["CalendarEvent"]["bookable"] . "'",
            'spaces' => "'" . $event["CalendarEvent"]["spaces"] . "'",
            'colour' => "'" . $event["CalendarEvent"]["colour"] . "'",
            'title' => "'" . $event["CalendarEvent"]["title"] . "'",
        );

        // if status is not active update all future events (lets cancelled events stay cancelled if you are just saving other data)
        if($event["CalendarEvent"]["status"] != 2){
            $fields["status"] = $event["CalendarEvent"]["status"];
        }

        $conditions = array(
            "date >= " => date("Y-m-d", strtotime($start_date)),
            "event" => $event_id
        );

//        return print_r($fields, true) . print_r($conditions, true);

        $response["instances updated"] = $this->CalendarEventInstance->updateAll($fields, $conditions);

//        return $response;

        // if no original check do no further checks
        if(!$original_event){
            return true;
        }

        // Now check if days have changed
        $original_days = explode(",", $original_event["CalendarEvent"]["day"]);
        $new_days = explode(",", $event["CalendarEvent"]["day"]);

        $matching_days = array_intersect($original_days, $new_days);
        $removed_days = array_diff($original_days, $matching_days);
        $added_days = array_diff($new_days, $matching_days);

        // no days added or removed
        if(count($removed_days) == 0 && count($added_days) == 0){
            return true;
        }

        // if only 1 day added and 1 removed
        if(count($removed_days) == 1 && count($added_days) == 1){

            $old_day_number = date("N", strtotime($removed_days[0]));
            $new_day_number = date("N", strtotime($added_days[0]));

            $days_to_add = $old_day_number - $new_day_number;

            $instances = $this->CalendarEventInstance->find('all', array(
                'conditions' => array(
                    'event' => $event_id,
                    'original_day' => $removed_days[0]
                )
            ));
            $instance_updates = array();
            foreach($instances as $instance){
                $new_date = false;
                if($days_to_add > 0) {
                    $new_date = date("l", strtotime($instance["CalendarEventInstance"]["date"] . " +" . $days_to_add . " days"));
                } else {
                    $new_date = date("l", strtotime($instance["CalendarEventInstance"]["date"] . " -" . abs($days_to_add) . " days"));
                }
                $updated_instance = array(
                    "id" => $instance["CalendarEventInstance"]["id"],
                    "date" => $new_date,
                    "original_day" => $added_days[0]
                );
                $instance_updates[] = $updated_instance;
            }
            $this->CalendarEventInstance->saveMany($instance_updates);

            return true;
        }

        // if not either of the above situations delete all old days and add all new days
        if(count($removed_days)){
            foreach($removed_days as $day) {
                $this->CalendarEventInstance->deleteAll(array(
                    'event' => $event_id,
                    'original_day' => $day
                ));
            }
        }

        if(count($added_days)) {
            $this->createInstances($start_date, $event["CalendarEvent"]["instanced_up_to"], $event, $added_days);
        }
    }

    public function attend_event($event_id, $registration_id){
        $this->CalendarEvent = ClassRegistry::init("CalendarEvent");
        $this->CalendarEventInstance = ClassRegistry::init("CalendarEventInstance");
        $this->EventRegistration = ClassRegistry::init("EventRegistration");
        $this->EventAttendee = ClassRegistry::init("EventAttendee");

        $event = $this->CalendarEvent->find('first', array(
            'conditions' => array(
                'CalendarEvent.id' => $event_id
            )
        ));

        $registration = $this->EventRegistration->find('first', array(
            'conditions' => array(
                'EventRegistration.id' => $registration_id
            )
        ));

        $currently_attending = $this->EventAttendee->find('all', array(
            'conditions' => array(
                'registration' => $registration_id
            )
        ));

        // Find instances you are already attending
        $already_attending_instances = array();

        foreach($currently_attending as $cur_att){
            $already_attending_instances[$cur_att["EventAttendee"]["event_instance"]] = true;
        }

        // Find all instances for this event
        $day = date("l", strtotime($registration["EventRegistration"]["date_start"]));

        $instances = $this->CalendarEventInstance->find('all', array(
            'conditions' => array(
                'event' => $event_id,
                "status" => 2,
                "original_day" => $day,
                "date >=" => $registration["EventRegistration"]["date_start"]
            )
        ));

        $num_sessions = $registration["EventRegistration"]["num_sessions"];

        $attendances_to_add = array();
        $attending_sessions = 0;
        foreach($instances as $instance){

            $new_attendance = array(
                "event" => $event_id,
                "event_instance" => $instance["CalendarEventInstance"]["id"],
                "user" => $registration["EventRegistration"]["user"],
                "to_user" => $registration["EventRegistration"]["to_user"],
                "payment_id" => $registration["EventRegistration"]["payment"],
                "status" => $registration["EventRegistration"]["status"],
                "registration" => $registration["EventRegistration"]["id"]
            );

            $attendances_to_add[] = $new_attendance;

            $attending_sessions++;
            if($attending_sessions >= $num_sessions){
                break;
            }

        }

        $this->EventAttendee->saveMany($attendances_to_add);

    }

}