<h1 class="heading"><div class="container"><i class="fa fa-calendar" aria-hidden="true"></i><strong>My Bookings</strong></div></h1>
<div class="container" ng-controller="mybookings">
<?php if($error_messages){ ?>
    <div class="error_messages">
    <?php foreach($error_messages as $error_msg){ ?>
        <div class="error_msg">
            <?php echo $error_msg; ?>
        </div>
    <?php } ?>
    </div>
<?php } ?>

<?php if($bookings){ ?>
		<div class="col-xs-12 col-sm-6">
	    <h2>Bookings</h2>
	    <div class="bookings">
	    <?php foreach($bookings as $booking){ ?>
	        <div class="well">
                <?php if($userview == "trainer"){ ?>
                <a class="btn btn-secondary pull-right" ng-click="parq(<?php echo $booking["Participant"]["id"]; ?>);">View ParQ</a>
                <?php } ?>
	            <div class="field-name">Time: </div><?php echo $booking["PtBooking"]["time"]; ?><br/>
	            <div class="field-name">Duration: </div><?php echo $booking["PtBooking"]["length"]; ?><br/>
	            <div class="field-name">Status: </div><?php echo $booking["PtBooking"]["pay_status"]; ?><br/>
	            <div class="field-name">Participant: </div><?php echo $booking["Participant"]["first_name"]; ?><br/>
	            <div class="field-name">Trainer: </div><?php echo $booking["Trainer"]["first_name"]; ?>
	            <?php if($booking["Location"]){ ?>
	                <br/>
	                <div class="field-name">Location: </div><?php echo $booking["Location"]["Location"]["name"]; ?><br/>
	                <div class="field-name">Address: </div><?php echo $booking["Location"]["Addresses"][0]["address"]; ?>
	            <?php } ?>
	            <?php
	//            echo "<pre>";
	//            print_r($booking);
	//            echo "</pre>";
	            ?>
	        </div>
	    <?php } ?>
	    </div>
    </div>
<?php } ?>

<?php if($events){ ?>
		<div class="col-xs-12 col-sm-6">
	    <h2>Events</h2>
	    <div class="events">
	        <?php foreach($events as $event){ ?>
	            <div class="well">
	            	<div class="field-name"><h3><?php echo $event["CalendarEventInstance"]["title"]; ?></h3></div><br/>
	                <div class="field-name">Time: <?php echo $event["CalendarEventInstance"]["time_start"]; ?></div><br/>
	                <div class="field-name">Duration: <?php echo $event["CalendarEventInstance"]["duration"]; ?></div><br/>
	                <div class="field-name">Date: <?php echo $event["CalendarEventInstance"]["date"]; ?></div><br/>
	                <div class="field-name">Original Day: <?php echo $event["CalendarEventInstance"]["original_day"]; ?></div>
	                <?php if($event["Location"]){ ?>
	                    <br/>
	                    <div class="field-name">Location: <?php echo $event["Location"]["Location"]["name"]; ?></div><br/>
	                    <div class="field-name">Address: <?php echo $event["Location"]["Addresses"][0]["address"]; ?></div>
	                <?php } ?>
	                <a class="btn btn-primary pull-right" ng-click="editEvent({id: <?php echo $event["CalendarEventInstance"]["id"]; ?>});">Edit Event</a>
	                <?php
	                //            echo "<pre>";
	                //            print_r($event);
	                //            echo "</pre>";
	                ?>
	            </div>
	        <?php } ?>
	    </div>
    </div>
<?php } ?>

<?php if($event_attendances){ ?>
		<div class="col-xs-12 col-sm-6">
	    <h2>Events Attending</h2>
	    <div class="events-attending">
	        <?php foreach($event_attendances as $event_attendance){ ?>
	            <div class="well">
	                <!--<div>id: <?php echo $event_attendance["EventAttendee"]["id"]; ?></div>-->
	                <div class="field-name">Time: </div><?php echo $event_attendance["CalendarEventInstance"]["time_start"]; ?><br/>
	                <div class="field-name">Duration: </div><?php echo $event_attendance["CalendarEventInstance"]["duration"]; ?><br/>
	                <div class="field-name">Date: </div><?php echo $event_attendance["CalendarEventInstance"]["date"]; ?><br/>
	                <div class="field-name">Status: </div><?php echo $event_attendance["EventAttendee"]["status"]; ?>
	                <?php if($event_attendance["Location"]){ ?>
	                    <br/>
	                    <div class="field-name">Location: </div> <?php echo $event_attendance["Location"]["Location"]["name"]; ?><br/>
	                    <div class="field-name">Address: </div> <?php echo $event_attendance["Location"]["Addresses"][0]["address"]; ?>
	                <?php } ?>
	                <?php
	                //            echo "<pre>";
	                //            print_r($event);
	                //            echo "</pre>";
	                ?>
	            </div>
	        <?php } ?>
	    </div>
    </div>
<?php } ?>

</div>