<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 29/06/2016
 * Time: 4:26 PM
 */

?>
<div class="container">

    <h1>Participants</h1>
    <div class="participants">
        <?php foreach($participants as $participant){ ?>
            <div class="participant">
                <div>Name: <?php echo $participant["User"]["first_name"]; ?> <?php echo $participant["User"]["second_name"]; ?></div>
                <div>Email: <?php echo $participant["User"]["email"]; ?></div>
                <div>Created: <?php echo $participant["User"]["created"]; ?></div>
                <div>Modified: <?php echo $participant["User"]["modified"]; ?></div>
                <div>Status: <?php echo $participant["User"]["status"]; ?></div>
            </div>
        <?php } ?>
    </div>

    <h1>Trainers</h1>
    <div class="participants">
        <?php foreach($trainers as $trainer){ ?>
            <div class="participant">
                <div>Name: <?php echo $trainer["User"]["first_name"]; ?> <?php echo $trainer["User"]["second_name"]; ?></div>
                <div>Email: <?php echo $trainer["User"]["email"]; ?></div>
                <div>Created: <?php echo $trainer["User"]["created"]; ?></div>
                <div>Modified: <?php echo $trainer["User"]["modified"]; ?></div>
                <div>Status: <?php echo $trainer["User"]["status"]; ?>
                <?php switch($trainer["User"]["status"]) {
                    case "approved":
                        ?><a href="#">Disable</a><?php
                        break;
                    case "unapproved":
                    default:
                        ?><a href="#">Send for approval</a><?php
                } ?>
                </div>
            </div>
        <?php } ?>
    </div>

</div>