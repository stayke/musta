<div class="container" ng-controller="approvals">
    <h1 class="musta">Approvals</h1>
    
    <table class="table">
        <tr ng-repeat="approve in approvals track by approve.User.id">
            <td>
                {{approve.User.first_name}} {{approve.User.first_name}}<br />
                {{approve.User.email}}<br />
                {{approve.User.dob}}<br />
                {{approve.User.address1}}<br />
                {{approve.User.address2}}<br />
                {{approve.User.suburb}}<br />
                {{approve.User.state}}<br />
                {{approve.User.phone}}<br /><br />
            </td>
            <td>
                {{approve.User.licence_front}}
                {{approve.User.licence_back}}
            </td>
            <td>
                <button class="btn btn-primary">Approve Licence</button>
            </td>
        </tr>
    </table>
</div>