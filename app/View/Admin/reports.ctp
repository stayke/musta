<div class="container searchpage" ng-controller="adminReports">
    <h1 class="musta">Reports</h1>
	<div class="hidden-xs hidden-sm col-md-5">
		<div class="">
			<div class="row">
				<div class="col-xs-12 col-lg-6">
					<label class="filter clearfix">
						<div class="smallpad"><img src="/img/logo-gym.png" class="img-responsive" /></div>
						<div class="smallpad"><input type="checkbox" ng-model="search.filter.gym" ng-change="getResults()" />Gym/Studio 1:1</div>
					</label>
				</div>
				<div class="col-xs-12 col-lg-6">
					<label class="filter clearfix">
						<div class="smallpad"><img src="/img/logo-outdoor.png" class="img-responsive" /></div>
						<div class="smallpad"><input type="checkbox" ng-model="search.filter.outdoor" ng-change="getResults()" />Outdoor 1:1</div>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-lg-6">
					<label class="filter clearfix">
						<div class="smallpad"><img src="/img/logo-mobile.png" class="img-responsive" /></div>
						<div class="smallpad"><input type="checkbox" ng-model="search.filter.mobile" ng-change="getResults()" />Mobile 1:1</div>
					</label>
				</div>
				<div class="col-xs-12 col-lg-6">
					<label class="filter clearfix">
						<div class="smallpad"><img src="/img/logo-bootcamp.png" class="img-responsive" /></div>
						<div class="smallpad"><input type="checkbox" ng-model="search.filter.bootcamp" ng-change="getResults()" />Outdoor/Bootcamp</div>
					</label>
				</div>
			</div>
			<div class="row whiteback smallpadding">
				<div class="col-xs-12 smallpadding">
					<h3 class="musta">Filters</h3>
                    <h4 class="hr">Date</h4>
                    <div class="col-xs-6 smallpadding"><label>From</label><div class="form-group"><input type="date" ng-model="search.date.from"  class="form-control" /></div></div>
                    <div class="col-xs-6 smallpadding"><label>To</label><div class="form-group"><input type="date" ng-model="search.date.to" class="form-control" /></div></div>
					<h4 class="hr">Location</h4>
					<div class="">
						<div class="col-xs-6 smallpadding"><div class="form-group"><input type="text" ng-model="search.postcode" placeholder="Address" class="form-control" id="search-input" /></div></div>
						<div class="col-xs-4 smallpadding"><div class="input-group"><input type="number" ng-model="search.distance" placeholder="Distance" class="form-control" /><span class="input-group-addon">km</span></div></div>
						<div class="col-xs-2 smallpadding"><div class="form-group"><button class="btn btn-primary" ng-click="getResults()">GO</button></div></div>
					</div>
					<label><input type="checkbox" ng-model="search.filter.sgt" ng-change="getResults()" /> Training With Friends </label>
				</div>
			</div>
            
            <div class="row whiteback smallpadding">
				<div class="col-xs-12 smallpadding">
					<h3 class="musta">Statistics</h3>
                    <div class="">
                        <div class="col-xs-12"><h4>Bootcamp Sessions <span class="pull-right">{{ (viewStats.BCSessions || viewStats.BCSessions >= 0) ? viewStats.BCSessions : 'N/A' | number: 0 }}</span></h4></div>
                        <div class="col-xs-12"><h4>PT Sessions <span class="pull-right">{{ (viewStats.PTSessions || viewStats.PTSessions >= 0) ? viewStats.PTSessions : 'N/A' | number: 0 }}</span></h4></div>
                        <div class="col-xs-12"><h4>Trainer Locations <span class="pull-right">{{ (viewStats.locationsByArea || viewStats.locationsByArea >= 0) ? viewStats.locationsByArea : 'N/A' | number: 0 }}</span></h4></div>
                        <div class="col-xs-12"><h4>Unique Locations <span class="pull-right">{{ (viewStats.uniqueLocationsByArea || viewStats.uniqueLocationsByArea >= 0) ? viewStats.uniqueLocationsByArea : 'N/A' | number: 0 }}</span></h4></div>
                        <div class="col-xs-12"><h4>Trainers <span class="pull-right">{{ (viewStats.trainerCount || viewStats.trainerCount >= 0) ? viewStats.trainerCount : 'N/A' | number: 0 }}</span></h4></div>
                    </div>  
				</div>
			</div>
			
			<div class="row whiteback smallpadding">
				<div class="col-xs-12 smallpadding">
					<h3 class="musta">Clients booking more than 1 trainer</h3>
                    <table class="table">
					<tr ng-repeat="stat in stats.bookingsMoreThanOneTrainer | orderBy: 'overbook' : true">
						<td>{{stat.users.first_name}} {{stat.users.second_name}}</td>
						<td>{{stat.users.email}}</td>
						<td>{{stat.overbook}}</td>
					</tr>
				</table>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-xs-12 col-md-7">
		<div id="gmaps" style="height:500px;"></div>
        <div class="">
            <div class="col-xs-12 whiteback">
                <h3 class="musta">Profile Views</h3>
                <table class="table">
					<tr ng-repeat="stat in stats.profileViews | orderBy: 'users.profile_views' : true"">
						<td>{{stat.users.first_name}} {{stat.users.second_name}}</td>
						<td>{{stat.users.email}}</td>
						<td>{{stat.users.profile_views}}</td>
					</tr>
				</table>
            </div>
        </div>
		<div class="">
            <div class="col-xs-12 whiteback">
                <h3 class="musta">Bookings</h3>
                <table class="table">
					<tr ng-repeat="stat in stats.bookingsByTrainer | orderBy: 'bookings' : true">
						<td>{{stat.users.first_name}} {{stat.users.second_name}}</td>
						<td>{{stat.users.email}}</td>
						<td>{{stat.bookings}}</td>
					</tr>
				</table>
            </div>
        </div>
	</div>



</div>