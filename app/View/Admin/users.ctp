<?php
/**
 * Created by PhpStorm.
 * User: Nicholas.Jephcott
 * Date: 29/06/2016
 * Time: 4:26 PM
 */

?>
<div class="container admin-users" ng-controller="adminusers">

    <div class="participants row">
        <h2>Participants</h2>
        <?php foreach($participants as $participant){ ?>
            <div class="participant col-xs-12 col-sm-6">
                <div class="well">
                    <div>Name: <?php echo $participant["User"]["first_name"]; ?> <?php echo $participant["User"]["second_name"]; ?></div>
                    <div>Email: <?php echo $participant["User"]["email"]; ?></div>
                    <div>Created: <?php echo $participant["User"]["created"]; ?></div>
                    <div>Modified: <?php echo $participant["User"]["modified"]; ?></div>
                    <div>Status: <?php echo $participant["User"]["status"]; ?></div>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="trainers row">
        <h2>Trainers</h2>
        <?php foreach($trainers as $trainer){ ?>
            <div class="participant col-xs-12 col-sm-6">
                <div class="well">
                    <div class=" pull-right"><?php switch($trainer["User"]["status"]) {
                            case "approved":
                                ?><a class="btn btn-primary" ng-click="trainerdisable(<?php echo $trainer["User"]["id"]; ?>);">Disable</a><?php
                                break;
                            case "unapproved":
                            default:
                                ?><a class="btn btn-primary" ng-click="trainerapprove(<?php echo $trainer["User"]["id"]; ?>);">Approve</a><?php
                        } ?>
                        <br/>
                        <a ng-if="!details_sent[<?php echo $trainer["User"]["id"]; ?>]" class="btn btn-primary" ng-click="send_details(<?php echo $trainer["User"]["id"]; ?>);">Send Details</a>
                        <br/>
                        <a class="btn btn-primary" href="/user/<?php echo $trainer["User"]["id"]; ?>/">View User</a>
                    </div>
                    <div>Name: <?php echo $trainer["User"]["first_name"]; ?> <?php echo $trainer["User"]["second_name"]; ?></div>
                    <div>Email: <?php echo $trainer["User"]["email"]; ?></div>
                    <div>Created: <?php echo $trainer["User"]["created"]; ?></div>
                    <div>Modified: <?php echo $trainer["User"]["modified"]; ?></div>
                    <div>Status: <?php echo $trainer["User"]["status"]; ?></div>
                    <div>License Front: <a href="/users/viewUserImage/<?php echo $trainer["User"]["licence_front"]; ?>" target="_blank"><?php echo $trainer["User"]["licence_front"]; ?></a></div>
                    <div>License Back: <a href="/users/viewUserImage/<?php echo $trainer["User"]["licence_back"]; ?>" target="_blank"><?php echo $trainer["User"]["licence_back"]; ?></a></div>
                    <div>Bank Statement: <a href="/users/viewUserImage/<?php echo $trainer["User"]["bankstatement_pic"]; ?>" target="_blank"><?php echo $trainer["User"]["bankstatement_pic"]; ?></a></div>
                </div>
            </div>
        <?php } ?>
    </div>

</div>