<?php
// NDJ - DEPRECATED
//       this file has been replaced by the search/results.html partial and angular instead of php
?>
<div class="blackback">
	<h2 class="heading"><div class="container"><i class="fa fa-search" aria-hidden="true"></i><strong>Review</strong> trainers</div></h2>
	<div class="container">
	    <div class="trainers">
	      <?php foreach($trainers as $t): ?>
	           <div class="trainer whiteback">
	             <div class="trainer-img" style="background-image: url(/users/viewUserImage/<?php echo $t['profile_pic'] ?>);"></div>
	             
	             <div class="trainer-description">
<?php
// echo "<pre>";

// print_r($t['short_desc']);

// echo "</pre>";
?>
		             <h2><?php echo $t['User']['first_name'] ?></h2>
		             <h3><?php echo $t['short_desc'][0]['Userinfo']['meta_value'] ?></h3>
		             
		             <?php /* if($t['specialities']) { ?>
		              <h4>Specialties</h4>
		              <ul><?php foreach($t['specialities'] as $spesh){ echo '<li>' . $spesh['Userinfo']['meta_value'] . '</li>'; } ?></ul>
		             <?php } */ ?>
		             
		             <p>
		              <?php
		                $string = (strlen($t['long_desc'][0]['Userinfo']['meta_value']) > 193) ? substr($t['long_desc'][0]['Userinfo']['meta_value'],0,190).'...' : $t['long_desc'][0]['Userinfo']['meta_value'];
		                echo $string;
		              ?>
		             </p>
		             
		             <a href="/user/<?php echo $t['User']['id'] ?>/?trainers=<?php echo $results ?>/" class="btn btn-primary btn-lg">View Profile</a><br /><br />
		             
	             </div>
	             
	             <div class="trainer-favourites">
		             <button class="favourite-add" ng-show="!amIFave(<?php echo $t['User']['id'] ?>)" ng-click="updateFave(<?php echo $t['User']['id'] ?>, 1)"><i class="fa fa-star-o" aria-hidden="true"></i></button>
			         <button class="favourite-remove" ng-show="amIFave(<?php echo $t['User']['id'] ?>)" ng-click="updateFave(<?php echo $t['User']['id'] ?>, 0)"><i class="fa fa-star" aria-hidden="true"></i></button>
		         </div>
	           </div>
	      <?php endforeach; ?>
	   </div>
	</div>
</div>