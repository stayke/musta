<div ng-controller="search" class="searchpage">
	<h2 class="heading"><div class="container"><i class="fa fa-search" aria-hidden="true"></i><strong>Find</strong> a trainer</div></h2>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-7">
                <div class="">
                    <div class="container">
                        <div class="col-xs-12">


                            <div class="row">
                                <!--
                                <div class="col-xs-12 col-md-6 col-lg-6"><h4 class="hr">Location</h4><div class="form-group"><input type="text" ng-model="search.postcode" placeholder="Address" class="form-control" id="search-input" ng-focus="allowSearch = false; search.postcode= ''" /></div></div>
                                <div class="col-xs-12 col-md-3 col-lg-4"><h4 class="hr">Radius (km)</h4><div class="input-group"><input type="number" ng-model="search.distance" placeholder="Distance" class="form-control" /></div></div>
                                -->

                                <div class="col-md-8">
                                    <div class="col-md-12"><h4>Location</h4></div>
                                    <div class="col-md-12"><div class="form-group"><input type="text" ng-model="search.postcode" placeholder="Address" class="form-control" id="search-input" ng-focus="allowSearch = false; search.postcode= ''" /></div></div>
																		
                                    <div class="col-md-12">
                                        <h4>Gender</h4>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <select ng-model="search.gender" class="form-control">
                                                <option value="">Any</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
																			<div class="row">
																				<div class="col-md-6">
																					<h4>Class/Bootcamp</h4>
																				</div>
																				<div class="col-md-6">
																					<label class="filter" style="margin-top: 20px;"><input type="checkbox" ng-model="search.filter.event" /><span></span> </label>
																				</div>
																				<div class="col-md-6 clear">
																					<h4>Training With Friends </h4>
																				</div>
																				<div class="col-md-6">
																					<label class="filter" style="margin-top: 20px;"><input type="checkbox" ng-model="search.filter.sgt" /><span></span> </label>
																				</div>
																			</div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="col-md-12"><h4>Radius (km)</h4></div>
                                    <div class="col-md-12"><div class="input-group"><input type="number" ng-model="search.distance" placeholder="Distance" class="form-control" /></div></div>
																		<br/>
																		<br/>
																		<br/>
																		<br/>
																		<br/>
																		<br/>
                                </div>

                                <!--<div class="col-md-8">
                                        <!--<div class="col-md-2"><h4>Start Date</h4></div>
                                        <div class="col-md-4"><div class="form-group"><input type="date" ng-model="search.time.date_start" placeholder="Start Date" class="form-control" /></div></div>

                                        <div class="col-md-2"><h4>End Date</h4></div>
                                        <div class="col-md-4"><div class="input-group"><input type="date" ng-model="search.time.date_end" placeholder="End Date" class="form-control" /></div></div>

                                        <div class="col-md-4" style="clear: both;"><h4>Start Time</h4></div>
                                        <div class="col-md-8"><div class="form-group"><input ui-timepicker="timePickerOptions" ng-model="search.time.time_start" class="form-control" /></div></div>

                                        <div class="col-md-4"><h4>End Time</h4></div>
                                        <div class="col-md-8"><div class="input-group"><input ui-timepicker="timePickerOptions" ng-model="search.time.time_end" class="form-control" /></div></div>

                                </div>-->

                                <div class="col-md-4">
                                    <label class="filter">
                                        <input type="checkbox" ng-model="search.filter.gym" /><span></span>Gym/Studio
                                    </label>

                                    <label class="filter">
                                        <input type="checkbox" ng-model="search.filter.outdoor" /><span></span>Outdoor
                                    </label>

                                    <label class="filter">
                                        <input type="checkbox" ng-model="search.filter.mobile" /><span></span>Mobile
                                    </label>

                                </div>

                                <div class="col-md-4">
                                    <hr style="margin-top: 0;">
                                    <label class="filter">
                                        <input type="checkbox" ng-model="search.time.day.Monday" /><span></span>
                                        Monday
                                    </label>
                                    <label class="filter">
                                        <input type="checkbox" ng-model="search.time.day.Tuesday" /><span></span>
                                        Tuesday
                                    </label>
                                    <label class="filter">
                                        <input type="checkbox" ng-model="search.time.day.Wednesday" /><span></span>
                                        Wednesday
                                    </label>
                                    <label class="filter">
                                        <input type="checkbox" ng-model="search.time.day.Thursday" /><span></span>
                                        Thursday
                                    </label>
                                    <label class="filter">
                                        <input type="checkbox" ng-model="search.time.day.Friday" /><span></span>
                                        Friday
                                    </label>
                                    <label class="filter">
                                        <input type="checkbox" ng-model="search.time.day.Saturday" /><span></span>
                                        Saturday
                                    </label>
                                    <label class="filter">
                                        <input type="checkbox" ng-model="search.time.day.Sunday" /><span></span>
                                        Sunday
                                    </label>
                                </div>

                            </div>
                            <div class="col-xs-12"><div class="form-group"><button class="btn btn-primary" ng-click="getResults()" ng-disabled="!allowSearch">Search</button></div></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-md-5">
                <div class="map-container">
                    <div id="gmaps"></div>
                </div>
            </div>
        </div>
    </div>
	<div ng-if="search.done" class="blackback clear">
		<h2 class="heading"><div class="container"><i class="fa fa-calendar" aria-hidden="true"></i><strong>Search</strong> results <span class="pull-right right-buffer-sm"><h4 class="musta clearfix"> {{trainerList.length}} Trainers Found</h4></span></div></h2>
		<div class="container">
			<div class="col-xs-12">
				
				<div class="searchresults" ng-include="searchtemplate">

	            </div>
				<!--<h4 class="musta clearfix">Mobile Trainers</h4>
				<table class="table">
					<tr ng-repeat="place in search.mobile">
						<td>{{place.users.first_name}}</td>
						<td>{{(place.users.role == 1) ? 'Personal Trainer' : 'Bootcamp'}}</td>
						<td><a ng-href="/user/{{place.users.id}}" class="btn btn-primary pull-right">View Profile</a></td>
					</tr>
				</table>-->
			</div>
		</div>
	</div>
</div>

