
<h2 class="heading buffer-bottom-none">
	<div class="container">
		<i class="fa fa-user" aria-hidden="true"></i><strong>Trainer</strong> profile<a class="toggler pull-right" onClick="jQuery('#toggleCollapse').slideToggle(); jQuery('.toggler .fa').toggleClass('fa-angle-up'); jQuery('.toggler .fa').toggleClass('fa-angle-down');"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
	</div>
</h2>
<div id="toggleCollapse" class="container" ng-controller="viewprofile" ng-init="currU = <?php echo $user['User']['id'] ?>" style="position:relative;">
        <div class="clearfix whiteback profile">
            </br>
            <!--<div class="col-xs-12">
                <a href="/favourites/" class="btn btn-primary">FAVOURITES</a>
                <button class="btn btn-primary addToFave" ng-show="!amIFave(<?php echo $user['User']['id'] ?>)" ng-click="updateFave(<?php echo $user['User']['id'] ?>, 1)">ADD TO FAVOURITES</button>
                <button class="btn btn-primary" ng-show="amIFave(<?php echo $user['User']['id'] ?>)" ng-click="updateFave(<?php echo $user['User']['id'] ?>, 0)">REMOVE FAVOURITE</button>
            </div>-->
            <div class="col-xs-12 col-sm-4">
                <div class="row">
                    <div class="col-xs-12">
	                      <div class="img-equal">
	                          <img src="/users/viewUserImage/<?php echo $user['meta']['profile_pic'] ?>" class="img-responsive img-round" style="width:100%;" />
	                      </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <br/>
	                      <?php /*<p><strong>Contact Details</strong><br/>
	                      Phone: <?php echo $user['User']['phone']; ?></p> */ ?>
<?php
// echo "<pre>";

// print_r($user['User']);

// echo "</pre>";
?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8">
                 <div class="row" style="padding-bottom:30px;">
                    <div class="col-xs-12">
                    <div class="trainer-favourites profile-favourites">
				             <button class="favourite-add" ng-show="!amIFave(<?php echo $user['User']['id'] ?>)" ng-click="updateFave(<?php echo $user['User']['id'] ?>, 1)"><i class="fa fa-star-o" aria-hidden="true"></i></button>
					           <button class="favourite-remove" ng-show="amIFave(<?php echo $user['User']['id'] ?>)" ng-click="updateFave(<?php echo $user['User']['id'] ?>, 0)"><i class="fa fa-star" aria-hidden="true"></i></button>
				           </div>
                        <h1><?php echo $user['User']['first_name'] ?></h1>
                        <h4 class="short-description"><?php echo (isset($user['meta']['short_desc'][0])) ? $user['meta']['short_desc'][0]['Userinfo']['meta_value'] : ''; ?></h4>
                        <p><?php echo (isset($user['meta']['long_desc'][0])) ? $user['meta']['long_desc'][0]['Userinfo']['meta_value'] : ''; ?></p>
                    </div>
                </div>
                <?php
                $first_aid = false;
                $insurance = false;
                $cpr = false;
                if(isset($user['meta']['firstaid'])) {
                    $first_aid = $user['meta']['firstaid'];
                }
                if(isset($user['meta']['insurance'])) {
                    $insurance = $user['meta']['insurance'];
                }
                if(isset($user['meta']['cpr'])) {
                    $cpr = $user['meta']['cpr'];
                }
                if($first_aid || $insurance || $cpr){
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="">
                            <h4 class="hr">Other Info</h4>
                            <ul>
                                <?php if($first_aid){ ?>
                                <li>First Aid </li>
                                <?php } ?>
                                <?php if($insurance){ ?>
                                <li>Insurance </li>
                                <?php } ?>
                                <?php if($cpr){ ?>
                                <li>CPR </li>
                                <?php } ?>
                                <?php if($user["User"]["fregistertype"] && $user["User"]["fregistertype"] != "-1"){ ?>
                                <li>Fitness Association Member</li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <!--<div class="row">
                    <div class="col-xs-12">
                        <h4 class="hr">Trainer History <span class="glyphicon glyphicon-plus pull-right"></span></h4>
                        <div class="">

                        </div>
                    </div>
                </div>-->
                <div class="row profile-quals" style="padding-bottom:15px;">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="hr">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        <br/>
                        Qualifications</h4>
                        <table class="table table-hover">
                            <tr>
                                <td><strong>Institute</strong></td>
                                <td><strong>Qualification</strong></td>
                            </tr>
                        <?php foreach($user['meta']['qualifications'] as $q): ?>
                                <?php $qual = json_decode($q['Userinfo']['meta_value']); if(!is_object($qual)) continue; ?>
                                <tr>
                                    <td><?php echo $qual->institute ?></td>
                                    <td><?php echo $qual->qualification  ?></td>
                                </tr>
                        <?php endforeach; ?>
                        </table>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <h4 class="hr">
                        <i class="fa fa-diamond" aria-hidden="true"></i>
                        <br/>
                        Specialities</h4>
                        <?php foreach($user['meta']['specialities'] as $key => $spesh): ?>
                            <?php echo ($key > 0) ? ' <br /> ' :  '' ; ?> <?php $arr = preg_split('/(?=[A-Z])/',$spesh['Userinfo']['meta_value']); echo implode(' ', $arr);  ?>
                        <?php endforeach; ?>

                    </div>

                </div>
                <!--<div class="row" style="padding-bottom:15px;">
                    <div class="col-xs-12">
                        <h4 class="hr">Fitness Organisation</h4>
                    </div>
                    <div class="col-xs-12 sub">
                        <table class="table table-hover">
                            <tr>
                                <td><strong>Type</strong></td>
                                <td><strong>Number</strong></td>
                            </tr>
                            <tr>
                                <td style="text-transform: capitalize;"><?php echo $user['User']['fregistertype'] ?></td>
                                <td><?php echo $user['User']['fregisternumber'] ?></td>
                            </tr>
                        </table>
                    </div>
                </div>-->
            </div>
        </div>
</div>
        
<div ng-include="calendarbook" onload="viewUser = <?php echo $user['User']['id'] ?>;trimCal = true" class="viewcalendar greyback profile-calendar">
                                
</div>