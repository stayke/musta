<div class="blackback">
    <h1 class="heading">
        <div class="container"><i class="fa fa-file-o" aria-hidden="true"></i><strong>Privacy</strong> policy</div>
    </h1>
    <div class="container privacy">
        <div class="col-xs-12">

            <h1>1. Privacy policy</h1>

            <p>Musta Australia Pty Ltd ACN 609 770 050 (<strong>Company, We, Our, Us and other similar terms</strong>)
                is committed to protecting the personal information We collect from Our Clients (<strong>Clients, You,
                    Your and other similar terms</strong>). However, We are not required to comply with the provisions
                of the <em>Privacy Act 1988</em> (Cth) or the Australian Privacy Principles (<strong>Law</strong>).</p>

            <p>This Privacy Policy applies to all personal information collected by Us regardless of its source. While
                not purporting to be a statement of compliance with the Law this policy sets out how We will manage
                personal information collected from You. Terms that are capitalised in this Privacy Policy, are defined
                herein.</p>

            <h2>1.1 How to contact the Company about privacy</h2>

            <p>Privacy is very important to Us. For that reason, please read this document carefully and contact Us per
                the details below if You have any questions: </p>

            <p>Musta Australia Pty Ltd ACN 609 770 050<br/>
                PO Box 6126, Fairfield Gardens, Qld, 4103<br/>
                Nominated email address: <a href="info@musta.global">info@musta.global</a><br/>
                Telephone: <a href="tel:1800988475">1800 988 475</a></p>

            <h2>1.2 Collection </h2>
            <ol class="letter-list">
                <li>We collect personal information You provide it to Us by electronic means and provide it to
                    Trainers and our third party payment providers. To the extent possible, We will collect personal
                    information by only lawful and fair means. We endeavour to only collect personal information about
                    You from You directly, if it is reasonable and practical to do so.
                </li>
                <li>We collect information that is reasonably necessary for the performance of Our operations and
                    activities. We automatically receive and record information on Our server logs from Your browser,
                    including Your IP address and cookie information. No personal information about You is linked to the
                    cookie.
                </li>
            </ol>
            <h2>1.3 Purpose</h2>

            <p>We collect, hold, use and disclose personal information in order to provide You with the best service
                possible. Generally, You are only obliged to provide the information necessary for Us to provide You
                with Our goods and services.</p>

            <p>We take reasonable steps to protect the security of personal information. We customarily disclose
                personal information to Our service providers who assist Us in providing Our goods and services to You
                and other Clients, including but not limited to:</p>
            <ol class="letter-list">
                <li>personal trainers who provide personal training services;</li>
                <li>other agents; and</li>
                <li>Our related entities, both present and future.</li>
            </ol>
            <p>Your personal information will not be disclosed to any other third party, unless such disclosure is:</p>
            <ol class="letter-list">
                <li>necessary as part of Our dealings with You; and</li>
                <li>permitted by Law.</li>
            </ol>
            <h2>1.4 Direct marketing</h2>

            <p>If We use Your personal information to provide promotional and marketing information, We will provide You
                with the option to opt out of this service. You may also advise Us directly using the contact details
                above if You prefer. </p>

            <h2>1.5 Data quality</h2>

            <p>We take reasonable steps to ensure that the personal information We collect, use and disclose is
                accurate, complete and up-to-date. </p>

            <h2>1.6 Security</h2>

            <p>We take reasonable steps to protect the personal information We hold from misuse, loss and unauthorised
                access, modification or disclosure.</p>

            <p>Further, We take reasonable steps to destroy or permanently de-identify personal information if it is no
                longer needed.</p>

            <h2>1.7 Access and correction</h2>

            <p>Where practicable, We allow You to get access to, and correct, the personal information We hold about
                You. You can obtain such access and update Your personal information by contacting Us on the details set
                out above. We will endeavour to respond to Your request within a reasonable time. </p>

            <h2>1.8 Transfer out of Australia</h2>

            <p>Our website is hosted in Australia and no personal information is sent overseas.</p>

            <h2>2.0 Complaints procedure </h2>

            <p>If You have a complaint or concern regarding this Privacy Policy You may contact Us on the details set
                out above. We aim to process complaints within a reasonable time after receipt. If unsatisfied with Our
                response, You may refer the matter to the Australian Information Privacy Commissioner. </p>

        </div>
    </div>
</div>
