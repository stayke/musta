<h2 class="heading ng-scope" ng-if="!viewUser">
    <div class="container">
      <i class="fa fa-phone" aria-hidden="true"></i>
      <strong>Contact</strong> Musta
    </div>
</h2>
<div class="container">
	<div class="well well-darker">
			<p>To send a query through to the helpful Musta staff, you can use the <a href="https://www.musta.net.au/contact/#gf_4" target="_blank">online form</a>.</p>
	    <p>
	      <strong class="field-name">Email Address:</strong> <a href="mailto:info@musta.global">info@musta.global</a><br>
		    <strong class="field-name">Facebook:</strong>&nbsp;<a href="https://www.facebook.com/mustaglobal" target="_blank">facebook.com/mustaglobal</a><br>
		    <strong class="field-name">Twitter:</strong>&nbsp;<a href="https://twitter.com/mustaglobal" target="_blank">twitter.com/mustaglobal</a><br>
		    <strong class="field-name">Instagram:</strong>&nbsp;<a href="https://www.instagram.com/mustaglobal/" target="_blank">instagram.com/mustaglobal</a>
	    </p>
	</div>
</div>

