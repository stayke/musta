<div class="backimage">
</div>

<div class="jumbotron">
    <div class="row">
        <div class="content-holder">
            <h2 class="text-inverse">Find a trainer <br/>to suit your lifestyle.</h2>
            <a class="btn btn-primary" ng-click="signupForm()">Join now</a>&nbsp;&nbsp;&nbsp;
            <a class="btn btn-primary" href="/search/">Search</a>
            <br/>
            <a class="link-inverse" ng-click="loginForm()">Login</a>
        </div>
    </div>
          
    <div class="row">
        <div class="content-holder naked">
            <a class="btn" ng-click="signupFormTrainer(true)">Join as a trainer</a>
            <a class="link-inverse" href="http://musta.net.au" target="_new">More info</a>
        </div>
    </div>
</div>
