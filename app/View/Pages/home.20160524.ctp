<div class="backimage">
    <div class="container">
        <div class="col-xs-12 col-sm-6 col-sm-push-6">
            <div class="homeBox">
                <div class="row">
                    <div class="col-xs-1 col-md-2"></div>
                    <div class="col-xs-10 col-md-8">
                        <h2>LOOKING FOR A TRAINER?</h2>
                        <a class="btn btn-secondary btn-block nudge" href="/search/">CLICK HERE</a>
                    </div>
                    <div class="col-xs-1 col-md-2"></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-pull-6">
            <div class="homeBox">
                <div class="row">
                    <div class="col-xs-1 col-md-2"></div>
                    <div class="col-xs-10 col-md-8">
                        <h2>TRAINER OR BOOTCAMP?</h2>
                        <a class="btn btn-secondary btn-block" ng-click="signupForm(true)">SIGN UP HERE</a>
                        <a class="btn btn-primary btn-block" href="http://musta.net.au" target="_new">MORE INFO</a>
                    </div>
                    <div class="col-xs-1 col-md-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
   <!-- <h4 class="musta text-center">WHAT IS MUSTA?</h4>
    <p class="text-justify">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
    totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
    Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et
    dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea
    commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
    </p>-->
</div>