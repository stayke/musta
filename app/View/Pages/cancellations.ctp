<div class="blackback">
	<h1 class="heading"><div class="container"><i class="fa fa-file-o" aria-hidden="true"></i><strong>Cancellations</strong> and Refunds</div></h1>
	<div class="container cancellations">
        <div class="col-xs-12">


            <h2>1. Cancellation and Refund Policy</h2>
            <p>This cancellation and refund policy (Cancellation and Refund Policy ) contains the terms and conditions that apply when Musta Australia Pty Ltd ACN 609 770 050 (We, us, our and other like terms) are required to provide you with a refund.  This Cancellation and Refund Policy  must be read alongside the Terms of Service between You and Us and any Credit Card Authority Request Services Agreement and / or Director Debit Request Service Agreement (Services Agreement)</p>
            <p>Terms that are capitalised in this document take their meaning from the Services Agreement, the Terms of Service with the Client or are defined herein.</p>
            <h2>2. Requesting a refund</h2>
            <ol class="letter-list">
                <li>You may request that we provide you with a refund in accordance with this Cancellation and Refund Policy .</li>
                <li>All refund requests must be made using the refund request form contained on the Musta website (Refund Request Form).</li>
                <li>We reserve the right to refuse all refund requests that are not made using the Refund Request Form.</li>
            </ol>
            <h2>3. Method of providing refund</h2>
            <ol class="letter-list">
                <li>Where you are entitled to a refund, we will only provide such refund by crediting the Nominated Bank Account or Nominated Card to which the Transaction for which a refund is being provided was originally made.  We will not issue a refund by way of cash, cheque, or credit to any other card or account.</li>
                <li>We aim to process all refund requests within five (5) Business Days of receiving them, but we make no warranty that such requests will be processed in this time frame.</li>
            </ol>
            <h2>4. Refund where Trainer unable to provide Training</h2>
            <h3>4.1 Refunds generally</h3>
            <ol class="letter-list">
                <li>Where you have paid for Training which the Trainer has not provided, you are entitled to a refund for the amount paid.</li>
                <li>All refund requests made pursuant to this clause 4 must be made within forty eight (48) hours of the time for which the Training Services were to be provided.  We reserve the right to refuse all refund requests made outside of this time frame.</li>
                <li>This does not apply where the Trainer was unable to provide the Training due to the occurrence of an Event of Force Majeure.</li>
            </ol>
            <h3>4.2 Events of Force Majeure</h3>
            <ol class="letter-list">
                <li>If the Trainer is unable to provide the Training due to an occurrence of an Event of Force Majeure, the Trainer will offer You an alternative session for the provision of Training at no extra charge.</li>
                <li>If there is no suitable alternative time for the provision of Training pursuant to clause 4.2(a), You may request a refund using the Refund Request Form.</li>
            </ol>
            <h3>4.3 Definition of Event of Force Majeure</h3>
            <p>In this Cancellation and Refund Policy, Event of Force Majeure means the occurrence of a natural disaster, national emergency, war, health issues, prohibitive governmental regulations or any other event beyond the reasonable control of the Trainer that renders the provision of Training impossible.</p>
            <h2>Refunds where you do not attend</h2>
            <ol class="letter-list">
                <li>You may cancel a request for Training no later than forty-eight (48) hours prior to their scheduled time (Cancellation Time).</li>
                <li>If you wish to cancel a request for Training after the Cancellation Time, you will be charged the fee of $1.65 (including GST) (Cancellation Fee).</li>
                <li>If you do not attend for Training and you have not cancelled the relevant session within the Cancellation Time, you will not be entitled to a refund and you must pay the Training Fee.</li>
            </ol>


        </div>
	</div>
</div>