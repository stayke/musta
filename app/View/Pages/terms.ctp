<div class="blackback">
	<h1 class="heading"><div class="container"><i class="fa fa-file-o" aria-hidden="true"></i><strong>Terms</strong> and Conditions</div></h1>
	<div class="container terms">
		<div class="col-xs-12">

            <h2>1 Website Terms of Use </h2>
            <p>As consideration for allowing you, the user (<strong>User, You, Your and other similar terms</strong>) to view the Content on the website, located at the url www.musta.global (<strong>Site</strong>), You agree to the following terms of use (<strong>Terms of Use</strong>). </p>
            <p>The Site is operated by Musta Australia Pty Ltd ACN 609 770 050 (Company, We, Our, Us and other similar terms). We provide You with access to Site pursuant the terms and conditions contained herein.</p>
            <p>You acknowledge and agree that You have had sufficient opportunity to read and understand the terms and conditions contained in these Terms of Use, and that You are legally able to agree to be bound by them.  If You do not agree to these Terms of Use, You must leave this Site immediately.</p>
            <h2>2 Permitted use</h2>
            <h3>2.1 Content</h3>
            <p><strong>Content</strong> means text, data, speech, music or other sounds, visual images (animated or otherwisin any form, or in any combination of forms as defined in Schedule 7 of Broadcasting Services Act 1992 (Cth).</p>
            <ol class="letter-list">
                <li>Unless otherwise indicated, We reserve all copyright in the Content and design of the Site.  We own or are the licensee of all such copyright and provide it to You under the terms of a limited licence embodied in these Terms of Use each time You visit the Site. </li>
                <li>You may download, print or copy Content provided on the Site for Your own use. Unless provided with a mechanism to do so, You must not sell, lease, furnish or otherwise permit or cause others to access Content on the Site.</li>
                <li>You must not use, reproduce, communicate, publish, or distribute any of the Content on the Site, unless it constitutes a fair dealing for the purposes of the Copyright Act 1968 (Cth)(Act).  In particular, You must not reproduce or use any of the information on the Site for commercial benefit. </li>
                <li>Other than for the purposes of and subject to the conditions prescribed under the Act as otherwise provided for in these Terms of Use, no part of the Content may in any form or by any means (including framing, screen scraping, electronic, mechanical, photocopying or recordinbe reproduced, adapted, stored in a retrieval system or transmitted without prior written permission.</li>
            </ol>
            <h2>3 Prohibitions on use</h2>
            <p>The Site and the information contained herein must not be used in any manner that infringes Our rights. You must not:</p>
            <ol class="letter-list">
                <li>data mine or conduct automated searches on the Site or the Content, whether through the use of additional software or otherwise; </li>
                <li>frame or mirror the Site; </li>
                <li>tamper with, hinder the operation of, or make unauthorised modifications to the Site or any of its Content; </li>
                <li>transmit any virus, worm or other disabling feature to or via the Site; </li>
                <li>abuse, defame, harass, stalk, threaten or otherwise violate the Our legal rights;</li>
                <li>advertise or offer to sell any goods or services, or conduct or forward surveys, contests, or chain letters from the Site;</li>
                <li>delete any author attributions, legal notices or proprietary designations or labels unless authorised to do so; and</li>
                <li>use the Site to send commercial, unsolicited or bulk electronic messages to anyone or in any other way which would constitute an infringement of the Spam Act 2003 (Cth).</li>
            </ol>
            <h2>4 Privacy Policy</h2>
            <p>In using the Site, You are deemed to accept Our <a href="/privacy/">Privacy Policy</a>.</p>
            <h2>5 Provision of service</h2>
            <ol class="letter-list">
                <li>We may, without notice, suspend access to the Site or disconnect or deny You access to any part of the Site during any technical failure or maintenance period.</li>
                <li>We may also choose in Our sole discretion to block or deny You access to any of the Content contained on the Site.</li>
                <li>We may make improvements and or changes to Site and the Content at any time without notice.  We do not warrant that the information architecture or navigation will not change now or at any time in future.</li>
            </ol>
            <h2>6 Limitation of liability </h2>
            <h3>6.1 No duty of care</h3>
            <ol class="letter-list">
                <li>You access the Site at Your own risk and are responsible for compliance with the laws of Your jurisdiction in addition to these Terms of Use.</li>
                <li>The Site is available for You to use, however We assume no duty of care to You.  We make no representation and provide no warranty regarding the quality, accuracy, completeness, merchantability or fitness for purpose of the Content on the Site.</li>
                <li>If relying on the Content, You must make You own investigations to ensure its accuracy before doing so.</li>
            </ol>
            <h3>6.2 Disclaimer of warranties</h3>
            <p>You expressly acknowledge and agree that, to the maximum extent permitted by law: </p>
            <ol class="letter-list">
                <li>Your use of the Site is at Your sole risk.  The Site is provided on an "as is" and "as available" basis.  We and Our officers, employees and agents, expressly disclaim all warranties of any kind, whether express or implied, including, but not limited to the implied warranties of merchantability, fitness for a particular purpose and non-infringement;</li>
                <li>We and Our officers, employees and agents make no warranty that:</li>
                <ol class="roman-list">
                    <li>the Content will meet Your requirements; </li>
                    <li>the information contained in the Content is accurate or reliable;</li>
                    <li>the Content will be uninterrupted, timely, secure or error-free; </li>
                    <li>the quality the Content, or other material obtained by You through the Site will meet Your expectations; and </li>
                    <li>any errors will be corrected; </li>
                </ol>
                <li>any Content downloaded or otherwise obtained through the use of the Site is accessed at Your own discretion and risk, and You will be solely responsible for any damage to Your computer or loss of data that results from the download of the Content; and</li>
                <li>no advice or information, whether oral or written, obtained by You from the Site or through or from the Content creates any warranty not expressly stated herein.</li>
            </ol>
            <h3>6.3 Limitation of liability</h3>
            <ol class="letter-list">
                <li>Except for certain statutory warranties under consumer protection laws We do not provide any guarantee or warranty or make any representation of any kind, either express or implied, in relation to the Content or Your use of the Content on the Site.  </li>
                <li>Subject to any claims available under consumer protection laws We and Our officers, employees and agents are not liable for any loss or damage, including, but not limited to, direct, indirect or consequential losses including any form of consequential loss such as any third party loss, loss of profits, loss of revenue, loss of opportunity, loss of anticipated savings, pure economic loss and an increased operating cost, personal injury or death, however suffered or sustained in connection with:</li>
                <ol class="roman-list">
                    <li>any inaccurate or incorrect information provided on the Site; </li>
                    <li>Your use of the Content; </li>
                    <li>any failure or delay including, but not limited to, the use or inability to use any  of the Content; </li>
                    <li>any interference with or damage to Your computer systems which occurs in connection with use of the Site or any of its Content;</li>
                    <li>the cost of procurements of substitute Content resulting from any goods or Content purchased or obtained through the Content;</li>
                </ol>
                <li>For claims that cannot be excluded or restricted under consumer protection laws, Our liability for such a claim will (at Our option and to the extent permitted by law) be limited to: </li>
                <ol class="roman-list">
                    <li>resupplying the Content or its equivalent; or</li>
                    <li>paying the cost of having the Content or its equivalent resupplied.</li>
                </ol>
            </ol>
            <h3>6.4 Links to third party Sites</h3>
            <ol class="letter-list">
                <li>We do not represent that the Content on other websites to which the Site contains links to does not inadvertently infringe the intellectual property rights of any person anywhere in the world. </li>
                <li>By inadvertently linking to infringing third party content, We are not authorising infringement of any intellectual property rights contained on those websites.  </li>
            </ol>
            <h3>7 Miscellaneous provisions</h3>
            <ol class="letter-list">
                <li><strong>Access to the Site outside the Jurisdiction</strong> - No representation or warranty is made that the Content on the Website complies with the laws of any country outside of Australia.  If You access the Site from outside Australia, You do so at their own risk.</li>
                <li><strong>Changes to Terms of Use</strong> – We may change these Terms of Use at Our discretion by providing notice on the Site.  The version of the Terms of Use that applies to You will be available on the Site each time You visit the Site.</li>
                <li><strong>Entire agreement</strong> - These Terms of Use and any warranties implied by law which are not capable of being excluded or modified amount to the entire agreement between You and Us.  Any contact with Us or Our officers, employees or agents that includes any statements representations, warranties (if any) whether expressed or implied, including any collateral agreement or warranty, with reference to the subject matter or the intentions of either You or Us are merged and otherwise are excluded and cancelled by those contained in these Terms of Use.</li>
                <li><strong>Governing law and jurisdiction</strong> - The laws of Queensland and Australia govern this Agreement.  You submit to the non-exclusive jurisdiction of the Supreme Court of Queensland and the Federal Court of Australia.</li>
                <li><strong>Indemnity</strong> - By using the Site, You indemnify Us and Our officers, employees and agents against any claim or proceeding that is made, threatened or commenced, and any liability, loss, damage or expense (including reasonable legal costs on a full indemnity basis) that We may incur or suffer as a direct or indirect result of: </li>
                <ol class="roman-list">
                    <li>Your breach of these Terms of Use;</li>
                    <li>an actual or alleged breach by You of any law, legislation, regulation, by-law or code of conduct caused by data uploaded or downloaded from the Site;</li>
                    <li>any claims brought by or on behalf of any third party relating to any act or omission by You, including breach of a third parties copyright or trade mark.</li>
                </ol>
                <li><strong>RSS Feeds</strong> - If You utilise any of the facilities on the Site such as RSS Feed(s) (Feethat may or may not (as the case may bavailable from time to time, the format of the Feed must not be interfered with.  You must include the back link to the full article on the Site and show any included attestation.</li>
                <li><strong>Severance</strong> - If any part of these Terms of Use are found to be void, unlawful or unenforceable then that part will be deemed to be severable from the balance of the document and the severed part will not affect the validity and enforceability of the remaining Terms of Use. </li>
                <li><strong>Trademarks</strong> – We may be the owner of several common law, or where indicated, registered trade marks which appear on the Site.  Unauthorised use of these trademarks will infringe Our intellectual property rights.  We reserve Our right to take action against You if You infringe Our intellectual property rights. </li>
                <li><strong>Waiver</strong> - If We do not act in relation to a particular breach of these Terms of Use by You, this will not be treated as a waiver of Our right to act with respect to subsequent or similar breaches.</li>
                <li><strong>Contact Details</strong> – You may contact Us using the following details: </li>
            </ol>
            <p>
                <strong>Musta Australia Pty Ltd ACN 609 770 050</strong><br/>
                PO Box 6126, Fairfield Gardens, Qld, 4103<br/>
                Nominated email address: info@musta.global<br/>
                Telephone: 1800 988 475
            </p>

		</div>
	</div>
</div>
