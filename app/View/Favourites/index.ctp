<div class="blackback">
<h2 class="heading"><div class="container"><i class="fa fa-search" aria-hidden="true"></i><strong>Favourites</strong></div></h2>
<div class="container">
    <div class="trainers">
        
        <?php foreach($favourites as $tr): ?>
        <?php $t = $tr['deets'] ?>
            <div class="trainer whiteback">
                <div class="trainer-img" style="background-image: url(/users/viewUserImage/<?php echo $t['profile_pic'] ?>"></div>
	            <div class="trainer-description">
	              <h2><?php echo $t['User']['first_name'] ?></h2>
	              <h3><?php echo $t['Userinfo'][3]['meta_value'] ?></h3>
	              <ul>
	                <?php foreach($t['specialities'] as $spesh) { ?>
	                  <li><?php echo $spesh['Userinfo']['meta_value'] ?></li>
	                <?php } ?>
	              </ul>
	              
	              <a href="/user/<?php echo $t['User']['id'] ?>/" class="btn btn-primary btn-lg">View Profile</a><br /><br />
	            </div>
	            <div class="trainer-favourites">
                    <button class="favourite-add" ng-show="!amIFave(<?php echo $t['User']['id'] ?>)" ng-click="updateFave(<?php echo $t['User']['id'] ?>, 1)"><i class="fa fa-star-o" aria-hidden="true"></i></button>
                    <button class="favourite-remove" ng-show="amIFave(<?php echo $t['User']['id'] ?>)" ng-click="updateFave(<?php echo $t['User']['id'] ?>, 0)"><i class="fa fa-star" aria-hidden="true"></i></button>
                </div>
            </div>
        <?php endforeach; ?>
        <?php if(count($favourites)<1): ?>
            <tr>
                <td colspan=4 class="text-center">You currently have no favourites.</td>
            </tr>
        <?php endif; ?>

    </div>
</div>
</div>