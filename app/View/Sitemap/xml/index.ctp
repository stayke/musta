<urlset xmlns="http://www.google.com/schemas/sitemap/0.84" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84 http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">

    <url>
        <loc>https://musta.global/</loc>
    </url>
    <?php
        foreach($users as $u):
    ?>
    <url>
        <loc>https://musta.global/user/<?php echo $u['User']['id'] ?></loc>
    </url>
    <?php endforeach; ?>
    <url>
        <loc>https://musta.global/terms</loc>
    </url>
    <url>
        <loc>https://musta.global/privacy</loc>
    </url>
    <url>
        <loc>https://musta.global/cancellations</loc>
    </url>
    <url>
        <loc>https://musta.global/search</loc>
    </url>
</urlset>