<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version());

// $this->Html->script('js/main.js', array('inline' => false));
// $this->Html->script('js/spectrum.js', array('inline' => false));
// $this->Html->script('js/custom/angularapp.js', array('inline' => false));
// $this->Html->script('js/custom/app/calendar.js', array('inline' => false));
// $this->Html->script('js/custom/app/signup.js', array('inline' => false));
// $this->Html->script('js/custom/app/notifications.js', array('inline' => false));
// $this->Html->script('js/custom/app/locations.js', array('inline' => false));
// $this->Html->script('js/custom/app/profile.js', array('inline' => false));
// $this->Html->script('js/custom/app/holiday.js', array('inline' => false));
// $this->Html->script('js/custom/app/search.js', array('inline' => false));
// $this->Html->script('js/custom/app/personalProfile.js', array('inline' => false));
// $this->Html->script('js/custom/app/profileEdit.js', array('inline' => false));
// $this->Html->script('js/custom/app/yourTCs.js', array('inline' => false));
// $this->Html->script('js/custom/app/mustaterms.js', array('inline' => false));
// $this->Html->script('js/custom/app/bankdetails.js', array('inline' => false));
// $this->Html->script('js/custom/app/viewprofile.js', array('inline' => false));
// $this->Html->script('js/custom/app/payments.js', array('inline' => false));
// $this->Html->script('js/custom/app/admin.js', array('inline' => false));
// $this->Html->script('js/custom/app/register.js', array('inline' => false));
// $this->Html->script('js/custom/app/event.js', array('inline' => false));
// $this->Html->script('js/custom/app/adminReports.js', array('inline' => false));
// $this->Html->script('js/bootstrap-switch.min.js', array('inline' => false));
// $this->Html->script('js/custom/app/paymentSection.js', array('inline' => false));
// $this->Html->script('js/custom/app/approvals.js', array('inline' => false));
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/css/spectrum.css">
	<link rel="stylesheet" href="/bower_components/angular-chart.js/dist/angular-chart.css">
	<link rel="stylesheet" href="/css/bootstrap-switch.min.css">
    <link rel="stylesheet" href="/css/ui-grid.min.css">

	<?php echo $this->Html->charset(); ?>
	
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('main');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<link rel='apple-touch-icon' type='image/png' href='/icon.57.png'> <!-- iPhone -->
	<link rel='apple-touch-icon' type='image/png' sizes='72x72' href='/icon.72.png'> <!-- iPad -->
	<link rel='apple-touch-icon' type='image/png' sizes='114x114' href='/icon.114.png'> <!-- iPhone4 -->
	<link rel='icon' type='image/png' href='/icon.114.png'> <!-- Opera Speed Dial, at least 144×114 px -->
	<meta name="viewport" content="width=device-width,maximum-scale=1,user-scalable=no" >
	<?php
	if (!empty($meta_description)) {
		echo $this->Html->meta(
			'description',
			$meta_description
		);
	}
	if (!empty($meta_title)) {
		?>
		<title>
			<?php echo $meta_title; ?>
		</title>
		<?php
	} else { ?>
	<title>
		Musta Global
	</title>
	<?php
	}
	?>
</head>
<body ng-app="mustaTrainer" ng-class="{bodyLock: page.rightSlider.display || page.leftSlider.display || page.bottomSlider.display, settings: page.settings.display }" ng-keyup="closeSlider($event);" class="ng-cloak <?php if ($this->request->here == '/') { echo ' home';} ?>">
	<header id="header" class="text-center hideme" ng-class="{hideMe: !showHead }">
		<div class="container">
			<div class="logo"><a href="/">
				<img class="img-responsive horizontal" src="/img/musta-logo-horizontal.png" ng-if="!user.id" />
				<img class="img-responsive lockup" src="/img/musta-logo-icon.png" ng-if="user.id" /></a>
			</div>
			<div class="headIcon inverse" >
				<span class="fa fa-bell btn-icon" ng-click="toggleNotifications()" ng-if="user.id"></span>
				<span class="notifications counter" ng-if="user.id" ng-if="notifications.length > 0">{{notifications.length}}</span>
				<span class="fa fa-bars header-icon settings-button btn-icon" ng-if="user.id" ng-click="toggleSettings()" ng-class="{active: page.settings.display}">Menu</span>
				<!-- <span ng-class="{active: page.settings.display}" class="iconText"></span></div> -->

				<!-- </span> -->
				<!-- <span class="glyphicon glyphicon-bell pull-right header-icon notifications-button headIcon visible-xs" ng-click="toggleNotifications()" ng-if="user.id"><span class="counter" ng-if="notifications.length > 0">{{notifications.length}}</span></span> -->
				<button class="btn btn-icon" ng-if="!user.id" ng-click="loginForm()" ><span class="fa fa-user"></span>Login</button>
				<!-- <button class="btn btn-icon"  ng-if="!user.id" ng-click="signupForm()"><span class="glyphicon glyphicon-open-file"></span> Sign Up</button> -->
			</div>
		</div>	
	</header>

	
	<div id="settings">
		<ul ng-include="page.settings.template">
		</ul>
	</div>
	
	<div id="main">
		<div class="transition" ng-class="{blur: page.rightSlider.display || page.leftSlider.display || page.bottomSlider.display }">
			<?php echo $this->fetch('content'); ?>
		</div>
		<div class="uiblocker"
			ng-if="page.rightSlider.display || page.leftSlider.display || page.bottomSlider.display || page.settings.display"
			ng-click="goBack()"
		>
		</div>
	</div>

	<footer class="footer">
		<div class="container">	

			<div class="columns">
				<div class="legals">
			
					<a class="social" href="mailto:info@musta.global"><i class="fa fa-envelope" aria-hidden="true"></i></a>
					<a class="social" href="https://www.facebook.com/mustaglobal" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a class="social" href="https://twitter.com/mustaglobal" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a class="social" href="https://www.instagram.com/mustaglobal/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					
					<br/>
					<a href="/terms/">Terms & Conditions</a> | <a href="/privacy/">Privacy Policy</a><!-- | <a href="/cancellations/">Cancellation and Refunds</a>--> | <a href="/files/IP-FSG-PDS-20151201-v1.1.pdf">Integra Pay Financial Service Guide and Product Disclosure Statement</a><br/>
					Copyright <?php echo date("Y"); ?> Musta Australia Pty Ltd, all rights reserved. <a href="http://ifactory.com.au" class="lellow" target="_new">Digital agency</a> iFactory
				</div>
			</div>

			<div class="columns">
				<div class="pill">	
					<p>We accept: </p>
						<!-- <img src="/img/transparent-logo-visa.png" class="img-responsive pull-right" style="width:100px;" /> -->
						<picture class="external-logo">
						  <source srcset="/img/musta-logo-visa.svg">
						  <img src="/img/musta-logo-visa.png" alt="Visa">
						</picture>
						<picture class="external-logo">
						  <source srcset="/img/musta-logo-mastercard.svg">
						  <img src="/img/musta-logo-mastercard.png" alt="Mastercard">
						</picture>
				</div>
			</div>
		</div>
	</footer>	
	
	<div id="bottomSlider" ng-class="{inactive: page.bottomSlider.display != true}" class="">
		<div ng-include="page.bottomSlider.template" ng-class="{wide: page.bottomSlider.wide}">
			
		</div>
		<div class="spinner square" ng-hide="page.bottomSlider.template.length>0"><img class="spinme" src="/img/spinner.png" /><br /><img src="/img/notspinner.png" /></div>
	</div>
		
		<div id="leftSlider" ng-class="{inactive: page.leftSlider.display != true}"class="inactive">
			<span ng-click="goBack()" class="closeDialog"></span>
			<div class="leftSliderInner" ng-include="page.leftSlider.template">
			</div>
		</div>
		
		<div id="rightSlider" ng-class="{inactive: page.rightSlider.display != true}" class="inactive">
			<span ng-click="goBack()" class="closeDialog"></span>
			<div class="rightSliderInner" ng-include="page.rightSlider.template">
			</div>
		</div>
		
		<div id="topSlider" ng-class="{inactive: page.topSlider.display != true}" class="inactive">
			<div class="topSlider" ng-include="page.topSlider.template">
			</div>
		</div>
		
		
	<script src="//localhost:35729/livereload.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/spectrum.js"></script>
    <script src="/js/ui-grid.min.js"></script>
	<script src="/js/jquery.timepicker.min.js"></script>
	<script src="/js/timepickerdirective.min.js"></script>
	<script src="/js/custom/angularapp.js"></script>
	<script src="/js/custom/app/calendar.js"></script>
	<script src="/js/custom/app/calendarrule.js"></script>
	<script src="/js/custom/app/payform.js"></script>
	<script src="/js/custom/app/booking.js"></script>
	<script src="/js/custom/app/mybookings.js"></script>
	<script src="/js/custom/app/signup.js"></script>
	<script src="/js/custom/app/notifications.js"></script>
	<script src="/js/custom/app/locations.js"></script>
	<script src="/js/custom/app/locationedit.js"></script>
	<script src="/js/custom/app/profile.js"></script>
	<script src="/js/custom/app/holiday.js"></script>
	<script src="/js/custom/app/search.js"></script>
	<script src="/js/custom/app/personalProfile.js"></script>
	<script src="/js/custom/app/profileEdit.js"></script>
	<script src="/js/custom/app/yourTCs.js"></script>
	<script src="/js/custom/app/mustaterms.js"></script>
	<script src="/js/custom/app/bankdetails.js"></script>
	<script src="/js/custom/app/viewprofile.js"></script>
	<script src="/js/custom/app/payments.js"></script>
	<script src="/js/custom/app/parq.js"></script>
	<script src="/js/custom/app/admin.js"></script>
	<script src="/js/custom/app/adminusers.js"></script>
	<script src="/js/custom/app/register.js"></script>
	<script src="/js/custom/app/event.js"></script>
	<script src="/js/custom/app/eventattend.js"></script>
	<script src="/js/custom/app/adminReports.js"></script>
	<script src="/js/bootstrap-switch.min.js"></script>
	<script src="/js/custom/app/paymentSection.js"></script>
	<script src="/js/custom/app/approvals.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/utils/Draggable.min.js"></script>
	<script 
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAX1f6YC-0zVwsuLD_34CrCjnlcaTHMUDY&libraries=places">
    </script>
	<script src='/bower_components/angular-upload/angular-upload.min.js'></script>
	<script src='/bower_components/Chart.js/Chart.min.js'></script>
	<script src='/bower_components/angular-chart.js/dist/angular-chart.js'></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>
