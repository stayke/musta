<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		Musta Global
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('main');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<link rel="stylesheet" href="/css/spectrum.css">
	<link rel="stylesheet" href="/css/bootstrap-switch.min.css">
	<link rel="stylesheet" href="/bower_components/angular-chart.js/dist/angular-chart.css">
	<link rel="icon" type="image/png" href="/favicon.png" />
	<meta name="viewport" content="width=device-width,maximum-scale=1,user-scalable=no" >
</head>
<body ng-app="mustaTrainer" ng-class="{bodyLock: page.rightSlider.display || page.leftSlider.display || page.bottomSlider.display, settings: page.settings.display }" ng-keyup="closeSlider($event);" class="ng-cloak">
	<header id="header" class="text-center hideme" ng-class="{hideMe: !showHead }">
		<div class="pull-left headIcon"  ng-if="user.id" ng-click="toggleSettings()">
			<span class="glyphicon glyphicon-menu-hamburger header-icon settings-button"  ng-class="{active: page.settings.display}"></span>
			<span ng-class="{active: page.settings.display}" class="iconText hidden-xs">Menu</span></div>
		<span class="glyphicon glyphicon-bell pull-left header-icon notifications-button headIcon hidden-xs" ng-click="toggleNotifications()" ng-if="user.id"><span class="counter" ng-if="notifications.length > 0">{{notifications.length}}</span></span>
		<span class="glyphicon glyphicon-bell pull-right header-icon notifications-button headIcon visible-xs" ng-click="toggleNotifications()" ng-if="user.id"><span class="counter" ng-if="notifications.length > 0">{{notifications.length}}</span></span>
		<div class="pull-right headIcon" ng-if="!user.id">
			<button class="btn btn-secondary"  ng-if="!user.id" ng-click="loginForm()" ><span class="glyphicon glyphicon-user"></span> Sign In</button><br class="visible-xs" />
			<button class="btn btn-secondary"  ng-if="!user.id" ng-click="signupForm()" style="margin-right:15px;"><span class="glyphicon glyphicon-open-file"></span> Sign Up</button>
		</div>
		<span class="logo"><a href="/"><img class="img-responsive" src="/img/logo-trans.png" /></a></span>
	</header>
	
	<div id="settings">
		<ul ng-include="page.settings.template">
		</ul>
	</div>
	
	<div id="main">
		<div class="container transition" ng-class="{blur: page.rightSlider.display || page.leftSlider.display || page.bottomSlider.display }">
			<div id="content" ng-include="page.template">
	
			</div>
			<?php echo $this->fetch('content'); ?>
		</div>
		<div class="uiblocker"
			ng-if="page.rightSlider.display || page.leftSlider.display || page.bottomSlider.display || page.settings.display"
			ng-click="goBack()"
		>
		</div>
		<div class="clearfix"></div>
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-7">
						Copyright 2015 Musta Global, All rights reserved. Built by <a href="http://smallbusinessinternetmarketing.com.au" class="lellow" target="_new">SBIMarketing</a>
					</div>
					<div class="col-xs-12 col-sm-5 text-right">
						<a href="/terms/">Terms & Conditions</a> | <a href="/privacy/">Privacy Policy</a> | <a href="/cancellations/">Cancellation and Refunds</a><br /><br />
						<img src="/img/transparent-logo-visa.png" class="img-responsive pull-right" style="width:100px;" /><br />
						<span class="pull-right" style="clear:right">accepted here</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="bottomSlider" ng-class="{inactive: page.bottomSlider.display != true}" class="">
			<div ng-include="page.bottomSlider.template" ng-class="{wide: page.bottomSlider.wide}">
			</div>
			<div class="spinner square" ng-hide="page.bottomSlider.template.length>0"><img class="spinme" src="/img/spinner.png" /><br /><img src="/img/notspinner.png" /></div>
		</div>
		
		<div id="leftSlider" ng-class="{inactive: page.leftSlider.display != true}"class="inactive">
			<span ng-click="goBack()" class="glyphicon glyphicon-remove close-left"></span>
			<div class="leftSliderInner" ng-include="page.leftSlider.template">
			</div>
		</div>
		
		<div id="rightSlider" ng-class="{inactive: page.rightSlider.display != true}" class="inactive">
			<span ng-click="goBack()" class="glyphicon glyphicon-remove close-left"></span>
			<div class="rightSliderInner" ng-include="page.rightSlider.template">
			</div>
		</div>
		
		<div id="topSlider" ng-class="{inactive: page.topSlider.display != true}" class="inactive">
			<div class="topSlider" ng-include="page.topSlider.template">
			</div>
		</div>
		
		
	<script src="//localhost:35729/livereload.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/spectrum.js"></script>
	<script src="/js/custom/angularapp.js"></script>
	<script src="/js/custom/app/calendar.js"></script>
	<script src="/js/custom/app/calendarrule.js"></script>
	<script src="/js/custom/app/signup.js"></script>
	<script src="/js/custom/app/notifications.js"></script>
	<script src="/js/custom/app/locations.js"></script>
	<script src="/js/custom/app/profile.js"></script>
	<script src="/js/custom/app/holiday.js"></script>
	<script src="/js/custom/app/search.js"></script>
	<script src="/js/custom/app/personalProfile.js"></script>
	<script src="/js/custom/app/profileEdit.js"></script>
	<script src="/js/custom/app/yourTCs.js"></script>
	<script src="/js/custom/app/mustaterms.js"></script>
	<script src="/js/custom/app/bankdetails.js"></script>
	<script src="/js/custom/app/viewprofile.js"></script>
	<script src="/js/custom/app/payments.js"></script>
	<script src="/js/custom/app/admin.js"></script>
	<script src="/js/custom/app/register.js"></script>
	<script src="/js/bootstrap-switch.min.js"></script>
	<script src="/js/custom/app/paymentSection.js"></script>
	<script src="/js/custom/app/event.js"></script>
	<script src="/js/custom/app/adminReports.js"></script>
	<script src="/js/custom/app/approvals.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/utils/Draggable.min.js"></script>
	<script 
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAX1f6YC-0zVwsuLD_34CrCjnlcaTHMUDY&libraries=places">
    </script>
	<script src='/bower_components/angular-upload/angular-upload.min.js'></script>
	<script src='/bower_components/Chart.js/Chart.min.js'></script>
	<script src='/bower_components/angular-chart.js/dist/angular-chart.js'></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>
