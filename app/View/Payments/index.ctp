<h1 class="heading"><div class="container"><i class="fa fa-credit-card-alt" aria-hidden="true"></i><strong>Payments</strong></div></h1>
<div class="container" ng-controller="paymentSection">
    <div ng-if="!confirmedPW">
        <form class="well col-xs-12 col-md-6 col-md-push-3" ng-submit="checkPW(security.pw)" method="POST">
            <label>Please confirm your password to continue</label>
            <div class="form-group"><input type="password" ng-model="security.pw" class="form-control" required /></div>
            {{error}}
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <div ng-if="confirmedPW">
        <!--<div class="col-xs-2">
            <button style="width: 100%;" class="btn" ng-class="{'btn-primary': (activePanel != 'clients'), 'btn-secondary': (activePanel == 'clients')}" ng-click="activePanel = 'clients'">Clients</button>
            <button style="width: 100%;" class="btn" ng-class="{'btn-primary': (activePanel != 'cc'), 'btn-secondary': (activePanel == 'cc')}" ng-click="activePanel = 'cc'">Payments</button>
            <button style="width: 100%;" class="btn" ng-class="{'btn-primary': (activePanel != 'deets'), 'btn-secondary': (activePanel == 'deets')}" ng-click="activePanel = 'deets'">Bank Details</button>
        </div>-->
        <div class="col-xs-12">
        
        
            <ul class="nav nav-tabs">
              <li role="presentation" ng-class="{'': (activePanel != 'clients'), 'active': (activePanel == 'clients')}" ng-click="activePanel = 'clients'" class="active"><a href="#">Clients</a></li>
              <li role="presentation" ng-class="{'': (activePanel != 'cc'), 'active': (activePanel == 'cc')}" ng-click="activePanel = 'cc'"><a href="#">Payments</a></li>
              <!--<li role="presentation" ng-class="{'': (activePanel != 'deets'), 'active': (activePanel == 'deets')}" ng-click="activePanel = 'deets'"><a href="#">Bank Details</a></li>-->
            </ul>
            
            
            <div class="panel clearfix" style="padding:15px 0;" ng-show="activePanel == 'overview'">
                <div class="col-xs-12">
                    <h2>Overview</h2>
                    <h3>{{favouriteCount}} People have added you to favourites</h3>
                   <div class="row">
                        <div class="col-xs-6 text-center">${{currentBalance.balance | number:2 }} <br /> Balance</div>
                        <div class="col-xs-6 text-center">
                             ${{currentBalance.taxbalance | number:2 }} <br />
                             Estimated Tax <br />
                             <p>Tax disclaimer</p>
                        </div>
                   </div> 
                   <h3>Reports</h3>
                   <div class="form-group">
                        <select class="form-control" ng-model="period">
                            <option value="7" selected>Past 7 days</option>
                            <option value="30">Past 30 days</option>
                            <!--<option value="365">Past 365 days</option>-->
                        </select>
                   </div>
                   <div class="row">
                        <!--<div class="col-xs-12">
                            <h4>Views</h4>
                            <canvas id="views" class="chart chart-line" chart-data="charts.views.data"
                            chart-labels="charts.views.labels" chart-legend="false"
                            >
                            </canvas> 
                        </div>-->
                        <div class="col-xs-12">
                            <h4>Bookings</h4>
                            <canvas id="bookings" class="chart chart-line" chart-data="charts.bookings.data"
                            chart-labels="charts.bookings.labels" chart-legend="false" 
                            >
                            </canvas> 
                        </div>    
                   </div>
                </div>
            </div>
            <div class="panel clearfix" style="padding:15px 0;" ng-show="activePanel == 'cc'">

                <div class="col-xs-12">
                    <h3 class="hr">Reccuring Payments</h3>
                    <div id="recurringGrid" ui-grid="recurringGridOptions" ui-grid-edit class="ang-grid" style="width: 1080px; max-width: 100%; max-height: 600px; overflow:hidden; margin-bottom: 20px;"></div>
                    <button class="btn btn-secondary pull-right" ng-click="addSomethingChange('card')"><span class="glyphicon glyphicon-plus"></span>New Payment</button>
                    </div>
                    <div ng-hide="addSomething == 'card'">
                    <div id="cardGrid" ui-grid="cardGridOptions" ui-grid-edit class="ang-grid" style="width: 1080px; max-width: 100%; max-height: 600px; overflow:hidden; margin-bottom: 20px;"></div>
                <div class="col-xs-12">
                    <h2 class="pull-left">Payments</h2>
                </div>

                    <!--<div class="well" ng-hide="addSomething == 'card'">
                        <h3 class="hr">Recent Payments</h3>
                        <table class="table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Transaction ID</td>
                                    <td>Name</td>
                                    <td>Paid You</td>
                                    <td>Fee Paid</td>
                                    <td>GST</td>
                                    <td>Status</td>
                                    <td>Date</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="payment in cardPayments">
                                    <td><img ng-src="{{(payment.Customer.User.clientpic) ? '/users/viewUserImage/' + payment.Customer.User.clientpic : '/img/userblank.jpg'}}" class="img-responsive" style="max-width:50px;" /></td>
                                    <td>{{payment.Payment.transaction_id}}</td>
                                    <td>{{payment.Customer.User.first_name}} {{payment.Customer.User.second_name}}</td>
                                    <td>${{payment.Payment.amount | number: 2}}</td>
                                    <td>${{payment.Payment.fee | number: 2}}</td>
                                    <td>${{payment.Payment.gst | number: 2}}</td>
                                    <td>{{payment.Payment.status}}</td>
                                    <td>{{payment.Payment.datestamp}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>-->
                    <!--<div class="well" ng-hide="addSomething == 'card'">
                        <h3 class="hr">Reccuring Payments</h3>
                        <table class="table table-striped table-hover" ng-hide="addSomething == 'card'">
                            <thead>
             
                            </thead>
                            <tbody>
                                <tr ng-repeat="payment in repeatPayments">
                                    <td><img ng-src="{{(payment.Customer.User.clientpic) ? '/users/viewUserImage/' + payment.Customer.User.clientpic : '/img/userblank.jpg'}}" class="img-responsive" style="max-width:50px;" /></td>
                                    <td>{{payment.Customer.User.first_name}} {{payment.Customer.User.second_name}}</td>
                                    <td>${{payment.RepeatPayment.total | number: 2}}</td>
                                    <td>${{payment.RepeatPayment.fee | number: 2}}</td>
                                    <td>{{(payment.RepeatPayment.approved == 1) ? 'Approved' : 'Pending'}}</td>
                                    <td><span ng-if="payment.RepeatPayment.approved == 1" class="glyphicon" ng-class="{'glyphicon-remove': payment.RepeatPayment.paused == 1, 'glyphicon-ok': payment.RepeatPayment.paused == 0}"></span></td>
                                    <td>{{ payment.RepeatPayment.frequency }}</td>
                                    <td>{{ getNextDate(payment.RepeatPayment.last_payment, payment.RepeatPayment.frequency) }}</td>
                                    <td><button class="btn btn-primary">Suspend</button><br /><button class="btn btn-primary">Delete</button></td>
    
                                </tr>
                            </tbody>
                        </table>
                    </div>-->
                    <!--<div class="well" ng-hide="addSomething == 'card'">
                        <h3 class="hr">Reccuring Payments</h3>
                        <table class="table table-striped table-hover" ng-hide="addSomething == 'card'">
                            <thead>
           
                            </thead>
                            <tbody>
                                <tr ng-repeat="payment in recurringPayments">
                                    <td><img ng-src="{{(payment.Customer.User.clientpic) ? '/users/viewUserImage/' + payment.Customer.User.clientpic : '/img/userblank.jpg'}}" class="img-responsive" style="max-width:50px;" /></td>
                                    <td>{{payment.Customer.User.first_name}} {{payment.Customer.User.second_name}}</td>
                                    <td>${{payment.RecurringPayment.total | number: 2}}</td>
                                    <td>${{payment.RecurringPayment.fee | number: 2}}</td>
                                    <td>{{payment.RecurringPayment.status}}</td>
                                    <td>{{ payment.RecurringPayment.frequency }}</td>
                                    <td>{{ payment.RecurringPayment.day }}</td>
                                    <td>{{ payment.RecurringPayment.date_start }}</td>
                                    <td>{{ payment.RecurringPayment.date_end }}</td>
                                    <td>{{ payment.RecurringPayment.num_repeat }}</td>
                                    <td><button class="btn btn-primary">Suspend</button><br /><button class="btn btn-primary">Delete</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>-->
                    <div class="well" ng-show="addSomething == 'card'">
                         <table class="table table-striped table-hover">
                            <tbody>
                                <tr>
                                    <td colspan=3>Client</td>
                                </tr>
                                <tr>
                                    <td colspan=3>
                                        <select class="form-control" ng-model="addCreditCard.client">
                                            <option value="">Choose a client</option>
                                            <option ng-repeat="client in clientList" value="{{client.TrainerClient.user_id || 0 }},{{client.TrainerClient.email}}">{{client.TrainerClient.name}} ({{client.TrainerClient.email}})</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=3>Price</td>
                                </tr>
                                <tr>
                                    <td colspan=2>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                $
                                            </span>
                                            <input type="number" class="form-control" ng-model="addCreditCard.price" />
                                            <span class="input-group-addon"><label><input type="checkbox" ng-model="addCreditCard.addGST" />GST</label></span>
                                        </div>
                                    </td>
                                    <td colspan=1></td>
                                </tr>
                                <tr>
                                    <td colspan=3>Repeat</td>
                                </tr>
                                <tr>
                                    <td colspan=2>
                                        <div class="form-group">
                                           <label><input type="checkbox" ng-model="addCreditCard.repeat" />Repeat Weekly</label>
                                        </div>
                                        <!--<div class="form-group" ng-if="addCreditCard.repeat">
                                            <select class="form-control" ng-model="addCreditCard.frequency">
                                                <option value="weekly">Weekly</option>
                                                <option value="fortnightly">Fortnightly</option>
                                                <option value="monthly">Monthly</option>
                                            </select>
                                            <br />Please note, first payment will be made upon accepting, and every week/month thereafter.
                                        </div>-->
                                    </td>
                                    <td colspan=1></td>
                                </tr>
                                <tr ng-if="addCreditCard.repeat">
                                    <td colspan=3>Num. Repeats</td>
                                </tr>
                                <tr ng-if="addCreditCard.repeat">
                                    <td colspan=3>
                                        <div class="input-group">
                                            <input type="number" class="form-control" ng-model="addCreditCard.num_repeat" />
                                        </div>
                                    </td>
                                </tr>
                                <tr ng-if="addCreditCard.repeat">
                                    <td colspan=3>Date Start (Give your new client enough notice to enter their bank/CC details before the start date)</td>
                                </tr>
                                <tr ng-if="addCreditCard.repeat">
                                    <td colspan=3>
                                        <div class="input-group">
                                            <input class="form-control" type="date" ng-model="addCreditCard.date_start">
                                            <small>Payments will take two days to process and transfer to your account</small>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=3>Notes (These are visible to the client)</td>
                                </tr>
                                <tr>
                                    <td colspan=3>
                                        <textarea rows=6 class="form-control" ng-model="addCreditCard.notes"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=3>Payment</td>
                                </tr>
                                <tr>
                                    <td colspan=2>Trainer Cost</td>
                                    <td>${{addCreditCard.price  | number: 2}}</td>
                                </tr>
                                <tr ng-show="addCreditCard.addGST">
                                    <td colspan=2>GST</td>
                                    <td>${{addCreditCard.price/10 | number: 2}}</td>
                                </tr>
                                <!--<tr>
                                    <td colspan=2>Musta Fee</td>
                                    <td>${{1.50  | number: 2}}</td>
                                </tr>-->
                                <tr>
                                    <td colspan=2><strong>Total Due</strong></td>
                                    <td><strong>${{ (addCreditCard.price*((addCreditCard.addGST)?1.1:1)) | number: 2 }}</strong></td>
                                </tr>
                                <tr>
                                    <td colspan=3>
                                        <div class="radio">
                                            <label><input type="radio" name="optradio" ng-model="tandcType" ng-value="1">Use Default T&C's</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="optradio"  ng-model="tandcType" ng-value="2">Custom T&C's</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr ng-hide="tandcType != 2">
                                    <td colspan=3>

                                        
                                        <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.maximum.active" /> a maximum of
                                            </label>
    
                                            <div class="termOption">
                                                <input type="number" ng-model="terms.maximum.amount" /> x
                                                <select ng-model="terms.maximum.hours">
                                                    <option value="30">30</option>
                                                    <option value="30">45</option>
                                                    <option value="30">60</option>
                                                </select> minute training sessions
                                            </div>
                                        </div>
                                        
                                        <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.per.active" /> per
                                            </label>
    
                                            <div class="termOption">
                                                <select ng-model="terms.per.frequency">
                                                    <option value="Week">Week</option>
                                                    <option value="Fortnight">Fortnight</option>
                                                    <option value="Month">Month</option>
                                                </select> 
                                            </div>
                                        </div>
                                        
                                         <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.used.active" /> to be used within
                                            </label>
    
                                            <div class="termOption">
                                                <input type="number" ng-model="terms.used.within" />
                                                weeks
                                            </div>
                                        </div>
                                         
                                         <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.paystop.active" /> Payments will stop after the minimum number has been settled
                                            </label>
                                        </div>
                                         
                                         <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.minpay.active" /> After the minimum number of payments the trainee will continue on the recurring payments with no end date.
                                            </label>
                                        </div>
                                         
                                         
                                         
                                         <!---- Default from here active ----->
                                         
                                         <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.payfull.active"  /> Payment for all sessions must be made in full prior to the session
                                        commencing.
                                            </label>
                                        </div>
                                         
                                          <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.failedpayment.active" /> Failed payments will incur a fee which will be added to the next successful payment.
                                            </label>
                                        </div>
                                         
                                         <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.ifcancel.active" />
                                                If training is cancelled due to unavoidable reasons (ie. pregnancy, health 
                                                issues) and this falls before the next paid session, the trainee will be charged for 
                                                times they have trained only
                                            </label>
                                        </div>
                                         
                                         <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.custompaymentpolicy.active" /> Add own payment policy
                                                <div class="termOption" ng-if="terms.custompaymentpolicy.active">
                                                    <textarea ng-model="terms.custompaymentpolicy.text" class="form-control"></textarea>
                                                </div>
                                            </label>
                                        </div>
                                         
                                         <h4>Cancelation & Rescheduling sessions</h4>
                                    
                                        <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.sessioncancelled.refunded" /> A session cancelled or missed by the trainee will not be refunded under any 
                                        circumstances and full payment taken.
                                            </label>
                                        </div>
                                        
                                         <div class="form-input">
                                            <label>
                                                <input type="checkbox" ng-model="terms.sessioncancelled.minof" /> A session cancelled by the trainee with a minimum of
                                            </label>
    
                                            <div class="termOption">
                                                <input type="number" ng-model="terms.sessioncancelled.min" />
                                                <select ng-model="terms.cancel.frequency">
                                                    <option value="Hours">Hours</option>
                                                    <option value="Days">Days</option>
                                                    <option value="Weeks">Weeks</option>
                                                    <option value="Months">Months</option>
                                                </select>
                                                notice will be –
                                                <div class="termOption">
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.makeup" /> Offered as a make up session at a time the trainer is available </label></div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.makeupweek" /> in that week. Failure to do so will incur the full cost of your session </label></div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.fullyrefund" /> Fully refunded by the trainer </label></div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.norefunded" /> Sessions cancelled with less than this notice period will not be refunded or 
                                        offered as a make up session </label></div>
                                                </div>
                                                <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.active" /> Add own cancelation policy</label> </div>
                                                <div class="termOption" ng-show="terms.cancel.addcancelation.active">
                                                   <div><label><textarea class="form-control" ng-model="terms.cancel.addcancelation.text"></textarea></label></div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-input">
                                                <label>
                                                    <input type="checkbox" ng-model="terms.cancelledbytrainer.active" /> A session cancelled by the trainer will be …</label>
                                                </label>
                                                <div class="termOption">
                                                    <div><label><input type="checkbox" ng-model="terms.cancelledbytrainer.makeup" /> offered as a make up session at a time agreed between the trainer and trainee</label> </div>
                                                    <h5><label><input type="checkbox" ng-model="terms.cancelledbytrainer.or" />  Or</label></h5>
                                                    <div><label><input type="checkbox" ng-model="terms.cancelledbytrainer.fullyrefund" /> Fully refunded</label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancelledbytrainer.bootcampattendance" /> <strong>Boot camp attendance, freezing membership and Public holidays</strong></label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancelledbytrainer.entitled" /> Members are entitled to attend all sessions they have signed up for and are encouraged not to miss any sessions.</label> </div>
                                                </div>
                                                  <div><label><input type="checkbox" ng-model="terms.cancelledbytrainer.brokendown" /> As memberships are broken down into <select ng-model="terms.breakdown.period"><option value="weekly">weekly</option><option value="monthly">monthly</option></select> payments, [the Bootcamp] will not compensate members who do not show up to sessions they have paid for. However members have the opportunity to place their membership on hold in advance if they have a medical</label> </div>
                                            <div><label><input type="checkbox" ng-model="terms.cancelledbytrainer.publicholiday" /> [The Bootcamp] is closed on Public Holidays, hence training sessions will not be run on Public holidays and will also not be compensated for.</label> </div>
                                                <div><label><input type="checkbox" ng-model="terms.cancel.addcancelationandreschedle.active" /> Add own cancelation and reschedule policy</label> </div>
                                                    <div class="termOption" ng-show="terms.cancel.addcancelationandreschedle.active">
                                                       <div><label><textarea class="form-control" ng-model="terms.cancel.addcancelationandreschedle.text"></textarea></label></div>
                                                    </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.lateness" /> <strong>Lateness</strong></label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.ifyouarelate" /> If you are late for a session your training will still finish at the end of the scheduled time</label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.weather" /> <strong>Weather</strong></label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.assessionsareheld" /> As sessions are held outdoors, weather is uncontrollable, and training sessions will run in rain unless the weather is deemed dangerous due to hail or lightning.</label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.washedout" /> Sessions that are washed out due to rain will not be compensated for</label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.unlessawholeweek" /> unless a whole week of training has been cancelled</label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.ifsessionscancelled" /> If sessions are cancelled due to dangerous weather conditions they will be</label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.offeredasamakeup" /> Offered as a make up session at a time the trainer is available</label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.inthatweek" /> in that week. Failure to do so will incur the full cost of your session</label> </div>
                                                    <div><label><input type="checkbox" ng-model="terms.cancel.addcancelation.fullyrefunded" /> Fully refunded by the trainer</label> </div>
       
                                         </div>
                                            
                                          
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=3>
                                        <button type="button" class="btn btn-secondary pull-right" ng-click="addPayment()">Request Payment</button>
                                        <button type="button" class="btn btn-secondary pull-right" ng-click="addSomethingChange(false)" style="margin-right:15px;">Cancel</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel clearfix" style="padding:15px 0;" ng-show="activePanel == 'clients'">
                <div class="col-xs-12">
                    <h2 class="pull-left">Clients</h2>
                    <button class="btn btn-secondary pull-right" ng-click="addSomethingChange('client')"><span class="glyphicon glyphicon-plus"></span>New Client</button>
                </div>
                <div class="col-xs-12">
                    <div class="well">
                        <table class="table table-striped table-hover" ng-hide="addSomething == 'client'">
                            <thead>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Name</td>
                                    <td>Mobile</td>
                                    <td>Email</td>
                                    <td>On Musta?</td>
                                    <td>Actions</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="client in clientList">
                                    <td><img ng-src="{{(client.TrainerClient.clientpic) ? '/users/viewUserImage/' + client.TrainerClient.clientpic : '/img/userblank.jpg'}}" class="img-responsive" style="max-width:50px;" /></td>
                                    <td>{{client.TrainerClient.name}}</td>
                                    <td>{{client.TrainerClient.mobile}}</td>
                                    <td>{{client.TrainerClient.email}}</td>
                                    <td><span class="glyphicon" ng-class="{'glyphicon-remove': !client.TrainerClient.user_id, 'glyphicon-ok': client.TrainerClient.user_id}"></span></td>
                                    <td><a class="btn btn-secondary" ng-click="parq(client.TrainerClient.user_id);">View ParQ</a></td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-striped table-hover" ng-show="addSomething == 'client'">
                            <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Mobile</td>
                                    <td>Email</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" class="form-control" ng-model="addCl.name" /></td>
                                    <td><input type="text" class="form-control" ng-model="addCl.mobile" /></td>
                                    <td><input type="email" class="form-control" ng-model="addCl.email" /></td>
                                </tr>
                                 <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button" class="btn btn-secondary pull-right" ng-click="addClient(addCl.name,addCl.mobile,addCl.email)">Add Client</button>
                                        <button type="button" class="btn btn-secondary pull-right" ng-click="addSomethingChange(false)" style="margin-right:15px;">Cancel</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel clearfix" style="padding:15px 0;" ng-show="activePanel == 'deets'">
                <div ng-include="p + 'pay/bankdetails.html'">
                    
                </div>
            </div>
        </div>
    </div>
</div>