<div class="container">
    <div class="thank-you-page">
<?php if($response["error_messages"]) { ?>
        <h1>An Error Occured</h1>
    <?php foreach($response["error_messages"] as $error_msg){ ?>
    <div><?php echo $error_msg; ?></div>
    <?php } ?>
    <?php if($response["payment"]["Payment"]["status"] == "unpaid" || $response["payment"]["Payment"]["status"] == "pending"){ ?>
    <div><a ng-click="makePayment({id: '<?php echo $response["data"]->transactionID; ?>'});">Make Payment</a></div>
    <?php } ?>

<?php } else { ?>
        <h1>Thank you for making a payment</h1>
        <p>Fusce neque. Suspendisse potenti. Nullam vel sem. Nulla facilisi. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla.</p>

        <p>Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Nunc nonummy metus. Vivamus elementum semper nisi. Quisque ut nisi.</p>

        <p>Nulla consequat massa quis enim. Etiam vitae tortor. Nulla porta dolor. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>
<?php } ?>
<?php
// echo "<pre>";
// echo "test a";
// print_r($response);
// echo "</pre>";
?>
<?php if($response["data"] && $response["data"]->payerFirstName){ ?>
        <div class="payment-details">
            <h2>Payment Details</h2>
            <div class="info-row">
                <div class="label">Name:</div>
                <div class="info"><?php echo $response["data"]->payerFirstName . " " . $response["data"]->payerLastName; ?></div>
            </div>
            <div class="info-row">
                <div class="label">Address:</div>
                <div class="info"><?php echo $response["data"]->payerAddressStreet . " " . $response["data"]->payerAddressSuburb . " " . $response["data"]->payerAddressPostcode . " " . $response["data"]->payerAddressState . " " . $response["data"]->payerAddressCountry; ?></div>
            </div>
            <div class="info-row">
                <div class="label">Phone:</div>
                <div class="info"><?php echo $response["data"]->payerPhone; ?></div>
            </div>
            <div class="info-row">
                <div class="label">Transaction Amount:</div>
                <div class="info"><?php echo $response["data"]->transactionAmount; ?></div>
            </div>
            <div class="info-row">
                <div class="label">Transaction Date:</div>
                <div class="info"><?php echo $response["data"]->transactionDateTime; ?></div>
            </div>
        </div>
<?php } else if(isset($response["user"]) && $response["user"]) { ?>
    <div class="payment-details">
        <h2>Payment Details</h2>
        <div class="info-row">
            <div class="label">Name:</div>
            <div class="info"><?php echo $response["user"]["User"]["first_name"] . " " . $response["user"]["User"]["second_name"]; ?></div>
        </div>
        <div class="info-row">
            <div class="label">Address:</div>
            <div class="info"><?php echo $response["user"]["User"]["address1"] . " " . $response["user"]["User"]["suburb"] . " " . $response["user"]["User"]["postcode"] . " " . $response["user"]["User"]["state"]; ?></div>
        </div>
        <div class="info-row">
            <div class="label">Phone:</div>
            <div class="info"><?php echo $response["user"]["User"]["phone"]; ?></div>
        </div>
        <?php if(isset($response["payment"]) && $response["payment"]) { ?>
        <div class="info-row">
            <div class="label">Transaction Amount:</div>
            <div class="info">$<?php echo $response["payment"]["Payment"]["total"]; ?></div>
        </div>
        <div class="info-row">
            <div class="label">Transaction Date:</div>
            <div class="info"><?php echo $response["payment"]["Payment"]["datestamp"]; ?></div>
        </div>
        <?php } ?>
    </div>
<?php } ?>

<?php if(isset($response["booking"])){ ?>
        <div class="booking-details">
            <h2>Booking Details</h2>
            <div class="info-row">
                <div class="label">Trainer:</div>
                <div class="info"><?php echo $response["trainer"]["User"]["first_name"] . " " . $response["trainer"]["User"]["second_name"]; ?></div>
            </div>
            <div class="info-row">
                <div class="label">Location:</div>
                <div class="info"><?php echo $response["location"]["Location"]["name"]; ?></div>
                <?php if(isset($response["location"]["Addresses"])){ ?>
                    <?php foreach($response["location"]["Addresses"] as $address){ ?>
                    <div class="address"><?php echo $address["address"]; ?></div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="info-row">
                <div class="label">Time:</div>
                <div class="info"><?php echo $response["booking"]["PtBooking"]["time"]; ?></div>
            </div>
            <div class="info-row">
                <div class="label">Duration:</div>
                <div class="info"><?php echo $response["booking"]["PtBooking"]["length"]; ?> min</div>
            </div>
            <?php if($response["booking"]["PtBooking"]["session_notes"]){ ?>
                <div class="info-row">
                    <div class="label">Notes:</div>
                    <div class="info"><?php echo $response["booking"]["PtBooking"]["session_notes"]; ?></div>
                </div>
            <?php } ?>
        </div>
<?php } ?>

<?php if(isset($response["instance"])){ ?>
        <div class="event-details">
            <h2>Event Details</h2>
            <div class="info-row">
                <div class="label">Trainer:</div>
                <div class="info"><?php echo $response["trainer"]["User"]["first_name"] . " " . $response["trainer"]["User"]["second_name"]; ?></div>
            </div>
            <div class="info-row">
                <div class="label">Location:</div>
                <div class="info"><?php echo $response["location"]["Location"]["name"]; ?></div>
                <?php if(isset($response["location"]["Addresses"])){ ?>
                    <?php foreach($response["location"]["Addresses"] as $address){ ?>
                    <div class="address"><?php echo $address["address"]; ?></div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="info-row">
                <div class="label">Time:</div>
                <div class="info"><?php echo $response["instance"]["CalendarEventInstance"]["time_start"]; ?></div>
            </div>
            <div class="info-row">
                <div class="label">Duration:</div>
                <div class="info"><?php echo $response["instance"]["CalendarEventInstance"]["duration"]; ?></div>
            </div>
            <?php if($response["instance"]["CalendarEventInstance"]["notes"]){ ?>
                <div class="info-row">
                    <div class="label">Notes:</div>
                    <div class="info"><?php echo $response["instance"]["CalendarEventInstance"]["notes"]; ?></div>
                </div>
            <?php } ?>
        </div>
<?php } ?>
    </div>
</div>