  module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "css/main.css": "less/bootstrap.less" // destination file and source file
        }
      }
    },
    watch: {
      styles: {
        files: ['less/**/*'], // which files to watch
        tasks: ['less'],
        options: {
          nospawn: true,
          livereload: true
        }
      },
      js: {
        files: ['js/custom/**/*'], // which files to watch
        tasks: ['concat'],
        options: {
          nospawn: true,
          livereload: true
        }
      }
    },
    concat: {
        options: {
          separator: '\n',
        },
        dist: {
          src: [
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/angular/angular.min.js',
            //'bower_components/angular-route/angular-route.min.js',
            'bower_components/angular-animate/angular-animate.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'js/spin.min.js',
            'js/ladda.min.js'
          ],
          dest: 'js/main.js',
        }
      }
  });
  
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.registerTask('default', ['less', 'watch','concat']);
};