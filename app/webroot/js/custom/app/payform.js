/**
 * Created by Nicholas.Jephcott on 2/06/2016.
 */
app.controller('payform', function($scope, $http, $rootScope, $sce, $q) {

    $rs = $rootScope;
    $scope.data = {}
    $scope.error_messages = {}
    $scope.webPageToken = "";
    $scope.integraFormAction  = "";
    $scope.submit_button = "Process Payment";
    $scope.vault_save = false;

    $scope.loadPayment = function(curPaymentId){
        // todo make this work
        $http.post('/payments/getPayment/' + curPaymentId).then(function(r){
            console.log('test b');
            $scope.handleDetails(r);
        });
    }

    $scope.resetPayment = function(curPayment){
        $scope.data = {
        };
    }

    $scope.calcTotal = function(base_amount){
        var amount = (base_amount * 0.0176) + 0.33;
        if(amount < 1.1){
            amount = 1.1;
        }
        var full_amount = parseFloat(base_amount) + parseFloat(amount);
        var total = full_amount.toFixed(2);
        return total;
    }

    $scope.handleDetails = function(deets){
        $scope.resetPayment($scope.data);
        console.log(deets, "payment deets");
        if(deets.data.webPageToken){
            $scope.data = deets.data;
            if($scope.data.payment){
                if($scope.data.payment.Payment){
                    $scope.data.payment = $scope.data.payment.Payment;
                }
            }
            $scope.webPageToken = deets.data.webPageToken;
            //$scope.integraFormAction = $sce.trustAsUrl('https://testpayments.integrapay.com.au/RTP/Payment.aspx?webPageToken=' + $scope.webPageToken);
            if(deets.data.vault_save){ // Adding payment details to vault
                $scope.integraFormAction = $sce.trustAsResourceUrl('https://testpayments.integrapay.com.au/Vault/AddToVault.aspx?webPageToken=' + $scope.webPageToken);
                $scope.submit_button = "Add to Vault";
                $scope.vault_save = true;
            } else { // Submitting direct payment
                $scope.integraFormAction = $sce.trustAsResourceUrl('https://testpayments.integrapay.com.au/RTP/Payment.aspx?webPageToken=' + $scope.webPageToken);
                $scope.submit_button = "Process Payment";
                $scope.vault_save = false;
            }
            console.log($scope.webPageToken, "web page token");
            console.log($scope.integraFormAction, "form action");
        } else if(deets.data.id){
            $scope.loadPayment(deets.data.id);
        } else if(deets.data.error_messages){
            $scope.error_messages = deets.data.error_messages;
        }
    }

    $rs.$on('returnPaymentDetails', function(e,deets){
        $scope.handleDetails(deets);
    });

    $rs.$broadcast('requestPaymentDetails');
});