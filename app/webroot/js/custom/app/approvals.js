app.controller('approvals', function($scope, $http, $rootScope) {
	$rs = $rootScope;
    
    $scope.getApprovals = function(){
        $http.get('/admin/getapprovals')
        .then(function(r){
            $scope.approvals = r.data;
        });
    }
    
    $scope.getApprovals();
});