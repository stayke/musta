app.controller('search', function($scope, $http, $rootScope, $timeout, $filter, $anchorScroll, $location) {
	$rs = $rootScope
	$scope.allowSearch = false;
	$scope.markers = [];

    $scope.timePickerOptions = {
        step: 15,
        timeFormat: 'H:i:s'
    }
	
	$rs.search = {
		filter: {
			gym: true,
			outdoor: true,
			mobile: true,
			bootcamp: true
		},
        time: {
            day: {
                Monday: true,
                Tuesday: true,
                Wednesday: true,
                Thursday: true,
                Friday: true,
                Saturday: true,
                Sunday: true
            }
        }
	};
	$scope.search.results = [];
    $scope.search.done = false;
	$scope.defaultDistance = 10;
	$scope.trainerList = [];
	
		var input = /** @type {!HTMLInputElement} */(
      document.getElementById('search-input'));
	$scope.search.autocomplete = new google.maps.places.Autocomplete(input);
	
	 $scope.search.autocomplete.addListener('place_changed', function() {
		var place = $scope.search.autocomplete.getPlace();
		if (!place.geometry) {
		  window.alert("Autocomplete's returned place contains no geometry");
		  return;
		} else {
			console.log(place.geometry.location);
			$scope.updateSearchPostcode(place.geometry.location);
			$scope.allowSearch = true;
			$scope.$digest();
		}
	 });
	
	if ($rs.userLocation) {
		lat = $rs.userLocation.coords.latitude;
		lon = $rs.userLocation.coords.longitude;
		zoom = 14
	} else {
		lat = -27.1861734
		lon = 134.9509604
		zoom = 4
	}
	
	$scope.$on('LocationFound', function(e, a){
		if (!$rs.search.filters) {
			console.log($rs.userLocation.coords);
			
			$scope.updateSearchPostcode({ lat: $rs.userLocation.coords.latitude, lng: $rs.userLocation.coords.longitude });
			$rs.geocoder.geocode({'location': { lat: $rs.userLocation.coords.latitude, lng: $rs.userLocation.coords.longitude }}, function(results, status) {
				$scope.search.postcode = results[0].formatted_address;
				$scope.getResults();
				$scope.allowSearch = true;
			});
		}
	});
	
	
	$scope.search.distance = $scope.defaultDistance;
	
	$scope.updateSearchPostcode = function(r){
		$scope.search.geometry = r;
	}
	
	$scope.resetMarkers = function(){
		$scope.markers.forEach(function(marker) {
			marker.setMap(null);
		});
		$scope.markers = [];
	}

    $scope.mapMarkerClick = function(marker){
        console.log(marker, "map marker clicked");
        if(marker.user){
            //var newHash = "user-" + marker.user;
            //if($location.hash() !== newHash){
            //    $location.hash(newHash);
            //    $anchorScroll();
            //} else {
            //    $anchorScroll();
            //}
            jQuery(".user-result").removeClass("active");
            var goto_user = jQuery("#user-" + marker.user);
            goto_user.addClass("active");
            if(goto_user){
                var top = goto_user.offset().top;
                top = top - jQuery("header#header").height() - 30;
                $('html, body').animate({
                    scrollTop: top
                }, 1000);
            }
        }
    }
	
	$scope.addMarker = function(marker, number, user){
		if (!number) {
			number=1;
		}
        var marker = new google.maps.Marker({
            map: $scope.search.map,
            position: marker,
            icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+number+'|FF0000|000000',
            user: user
        });
        google.maps.event.addListener(marker, 'click', function(){
            $scope.mapMarkerClick(marker);
        });
        //marker.addListener("click", $scope.mapMarkerClick);
		$scope.markers.push(marker);
	}

    $scope.handleSearchResponse = function(r) {
        console.log(r.data, "locations search result");

        $scope.search.done = true;

        $scope.search.placeList = [];
        $scope.search.placeListBeta = [];
        $scope.search.results = [];
        $scope.search.mobile = [];
        $scope.resetMarkers();
        $scope.trainerList = [];

        // Add home icon
        if($scope.search.geometry) {
            var marker = new google.maps.Marker({
                map: $scope.search.map,
                position: {lat: $scope.search.geometry.lat, lng: $scope.search.geometry.lng},
                icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + 0 + '|0000FF|000000'
            });
        }

        if (r.data.results.length < 1) {
            $scope.search.trainers = {};
            return;
        }

        var bounds = new google.maps.LatLngBounds();

        var location_ids = [];

        r.data.results.forEach(function(place){
            location_ids.push(place.locations.id);
            if (place.locations.type == 'mobile') {
                if($filter('filter')($scope.search.mobile, {users: {id: place.users.id}}).length == 0) { $scope.search.results.push(place); $scope.addUserToArray(place); $scope.search.mobile.push(place); }
                return;
            }
            if ($.inArray(place.locationdbs.placesid, $scope.search.placeList) < 0) {
                $scope.search.placeList.push(place.locationdbs.placesid);
                $scope.search.placeListBeta.push({id: place.locationdbs.placesid, address: place.locationdbs.address, number: 1, lat: parseFloat(place.locationdbs.lat)  , lng: parseFloat(place.locationdbs.lng), user: place.users.id });
            } else {
                $scope.search.placeListBeta[$.inArray(place.locationdbs.placesid, $scope.search.placeList)].number++;
            }
            place[0].distance = Math.round(place[0].distance);
            $scope.search.results.push(place);
            $scope.addUserToArray(place);
        });
        console.log($scope.search.placeListBeta, "place list beta");
        console.log($scope.search, "search object");
        for(i=0;i<$scope.search.placeListBeta.length;i++) {
            $scope.addMarker({lat: $scope.search.placeListBeta[i].lat , lng: $scope.search.placeListBeta[i].lng }, $scope.search.placeListBeta[i].number, $scope.search.placeListBeta[i].user);
            bounds.extend($scope.markers[i].getPosition());
        }

        console.log(location_ids, "location ids");

        console.log("/search/reviewobj/?trainers=" + $scope.trainerList.join(',') + "&locations=" + location_ids.join(",") + "/", "search call");

        // #$#$# todo make these work in 1 call
        // NDJ - functionality to load search results below map rather than in new page
        $http.post("/search/reviewobj/?trainers=" + $scope.trainerList.join(',') + "&locations=" + location_ids.join(",") + "/").then(function(r){
            $scope.searchtemplate = $rs.p + "/search/results.html";
            $scope.search.trainers = r.data.trainers;
            console.log(r, "trainer results");
        });

    }

    $scope.initialShowAll = function(){
        $http.post('/locations/search', {
            lat: 0,
            lon: 0,
            filters: $scope.search.filter,
            distance: $scope.search.distance,
            gender: $scope.search.gender,
            time: {},
            showAll: true
        })
        .then(function(r){
            $scope.handleSearchResponse(r);
        });

    }
	
	$scope.getResults = function(){
		if (!$scope.search.geometry || !$scope.search.distance) {
			return false;		
		}
		
		if (typeof $scope.search.geometry.lat == 'function') {
            $scope.search.geometry.lat = $scope.search.geometry.lat();
            $scope.search.geometry.lng = $scope.search.geometry.lng();
        }
		
		console.log($scope.search, "search info");

        var search_time = angular.copy($scope.search.time);

        if(search_time.time_start) {
            search_time.time_start = search_time.time_start.toLocaleTimeString();
        }
        if(search_time.time_end) {
            search_time.time_end = search_time.time_end.toLocaleTimeString();
        }

		$http.post('/locations/search', {
			lat: $scope.search.geometry.lat,
			lon: $scope.search.geometry.lng,
			filters: $scope.search.filter,
			distance: $scope.search.distance,
            gender: $scope.search.gender,
            time: search_time
		})
		.then(function(r){
            $scope.handleSearchResponse(r);
		});

		if ($scope.search.circle) {
			$scope.search.circle.setMap(null);
		}
		
		$scope.search.circle = new google.maps.Circle({
			strokeColor: '#faf125',
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: '#363636',
			fillOpacity: 0.20,
			map: $scope.search.map,
			center: {lat: $scope.search.geometry.lat, lng: $scope.search.geometry.lng},
			radius: $scope.search.distance * 1000
		});

        $scope.search.map.fitBounds($scope.search.circle.getBounds());

	}
	
	$scope.addUserToArray = function(place){
		//console.log($.inArray(place.users.id, $scope.trainerList) > -1);
		if($.inArray(place.users.id, $scope.trainerList) < 0) {
			$scope.trainerList.push(place.users.id);
		}
	}
	
	$scope.search.map = new google.maps.Map(document.getElementById('gmaps'), {
		center: {lat: lat, lng: lon},
		zoom: zoom,
		disableDefaultUI: true,
		zoomControl: true,
		scrollwheel: false,
		useStaticMap: true
	});

    $scope.toggleLocationShow = function(trainer_id) {
        jQuery('.locations-user-' + trainer_id).slideToggle();
        jQuery('.toggler-user-' + trainer_id + ' .fa').toggleClass('fa-angle-down');
        jQuery('.toggler-user-' + trainer_id + ' .fa').toggleClass('fa-angle-up');
    }

    $scope.initialShowAll();

});