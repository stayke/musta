app.controller('viewprofile', function($scope, $http, $rootScope) {
	$rs = $rootScope;
    $scope.currentOne = -1;
	
	$scope.parse = function(val) {
		var result = "Not found",
			tmp = [];
		location.search
		//.replace ( "?", "" ) 
		// this is better, there might be a question mark inside
		.substr(1)
			.split("&")
			.forEach(function (item) {
			tmp = item.split("=");
			if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
		});
		result = result.replace('/', '')
		result = result.replace('/', '')
		return result;
	}
	
	$scope.loadOthers = function(){
		users = $scope.parse('trainers').split(',');
		users.splice(0,1);
		$http.post('/users/getUsers/', {users: users})
		.then(function(r){
			$scope.others = r.data;
		});
	}
	
	$scope.$watch('currU', function(n,o){
		if(!n) return;
		$scope.userView();
		
	});
	
	$scope.userView = function(){
		$http.post('/users/pageview', { uid: $scope.currU })
	}
	
	Draggable.create("#tinder", {
		type:"x",
			edgeResistance:0.65,
			throwProps:true,
			onRelease: function(endValue) {
				if (this.x > (window.innerWidth / 4)) {
					TweenMax.to(this.target, 1, {x: window.innerWidth + 20  });
					$(this.target).find('.addToFave').click();
					$scope.callNext();
				} else if (this.x < 0-(window.innerWidth / 4)) {
					TweenMax.to(this.target, 1, {x: 0 - (window.innerWidth + $(this.target).outerWidth(true))  });
					$scope.callNext();
				} else {
					TweenMax.to(this.target, 1, {x: 0});
				}
			}
	});
	
	$scope.callNext = function(){
		$scope.currentOne++;
		$scope.currU = $scope.others[$scope.currentOne].User.id;
		$scope.$digest();
		TweenMax.set('#tinder-' + $scope.currentOne, {zIndex: 5});
		TweenMax.to('#tinder-' + $scope.currentOne, 1, {scale: 1});
		if ($scope.currentOne >= $scope.others.length) {
            return false;
        }
		Draggable.create('#tinder-' + $scope.currentOne, {
			type:"x",
			edgeResistance:0.65,
			throwProps:true,
			onRelease: function(endValue) {
				if (this.x > (window.innerWidth / 4)) {
					TweenMax.to(this.target, 1, {x: window.innerWidth + 20  });
					$(this.target).find('.addToFave').click();
					$scope.callNext();
				} else if (this.x < 0-(window.innerWidth / 4)) {
					TweenMax.to(this.target, 1, {x: 0 - (window.innerWidth + $(this.target).outerWidth(true))  });
					$scope.callNext();
				} else {
					TweenMax.to(this.target, 1, {x: 0});
				}
			}
		});
	}
	
	$scope.$on('modalOpen', function(){
		$scope.openModal = true;
	});
	
	$scope.$on('modalClose', function(){
		$scope.openModal = false;
	});
	
	$scope.loadOthers();
	
});