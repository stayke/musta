/**
 * Created by Nicholas.Jephcott on 7/07/2016.
 */
app.controller('parq', function($scope, $http, $rootScope) {
    $rs = $rootScope;

    $scope.data = {};
    $scope.parqid = 0;
    $scope.trainer_access = false;

    $scope.loadParq = function(userid){
        $scope.parqid = userid;
        $http.post('/parq/view/' + userid).then(function(r){
            $scope.data = r.data.parq;
            $scope.trainer_access = r.data.trainer_access;
            console.log(r, "loaded Parq");
        });
    }

    $scope.saveParq = function(){
        console.log($scope.data, "parq save");
        $http.post('/parq/save',  {data: $scope.data, id: $scope.parqid })
            .then(function(r){
                console.log(r, "parq save result");
                $rs.goBack();
            });
    }

    $rs.$on('returnParqDetails', function(e,parqid){
        $scope.loadParq(parqid);
    });

    $rs.$broadcast('requestParqDetails');

});






