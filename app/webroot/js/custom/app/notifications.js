app.controller('notifications', function($scope, $http, $rootScope, $sce) {
	$rs = $rootScope;
	
	
	$scope.approve = function(item){
        console.log(item, "approve notification item");
		$i = item.n.Notification;
		
        if ($i.type == 'paymentRequest') {

            $rs.makePayment({id: "OT" + $i.target});
            /*
            $http.get('/payments/getRequest/' + $i.target)
            .then(function(r){

                $item = r.data.PendingPayment;

                info = {
                    items: [
                        {
                            title: $item.notes,
                            price: $item.price,
                        }
                    ],
                    title: 'Payment Request',
                    gst: $item.gst,
                    gstAmount: ($item.gst) ? $item.price*0.1 : 0,
                    bookingFee: $item.fee,
                    total: $item.total,
                    itemTotal: $item.price,
                    type: 'pending_payment',
                    target: $i.target,
                    terms: $sce.trustAsHtml(r.data.terms),
                    additional: {
                        viewing: $item.trainer_id,
                        notification_id: $i.id,
                        pending_payment_id: $i.target
                    }
                }

                console.log(info);

                $rs.$broadcast('timeToPay', info);

            });
            */

        } else if ($i.type == 'information') {
            $http.get('/notifications/read/' + $i.id)
            .then(function(){
                console.log($i);
                $rs.checkNotifications();
                if ($rs.notifications.length ==0) {
                    $rs.goBack();
                }
            });
        } else if ($i.type == 'repeatPaymentRequest') {

            $rs.makePayment({id: "REC" + $i.target});
            //
            //$http.get('/payments/getRepeatRequest/' + $i.target)
            //.then(function(r){
            //
            //    $item = r.data.RepeatPayment;
            //
            //    info = {
            //        items: [
            //            {
            //                title: $item.notes,
            //                price: $item.price,
            //            }
            //        ],
            //        title: 'Repeat Payment Request',
            //        gst: $item.gst,
            //        gstAmount: ($item.gst) ? $item.price*0.1 : 0,
            //        bookingFee: $item.fee,
            //        total: $item.total,
            //        itemTotal: $item.price,
            //        type: 'recurring_payment',
            //        additional: {
            //            viewing: $item.trainer_id,
            //            notification_id: $i.id,
            //            pending_payment_id: $i.target
            //        },
            //        repeating: $item.repeating,
            //        frequency: $item.frequency,
            //        target: $i.target
            //    }
            //
            //    console.log(info);
            //
            //    $rs.$broadcast('timeToPay', info);
            //
            //});



        }
	}
	
	$scope.deny = function(item){
		console.log(item);
	}
	
});