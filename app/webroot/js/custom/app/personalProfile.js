app.controller('personalProfile', function($scope, $http, $rootScope, $filter) {
	$rs = $rootScope;
	$scope.data = {};
    $scope.show_errors = false;
	
	if ($scope.user.id) {
        $scope.data = {
			id: $scope.user.id,
			firstName: $scope.user.first_name,
			lastName: $scope.user.second_name,
			add1: $scope.user.address1,
			add2: $scope.user.address2,
			suburb: $scope.user.suburb,
			postcode: $scope.user.postcode,
			state: $scope.user.state,
			country: 'au',
			phone: $scope.user.phone,
			email: $scope.user.email,
			gender: $scope.user.gender,
			dob: new Date($scope.user.dob),
			businessname: $scope.user.businessname,
			abn: $scope.user.abn,
			fregtype: $scope.user.fregistertype,
			freg: $scope.user.fregisternumber,
			bname: $scope.user.businessname
		}
		
		if ($scope.user.licence_front) {
			if (!$scope.data.licence) {
                $scope.data.licence = {};
            }
			front = $scope.user.licence_front.split('.');
			console.log(front);
			$scope.data.licence.front = {
					url: '/users/viewUserImage/' + front[0],
					ext: front[1],
					id: false
				}
		}
		
		if ($scope.user.licence_back) {
			if (!$scope.data.licence) {
                $scope.data.licence = {};
            }
			front = $scope.user.licence_back.split('.');
			console.log(front);
			$scope.data.licence.back = {
					url: '/users/viewUserImage/' + front[0],
					ext: front[1],
					id: false
				}
		}
		if ($scope.user.bankstatement_pic) {
            var bs_pic = $scope.user.bankstatement_pic.split('.');
			console.log(bs_pic, "bank statement pic");
			$scope.data.bankstatement_pic = {
					url: '/users/viewUserImage/' + bs_pic[0],
					ext: bs_pic[1],
					id: false
				}
		}

		console.log($scope.user);
    }
   
    $scope.addedImg = function(r, type){
		if (type == 'clientpic') {
            $scope.data.clientpic = {
				url: '/signup/viewTempImage/' + r.data.data.secure,
				ext: r.data.data.ext,
				id: r.data.data.id
			}
        } else if(type == "bankstatement"){
            $scope.data.bankstatement_pic = {
                url: '/signup/viewTempImage/' + r.data.data.secure,
                ext: r.data.data.ext,
                id: r.data.data.id
            }
        } else {
			 $scope.data.licence[type] = {
				url: '/signup/viewTempImage/' + r.data.data.secure,
				ext: r.data.data.ext,
				id: r.data.data.id
			}
            console.log($scope.data.licence, "Scope license");
		}

    }

    $scope.$on('retrieveDataForSignup', function(e, o){
        console.log('recieved: retrieveDataForSignup');
		
		// Validate
		if (o.validate) {
			console.log($scope.myForm.$valid);
            if (!$scope.myForm.$valid) {
                $scope.show_errors = true;
                console.log($scope.myForm);
                alert('Please fill in all fields');
				return false;
            } else {
				if ($scope.data.password != $scope.data.passwordConfirm) {
                    alert('Passwords do not match');
					return false;
                }
			}
        }
		
		$http.post('/users/checkemail', {email: $scope.data.email})
		.then(function(r){
            console.log(r, "email check");
			if (r.data.exists && !r.data.user_id) {
                alert('Email is already registered');
            } else {
                if(r.data.user_id){
                    $scope.data.id = r.data.user_id;
                }
				$rs.$broadcast('returnDataForSignup', $scope.data);
				$rs.$broadcast('formIsValid', o);
			}
		});

    });
    
    $scope.$on('hereIsSomeDataForYou', function(e, o){
        console.log('recieved: hereIsSomeDataForYou');
        $scope.data = o;
		if (!$scope.data.dob) {
             $scope.data.dob  = new Date('01/01/1960')
        }
    });
    
    $rs.$broadcast('canIhaveSomeDataPlease');
    console.log('sent: canIhaveSomeDataPlease');

    if (!$scope.data.licence) {
        $scope.data.licence = {
            front: {
                url: '/img/fol',
                ext: 'jpg',
                id: false
            },
            back: {
                url: '/img/bol',
                ext: 'jpg',
                id: false
            }
        }
    }
	
	if (!$scope.data.clientpic) {
        $scope.data.clientpic = {
			url: '/img/userblank',
			ext: 'jpg',
			id: false
        }
    }

	if (!$scope.data.bankstatement_pic) {
        $scope.data.bankstatement_pic = {
			url: '/img/bankstatementUpload',
			ext: 'jpg',
			id: false
        }
    }
	
	$scope.savePersonal = function(){
		console.log($scope.myForm.$valid);
            if (!$scope.myForm.$valid) {
                $scope.show_errors = true;
                console.log($scope.myForm);
                alert('Please fill in all fields');
				return false;
            } else {
				if ($scope.data.password != $scope.data.passwordConfirm) {
                    alert('Passwords do not match');
					return false;
                }
			}
		console.log($scope.data);
		$http.post('/users/savepersonal',  $scope.data )
		.then(function(r){
            console.log(r, "personal save response");
			$rs.getUser();
			$rs.goBack();
		});
	}

});