app.controller('yourTCs', function($scope, $http, $rootScope) {
	$rs = $rootScope;
    
     $scope.$on('retrieveDataForSignup', function(e, o){
        console.log('recieved: retrieveDataForSignup');
		
		// Validate
		if (o.validate) {
			console.log($scope.myForm.$valid);
            if (!$scope.myForm.$valid) {
                alert('Please fill in all fields');
				return false;
            } else {
				
				console.log(o);
			}
        }
		
		$rs.$broadcast('returnDataForSignup', $scope.terms);
		$rs.$broadcast('formIsValid', o);

    });
    
    $scope.$on('hereIsSomeDataForYou', function(e, o){
        console.log('recieved: hereIsSomeDataForYou');
        $scope.terms = o;
    });
	
	$scope.saveTandC = function(terms){
		console.log(terms);
		
		$http.post('/Users/saveDefaultTerms', {terms: terms});
		$rs.goBack();
	}
	
	$scope.$on('defaultTandCs', function(a,b){
		$scope.terms = b;
	});
    
    $rs.$broadcast('canIhaveSomeDataPlease');
    console.log('sent: canIhaveSomeDataPlease');
});