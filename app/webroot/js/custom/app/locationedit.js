app.controller('locationedit', function($scope, $http, $rootScope, $timeout, $filter) {
    $rs = $rootScope;
    $scope.marker = {lat: false, lon: false, object: false}
    $scope.place = false;
    $scope.loc_saving = false;
    $scope.cur_location =
    {
        gmaps: false,
        type: 'gym',
        Deals: [{id: 0,deal_text: ""}],
        pricing: {
            bc: [{"name":"","fee":"","sessions":""}],
            pt: {
                firstSession: {
                    active: true,
                    fee: 0
                },
                thirtymin: {
                    active: false,
                    fee: 0,
                    sgtactive: false,
                    sgtfee: 0
                },
                fourtyfivemin: {
                    active: false,
                    fee: 0,
                    sgtactive: false,
                    sgtfee: 0
                },
                sixtymin: {
                    active: false,
                    fee: 0,
                    sgtactive: false,
                    sgtfee: 0
                }
            }
        }
    };


    $scope.addMapShow = function(){
        $rs.getLocations();
        var markers = [];
        if ($rs.userLocation) {
            lat = $rs.userLocation.coords.latitude;
            lon = $rs.userLocation.coords.longitude;
            zoom = 14;
        } else {
            lat = -27.1861734;
            lon = 134.9509604;
            zoom = 4;
        }

        $scope.map = new google.maps.Map(document.getElementById('addMap'), {
            center: {lat: lat, lng: lon},
            zoom: zoom,
            disableDefaultUI: true,
            zoomControl: true,
            scrollwheel: false,
            useStaticMap: true
        });
        var wrapper = document.getElementById('pac-wrapper');
        var input = document.getElementById('pac-input');
        $scope.Autocomplete = new google.maps.places.Autocomplete(input, {componentRestrictions: {country: "au"}});
        //$scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(wrapper);

        $scope.Autocomplete.addListener('place_changed', function() {
            console.log($rs.userLocations);
            var place = $scope.Autocomplete.getPlace();
            console.log(place);

            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();

            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: $scope.map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }

            $scope.selectOrAdd(place);
            $scope.map.fitBounds(bounds);
            $scope.map.setZoom(18);
            $scope.$digest();
        });
    }

    $scope.saveLocation = function(){

        $scope.error = '';

        addSave = [];

        for(var i=0;i<$scope.cur_location.gmaps.length;i++){
            addSave.push({ address: $scope.cur_location.gmaps[i].formatted_address });
        }

        if (!$scope.cur_location.event) {

            error = false;

            if (
                !$scope.cur_location.pricing.pt
                || (
                !$scope.cur_location.pricing.pt.thirtymin.active
                && !$scope.cur_location.pricing.pt.fourtyfivemin.active
                && !$scope.cur_location.pricing.pt.sixtymin.active
                )
            ) {
                if(!$scope.cur_location.event) {
                    $scope.error = 'Please select at least one pricing tier. ';
                    error = true;
                }
            }

            if (
                ($scope.cur_location.pricing.pt.firstSession.active && $scope.cur_location.pricing.pt.firstSession.fee === '')
                || ($scope.cur_location.pricing.pt.thirtymin.active && !$scope.cur_location.pricing.pt.thirtymin.fee === '')
                || ($scope.cur_location.pricing.pt.fourtyfivemin.active && !$scope.cur_location.pricing.pt.fourtyfivemin.fee === '')
                || ($scope.cur_location.pricing.pt.sixtymin.active && !$scope.cur_location.pricing.pt.sixtymin.fee === '')

                || ($scope.cur_location.pricing.pt.thirtymin.sgtactive && !$scope.cur_location.pricing.pt.thirtymin.sgtfee === '')
                || ($scope.cur_location.pricing.pt.fourtyfivemin.sgtactive && !$scope.cur_location.pricing.pt.fourtyfivemin.sgtfee === '')
                || ($scope.cur_location.pricing.pt.sixtymin.sgtactive && !$scope.cur_location.pricing.pt.sixtymin.sgtfee === '')
            ) {
                error = true;
                $scope.error += 'Please fill in all fields for active pricing tiers.';
            }
            if (error) {
                return false;
            }

        } else if ($scope.cur_location.event) {
            error = false;
            for (var i=0;i<$scope.cur_location.pricing.bc.length;i++) {
                var entry = $scope.cur_location.pricing.bc[i];
                if (!entry.name || entry.name.length<3) {
                    error = true;
                }

                if (!entry.fee || entry.fee < 1) {
                    error = true;
                }
            }
            if (error) {
                $scope.error = 'Please fill out all pricing fields';
                return false;
            }
        }

        locationSave = {
            id: $scope.cur_location.id,
            locations: $scope.cur_location.gmaps,
            name: $scope.cur_location.name,
            notes: $scope.cur_location.notes,
            Deals: $scope.cur_location.Deals,
            type: $scope.cur_location.type,
            prices: ($scope.cur_location.event) ? $scope.cur_location.pricing.bc : $scope.cur_location.pricing.pt,
            gst: $scope.cur_location.pricing.gst,
            event: $scope.cur_location.event,
            //image: ($scope.locimg.id) ? $scope.locimg : false,
            role: ($scope.user.role) ? $scope.user.role : $rs.tempRole,
            Location: {
                name: $scope.cur_location.name,
                type: $scope.cur_location.type,
                color: $scope.cur_location.colour
            },
            Addresses: addSave,
            participants: ($scope.cur_location.limitblock) ? 0 : $scope.cur_location.participants
        };

        for(var i=0;i<locationSave.locations.length;i++){
            if(!locationSave.locations[i].id) {
                console.log(locationSave.locations[i].geometry.lat);
                locationSave.locations[i].geometry.location = {
                    lat: locationSave.locations[i].geometry.location.lat(),
                    lng: locationSave.locations[i].geometry.location.lng()
                }
            }
        }

        console.log(locationSave, "location save");

        $scope.loc_saving = true;
        $http.post('/locations/add',
            locationSave
        )
        .then(function(response){
            $scope.loc_saving = false;
            $rs.getLocations();
            console.log($rs.userLocations, "User Locations");
            console.log(response, "Save response");
            $rs.$broadcast('newLocation');
            $rs.goBack();
        });
    }

    $scope.editLocation = function(location){
        console.log(location, "editing location");
        console.log($scope.cur_location, "new location");
    }

    $scope.selectOrAdd = function(place){

        if ($scope.cur_location.type == 'mobile') {
            if (!$scope.containsObject(place, $scope.cur_location.gmaps)) {
                $scope.cur_location.gmaps.push(place);
            }
        } else {
            type = ['(geocode)', '(establishment)'];
            $scope.cur_location.gmaps = [place];
        }

        $scope.place = false;
        $scope.mapSearch = '';

    }

    $scope.$watch('cur_location.type', function(n,o){
        if (n===o) {
            return false;
        }

        $timeout(function(){google.maps.event.trigger($scope.map, "resize")}, 200);

        if (n == 'mobile') {
            type = ['(cities)'];
        } else {
            type = ['geocode', 'establishment'];
        }

        $scope.cur_location.gmaps = [];
        $scope.Autocomplete.setTypes(type);
    });

    $scope.containsObject = function(obj, list) {
        var i;
        for (i = 0; i < list.length; i++) {
            if (list[i].formatted_address === obj.formatted_address) {
                return true;
            }
        }
        return false;
    }

    $rs.$on('returnLocationDetails', function(e,locationid){
        $http.post('/locations/getLocation/', {id: locationid}).then(function(r){
            console.log(r, "get location");
            if(r.data.data) {
                $scope.cur_location = r.data.data.Location;

                // Process the standard fields to match the format in this form
                $scope.cur_location.colour = $scope.cur_location.color;
                $scope.cur_location.gmaps = [];
                for(i = 0; i< r.data.data.Addresses.length; i++) {
                    $scope.cur_location.gmaps[i] = r.data.data.Addresses[i];
                    $scope.cur_location.gmaps[i].formatted_address = r.data.data.Addresses[i].address;
                    $scope.cur_location.gmaps[i].place_id = r.data.data.Addresses[i].placesid;
                }
                $scope.cur_location.Deals = [{id: 0,deal_text: ""}];
                $scope.cur_location.pricing = {
                    bc: [{"name":"","fee":"","sessions":""}],
                    pt: {
                        firstSession: {
                            active: false,
                                fee: 0
                        },
                        thirtymin: {
                            active: false,
                                fee: 0,
                                sgtactive: false,
                                sgtfee: 0
                        },
                        fourtyfivemin: {
                            active: false,
                                fee: 0,
                                sgtactive: false,
                                sgtfee: 0
                        },
                        sixtymin: {
                            active: false,
                                fee: 0,
                                sgtactive: false,
                                sgtfee: 0
                        }
                    }
                };
                for(i = 0; i< r.data.data.PtPricing.length; i++) {
                    if(r.data.data.PtPricing[i].first_only) {
                        $scope.cur_location.pricing.pt.firstSession = {
                            id: r.data.data.PtPricing[i].id,
                            active: true,
                            fee: parseFloat(r.data.data.PtPricing[i].price)
                        };
                    } else {
                        var temp_pricing = {
                            id: r.data.data.PtPricing[i].id,
                            active: true,
                            fee: parseFloat(r.data.data.PtPricing[i].price),
                            sgtactive: r.data.data.PtPricing[i].sgt,
                            sgtfee: parseFloat(r.data.data.PtPricing[i].sgt_fee)
                        };
                        if(r.data.data.PtPricing[i].session_length == "30") {
                            $scope.cur_location.pricing.pt.thirtymin = temp_pricing;
                        }
                        if(r.data.data.PtPricing[i].session_length == "45") {
                            $scope.cur_location.pricing.pt.fourtyfivemin = temp_pricing;
                        }
                        if(r.data.data.PtPricing[i].session_length == "60") {
                            $scope.cur_location.pricing.pt.sixtymin = temp_pricing;
                        }
                    }
                }
                for(i = 0; i< r.data.data.bcPricing.length; i++) {
                    var temp_pricing = {
                        id: r.data.data.bcPricing[i].id,
                        name: r.data.data.bcPricing[i].name,
                        fee: parseFloat(r.data.data.bcPricing[i].price)
                    };
                    $scope.cur_location.pricing.bc[i] = temp_pricing;
                }
                for(i = 0; i< r.data.data.Deals.length; i++) {
                    var temp_deal = {
                        id: r.data.data.Deals[i].id,
                        deal_text: r.data.data.Deals[i].deal_text
                    };
                    $scope.cur_location.Deals[i] = temp_deal;
                }

                console.log($scope.cur_location, "loaded location");
            }
        });
    });

    $rs.$broadcast('requestLocationDetails');

});