app.controller('mustaterms', function($scope, $http, $rootScope) {
	$rs = $rootScope;
	
	
	console.log($scope.data);
    
     $scope.$on('retrieveDataForSignup', function(e, o){
        console.log('recieved: retrieveDataForSignup');
		
		// Validate
		if (o.validate) {
            if (!$scope.myForm.$valid) {
                alert('Please accept Mustas Terms and Conditions to continue');
				return false;
            } else {
				if ($('#captcha').length > 0) {
					recap = $('#g-recaptcha-response').val();
					$('#g-recaptcha-response').remove();
                    $http.post('/signup/verify', { 'g-recaptcha-response': recap }).
					then(function(r){
						if (r.data.success) {
                            $rs.$broadcast('returnDataForSignup', $scope.data);
							$rs.$broadcast('formIsValid', o);
                        } else {
							alert('Captcha Failed. Please retry');
							return false;
						}
					});
                } else {
					$rs.$broadcast('returnDataForSignup', $scope.data);
					$rs.$broadcast('formIsValid', o);
				}
			}
        } else {
			$rs.$broadcast('returnDataForSignup', $scope.data);
			$rs.$broadcast('formIsValid', o);
		}
		
        

    });
    
    $scope.$on('hereIsSomeDataForYou', function(e, o){
        console.log('recieved: hereIsSomeDataForYou');
		
        $scope.data = o;
    });
	
    console.log('sent: canIhaveSomeDataPlease');
    $rs.$broadcast('canIhaveSomeDataPlease');
    
});