/**
 * Created by Nicholas.Jephcott on 8/06/2016.
 */
app.controller('eventattend', function($scope, $http, $rootScope, $filter) {
    $rs = $rootScope;
    $scope.error_messages = false;
    $scope.no_spaces = false;
    $scope.event = {
        date: new Date(),
        selectedClients: new Array()
    };
    $scope.attendance = {
        status: false
    }

    $scope.registration = {
        num_sessions: 1,
        frequency_pay: "one_time"
    }

    $scope.resetAttendance = function(){
        $scope.attendance = {
            status: false
        }
        $scope.registration = {
            num_sessions: 1,
            frequency_pay: "one_time"
        }
    }

    $rs.$on('returnEventDetails', function(e,deets){
        if(deets.id) {
            console.log(deets, "event attend details");
            $scope.loadEvent(deets.id);
        } else {
            console.log(deets, "event attend details 2");
            alert("could not find the event you where trying to attend");
        }
    });

    $scope.loadEvent = function(instanceid){
        // NDJ - #$#$# todo add more error handling to request
        $http.post('/event/getEventInstanceAttendee/' + instanceid).then(function(r){
            console.log(instanceid);
            console.log(r, "loaded event instance attendee");
            // r.data.event.date_start = $rs.dateFormatForInput(r.data.event.date_start);
            // r.data.event.date_start = new Date(r.data.event.date_start);
            // r.data.event.date_start = $rs.dateFormatForInput("05/14/2016");
            // r.data.event.date_end = $rs.dateFormatForInput(r.data.event.date_end);
            //if(r.data.event.date) {
            //    r.data.event.date = new Date(r.data.event.date);
            //}
            $scope.instance = r.data.instance; // NDJ - #$#$# todo move these between individually
            if($scope.instance.date) {
                $scope.instance.date = new Date($scope.instance.date);
            }
            if($scope.instance.time_start) {
                var date_string = $scope.instance.date.getFullYear() + "-" + $scope.instance.date.getMonth() + "-" + $scope.instance.date.getDate() + " " + $scope.instance.time_start;
                $scope.instance.time_start = new Date(date_string);
            }
            if(r.data.inst_pricing.id){
                $scope.inst_pricing = r.data.inst_pricing;
                $scope.instance.title = $scope.inst_pricing.name;
                $scope.instance.price = $scope.inst_pricing.price;
            }
            $scope.event = r.data.event; // NDJ - #$#$# todo move these between individually
            if($scope.event) {
                if(!$scope.instance.title) {
                    $scope.instance.title = $scope.event.title;
                }
                $scope.event.days = $scope.event.day;
                if ($scope.event.time_start) {
                    var date_string = $scope.instance.date.getFullYear() + "-" + $scope.instance.date.getMonth() + "-" + $scope.instance.date.getDate() + " " + $scope.event.time_start;
                    $scope.event.time_start = new Date(date_string);
                }
                if ($scope.event.date_start) {
                    $scope.event.date_start = new Date($scope.event.date_start);
                }
                if ($scope.event.date_end) {
                    $scope.event.date_end = new Date($scope.event.date_end);
                }
            }
            $scope.resetAttendance();
            $scope.no_spaces = false;
            if(r.data.attendance){
                $scope.attendance = r.data.attendance;
                console.log($scope.attendance, "loaded attendance");
            } else {
                var open_slots = $scope.instance.spaces - $scope.instance.attendance_count - $scope.instance.spaces_unusable;
                if(!(open_slots > 0)){
                    $scope.no_spaces = true;
                }
            }
            if(r.data.registration){
                $scope.registration = r.data.registration;
                console.log($scope.registration, "loaded registration");
            }
            console.log($scope.instance, "loaded event instance");
            console.log($scope.event, "loaded event rule");
        });
    }

    $scope.saveAttendee = function(instance, attendance, registration){
        $http.post('/event/saveAttendee/',{instance: instance, attendance: attendance, registration: registration}).then(function(r) {
            console.log(r, "save attendee result");
            if(!r.data.error_messages){
                r.data.error_messages = [];
            }
            if(r.data.error_messages.length){
                $scope.error_messages = r.data.error_messages;
                console.log(r.data.error_messages, "save attendee error messages");
            } else if(r.data.webPageToken){
                $rs.makePayment(r.data);
            }
        });
    }

    $rs.$broadcast('requestEventDetails');
});