app.controller('signup', function($scope, $http, $rootScope) {
	$rs = $rootScope;
	
	$rs.setupSignup = function(){
		$scope.redirect = $rs.signupredirect;
		$rs.percentComplete = 0;
		$scope.controls = $rs.p + 'signup/controls.html';
		$rs.animation = {direction: 'l'};
		$rs.step = 0;
		$scope.accountType = false;
		
		$rs.allSteps = [
			{
				role: 1,
				steps: [
					{
						id: 1,
						name: 'personal',
						template: $rs.p + 'signup/pt/personal.html',
						data: {}
					},
					{
						id:2,
						name: 'profile',
						template: $rs.p + 'profile/edit.html',
						data: {}
					},
					{
						id:3,
						name: 'locations',
						template: $rs.p + 'locations/list.html',
						data: {}
					},
					//{
					//	id:4,
					//	name: 'terms',
					//	template: $rs.p + 'signup/pt/terms.html',
					//	data: {}
					//},
					//{
					//	id:4,
					//	name: 'bankdetails',
					//	template: $rs.p + 'pay/bankdetails.html',
					//	data: {}
					//},
					{
						id:4,
						name: 'mustaterms',
						template: $rs.p + 'signup/pt/mustaterms.html',
						data: {}
					}
				]
			},
			{
				role: 2,
				steps: [
					{
						id: 1,
						name: 'personal',
						template: $rs.p + 'signup/pt/personal.html',
						data: {}
					},
					{
						id:2,
						name: 'profile',
						template: $rs.p + 'profile/edit.html',
						data: {}
					},
					{
						id:3,
						name: 'locations',
						template: $rs.p + 'locations/list.html',
						data: {}
					},
					//{
					//	id:4,
					//	name: 'terms',
					//	template: $rs.p + 'signup/pt/terms.html',
					//	data: {}
					//},
					//{
					//	id:4,
					//	name: 'bankdetails',
					//	template: $rs.p + 'pay/bankdetails.html',
					//	data: {}
					//},
					{
						id:4,
						name: 'mustaterms',
						template: $rs.p + 'signup/pt/mustaterms.html',
						data: {}
					}
				]
			},
			{
				role: 3,
				steps: [
					{
						id: 1,
						name: 'personal',
						template: $rs.p + 'signup/client/personal.html',
						data: {}
					},
					{
						id:6,
						name: 'mustaterms',
						template: $rs.p + 'signup/client/mustaterms.html',
						data: {}
					}
				]
			}
		];

        if($rs.start_at_trainer){
            $scope.accountType = 1;
            $scope.nextStep();
        }
	}
	
	
	
	
	$scope.nextStep = function(){
		console.log('sent: retrieveDataForSignup');
		$rs.$broadcast('retrieveDataForSignup', { step: $scope.step+1, direction: 'l', validate: true });
		$rs.tempRole = $scope.accountType;
		if ($scope.step == 0) {
            $rs.$broadcast('formIsValid', {step: $scope.step+1, direction: 'l'});
        }
	}
	
	$scope.previousStep = function(){
		console.log('sent: retrieveDataForSignup');
		$rs.$broadcast('retrieveDataForSignup', { step: $scope.step-1, direction: 'r', validate: false });
		$rs.tempRole = $scope.accountType;
	}
	
	$scope.$on('formIsValid', function(e,o){
		$rs.animation.direction = o.direction;
		
		if (o.step > $rs.allSteps[$scope.accountType-1].steps.length) {
            $scope.submitSignup();
			return true;
        }
		
		$rs.step = o.step;
	});

	$scope.$watch('accountType', function(newValue, oldValue){
		console.log(newValue)
		if (!newValue) {
			$rs.steps = [];
			return false;
		}
		$rs.steps = $rs.allSteps[$scope.accountType-1].steps;
	});
	
	$rs.$watch('step', function(newValue, oldValue){
		if (!$rs.steps) {
			$rs.percentComplete = 0;
            return true;
        }
		TweenMax.to('#bottomSlider', 0.6,{scrollTop: 0});
		$rs.percentComplete = (100/($rs.steps.length)) * $rs.step;
	});
	
	$scope.$on('resetSignup', function(){
		$rs.setupSignup();
	});
	
	$scope.submitSignup = function(){
		
		$scope.submitting = true;

		$scope.steps.push({
			id:6,
			name: 'submit',
			template: $rs.p + 'signup/submit.html'
		});
		$scope.step++;
		
		$http.post('/signup/signup', { steps: $scope.steps, role: $scope.accountType })
		.then(function(r){

            console.log(r, "signup response");

			if (r.data.error) {
                $scope.emailerror = r.data.error.text;
				$scope.step = 1;
				$rs.tempRole = $scope.accountType;
				return false;
            }
			
			$http.post('/users/login', {email: r.data.user.email, password: r.data.user.password})
			.then(function(response){
			   if (response.data.error) {
				 
			   } else{
				
				if (response.data.user.role == 1 || response.data.user.role == 2) {
					window.location.href='/trainer/';
				} else {
					if ($scope.redirect) {
                        window.location.href='/search/';
                    } else {
						$rs.getUser(function(){ $rs.goBack(); });
					}
					
				}

			}
			});
		});
	}
	
	$scope.$on('returnDataForSignup', function(e,o){
		console.log('recieve: returnDataForSignup');
        console.log(o, "returnDataForSignup o");
        console.log($scope, "returnDataForSignup scope");
        console.log($rs.step, "returnDataForSignup step");
		$scope.steps[$rs.step-1].data = o;
		
	});
	
	$scope.$on('canIhaveSomeDataPlease', function(){
		console.log('recieve: canIhaveSomeDataPlease');
		console.log('sent: hereIsSomeDataForYou');
		$rs.$broadcast('hereIsSomeDataForYou', $scope.steps[$rs.step-1].data);
	});
	
	$rs.setupSignup();
	
});

app.animation('.slideInOut', ['$timeout', '$rootScope' ,function($timeout, $rootScope) {
	return {
	  // make note that other events (like addClass/removeClass)
	  // have different function input parameters
	  enter: function(element, doneFn) {
			if ($rs.animation.direction == 'l') {
				var from = 110;
				var to = 0;
			} else {
				var from = -110;
				var to = 0;
			}
			
			TweenMax.set(element, {xPercent: from, position: 'absolute', opacity: 0});
			TweenMax.to(element,0.6,{ xPercent:to, opacity: 1, onComplete: doneFn });
			$timeout(function(){
				TweenMax.set(element, { position: 'static'});
			}, 600);
			// remember to call doneFn so that angular
			// knows that the animation has concluded
	  },
  
	  leave: function(element, doneFn) {
		
		if ($rs.animation.direction == 'l') {
			var to = -110;
		} else {
			var to = 110; 
		}
		TweenMax.to(element, 0.6,{ xPercent:to, opacity: 0, onComplete: doneFn });
	  }
	}
  }]
);

