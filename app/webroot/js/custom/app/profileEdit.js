app.controller('profileEdit', function($scope, $http, $rootScope) {
	$rs = $rootScope;
	$scope.data = ($rs.user.id) ? $rs.user.profilePage : {};
    
    $scope.addedImg = function(r, type){
        console.log(r);
        $scope.data.pic = {
            url: '/signup/viewTempImage/' + r.data.data.secure,
            ext: r.data.data.ext,
            id: r.data.data.id
        }
		$scope.data.profile_pic = false;
    }
	
	 $scope.$on('retrieveDataForSignup', function(e, o){
        console.log('recieved: retrieveDataForSignup');
		
		// Validate
		if (o.validate) {
			console.log($scope.myForm.$valid);
            if (!$scope.myForm.$valid) {
                alert('Please fill in all fields');
				return false;
            } else {
				
				console.log(o);
			}
        }
		
		$rs.$broadcast('returnDataForSignup', $scope.data);
		$rs.$broadcast('formIsValid', o);

    });
    
    $scope.$on('hereIsSomeDataForYou', function(e, o){
        console.log('recieved: hereIsSomeDataForYou');
        $scope.data = o;
		console.log($scope.data);
    });
    
    $rs.$broadcast('canIhaveSomeDataPlease');
	$rs.$broadcast('populateProfilePage');
	
    console.log('sent: canIhaveSomeDataPlease');
    
     if (!$scope.data.pic) {
        $scope.data.pic = {
            url: '/img/userblank',
            ext: 'jpg',
            id: false
        }
    }
	
	if (!$scope.data.qualifications) {
        $scope.data.qualifications = [{institute: null, qualification: null }];
    }
	
	$scope.countSpeciality = function(){
		var i = 0;
		for (var key in $scope.data.speciality) {
			if ($scope.data.speciality.hasOwnProperty(key)) {
				if($scope.data.speciality[key]){
					i++;
				}
			 }
		 }
		return i;
	}
	
	$scope.saveProfile = function(){
		console.log($scope.data);
		$http.post('/users/saveprofile',  $scope.data )
		.then(function(r){
			$rs.getUser();
			$rs.goBack();
		});
	}
	
});