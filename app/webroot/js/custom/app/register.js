app.controller('register', function($scope, $http, $rootScope, $q, $interval, $filter) {

        $rs = $rootScope;
        
        $scope.$on('giveRegisterID', function(event, data){
            $scope.loading = true;
            $scope.currClass = data;
            $http.get('/calendar/getRegister/' + data.event.timings.id)
            .then(function(data){
                $scope.register.data = data.data;
                $scope.loading = false;
            });
        });
        
        $scope.interval = $interval(function(){
            $(".changeme").bootstrapSwitch({
                onSwitchChange: function(e,s){
                    $scope.changedSession($(this).data('index'), s)
                }
            });   
        }, 500);
        
        $scope.$on('$destroy', function(){
            $interval.cancel($scope.interval); 
        });
        
        $scope.changedSession = function(i,s){
            $item = $scope.register.data[i];
            $item.Sessions.thisused = s;
            if (s) {
                $item.Sessions.used++;
                $item.Sessions.remaining--;
            } else {
                $item.Sessions.used--;
                $item.Sessions.remaining++;
            }
            $http.post('/calendar/saveRegister/' + $scope.currClass.event.timings.id, {used: s, user: $item.User.id} )
            .then(function(data){
                $scope.$broadcast('giveRegisterID', $scope.currClass);
            });
        }
        
        $rs.$broadcast('needRegisterID');
    
});