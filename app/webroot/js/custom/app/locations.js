app.controller('locations', function($scope, $http, $rootScope, $timeout, $filter) {
	$rs = $rootScope;
	$scope.marker = {lat: false, lon: false, object: false}
	$scope.place = false;
    $scope.console = console;
	$scope.addnew =
	{
		gmaps: false,
		type: 'gym',
        Deals: [{id: 0,deal_text: ""}],
		pricing: {
			bc: [{"name":"","fee":"","sessions":""}],
			pt: {
				firstSession: {
					active: true,
					fee: 0
				},
				thirtymin: {
					active: false,
					fee: 0,
					sgtactive: false,
					sgtfee: 0
				},
				fourtyfivemin: {
					active: false,
					fee: 0,
					sgtactive: false,
					sgtfee: 0
				},
				sixtymin: {
					active: false,
					fee: 0,
					sgtactive: false,
					sgtfee: 0
				}
			}
		}
	};
	
	
	$scope.addMapShow = function(){
		$rs.getLocations();
		var markers = [];
		if ($rs.userLocation) {
			lat = $rs.userLocation.coords.latitude;
			lon = $rs.userLocation.coords.longitude;
			zoom = 14
		} else {
			lat = -27.1861734
			lon = 134.9509604
			zoom = 4
		}
	
		$scope.map = new google.maps.Map(document.getElementById('addMap'), {
			center: {lat: lat, lng: lon},
			zoom: zoom,
			disableDefaultUI: true,
			zoomControl: true,
			scrollwheel: false,
			useStaticMap: true
		});
		var wrapper = document.getElementById('pac-wrapper');
		var input = document.getElementById('pac-input');
		$scope.Autocomplete = new google.maps.places.Autocomplete(input, {componentRestrictions: {country: "au"}});
        //$scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(wrapper);
		
		
		$scope.Autocomplete.addListener('place_changed', function() {
			console.log($rs.userLocations);
			var place = $scope.Autocomplete.getPlace();
			console.log(place);
			
			markers.forEach(function(marker) {
				marker.setMap(null);
			  });
			  markers = [];
			
		
			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();
			
			  var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			  };
		
			  // Create a marker for each place.
			  markers.push(new google.maps.Marker({
				map: $scope.map,
				icon: icon,
				title: place.name,
				position: place.geometry.location
			  }));
			  
			  
			  if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			  } else {
				bounds.extend(place.geometry.location);
			  }

			$scope.selectOrAdd(place);
			$scope.map.fitBounds(bounds);
			$scope.map.setZoom(18);
			$scope.$digest();
		  });
	}
	
	$scope.saveLocation = function(){
		
		$scope.error = '';
		
		addSave = [];
		
		for(var i=0;i<$scope.addnew.gmaps.length;i++){
			addSave.push({ address: $scope.addnew.gmaps[i].formatted_address });
		}
		
		if (!$scope.addnew.event) {
			
			error = false;
			
            if (
				!$scope.addnew.pricing.pt
				|| (
					!$scope.addnew.pricing.pt.thirtymin.active
					&& !$scope.addnew.pricing.pt.fourtyfivemin.active
					&& !$scope.addnew.pricing.pt.sixtymin.active
				)
			) {
                $scope.error = 'Please select at least one pricing tier. ';
				error = true;
            }
			
			if (
				($scope.addnew.pricing.pt.firstSession.active && $scope.addnew.pricing.pt.firstSession.fee === '')
				|| ($scope.addnew.pricing.pt.thirtymin.active && !$scope.addnew.pricing.pt.thirtymin.fee === '')
				|| ($scope.addnew.pricing.pt.fourtyfivemin.active && !$scope.addnew.pricing.pt.fourtyfivemin.fee === '')
				|| ($scope.addnew.pricing.pt.sixtymin.active && !$scope.addnew.pricing.pt.sixtymin.fee === '')
				
				|| ($scope.addnew.pricing.pt.thirtymin.sgtactive && !$scope.addnew.pricing.pt.thirtymin.sgtfee === '')
				|| ($scope.addnew.pricing.pt.fourtyfivemin.sgtactive && !$scope.addnew.pricing.pt.fourtyfivemin.sgtfee === '')
				|| ($scope.addnew.pricing.pt.sixtymin.sgtactive && !$scope.addnew.pricing.pt.sixtymin.sgtfee === '')
			) {
                error = true;
				$scope.error += 'Please fill in all fields for active pricing tiers.';
            }
			if (error) {
                return false;
            }
		
        } else if ($scope.addnew.event) {
            for (var i=0;i<$scope.addnew.pricing.bc.length;i++) {
				var entry = $scope.addnew.pricing.bc[i];
				error = false;
                if (!entry.name || entry.name.length<3) {
					error = true;
				}
				
				if (!entry.fee || entry.fee < 1) {
					error = true;
				}
            }
			if (error) {
                console.log($scope.addnew, "add new object");
				$scope.error = 'Please fill out all pricing fields';
				return false;
			}
        }
		
		
		
		locationSave = {
			locations: $scope.addnew.gmaps,
			name: $scope.addnew.name,
			notes: $scope.addnew.notes,
            Deals: $scope.addnew.Deals,
			type: $scope.addnew.type,
            prices: ($scope.addnew.event) ? $scope.addnew.pricing.bc : $scope.addnew.pricing.pt,
            gst: $scope.addnew.pricing.gst,
            image: ($scope.locimg.id) ? $scope.locimg : false,
            role: ($scope.user.role) ? $scope.user.role : $rs.tempRole,
            event: $scope.addnew.event,
            Location: {
				name: $scope.addnew.name,
				type: $scope.addnew.type,
				color: $scope.addnew.colour
			},
			Addresses: addSave,
			participants: ($scope.addnew.limitblock) ? 0 : $scope.addnew.participants
		};
		
		console.log(locationSave);
		
		for(var i=0;i<locationSave.locations.length;i++){
			console.log(locationSave.locations[i].geometry.lat);
			locationSave.locations[i].geometry.location = {lat: locationSave.locations[i].geometry.location.lat() , lng: locationSave.locations[i].geometry.location.lng() }
		}
		

		if (!$rs.user.id) {
			locationSave.Location.id = Math.ceil(new Date() / 1000);
			$rs.userLocations.push(locationSave);
        } else {
			$http.post('/locations/add', 
			locationSave
			)
			.then(function(response){
				$rs.getLocations();
				console.log($rs.userLocations);
				console.log(response);
				$rs.$broadcast('newLocation');
			});
		}

        // Scroll to the list of locations at the bottom
        $('#bottomSlider').animate({
            scrollTop: $("#locations-list").offset().top
        }, 500);
		
		$scope.mapSearch = '';
		$scope.addnew.name = '';
		$scope.addnew.notes = '';
		$scope.addnew.deals = '';
		$scope.locimg = {
			url: '/img/locationimage',
			ext: 'jpg',
			id: false
		};
		$scope.place = false;
		$scope.addnew =
		{
			gmaps: false,
			type: 'gym',
            Deals: [{id: 0,deal_text: ""}],
			pricing: {
				bc: [{"name":"","fee":"","sessions":""}],
				pt: {
					firstSession: {
						active: true,
						fee: 0
					},
					thirtymin: {
						active: false,
						fee: 0,
						sgtactive: false,
						sgtfee: 0
					},
					fourtyfivemin: {
						active: false,
						fee: 0,
						sgtactive: false,
						sgtfee: 0
					},
					sixtymin: {
						active: false,
						fee: 0,
						sgtactive: false,
						sgtfee: 0
					}
				}
			}
		};
		
	}

    $scope.editLocation = function(location){
        console.log(location, "editing location");
        console.log($scope.addnew, "new location");
        $rs.editLocationById(location.Location.id);
    }
	
	$scope.deleteLocation = function(locationid){
		if (!$rs.user.id) {
			$rs.userLocations = $filter('filter')($rs.userLocations, {Location: {id: '!' + locationid}});
			return true;
		}
		$http.get('/locations/delete/' + locationid)
		.then(function(response){
			$rs.getLocations();
			console.log(response);
			$rs.$broadcast('newLocation');
		});
	}
	

	$scope.$on('retrieveDataForSignup', function(e, o){
        console.log('recieved: retrieveDataForSignup');
		console.log('sent: returnDataForSignup');
		
		if ($rs.userLocations.length < 1) {
            $scope.error = 'You must have at least one location added.';
			return false;
        }
		
        $rs.$broadcast('returnDataForSignup', $rs.userLocations);
		$rs.$broadcast('formIsValid', o);
    });
	
	
	$scope.$watch('addnew.participants', function(n,o){
		console.log(n,o);
		if(n>99){
			$scope.addnew.participants = 99;	
		}
	});
	
	
	$scope.selectOrAdd = function(place){
		
		if ($scope.addnew.type == 'mobile') {
			if (!$scope.containsObject(place, $scope.addnew.gmaps)) {
				$scope.addnew.gmaps.push(place);
            }
        } else {
			type = ['(geocode)', '(establishment)'];
			$scope.addnew.gmaps = [place];
		}
		
		$scope.place = false;
		$scope.mapSearch = '';

	}
	
	$scope.$watch('addnew.type', function(n,o){
		if (n===o) {
            return false;
        }
		
		$timeout(function(){google.maps.event.trigger($scope.map, "resize")}, 200);
		
		if (n == 'mobile') {
            type = ['(cities)'];
        } else {
			type = ['geocode', 'establishment'];
		}
		
		$scope.addnew.gmaps = [];
		$scope.Autocomplete.setTypes(type);
	});
	
	$scope.containsObject = function(obj, list) {
		var i;
		for (i = 0; i < list.length; i++) {
			if (list[i].formatted_address === obj.formatted_address) {
				return true;
			}
		}
	
		return false;
	}
	
	$scope.addedImg = function(r, type){
        console.log(r);
        $scope.locimg = {
            url: '/signup/viewTempImage/' + r.data.data.secure,
            ext: r.data.data.ext,
            id: r.data.data.id
        }

    }
	
	$scope.locimg = {
			url: '/img/locationimage',
			ext: 'jpg',
			id: false
	}
	
	
	
});