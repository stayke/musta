app.controller('paymentSection', function($scope, $http, $rootScope, $filter) {
    $rs = $rootScope;    
    //$scope.cardLimit = 10;
    $scope.activePanel = 'clients';
    $scope.period = '7';
    $scope.addSomething = false;
    $scope.cardGridOptions = {
        enableSorting: true,
        enableFiltering: true,
        rowHeight: 50,
        columnDefs: [
            { name:'pic', field: 'Customer.User.clientpic', enableHiding: false, enableFiltering: false, enableSorting: false,
            cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP"><img ng-src="{{(COL_FIELD) ? \'/users/viewUserImage/\' + COL_FIELD : \'/img/userblank.jpg\'}}" class="img-responsive" style="max-width:50px;" /></div>'},
            { name:'transactionID', field: 'Payment.transaction_id'},
            { name:'name', field: 'Customer.User.first_name'},
            { name:'amount', field: 'Payment.total', type: 'number'},
            //{ name:'fee', field: 'Payment.fee | number: 2'},
            //{ name:'gst', field: 'Payment.gst'},
            { name:'status', field: 'Payment.status'},
            { name:'date', field: 'Payment.datestamp'}
        ]
    };

    $scope.recurringGridOptions = {
        enableSorting: true,
        enableFiltering: true,
        rowHeight: 50,
        columnDefs: [
            { name:'pic', field: 'Customer.User.clientpic', enableHiding: false, enableFiltering: false, enableSorting: false,
            cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP"><img ng-src="{{(COL_FIELD) ? \'/users/viewUserImage/\' + COL_FIELD : \'/img/userblank.jpg\'}}" class="img-responsive" style="max-width:50px;" /></div>'},
            { name:'name', field: 'Customer.User.first_name'},
            { name:'amount', field: 'RecurringPayment.total', type: 'number'},
            //{ name:'fee', field: 'RecurringPayment.fee | number: 2'},
            { name:'status', field: 'RecurringPayment.status'},
            { name:'frequency', field: 'RecurringPayment.frequency'},
            { name:'day', field: 'RecurringPayment.day'},
            { name:'date_start', field: 'RecurringPayment.date_start'},
            { name:'date_end', field: 'RecurringPayment.date_end'},
            { name:'num_repeat', field: 'RecurringPayment.num_repeat'}
            //{ name:'actions', field: 'RecurringPayment.id', enableFiltering: false}
        ]
    };
    
    $scope.updateCardPayments = function(){
        
        $http.get('/payments/viewCardPayments')
        .then(function(r){
            $scope.cardGridOptions.data = r.data;
            $scope.cardPayments = r.data;
            console.log($scope.cardGridOptions, "card grid options");
        });
    }

    // Deprecated use recurring payments instead
    $scope.updateRepeatPayments = function(){

        $http.get('/payments/viewRepeatPayments')
        .then(function(r){
            $scope.repeatPayments = r.data;
        });
    }

    $scope.updateRecurringPayments = function(){
        $http.get('/payments/viewRecurringPayments')
        .then(function(r){
            $scope.recurringGridOptions.data = r.data;
            $scope.recurringPayments = r.data;
            console.log($scope.recurringGridOptions, "recurring grid options");
        });
    }

    $scope.getNextDate = function(date, frequency){
        if (date == '0000-00-00') {
            return 'N/A';
        }
        var d = new Date(date);
        
        if (frequency == 'weekly') {
            d.setDate(d.getDate() + 7); 
        } else if (frequency == 'fortnightly') {
            d.setDate(d.getDate() + 14); 
        } else if (frequency == 'monthly') {
            d.setMonth(d.getMonth() + 1); 
        }
        
        
        return d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear() ;
    }
    
    $scope.$watch('addSomething', function(n,o){
        $scope.addCl = { email: '', mobile: '', name: '' };
        $scope.addCreditCard = {};
    });

    
    
    
    $scope.updateFavourites = function(){
        
        $http.get('/favourites/count')
        .then(function(r){
            $scope.favouriteCount = r.data;
        });     
    }
    
    
    
    $scope.getBalance = function(){
        $http.get('/payments/currentBalance')
        .then(function(r){
            $scope.currentBalance = r.data;
        });
    }
    
    $scope.getCustomers = function(){
        $http.get('/users/getClientList')
        .then(function(r){
            $scope.clientList = r.data;
        });
    }
    
    $scope.getCharts = function(){
        $http.get('/payments/getCharts/' + $scope.period)
        .then(function(r){
            console.log(r.data);
            $scope.charts = r.data;
        });
    }
    
    $scope.addClient = function(name,mobile,email){
        
        if (!name) {
            alert('Client name is required');
            return false;
        } else if (!email) {
            alert('Client email is required');
            return false;
        }
        
        $http.post('/users/addClient/', {name: name, mobile: mobile, email: email})
        .then(function(r){
            $scope.addSomething = false;
            $scope.getCustomers();
        });
    }
    
    $scope.$watch('period', function(n,o){
        $scope.getCharts();
    });
    
    $scope.resetForms = function(){
        $scope.addCreditCard = {
            price: 0,
            addGST: false,
            notes: '',
            client: ''
        }
    }
    
    $scope.addSomethingChange = function(e){
        $scope.addSomething = e;
    }
    
    $scope.addPayment = function(){
        if ($scope.addCreditCard.price > 1000) {
            alert('Maximum for any transaction is $1000');
            return false;
        }
        
        if ($scope.tandcType == 1) {
            $scope.terms = $scope.DefaultTerms;
        }
        
        $http.post('/payments/requestPayment/', {card: $scope.addCreditCard, terms: $scope.terms })
        .then(function(r){
            console.log(r, "request payment");
            $scope.resetForms();
            $scope.addSomething = false;
            $scope.updateCardPayments();
        });
    }
    
    $scope.checkPW = function(pw){
		console.log(pw);
        $http.post('/users/checkpw', {password: pw})
        .then(function(r){
            if (r.data.entry) {
                 $scope.confirmedPW = true;
            } else {
                $scope.error = 'Invalid Password';
            }
        });
        
        return false;
       
	}
    
    $scope.getDefaultTerms = function(){
        $http.get('/Users/getDefaultTerms').then(function(r){
            $scope.DefaultTerms = r.data;
        });
    }
    
    $scope.tandcType = 1;
    $scope.resetForms();
    $scope.updateCardPayments();
    $scope.getBalance();
    $scope.getCustomers();
    $scope.updateFavourites();
    $scope.updateRepeatPayments();
    $scope.updateRecurringPayments();
    $scope.getDefaultTerms();
    
});