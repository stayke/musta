/**
 * Created by Nicholas.Jephcott on 23/05/2016.
 */
app.controller('calendarrule', function($scope, $http, $rootScope, $q, $timeout, $filter) {

    $rs = $rootScope;
    var calendarRuleCtrl = this;
    $scope.curRule = {};

    $scope.timePickerOptions = {
        step: 15,
        timeFormat: 'H:i:s'
    }

    // Saves current rule
    $scope.saveRule = function(curRule){
        console.log(curRule, 'start save rule');
        var save_rule = angular.copy($scope.curRule);

        save_rule.time_start = save_rule.time_start.toLocaleTimeString();
        save_rule.time_end = save_rule.time_end.toLocaleTimeString();
        if(curRule.date_start) {
            save_rule.date_start = curRule.date_start.getFullYear() + "-" + (curRule.date_start.getMonth() + 1) + "-" + curRule.date_start.getDate();
        }
        if(curRule.date_end) {
            save_rule.date_end = $scope.curRule.date_end.getFullYear() + "-" + ($scope.curRule.date_end.getMonth() + 1) + "-" + $scope.curRule.date_end.getDate();
        }
        console.log(save_rule, 'processed save rule');

        $http.post('/calendar/saveRule/' + curRule.id,{newRule:save_rule}).then(function(r) {
            console.log(r, 'save response');
            if(r.data.error_messages){
                $scope.error_messages = r.data.error_messages;
            } else {
                $rs.$broadcast('refreshCalendar');
            }
        });
    }

    $scope.loadRule = function(curRuleId){
        $http.post('/calendar/getRule/' + curRuleId).then(function(r){
            console.log(r, "load rule data");
            $scope.curRule = r.data.rule;
            if($scope.curRule.date_start) {
                $scope.curRule.date_start = new Date($scope.curRule.date_start);
            }
            if($scope.curRule.date_end) {
                $scope.curRule.date_end = new Date($scope.curRule.date_end);
            }
            var cur_date = new Date();
            if($scope.curRule.time_start) {
                var date_string = cur_date.getFullYear() + "-" + cur_date.getMonth() + "-" + cur_date.getDate() + " " + $scope.curRule.time_start;
                $scope.curRule.time_start = new Date(date_string);
            }
            if($scope.curRule.time_end) {
                var date_string = cur_date.getFullYear() + "-" + cur_date.getMonth() + "-" + cur_date.getDate() + " " + $scope.curRule.time_end;
                $scope.curRule.time_end = new Date(date_string);
            }
            console.log($scope.curRule, "loaded rule");
        });
    }

    $scope.resetRule = function(curRule){
        $scope.curRule = {
            //name: '',
            tile: '',
            location: '',
            time_start: '',
            end_time_end: '',
            date_start: '',
            date_end: '',
            notes: '',
            repeat: '',
            day: {}
        };

    }

    $rs.$on('returnRuleDetails', function(e,deets){
        $scope.resetRule($scope.curRule);
        console.log(deets);
        if(deets.id){
            console.log(deets, "calendar rule details 1");
            $scope.loadRule(deets.id);
        } else {
            console.log(deets, "calendar rule details 2");
            $scope.curRule.id = 0;
            $scope.curRule.frequency = "weekly";
            $scope.curRule.day = {};
            var new_date = new Date();
            var new_time = new Date();
            if($rs.clickedTime){
                new_date = $rs.clickedTime.date;
                //var new_time_str = new_date.getFullYear() + "-" + (new_date.getMonth()+1) + "-" + new_date.getDate() + " " + $rs.clickedTime.time + ":00:00";
                //console.log(new_time_str);
                new_time =  new Date(new_date.getFullYear() + "-" + (new_date.getMonth()+1) + "-" + new_date.getDate() + " " + $rs.clickedTime.time + ":00:00");
                //// NDJ - convert day number to day multiselect
                if(new_date.getDay() == 0){$scope.curRule.day = {Sunday: true};}
                if(new_date.getDay() == 1){$scope.curRule.day = {Monday: true};}
                if(new_date.getDay() == 2){$scope.curRule.day = {Tuesday: true};}
                if(new_date.getDay() == 3){$scope.curRule.day = {Wednesday: true};}
                if(new_date.getDay() == 4){$scope.curRule.day = {Thursday: true};}
                if(new_date.getDay() == 5){$scope.curRule.day = {Friday: true};}
                if(new_date.getDay() == 6){$scope.curRule.day = {Saturday: true};}
            } else {
                new_time =  new Date(new_date.getFullYear() + "-" + (new_date.getMonth()+1) + "-" + new_date.getDate() + " " + new_time.getHours() + ":00:00");
            }
            $scope.curRule.date_start = new_date;
            $scope.curRule.date_end = new_date;
            $scope.curRule.time_start = new_time;
            $scope.curRule.time_end = new_time;
            console.log($scope.curRule, "filled current rule");
        }

    });

    $rs.$broadcast('requestRuleDetails');
});