/**
 * Created by Nicholas.Jephcott on 30/06/2016.
 */
app.controller('adminusers', function($scope, $http, $rootScope) {
    $rs = $rootScope;

    $scope.details_sent = {};

    $scope.trainerdisable = function(id){
        console.log(id, "Disable trainer called");
        $http.post("/admin/trainerdisable/" + id).then(function(r) {
            console.log(r, "trainer disable response");
            window.location.reload();
        });
    }

    $scope.trainerapprove = function(id){
        console.log(id, "Approve trainer called");
        $http.post("/admin/trainerapprove/" + id).then(function(r) {
            console.log(r, "trainer approve response");
            window.location.reload();
        });
    }

    $scope.send_details = function(id){
        $scope.details_sent[id] = true;
        console.log($scope.details_sent, "details sent");
        $http.post("/admin/sendDetails/" + id).then(function(r) {
            console.log(r, "send details response");
            window.location.reload();
        });
    }

});