app.controller('holiday', function($scope, $http, $rootScope) {
	$rs = $rootScope;
	
	$scope.submitHoliday = function(){
		var l = Ladda.create($('#submit')[0]);
		l.start();
		
		
		$http.post('/holidays/add', {date: $scope.holiday.date.toDateString(), duration: $scope.holiday.length })
		.then(function(r){
			if (!r.data.error) {
               $rs.goBack();
			   $rs.$broadcast('updateCalendar');
            }
			l.stop();
		});			
	}
	
});