app.controller('bankdetails', function($scope, $http, $rootScope, $sce) {
	$rs = $rootScope;

    $scope.cur_user = {};
    $scope.salesforceFormAction;

    $scope.loadUser = function(){
        $http.post('/users/getCurUser/').then(function(r){
            console.log(r.data, "loaded user");
            $scope.cur_user = r.data;
            $scope.salesforceFormAction = $sce.trustAsResourceUrl("https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
        });
    }

    $rs.$on('requestUserDetails', function() {
        $scope.loadUser();
    });

    $scope.loadUser();

});