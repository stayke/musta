app.controller('adminReports',  function($scope, $http, $rootScope, $timeout, $filter) {
	$rs = $rootScope;
    $scope.viewStats = {
		BCSessions: 0,
		PTSessions: 0,
		locationsByArea: 0,
		uniqueLocationsByArea: 0,
		trainerCount: 0
	}
	
	$scope.markers = [];
	
	var d = new Date();
	var nd = new Date();
	
	d.setMonth(d.getMonth() - 1);
	
	$rs.search = {
		filter: {
			gym: true,
			outdoor: true,
			mobile: true,
			bootcamp: true
		},
		date: {
			from:d,
			to: nd
		}
	};
	$scope.search.results = [];
	$scope.defaultDistance = 50;
	$scope.trainerList = [];
	
		var input = /** @type {!HTMLInputElement} */(
      document.getElementById('search-input'));
	$scope.search.autocomplete = new google.maps.places.Autocomplete(input);
	
	 $scope.search.autocomplete.addListener('place_changed', function() {
		var place = $scope.search.autocomplete.getPlace();
		if (!place.geometry) {
		  window.alert("Autocomplete's returned place contains no geometry");
		  return;
		} else {
			console.log(place.geometry.location);
			$scope.updateSearchPostcode(place.geometry.location);
		}
	 });
	
	if ($rs.userLocation) {
		lat = $rs.userLocation.coords.latitude;
		lon = $rs.userLocation.coords.longitude;
		zoom = 14
	} else {
		lat = -27.1861734
		lon = 134.9509604
		zoom = 4
	}
	
	$scope.$on('LocationFound', function(e, a){
		if (!$rs.search.filters) {
			console.log($rs.userLocation.coords);
			
			$scope.updateSearchPostcode({ lat: $rs.userLocation.coords.latitude, lng: $rs.userLocation.coords.longitude });
			$rs.geocoder.geocode({'location': { lat: $rs.userLocation.coords.latitude, lng: $rs.userLocation.coords.longitude }}, function(results, status) {
				$scope.search.postcode = results[0].formatted_address;
				$scope.getResults();
			});
		}
	});
	
	
	$scope.search.distance = $scope.defaultDistance;
	
	$scope.updateSearchPostcode = function(r){
		$scope.search.geometry = r;
	}
	
	$scope.resetMarkers = function(){
		$scope.markers.forEach(function(marker) {
			marker.setMap(null);
		});
		$scope.markers = [];
	}
	
	$scope.addMarker = function(marker, number){
		if (!number) {
			number=1;
		}
		$scope.markers.push(new google.maps.Marker({
			map: $scope.search.map,
			position: marker,
			icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+number+'|FF0000|000000'
		}));
	}
	
	$scope.getResults = function(){
		if (!$scope.search.geometry || !$scope.search.distance) {
			return false;		
		}
		
		if (typeof $scope.search.geometry.lat == 'function') {
            $scope.search.geometry.lat = $scope.search.geometry.lat();
            $scope.search.geometry.lng = $scope.search.geometry.lng();
        }
		
		$http.post('/locations/reportsearch', {
			lat: $scope.search.geometry.lat,
			lon: $scope.search.geometry.lng,
			filters: $scope.search.filter,
			distance: $scope.search.distance,
			dateFrom: $scope.search.date.from,
			dateTo: $scope.search.date.to
		})
		.then(function(r){
			$scope.stats = r.data
		});
		
		if ($scope.search.circle) {
			$scope.search.circle.setMap(null);
		}
		
		$scope.search.circle = new google.maps.Circle({
			strokeColor: '#faf125',
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: '#363636',
			fillOpacity: 0.20,
			map: $scope.search.map,
			center: {lat: $scope.search.geometry.lat, lng: $scope.search.geometry.lng},
			radius: $scope.search.distance * 1000
		});
			
			$scope.search.map.fitBounds($scope.search.circle.getBounds());
		
		
	}
	
	$scope.addUserToArray = function(place){
		console.log($.inArray(place.users.id, $scope.trainerList) > -1);
		if($.inArray(place.users.id, $scope.trainerList) < 0) {
			$scope.trainerList.push(place.users.id);
		}
	}
	
	$scope.search.map = new google.maps.Map(document.getElementById('gmaps'), {
		center: {lat: lat, lng: lon},
		zoom: zoom,
		disableDefaultUI: true,
		zoomControl: true,
		scrollwheel: false,
		useStaticMap: true
	});
	
	$scope.$watch('search.date.from', function(n,o){
		if($scope.search.date.to.valueOf()<n.valueOf()){ $scope.search.date.to = n; }
	});
	
	$scope.$watch('search.date.to', function(n,o){
		if($scope.search.date.from.valueOf()>n.valueOf()){ $scope.search.date.from = n; }
	});
	
	$scope.$watch('stats', function(n,o){
		if (n==o) {
            return;
        }
		TweenMax.to($scope.viewStats, 0.5, {
			ease:Circ.easeOut,
			BCSessions: n.BCSessions,
			PTSessions: n.PTSessions,
			locationsByArea: n.locationsByArea,
			uniqueLocationsByArea: n.uniqueLocationsByArea,
			trainerCount: n.trainerCount,
			onUpdate: function () {
				$scope.$digest()
			}
		})
	});
   
});