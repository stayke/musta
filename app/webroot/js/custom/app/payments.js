app.controller('payments', function($scope, $http, $rootScope, $filter) {
	$rs = $rootScope;
    
    
   
    
    $scope.payNow = function(){
		if ($rootScope.paymentinformation.terms && !$scope.paymentinformation.accepted) {
            alert('Please confirm trainers T&C\'s');
			return false;
        }
        $http.post('/payments/pay', {payload: {paymentData: $scope.paymentinformation, card: $scope.card}})
        .then(function(r){
            if (!r.data.error) {
                $rs.$broadcast('updateCalendar');
                $rs.goBack();
            }
        })
    }
    
//    $scope.resetPay = function(){
//        $scope.paymentData = $rs.paymentinformation;
//        $scope.type = ($scope.paymentData.type == 'pt') ? 'PtPricing' : 'BcPricing';
//        $scope.paymentData.item[0][$scope.type].price = parseFloat($scope.paymentData.item[0][$scope.type].price);
//        $scope.paymentData.item[0][$scope.type].fee = parseFloat($rs.getBookingFee($scope.paymentData.item[0][$scope.type].price));
//        $scope.paymentData.item[0][$scope.type].total = parseFloat($scope.paymentData.item[0][$scope.type].fee) + $scope.paymentData.item[0][$scope.type].price;
//		$scope.paymentData.item[0][$scope.type].gstamount = $scope.paymentData.item[0][$scope.type].price / 10;
//        if($scope.paymentData.gst){ $scope.paymentData.item[0][$scope.type].total += $scope.paymentData.item[0][$scope.type].gstamount;  }
//        $scope.paymentData.formatted = {
//            date: new Date($scope.paymentData.date + 'T' + $scope.paymentData.time + ':00')
//        }
//        console.log($scope.paymentData.date + 'T' + $scope.paymentData.time + ':00');
//    }
//    
//    $scope.$on('resetPay', function(){
//        $scope.resetPay();
//    });
});