app.controller('calendar', function($scope, $http, $rootScope, $q, $timeout, $filter, $location, $sce) {
    
    $rs = $rootScope;
    $rs.viewUser = false;
    $rs.trimCal = false;
    $scope.bookModal = {show: false};


    $scope.hour_options = {
        00: "00",
        01: "01",
        02: "02",
        03: "03",
        04: "04",
        05: "05",
        06: "06",
        07: "07",
        08: "08",
        09: "09",
        10: "10",
        11: "11",
        12: "12",
        13: "13",
        14: "14",
        15: "15",
        16: "16",
        17: "17",
        18: "18",
        19: "19",
        20: "20",
        21: "21",
        22: "22",
        23: "23"
    }

    $scope.visible_times = {
        time_start: "00",
        time_end: "23",
        days: {
            Monday: true,
            Tuesday: true,
            Wednesday: true,
            Thursday: true,
            Friday: true,
            Saturday: true,
            Sunday: true
        },
        day_width: 12.85 // 90/7
    }

    $scope.loadVisibleTimes = function(){
        //console.log($scope.viewUser, "test a");
        //console.log($rs.user.id, "test b");

        if(!$scope.viewUser){
            $http.post('/users/loadVisibleTimes/' + $rs.user.id).then(function(r){
                console.log(r, "loaded visible times");
                if(r.data.User) {
                    if (r.data.User.visible_times) {
                        $scope.visible_times = angular.fromJson(r.data.User.visible_times);
                        $scope.processVisibleTimes();
                    }
                }
            })
        }
        //if($rs.user.visible_times){
        //    if(typeof $rs.user.visible_times == "string"){
        //        $rs.user.visible_times = angular.fromJson($rs.user.visible_times);
        //        $scope.visible_times = angular.copy($rs.user.visible_times);
        //    } else if(typeof $rs.user.visible_times == "object"){
        //        $scope.visible_times = angular.copy($rs.user.visible_times);
        //    }
        //    $scope.processVisibleTimes();
        //}
        //console.log($rs.user, "cur user");
        //console.log($scope.visible_times, 'visible times loaded');
    }

    $scope.updateVisibleTimes = function(){

        $rs.user.visible_times = angular.copy($scope.visible_times);
        $http.post('/users/saveVisibleTimes', {visibleTimes: angular.copy($scope.visible_times)}).then(function(r){
            console.log(r, "saved visible times");
        })
        $scope.processVisibleTimes();

        console.log($scope.hours, "update visible hours");
        console.log($scope.visible_times, "update visible times");
    }

    $scope.processVisibleTimes = function(){
        $.each($scope.hours, function(index, value) {
            if (index < $scope.visible_times.time_start || index > $scope.visible_times.time_end) {
                value.hideme = true;
            } else {
                value.hideme = false;
            }

        });

        var days_visible = 0;
        if($scope.visible_times.days.Monday){days_visible++;}
        if($scope.visible_times.days.Tuesday){days_visible++;}
        if($scope.visible_times.days.Wednesday){days_visible++;}
        if($scope.visible_times.days.Thursday){days_visible++;}
        if($scope.visible_times.days.Friday){days_visible++;}
        if($scope.visible_times.days.Saturday){days_visible++;}
        if($scope.visible_times.days.Sunday){days_visible++;}

        if(days_visible > 0){
            $scope.visible_times.day_width = 90 / days_visible;
        }

    }

    $scope.timePickerOptions = {
        step: 60,
        timeFormat: 'H:i:s'
    }

    Date.prototype.getWeek = function(){
        var d = new Date(+this);
        d.setHours(0,0,0);
        d.setDate(d.getDate()+4-(d.getDay()||7));
        return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
    };
    
    Date.prototype.dayofYear = function(w,y) {        
        var d = (1 + (w - 1) * 7); // 1st of January + 7 days for each week
        return new Date(y, 0, d);
    }
    
    $scope.$watch('bookModal.show', function(n,o){
        if (n) {
            $rs.$broadcast('modalOpen');
        } else {
            $rs.$broadcast('modalClose');
        }
    });
    
    $scope.openBook = function(session, hour){
        if (!$rs.user.id) {
            alert('You need to sign up in order to make any bookings');
            $rs.triedBooking = true;
            //$rs.signupForm($location.absUrl()); // apperantly it takes bools and uses back instead of a return url
            $rs.signupForm();
            return false;
        }
        $scope.bookModal.show = true;
        $scope.bookModal.data = {};
        $scope.bookModal.data = session;
        $scope.bookModal.data.hour = hour;
        $scope.bookModal.pricing = $filter('filter')($scope.viewLocations, {Location: {id: session.location.Location.id}}, true);
        console.log($scope.bookModal.pricing, "booking pricing");
        $scope.booking = {
            time: '',
            durationplan: ''
        }
        
    }

    // Create payment for current booking using current user
    $scope.payForSession = function(){
        console.log($scope.bookModal, "bookModal");
        console.log($scope.booking, "booking");
        // NDJ - #$#$# todo should padleft on hour here
        var date_time = $scope.bookModal.data.timings.date + " " + $scope.bookModal.data.hour + ":" + $scope.booking.time + ":00";
        $scope.newBooking = {
            user: $rs.user.id,
            to_user: $scope.bookModal.data.location.Location.user,
            location: $scope.bookModal.data.location.Location.id,
            pricing: $scope.booking.durationplan,
            participant_notes: $scope.booking.participant_notes,
            time: date_time
        }
        console.log($scope.newBooking, "New Booking");
        $http.post('/payments/newPayment/',{newBooking:$scope.newBooking}).then(function(r){
            console.log(r, "new payment return");
            $rs.makePayment(r.data);
        });
    }

    // NDJ - the old payment method by previous developers
    $scope.oldpayForSession = function(){
    //$scope.payForSession = function(){

        if ($scope.bookModal.data.type == 'pt') {
            time = $filter('addZero')($scope.bookModal.data.hour)  + ':' + $scope.booking.time;
        } else {
            time = $filter('addZero')($scope.bookModal.data.hour)  + ':' + '00';
        }
        
        //info = {
        //    date: $scope.bookModal.data.timings.date,
        //    time: time,
        //    session: $scope.bookModal.data.timings.id,
        //    durationplan: $scope.bookModal.durationplan,
        //    type: $scope.bookModal.data.type,
        //    item: ($scope.bookModal.data.type == 'pt') ? $filter('filter')($scope.bookModal.pricing[0].pricing, {PtPricing: {id: $scope.booking.durationplan}}, true) : $filter('filter')($scope.bookModal.pricing[0].pricing, {BcPricing: {id: $scope.booking.durationplan}}, true),
        //    viewing: $scope.viewUser,
        //    gst: $scope.bookModal.data.location.Location.gst
        //};
        
        if (isNaN($scope.booking.durationplan)) {
            var id = $scope.booking.durationplan.split('-');
            id = id[1];
            var sgt = true;
        } else {
            var id = $scope.booking.durationplan;
            var sgt = false;
        }
        
        
        item = ($scope.bookModal.data.type == 'pt') ? $filter('filter')($scope.bookModal.pricing[0].pricing, {PtPricing: {id: id}}, true) : $filter('filter')($scope.bookModal.pricing[0].pricing, {BcPricing: {id: $scope.booking.durationplan}}, true)

        info = {
            items: [
                {
                    id: ($scope.bookModal.data.type == 'pt') ? item[0].PtPricing.id : item[0].BcPricing.id,
                    title: ($scope.bookModal.data.type == 'pt') ? (sgt) ? item[0].PtPricing.session_length + ' minute FWF session' :  item[0].PtPricing.session_length + ' minute session' : item[0].BcPricing.name + ' - ' + item[0].BcPricing.included_sessions + ' session' + ((item[0].BcPricing.included_sessions>1) ? 's' : ''),
                    price: ($scope.bookModal.data.type == 'pt') ? (sgt) ? item[0].PtPricing.sgt_fee  : item[0].PtPricing.price : item[0].BcPricing.price,
                    additional: ($scope.bookModal.data.type == 'pt') ? item[0].PtPricing : item[0].BcPricing,
                    date: $scope.bookModal.data.timings.date  + ' ' + time
                }
            ],
            title: $scope.bookModal.data.timings.date + ' @ ' + time,
            gst: $scope.bookModal.data.location.Location.gst,
            gstAmount:0,
            bookingFee: 0,
            total: 0,
            itemTotal: 0,
            type: $scope.bookModal.data.type,
            additional: {
                sgt: sgt,
                date: $scope.bookModal.data.timings.date,
                time: time,
                session: $scope.bookModal.data.timings.id,
                viewing: $scope.viewUser,
                durationplan: $scope.bookModal.durationplan
            }
        }
            
        var itemTotal = 0.00;
        
        for (var i=0;i<info.items.length;i++) {
            itemTotal += parseFloat(info.items[i].price);
        }
        
        info.itemTotal = itemTotal;
        
        if (info.gst) {
            info.gstAmount = itemTotal/10;
        }
        
        info.total = itemTotal + info.gst;
        $http.get('/payments/getFeeFront/'+'card_fee'+'/'+itemTotal)
        .then(function(r){
            info.bookingFee = parseFloat(r.data);
            info.total = itemTotal + info.gstAmount + info.bookingFee;
            $rs.$broadcast('timeToPay', info);
        });
    }
    

    $rs.getLocations();
    $scope.editMode = false;
    $scope.offset = 0;
    $scope.DoW = [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
    ];
    
    $scope.copyTime = function(){
        $scope.copyInfo = $scope.hours;
        angular.forEach($scope.copyInfo, function(value, key){
            angular.forEach(value, function(v, k){
                delete v.id;    
            });    
        });
    }
    
    $scope.filterSGT = function(item){
        if (item.PtPricing) {
            return item.PtPricing.sgt;
        }
    }
    
     $scope.pasteTime = function(){
        if (!$scope.copyInfo) {
            return false;
        }
        $scope.hours = $scope.copyInfo;
        $scope.copyInfo = false;
        $scope.saveChanges();
    }
    
    
    $scope.openHoliday = function(){
		$rs.page.bottomSlider.template = $rs.p + 'calendar/holiday.html';
		$rs.page.bottomSlider.display = true;
    }
    
    
    $scope.addAvailable = function(length, day, time){
        if (isNaN($scope.selectedLocation)) {
            $scope.selectedLocation = 0;
        }
        locationy = $rootScope.userLocations[$scope.selectedLocation];
        if (time.toString().length == 1) {
            time = '0' + time;
        }
        $scope.hours[time][$scope.DoW[day]] = {
            length: length,
            location: locationy,
            type: ($rs.user.role == 1) ? 'pt' : 'bc',
            event: false,
            title: false
        }
        $scope.saveChanges();
    }

    $scope.openOptions = function(date, time){

        if (time.toString().length == 1) {
            time = '0' + time;
        }
        console.log(time, "open options hour");
        console.log(date, "open options date");

        $rs.openAddOptions(date, time);
    }
    
    $scope.addEvent = function(day, time, location){
        
        if (time.toString().length == 1) {
            time = '0' + time;
        }
        
        $rs.showEvent(day, time, location);
    }

    // NDJ - #$#$# todo switch this to use id rather than timings event
    $scope.viewEvent = function(event){
        console.log(event);
    }

    $scope.openEventAttendance = function(event){
        if (!$rs.user.id) {
            alert('You need to sign up in order to make any bookings');
            $rs.signupForm();
            return false;
        }
        $rs.showEventAttendance(event);
    }

    // NDJ - refresh the calendar and close the popup
    $scope.$on('refreshCalendar', function(){
        $scope.setupDates($scope.offset); // reset the calendar #$#$#
        $scope.loadCalendar();
        $rs.goBack();
    });

    $scope.$on('addAnEvent', function(e,d){
       $scope.hours[d.time][$scope.DoW[d.day]] = d.event;
       $scope.saveChanges();
       $rs.goBack();
    });

    $scope.getUserId = function(){
        return $rs.user.id;
    }
    
    $scope.setupDates = function(offset){
         if (!offset) {
            offset = 0;
         }

         
         var d = new Date();
         
         week = new Date(d.getFullYear(), d.getMonth(), d.getDate() + (offset*7) + (7 - d.getDay() ) );
         $scope.currentWeek = week.getWeek();
         
         $scope.days = {
            'Monday': {
                dateObj: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-1)) + (offset*7)),
                cleanDate: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-1)) + (offset*7)).toDateString()
            },
            'Tuesday': {
                dateObj: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-2)) + (offset*7)),
                cleanDate: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-2)) + (offset*7)).toDateString()
            },
            'Wednesday': {
                dateObj: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-3)) + (offset*7)),
                cleanDate: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-3)) + (offset*7)).toDateString()
            },
            'Thursday': {
               dateObj: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-4)) + (offset*7)),
                cleanDate: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-4)) + (offset*7)).toDateString()
            },
            'Friday': {
                dateObj: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-5)) + (offset*7)),
                cleanDate: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-5)) + (offset*7)).toDateString()
            },
            'Saturday': {
                dateObj: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-6)) + (offset*7)),
                cleanDate: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-6)) + (offset*7)).toDateString()
            },
            'Sunday': {
                dateObj: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-7)) + (offset*7)),
                cleanDate: new Date(d.getFullYear(), d.getMonth(), d.getDate() - ((d.getDay()-7)) + (offset*7)).toDateString()
            },
         }
         
         
        $scope.hours = {
            '00': {order: 0  },
            '01': {order: 1  },
            '02': {order: 2  },
            '03': {order: 3  },
            '04': {order: 4  },
            '05': {order: 5  },
            '06': {order: 6  },
            '07': {order: 7  },
            '08': {order: 8  },
            '09': {order: 9  },
            '10': {order: 10  },
            '11': {order: 11  },
            '12': {order: 12  },
            '13': {order: 13  },
            '14': {order: 14  },
            '15': {order: 15  },
            '16': {order: 16  },
            '17': {order: 17  },
            '18': {order: 18 },
            '19': {order: 19  },
            '20': {order: 20  },
            '21': {order: 21  },
            '22': {order: 22  },
            '23': {order: 23  }
        }
        $scope.loadCalendar();
    }
    
    $scope.addWeek = function(){
        $scope.offset++;
    }
    
    $scope.removeWeek = function(){
        $scope.offset--;
    }
    
    $scope.resetWeek = function(){
        $scope.offset = 0;
    }
    
    $scope.calSettings = function(item){
        if (!item.relevant) {
            return false;
        }
        $scope.page.leftSlider.template = $rs.p + 'calendar/date-options.html';
        $scope.openLeft();
        $rs.dayInQuestion = item;
    }
    
    $scope.loadData = function(){
        $rs.availability = [];
        $scope.loadTimeslots();
        $scope.loadTimeranges();
    }
    
    $scope.checkAfterSession = function(date, hour, time, length){

        if(isNaN(parseInt(time))){ // if no time provided return false;
            return false;
        }

        //console.log("checkAfterSession");
        //console.log(date, "date");
        //console.log(hour, "hour");
        //console.log(time, "time");
        //console.log(length, "length");

        date = new Date(date);
        date = date.getDay()-1;
        if(date <0){date=6;}
        date = $scope.DoW[date];


        hour = hour.split(':')[0];
        
        test_hour = hour;
        test_minutes = time;
        test_length = length - 15;
        
        can_book = true;

        var loop_stopper = 100;
        var test_minute_string = "";
        var test_hour_string = "";

        // Check for as long as the test length is
        while(test_length > 0 && loop_stopper > 0){
            loop_stopper--;
            test_minutes = parseInt(test_minutes) + 15;
            test_length = parseInt(test_length) - 15;

            if(test_minutes >= 60){
                test_minutes = parseInt(test_minutes) - 60;
                test_hour++;
            }

            // add leading 0 if less than 10
            if(test_minutes < 10){
                test_minute_string = "0" + parseInt(test_minutes);
            } else {
                test_minute_string = test_minutes;
            }

            // add leading 0 if less than 10
            if(test_hour < 10){
                test_hour_string = "0" + parseInt(test_hour);
            } else {
                test_hour_string = test_hour;
            }

            if(test_hour >= 24){
                can_book = false;
                break;
            }

            if($scope.hours[test_hour_string][date] == undefined){
                can_book = false;
                break;
            }

            if(!$scope.hours[test_hour_string][date].bookable[test_minute_string]){
                can_book = false;
                break;
            }

        }

        //// Checks this hour block
        //while (parseInt(test_minutes) <= 45 && test_length > 0) {
        //    if(!$scope.hours[test_hour][date].bookable[parseInt(test_minutes)]){
        //        can_book = false;
        //    }
        //
        //    test_minutes = parseInt(test_minutes) + 15;
        //    test_length = parseInt(test_length) - 15;
        //}
        //
        //test_minutes = 0;
        //test_hour = parseInt(test_hour) + 1;
        //if (test_hour < 10) {
        //    test_hour = '0' + test_hour;
        //}
        //
        //// Check next hour
        //while (parseInt(test_minutes) <= 45 && test_length > 0) {
        //    if (!$scope.hours[test_hour][date]) {
        //        can_book = false;
        //    } else if(!$scope.hours[test_hour][date].bookable[parseInt(test_minutes)]){
        //        can_book = false;
        //    }
        //
        //
        //    test_minutes = parseInt(test_minutes) + 15;
        //    test_length = parseInt(test_length) - 15;
        //}

        return can_book;
    }
    
    $scope.checkSession = function(session, time){

        console.log(session, 'check session');
        console.log(time, 'check time');
        // NDJ - moved functions to find time each time the hour is called to just add the time/day before you call it
        // var s = session.timings.time_start.split(':');
        // if(!session.hour){
        //     session.hour = hour;
        // }
        // if(!session.day){
        //     session.day = "";
        // }
        // session.hour = s[0];
        //
        // if (session.hour < 10) {
        //     hour = '0' + session.hour;
        // } else {
        //     hour = session.hour;
        // }
        var day = session.day;
        var hour = session.hour;

        if (session.hour < 11) {
            prevhour = '0' + (parseInt(session.hour-1));
        } else {
            prevhour = parseInt(session.hour)-1;
        }

        // var dow = new Date(session.timings.date);
        // var day = $scope.DoW[dow.getUTCDay()-1];

        var length = session.length;
        
        thishour = session.pt_bookings;
        if (hour>0 && $scope.hours[prevhour][day]) {
            lasthour = $scope.hours[prevhour][day].pt_bookings;
        } else {
            lasthour = new Array();
        }
        
        var sesh_date = session.timings.date.split('-');
        var sesh_time = session.timings.time_start.split(':');
        
        var session = new Date(sesh_date[0],sesh_date[1]-1,sesh_date[2],sesh_time[0],time,sesh_time[2]);
        
        
        for(var i=0;i<lasthour.length;i++){
            var sesh = lasthour[i];
            
            var prev_data = lasthour[i].PtBooking.time.split(' ');
            var prev_sesh_date = prev_data[0].split('-');
            var prev_sesh_time = prev_data[1].split(':');
            var sesh_length = lasthour[i].PtBooking.length;
        
            var prev_session = new Date(prev_sesh_date[0],prev_sesh_date[1]-1,prev_sesh_date[2],prev_sesh_time[0],(parseInt(prev_sesh_time[1]) + parseInt(sesh_length)),prev_sesh_time[2]);
            if (prev_session > session) {
                return false;
            } else {
               
            }
        }
        
        
        for(var i=0;i<thishour.length;i++){
            var sesh = thishour[i];
            
            var prev_data = thishour[i].PtBooking.time.split(' ');
            var prev_sesh_date = prev_data[0].split('-');
            var prev_sesh_time = prev_data[1].split(':');
            var sesh_length = thishour[i].PtBooking.length;
        
            var prev_session = new Date(prev_sesh_date[0],prev_sesh_date[1]-1,prev_sesh_date[2],prev_sesh_time[0],(parseInt(prev_sesh_time[1]) + parseInt(sesh_length)),prev_sesh_time[2]);
            var prev_session_start = new Date(prev_sesh_date[0],prev_sesh_date[1]-1,prev_sesh_date[2],prev_sesh_time[0],parseInt(prev_sesh_time[1]),prev_sesh_time[2]);
            
            if (prev_session > session &&  prev_session_start <= session) {
                return false;
            } else {
            }
        }

        return true;
    }
    
    $scope.$on('newLocation', function(){
        $scope.loadCalendar();
         $scope.setupDates($scope.offset);
    });
    
    $scope.loadCalendar = function(){
        
        return $q(function(resolve, reject){
            if ($scope.loadDisabled) {
                resolve(true);
            }
            $http.post('/calendar/view/' + $scope.viewUser, {dateStart: ($scope.days.Monday.dateObj) }).then(function(response){
                console.log($scope.days.Monday.dateObj, "monday object");
                console.log(response);
                $scope.hours = angular.merge({}, $scope.hours, response.data.sessions);
                console.log($scope.hours, "hours");
                $scope.holiday = response.data.holiday;
                $scope.viewingType = response.data.usertype.User.role;
                $scope.viewLocations = response.data.locations;
                // trust location deals as html
                if($scope.viewLocations){
                    for(var i = 0; i < $scope.viewLocations.length; i++){
                        $scope.viewLocations[i].Location.dealshtml = $sce.trustAsHtml($scope.viewLocations[i].Location.deals);
                    }
                }
                if ($scope.trimCal) {
                    min = 24;
                    max = 0;
                    $.each($scope.hours, function(index, value) {
                        if (
                            $scope.hours[index].Monday ||
                            $scope.hours[index].Tuesday ||
                            $scope.hours[index].Wednesday ||
                            $scope.hours[index].Thursday ||
                            $scope.hours[index].Friday ||
                            $scope.hours[index].Saturday ||
                            $scope.hours[index].Sunday
                        ) {
                            if(index > max) max = index;
                            if (index < min) min = index;
                        } else {
                            
                        }
                    });
                    
                    $.each($scope.hours, function(index, value) {
                       if (index < min || index > max) {
                            value.hideme = true;
                       }

                    });
                    
                    if (min == 24 && max == 0) {
                        $scope.emptyCal = true;
                    } else {
                        $scope.emptyCal = false;
                    }

                }

                /* // NDJ - bookable times is now calculated in php rather than in js
                  $.each($scope.hours, function(index, value) {

                       $.each($scope.hours[index], function(index2, value2) {
                            if (index2 != 'order' && index2 != 'hideme') {
                                this.hour = index;
                                this.day = index2;
                                this.bookable = {
                                    00: $scope.checkSession(value2, 00),
                                    15: $scope.checkSession(value2, 15),
                                    30: $scope.checkSession(value2, 30),
                                    45: $scope.checkSession(value2, 45)
                                }
                            }
                        });

                    });*/

                // NDJ - what is this doing?
                 $http.post('/booking/view/' + $scope.viewUser, {dateStart: ($scope.days.Monday.dateObj) }).then(function(response){
                     resolve(true);
                 });

                $scope.loadVisibleTimes();
               
            });
        })
        
    }
    
    $scope.isUnavailable = function(checkit){
        if (!checkit) {
            return false;
        }
        if (checkit.length==0) {
            return false;
        }
        full = true;
        $.each(checkit, function(i, v){
            if (checkit[i]) {
                full = false;
            }
        });
        return full; 
    }
    
    $scope.$on('updateCalendar', function(){
        $scope.bookModal.show = false;
        $scope.setupDates($scope.offset);
    });
    
    $scope.$on('saveChanges', function(){
        $scope.saveChanges();
    });
    
    $scope.$watch('offset', function(n,o){
       $scope.setupDates(n);
    });
    
    $scope.saveChanges = function(saveThis, saveDate){
        
        return $q(function(resolve, reject) {
    
            

            if (!saveThis) {
                saveThis = $scope.hours;
            }
            if (!saveDate) {
                saveDate = $scope.days;
            }
            
            $http.post('/calendar/add', {calendar: saveThis, days: saveDate})
            .then(function(r){
                if(r.data.error){
                }
                
                resolve(true);
            });
        });
    }
    
    $scope.iAmDone = function(saveThis, saveDate){
        $scope.saveChanges();
        $scope.deleteMode = false;
        $scope.editMode = false;
        $scope.eventMode = false;
        $scope.loadCalendar();
    }
    
})


.filter('dateNth', function() {
  return function(d) {
    if(d>3 && d<21) return d + 'th'; // thanks kennebec
    switch (d % 10) {
          case 1:  return d + "st";
          case 2:  return d + "nd";
          case 3:  return d + "rd";
          default: return d + "th";
    }
  };
})

.filter('monthConvert', function() {
  return function(d) {
    switch (d){
          case 0:  return "January";
          case 1:  return "Febuary";
          case 2:  return "March";
          case 3:  return "April";
          case 4:  return "May";
          case 5:  return "June";
          case 6:  return "July";
          case 7:  return "August";
          case 8:  return "September";
          case 9:  return "October";
          case 10:  return "November";
          case 11:  return "December";
    }
  };
})

.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
})

.filter('addZero', function() {
  return function(item) {
    if (!item === false) {
        return item;
    }
    if (item.toString().length == 1) {
        return '0' + item;
    }
    
    return item;
  };
});


