app.controller('events', function($scope, $http, $rootScope, $filter) {
    $rs = $rootScope;
    $scope.instance = {
        date: new Date(),
        selectedClients: new Array(),
        bookable: "0"
    };
    $scope.event = {
        date_start: new Date(),
        date_end: new Date(),
        selectedClients: new Array(),
        bookable: "0"
    };

    $scope.timePickerOptions = {
        step: 15,
        timeFormat: 'H:i:s'
    }

    $scope.event_mode = false;
    $scope.event_creating = true;

    $scope.getCustomers = function(){
        $http.get('/users/getClientList')
        .then(function(r){
            console.log(r.data, "client list");
            $scope.clientList = r.data;
            // Check if client list and attendees are loaded
            if($scope.attendees){
                $scope.combineClientAttendees();
            }
        });
    }

    $scope.addUser = function(client){
        console.log(client);
    }

    $scope.resetData = function(){
        $scope.instance = {
            date: new Date(),
            selectedClients: new Array(),
            bookable: "0"
        };
        $scope.event = {
            date_start: new Date(),
            date_end: new Date(),
            selectedClients: new Array(),
            bookable: "0"
        };
    }
    
    $scope.$watch('instance.clients', function(n,o){
        if (n == "" || !n) {
            return;
        }
        deets = n.split(',');
        $item = $filter('filter')($scope.clientList, {TrainerClient: {email: deets[1]}});
        
        $item[0].hidden = true;
        $scope.instance.selectedClients.push($item[0]);
        $scope.instance.clients = '';
    });
    
    $scope.removeClient = function(ob, index){
        $scope.instance.selectedClients.splice(index, 1);
        ob.hidden = false;
    }
    
    $rs.$on('returnEventDetails', function(e,deets){
        if(deets.id) {
            console.log(deets, "event details");
            $scope.loadEvent(deets.id);
        } else {
            $scope.resetData();
            console.log($rs.userLocations, "user locations");
            console.log(deets, "event details 2");
            $scope.event.id = 0;
            $scope.event.frequency = "weekly";
            $scope.event.day = {};
            var new_date = new Date();
            var new_time = new Date();
            if($rs.clickedTime){
                new_date = $rs.clickedTime.date;
                //var new_time_str = new_date.getFullYear() + "-" + new_date.getMonth() + "-" + new_date.getDate() + " " + $rs.clickedTime.time + ":00:00";
                //console.log(new_time_str);
                new_time = new Date(new_date.getFullYear() + "-" + new_date.getMonth() + "-" + new_date.getDate() + " " + $rs.clickedTime.time + ":00:00");
                //// NDJ - convert day number to day multiselect
                if(new_date.getDay() == 0){$scope.event.day = {Sunday: true};}
                if(new_date.getDay() == 1){$scope.event.day = {Monday: true};}
                if(new_date.getDay() == 2){$scope.event.day = {Tuesday: true};}
                if(new_date.getDay() == 3){$scope.event.day = {Wednesday: true};}
                if(new_date.getDay() == 4){$scope.event.day = {Thursday: true};}
                if(new_date.getDay() == 5){$scope.event.day = {Friday: true};}
                if(new_date.getDay() == 6){$scope.event.day = {Saturday: true};}
            } else {
                new_time = new Date(new_date.getFullYear() + "-" + new_date.getMonth() + "-" + new_date.getDate() + " " + new_time.getHours() + ":00:00");
            }
            $scope.event.date_start = new_date;
            $scope.event.date_end = new_date;
            $scope.event.time_start = new_time;
            $scope.instance.date = new_date;
            $scope.instance.time_start = new_time;
            console.log($scope.event, "filled event rule");
        }
    });

    $scope.changeLocationInstance = function(){
        var selected_loc =  $filter('filter')($rs.userLocations, {Location: {id: $scope.instance.location}});
        var selected_loc_color = selected_loc[0].Location.color;
        //console.log(selected_loc_color, "selected loc color");
        $scope.instance.colour = selected_loc_color;
    }

    $scope.changeLocationEvent = function(){
        var selected_loc =  $filter('filter')($rs.userLocations, {Location: {id: $scope.event.location}});
        var selected_loc_color = selected_loc[0].Location.color;
        //console.log(selected_loc_color, "selected loc color");
        $scope.event.colour = selected_loc_color;
    }

    $scope.loadEvent = function(eventid){
        //$http.post('/event/getEventInstance/' + eventid).then(function(r){
        $http.post('/event/getEventInstanceAllAttendees/' + eventid).then(function(r){
            $scope.event_mode = false;
            $scope.event_creating = false;
            console.log(eventid);
            console.log(r, "event load response");
            // r.data.event.date_start = $rs.dateFormatForInput(r.data.event.date_start);
            // r.data.event.date_start = new Date(r.data.event.date_start);
            // r.data.event.date_start = $rs.dateFormatForInput("05/14/2016");
            // r.data.event.date_end = $rs.dateFormatForInput(r.data.event.date_end);
            //if(r.data.event.date) {
            //    r.data.event.date = new Date(r.data.event.date);
            //}
            $scope.instance = r.data.instance;
            $scope.instance.selectedClients = []; // NDJ - #$#$# todo when php/attendees is fixed this will no longer be need
            if($scope.instance.bookable){
                $scope.instance.bookable = "1";
            } else {
                $scope.instance.bookable = "0";
            }
            if($scope.instance) {
                $scope.instance.price = parseFloat($scope.instance.price);
                $scope.instance.spaces = parseFloat($scope.instance.spaces);
                $scope.instance.spaces_unusable = parseFloat($scope.instance.spaces_unusable);
                //$scope.instance.location = parseInt($scope.instance.location);
                $scope.instance.class = $scope.instance.pricing;
            }
            if($scope.instance.date) {
                $scope.instance.date = new Date($scope.instance.date);
            }
            if($scope.instance.time_start) {
                var date_string = $scope.instance.date.getFullYear() + "-" + $scope.instance.date.getMonth() + "-" + $scope.instance.date.getDate() + " " + $scope.instance.time_start;
                $scope.instance.time_start = new Date(date_string);
            }
            $scope.event = r.data.event;
            if($scope.event) {
                if($scope.event.bookable){
                    $scope.event.bookable = "1";
                } else {
                    $scope.event.bookable = "0";
                }
                $scope.event.class = $scope.event.pricing;
                $scope.event.selectedClients = []; // NDJ - #$#$# todo when php/attendees is fixed this will no longer be need
                $scope.event.days = $scope.event.day;
                if ($scope.event) {
                    $scope.event.price = parseFloat($scope.event.price);
                    $scope.event.spaces = parseFloat($scope.event.spaces);
                }
                if ($scope.event.time_start) {
                    var date_string = $scope.instance.date.getFullYear() + "-" + $scope.instance.date.getMonth() + "-" + $scope.instance.date.getDate() + " " + $scope.event.time_start;
                    $scope.event.time_start = new Date(date_string);
                }
                if ($scope.event.date_start) {
                    $scope.event.date_start = new Date($scope.event.date_start);
                }
                if ($scope.event.date_end) {
                    $scope.event.date_end = new Date($scope.event.date_end);
                }
            } else {
                $scope.event = {}
            }
            // Check if client list and attendees are loaded
            if(r.data.attendees){
                $scope.attendees = r.data.attendees;
                if($scope.clientList){
                    $scope.combineClientAttendees();
                }
            }

            console.log($scope.instance, "loaded instance");
            console.log($scope.event, "loaded event");
        });
    }

    // Loop through all attendees and combine with the client list
    $scope.combineClientAttendees = function(){
        angular.forEach($scope.attendees, function(value, key){
            console.log(value.EventAttendee, "attendee");
            $item = $filter('filter')($scope.clientList, {TrainerClient: {user_id: value.EventAttendee.user}});
            $item[0].hidden = true;
            $item[0].status = value.EventAttendee.status;
            $item[0].attendance_id = value.EventAttendee.id;
            $item[0].registration_id = value.EventAttendee.registration;
            $item[0].checked_off = value.EventAttendee.checked_off;
            $scope.instance.selectedClients.push($item[0]);
        });
    }
    
    $scope.oldsaveEvent = function(event){
        console.log(event);
        newEvent = {
            length: event.duration,
            time_start: event.time_start,
            start: event.time_start,
            type: ($rs.user.role == 1) ? 'pt' : 'bc',
            event: true,
            title: event.title,
            additional: event,
            location: event.location,
            color: event.colour
        }
        $rs.$broadcast('addAnEvent', {event: newEvent, day: $scope.event.day, time: $scope.event.time })
    }

    // NDJ - new save function that directly saves the event
    $scope.saveEvent = function(event){
        console.log(event, 'start save event rule');

        var save_event = angular.copy(event);
        save_event.time_start = save_event.time_start.toLocaleTimeString();
        if(event.date_start) {
            save_event.date_start = event.date_start.getFullYear() + "-" + (event.date_start.getMonth() + 1) + "-" + event.date_start.getDate();
        }
        if(event.date_end) {
            save_event.date_end = event.date_end.getFullYear() + "-" + (event.date_end.getMonth() + 1) + "-" + event.date_end.getDate();
        }

        $http.post('/event/saveEvent/' + event.id,{newEvent:save_event,startDate: $scope.instance.date}).then(function(r) {
            console.log(r, 'save response');
            $rs.$broadcast('refreshCalendar');
        });
    }

    // NDJ - new save function that directly saves the event
    $scope.saveEventInstance = function(instance){

        console.log(instance, 'start save event instance');
        var save_instance = angular.copy(instance);
        save_instance.time_start = save_instance.time_start.toLocaleTimeString();
        save_instance.date = instance.date.getFullYear() + "-" + (instance.date.getMonth() + 1) + "-" + instance.date.getDate();

        $http.post('/event/saveEventInstance/' + instance.id,{newEvent: save_instance}).then(function(r) {
            console.log(r, 'save response');
            $rs.$broadcast('refreshCalendar');
        });
    }

    $scope.getCustomers();
    $rs.$broadcast('requestEventDetails');
});