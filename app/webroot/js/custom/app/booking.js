/**
 * Created by Jephcott on 5/26/2016.
 */
app.controller('booking', function($scope, $http, $rootScope, $q) {

    $rs = $rootScope;
    this.curBooking = {};
    $scope.bookOptions = {show: false};
    $scope.curBooking = {
        editBooking: false,
        notifyClient: false,
        cancelBooking: false,
        cancelPayment: false,
        length: 0
    };
    $scope.canEdit = false;

    // Saves current Booking
    $scope.saveBooking = function(curBooking){
        console.log('start save');
        console.log(curBooking, "saved booking");
        $http.post('/booking/savePtbooking/' + curBooking.id,{newBooking:$scope.curBooking}).then(function(r) {
            console.log(r, 'save response');
        });
        $rs.$broadcast('refreshCalendar');
    }

    $scope.loadBooking = function(curBookingId){
        $http.post('/booking/getPtbooking/' + curBookingId).then(function(r){
            console.log(r);
            console.log($rs.user, "root user");
            $scope.curBooking = r.data.booking;

            $scope.curBooking.editBooking = false;
            $scope.curBooking.notifyClient = false;
            $scope.curBooking.cancelBooking = false;
            $scope.curBooking.cancelPayment = false;
            if($scope.curBooking.date_start) {
                $scope.curBooking.date_start = new Date($scope.curBooking.date_start);
            }

            // Check if the user has permissions to edit the current booking (checks again in php when saved)
            if($rs.user.id == $scope.curBooking.to_user){
                $scope.canEdit = true;
            }

            console.log($scope.curBooking);
        });
    }

    $scope.resetBooking = function(curBooking){
        $scope.curBooking = {
            editBooking: false,
            notifyClient: false,
            cancelBooking: false,
            cancelPayment: false,
            length: 0
        };
    }

    $rs.$on('returnBookingDetails', function(e,deets){
        $scope.resetBooking($scope.curBooking);
        console.log(deets);
        if(deets.id){
            $scope.loadBooking(deets.id);
        }
    });

    $rs.$broadcast('requestBookingDetails');
});