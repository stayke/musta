function forceNumericInput(arguments) {
    var allowed = false;
    // perform a number of tests
    if (key >= '0' && key <= '9' && key !='.') {
        allowed = true;
    }
    return allowed;    
}


var app = angular.module("mustaTrainer", ['ngAnimate', 'lr.upload','chart.js', 'ui.timepicker', 'ui.grid.edit'], function($httpProvider){

});

app.run(function($rootScope, $http, $timeout, $templateCache, $filter, $interval) {
    
    $rs = $rootScope;
    
    // PAYMENT DETAILS
    
    $rs.pay = {
        minFee: 1.50,
        percentFee: 2.5
    }
    $rs.triedBooking = false;
    $rs.start_at_trainer = false;

	$http.defaults.headers.common = {
		Accept: "application/json, text/plain, */*",
		'X-Requested-With': 'XMLHttpRequest'
	};
            
    $rs.defaults = function(){
	    $rs.p = '/js/custom/app/partials/'; 
		$rs.curLoading = 0;
		$rs.loaded = false;
        $rs.page = {};  
        $rs.header = {};  
        $rs.page.bottomSlider = {display: true};
        $rs.page.leftSlider = {display: false};
        $rs.page.rightSlider = {display: false};
        $rs.page.topSlider = {display: false, template: $rs.p + 'notifications.html'};
        $rs.loginform = {};
        $rs.user = false;
        $rs.getUser();
		$rs.viewingUser = false;
		$rs.availability = [];
		$rs.addSessionFormData = {};
		$rs.preloadTemplates();
		$rs.settings = {};
		$rs.page.settings = {template: $rs.p + '/headers/loggedout.html'};
		$rs.userLocation = $rs.getLocation();
		$rs.userLocations = [];
		$rs.profile = {view: false};
        $rs.geocoder = new google.maps.Geocoder;
        $rs.calendarbook = $rs.p + '/calendar.html';
		$rs.notifications = [];
		$rs.getLocation();
    }
	
	$rs.getLocation = function(){
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(location){
				$rs.userLocation = location;
				$rs.$broadcast('LocationFound');
			});
			
		} 
	}
    
    $rs.getUser = function(callback){
        $http.get('/users/get_token')
        .then(function(response){
           $rs.user = response.data;
           $rs.startup();
		   if (callback) {
				callback();
		   }
        });
    }
    
    $rs.loading = function(r, callback){
        if (r) {
            $rs.page.bottomSlider.template = $rs.p + 'loading.html';
            $rs.page.bottomSlider.display = true;
            $rs.page.bottomSlider.wide = false;
        } else {
            $rs.page.bottomSlider.display = false;
			$rs.loaded = true;
            $rs.$digest();
        }
        
        if (callback) {
            $timeout(function(){ callback() }, 400);
        }
        $rs.showHead = true;
    }
	
	$rs.preloadTemplates = function(){
		var templates = [
			// Base Templates
			'calendar.html',
			'home.html',
			'loading.html',
			'login.html',
			'notifications.html',
			// Calendar Templates
			'calendar/new-session.html',
			'calendar/new-available.html',
			'calendar/date-options.html',
            'calendar/holiday.html',
            'calendar/calendarbook.html',
            'calendar/register.html',
            'calendar/event.html',
			// Header Templates
			'headers/admin.html',
			'headers/bootcamp.html',
			'headers/client.html',
			'headers/loggedout.html',
			'headers/trainer.html',
			// Signup Templates
			'signup/entry.html',
			'signup/controls.html',
			'signup/pt/personal.html',
			'signup/pt/terms.html',
            'signup/pt/mustaterms.html',
            'signup/submit.html',
			// Locations Templates
			'locations/list.html',
			// Profile
			'profile/view.html',
			'profile/edit.html',
            // Pay Information
            'pay/bankdetails.html',
            // Payment Section
            'payments/pay.html'
		]
		
		for(var x=0;x<templates.length;x++){
			$rs.curLoading++;
			$http.get($rs.p + templates[x]).success((function(x){
				return function (t) {
					$templateCache.put($rs.p + templates[x], t);
					$rs.checkLoading();
				}	
			})(x));
		}
	}
	
	$rs.checkLoading = function(){
		$rs.curLoading--;
		if ($rs.curLoading == 0) {			
			$timeout(function(){  $rs.loading(false); }, 1000); 
		}
	}
    
    $rs.loginForm = function(){
		$rs.page.bottomSlider.wide = false;
        $rs.page.bottomSlider.error = false;
        $rs.page.bottomSlider.template = $rs.p + 'login.html';
        $rs.page.bottomSlider.display = true;
    }
    
    $rs.resetPassword = function(username){
        var l = Ladda.create($('#resetButton')[0]);
	 	l.start();
        $rs.page.bottomSlider.error = false;
        $rs.page.bottomSlider.message = false;
        $http.post('/users/reset', {email: username})
        .then(function(response){
            l.stop();
            if (response.data.error) {
                $rs.shakeAnimation($('#BoostCakeDisplayForm'));
                $rs.page.bottomSlider.error = response.data.error.text;
                $timeout(function(){ $rs.page.bottomSlider.error = false; }, 3000);
            } else {
                $rs.page.bottomSlider.message = response.data.message.text;
                $timeout(function(){ $rs.page.bottomSlider.message = false; }, 3000);
            }
        });
    }
    
    $rs.login = function(username,password){
        var l = Ladda.create($('#loginButton')[0]);
	 	l.start();
        $rs.page.bottomSlider.error = false;
        $http.post('/users/login', {email: username, password: password})
        .then(function(response){
           if (response.data.error) {
             $rs.shakeAnimation($('#BoostCakeDisplayForm'));
             $rs.page.bottomSlider.error = response.data.error.text;
             $timeout(function(){ $rs.page.bottomSlider.error = false; }, 3000);
           } else{
            
            if (response.data.user.role == 1 || response.data.user.role == 2) {
                window.location.href='/trainer/';
            } else if(window.location.pathname == '/') {
                window.location.href='/search/';
            } else {
                $rs.getUser(function(){ $rs.goBack(); });
            }
			
           } 
        l.stop();
        });
    }
    
    $rs.logout = function(){
	    $rs.loading(true);
        $http.post('/users/logout')
        .then(function(response){
           window.location = '/';
        });
    }
    
    $rs.shakeAnimation = function(element){
        TweenMax.to(element, .05, {
          x: -7,
          ease: Quad.easeInOut
        });
        TweenMax.to(element, .05, {
          repeat: 4,
          x: 7,
          yoyo: true,
          delay: .1,
          ease: Quad.easeInOut
        });
        TweenMax.to(element, .1, {
          x: 0,
          delay: .1 * 4
        });
    }
	
	$rs.viewCalendar = function(id){
		$rs.page.viewing = id;
        $rs.page.template = $rs.p + 'calendar.html';
		$rs.$broadcast('calendarDataUpdate');
	}
	
    
    $rs.startup = function(){
        $rs.page.settings.template = $rs.p + 'headers/loggedout.html';
        $rs.page.template = $rs.p + 'home.html';
		
        if ($rs.user.role == 0) { // Admin
            $rs.page.settings.template = $rs.p + 'headers/admin.html'
            $rs.page.template = $rs.p + 'headers/admin.html'
        } else if ($rs.user.role == 1) { // Personal Trainer
            $rs.page.settings.template = $rs.p + 'headers/trainer.html'
			$rs.page.viewing = $rs.user.id;
            $rs.page.template = $rs.p + 'calendar.html'
        } else if ($rs.user.role == 2) { // Bootcamp
            $rs.page.settings.template = $rs.p + 'headers/bootcamp.html'
			$rs.page.viewing = $rs.user.id;
            $rs.page.template = $rs.p + 'calendar.html'
        } else if ($rs.user.role == 3) { // Client
            $rs.page.settings.template = $rs.p + 'headers/client.html'
            $rs.page.template = $rs.p + 'search.html'
        }
       
    }
    
    $rs.openLeft = function(){
        $rs.page.leftSlider.display = true;
    }
    
    $rs.closeLeft = function(){
        $rs.page.leftSlider.display = false;
    }
    
    $rs.openRight = function(){
        $rs.page.rightSlider.display = true;
    }
    
    $rs.closeRight = function(){
        $rs.page.rightSlider.display = false;
    }
    
    $rs.addSessionForm = function(){
        $rs.page.bottomSlider.template = $rs.p + 'calendar/new-session.html';
        $rs.page.bottomSlider.display = true;
        $rs.page.bottomSlider.wide = true;
    }
	
	$rs.addAvailableForm = function(){
        $rs.page.bottomSlider.template = $rs.p + 'calendar/new-available.html';
        $rs.page.bottomSlider.display = true;
        $rs.page.bottomSlider.wide = true;
    }
	
	$rs.locationsPage = function(){
        $rs.page.bottomSlider.template = $rs.p + 'locations/list.html';
        $rs.page.bottomSlider.display = true;
        $rs.page.bottomSlider.wide = true;
        $rs.toggleSettings();
    }
	
    $rs.defaults();
    
    $rs.dateFormatForInput = function(date){
    date = new Date(date);
      MyDateString = date.getFullYear() + '-'
			+ ('0' + (date.getMonth()+1)).slice(-2) + '-'
             + ('0' + date.getDate()).slice(-2);
      return MyDateString;
    }
	
	$rs.formatDate = function(date){
		return ('0' + date).slice(-2)
	}
	
	
	
	
	$rs.getDuration = function(start, duration){
        var time = new Date(1,1,2000, start.slice(0,2), (parseInt(start.slice(3,5))+parseInt(duration)), start.slice(6,8), 00);

        return $rs.formatDate(time.getHours()) + ':' + $rs.formatDate(time.getMinutes());
    }
	
	$rs.closeSlider = function(e){
		if (e.keyCode == 27) {
			if ($rs.page.bottomSlider.display || $rs.page.rightSlider.display || $rs.page.leftSlider.display || $rs.page.topSlider.display) {
				window.history.back();
			} else if($rs.page.settings.display){
				$rs.page.settings.display = false;
			}		
		}
	}
	
	$rs.$watch('page.bottomSlider.display', function(newValue, oldValue) {
            if (newValue == oldValue) {
                return;
            }
			if (newValue && !oldValue) {
				$('#bottomSlider').scrollTop(0);
			}
			$rs.setHash();
    });
	
	$rs.$watch('page.leftSlider.display', function(newValue, oldValue) {
            if (newValue == oldValue) {
                return;
            }
			if (newValue && !oldValue) {
				$('#leftSlider').scrollTop(0);
			}
			$rs.setHash();
    });
	
	$rs.$watch('page.rightSlider.display', function(newValue, oldValue) {
            if (newValue == oldValue) {
                return;
            }
			if (newValue && !oldValue) {
				$('#rightSlider').scrollTop(0);
			}
			$rs.setHash();
    });
	
	$rs.$watch('page.settings.display', function(newValue, oldValue) {
            if (newValue == oldValue) {
                return;
            }
			if (newValue && !oldValue) {
				$('#settings').scrollTop(0);
			}
			$rs.setHash();
    });
	
	$rs.$watch('page.topSlider.display', function(newValue, oldValue) {
            if (newValue == oldValue) {
                return;
            }
			if (newValue && !oldValue) {
				$('#topSlider').scrollTop(0);
			}
			$rs.setHash();
    });
	
	$rs.setHash = function(){
	
		var hash = '/';
		var first = true;
	
		if ($rs.page.bottomSlider.display && $rs.page.bottomSlider.template && $rs.page.bottomSlider.template.indexOf('loading') < 0) {
			hash += 'bottom';
			first = false;
		}
		
		if ($rs.page.leftSlider.display  && $rs.page.leftSlider.template) {
			if (!first) {
				hash += '/';
			}
			hash += 'left';
			first = false;
		}
		
		if ($rs.page.rightSlider.display  && $rs.page.rightSlider.template) {
			if (!first) {
				hash += '/';
			}
			hash += 'right';
			first = false;
		}
		
		if ($rs.page.topSlider.display) {
			if (!first) {
				hash += '/';
			}
			hash += 'top';
			first = false;
		}
		
		
        if (hash.length > 1) {
            window.location.hash = hash;
        }
		
		
	
	}
	
	$rs.$watch(function () {
		return window.location.hash
	}, function (value, old) {
		if ( document.readyState !== 'complete' ) return;
		
		var bottom = false;
		var left = false;
		var right = false;
		var settings = false;
		var top = false;
		
		if (value.indexOf('bottom') > 0 && $rs.page.bottomSlider.template || ($rs.page.bottomSlider.template && $rs.page.bottomSlider.template.length < 1)) {
			bottom = true;
		}
		
		if (value.indexOf('left') > 0 && $rs.page.leftSlider.template) {
			left = true;
		}
		
		if (value.indexOf('right') > 0 && $rs.page.rightSlider.template) {
			right = true;
		}
		
		if (value.indexOf('settings') > 0) {
			settings = true;
		}
		
		if (value.indexOf('top') > 0) {
			top = true;
		}

		$rs.page.bottomSlider.display = bottom;
		$rs.page.leftSlider.display = left;
		$rs.page.rightSlider.display = right;
		$rs.page.topSlider.display = top;
		$rs.page.settings.display = settings;
		
		$rs.setHash();
		
	});
	
	$rs.goBack = function(){
		if ($rs.page.settings.display) {
			$rs.page.settings.display = false;
		} else {
			window.history.back();
		}
	}
	
	$rs.toggleSettings = function(){
		if ($rs.page.settings.display) {
			$rs.page.settings.display  = false;
		} else {
			$rs.page.settings.display  = true;
		}
	}
	
	$rs.toggleNotifications = function(){
		if ($rs.page.topSlider.display) {
			$rs.goBack();
		} else {
			$rs.page.topSlider.display = true;
		}
	}
	
	$rs.signupForm = function(redirect){
        if ($rs.user.id) {
            alert('You are logged in.  Please Log out to continue with sign up.');
            return false;
        }
        if (!redirect) {
            redirect = false;
        }
		$rs.page.bottomSlider.template = $rs.p + 'signup/entry.html';
		$rs.page.bottomSlider.wide = true;
        $rs.$broadcast('resetSignup');
		$rs.page.bottomSlider.display = true;
        $rs.signupredirect = redirect;
	}

    $rs.signupFormTrainer = function(redirect){
        if ($rs.user.id) {
            alert('You are logged in.  Please Log out to continue with sign up.');
            return false;
        }
        if (!redirect) {
            redirect = false;
        }
        $rs.page.bottomSlider.template = $rs.p + 'signup/entry.html';
        $rs.page.bottomSlider.wide = true;
        $rs.start_at_trainer = true;
        $rs.$broadcast('resetSignup');
        $rs.page.bottomSlider.display = true;
        $rs.signupredirect = redirect;
    }
	
	$rs.profilePage = function(){
			$rs.trainerProfilePageOpen();
            $rs.toggleSettings();

	}
	
	$rs.trainerProfilePageOpen = function(){
		$rs.page.bottomSlider.template = $rs.p + 'profile/edit.html';
		$rs.page.bottomSlider.wide = true;
		$rs.page.bottomSlider.display = true;
	}
    
    $rs.accountPage = function(){
        $rs.toggleSettings();
		$rs.page.bottomSlider.template = $rs.p + 'signup/pt/personal.html';
		$rs.page.bottomSlider.wide = true;
		$rs.page.bottomSlider.display = true;
	}
    
    $rs.personalDetails = function(){
        if ($rs.user.role == 3) {
            $rs.page.bottomSlider.template = $rs.p + 'signup/client/personal.html';
        } else {
            $rs.page.bottomSlider.template = $rs.p + 'signup/pt/personal.html';
        }
		$rs.page.bottomSlider.wide = true;
		$rs.page.bottomSlider.display = true;
	}

    $rs.parq = function(parqid){
        $rs.page.bottomSlider.template = $rs.p + 'signup/client/parq.html';
		$rs.page.bottomSlider.wide = true;
		$rs.page.bottomSlider.display = true;
        $rs.parqid = parqid;
        $rs.$broadcast('requestParqDetails');
    }

    $rs.$on('requestParqDetails', function(){
        $rs.$broadcast('returnParqDetails', $rs.parqid);
    });

    $rs.bank_details = function(){
        $rs.page.bottomSlider.template = $rs.p + 'profile/salesforce-bankdetails.html';
		$rs.page.bottomSlider.wide = true;
		$rs.page.bottomSlider.display = true;
        $rs.$broadcast('requestUserDetails');
    }

    $rs.$on('requestParqDetails', function(){
        $rs.$broadcast('returnParqDetails', $rs.parqid);
    });
    
    $rs.tandc = function(){
		$rs.page.bottomSlider.template = $rs.p + 'signup/pt/terms.html';
		$rs.page.bottomSlider.wide = true;
		$rs.page.bottomSlider.display = true;
        $rs.toggleSettings();
        $http.get('/Users/getDefaultTerms').then(function(r){ $rs.$broadcast('defaultTandCs', r.data) });
	}
	
	$rs.getLocations = function(userid){
        if (!$rs.user.id) {
            return;
        }
		if (!userid) {
			userid = $rs.user.id;
		}
		
		$http.get('/locations/view/' + userid)
		.then(function(response){
			$rs.userLocations = response.data; 
		});
	}
	
	$rs.getInfo = function(meta_name) {
		var found = $filter('filter')($rs.profile.view.Userinfo, {meta_key: meta_name}, true);
		if (found.length) {
			return found[0].meta_value;
		} else {
			return false;
		}
	}
	
	$rs.getLLfromPostcode = function(postcode, callback){
		if (!postcode) {
			return false;
		}
		if (postcode < 1000 || postcode > 9999) {
			return false;
		}
		$http.get('http://maps.googleapis.com/maps/api/geocode/json?address='+postcode+'%20Australia', { headers: { 'X-Requested-With': undefined} })
		.then(function(r){
			callback(r.data.results[0].geometry.location);
		});
	}
    
    $rs.$on('timeToPay', function(e,f){
        $rs.paymentinformation = f;
        $rs.page.bottomSlider.wide = false;
        $rs.page.bottomSlider.error = false;
        $rs.page.bottomSlider.template = $rs.p + 'payments/pay.html';
        $rs.page.bottomSlider.display = true;
    });
    
    $rs.getBookingFee = function(type, fee){
        
    }
    
    $rs.showRegister = function(event){
        if (event.type == 'pt' || event.event) {
            return false;
        }
        $rs.register = {event: event}
        $rs.$broadcast('needRegisterID', $rs.register);
        $rs.page.bottomSlider.template = $rs.p + 'calendar/register.html';
		$rs.page.bottomSlider.wide = true;
		$rs.page.bottomSlider.display = true;
    }
    
    $rs.showEvent = function(day, time, location){
        $rs.page.bottomSlider.template = $rs.p + 'calendar/event.html';
        $rs.page.bottomSlider.wide = true;
		$rs.page.bottomSlider.display = true;
        $rs.tempevent = {length: length, day: day, time: time, location: location};
        $rs.$broadcast('requestEventDetails');
    }

    // NDJ - used to edit existing events
    $rs.editEvent = function(event){
        $rs.page.bottomSlider.template = $rs.p + 'calendar/event.html';
        $rs.page.bottomSlider.wide = true;
        $rs.page.bottomSlider.display = true;
        $rs.tempevent = angular.copy(event);
        $rs.$broadcast('requestEventDetails');
    }

    $rs.$on('requestEventDetails', function(){
        $rs.$broadcast('returnEventDetails', $rs.tempevent);
    });

    // NDJ - used to edit existing bookings
    $rs.editBooking = function(booking){
        $rs.page.bottomSlider.template = $rs.p + 'calendar/booking.html';
		$rs.page.bottomSlider.wide = true;
		$rs.page.bottomSlider.display = true;
        $rs.tempbooking = angular.copy(booking);
		$rs.$broadcast('requestBookingDetails');
	}

    $rs.$on('requestBookingDetails', function(){
        $rs.$broadcast('returnBookingDetails', $rs.tempbooking);
    });

    $rs.showEventAttendance = function(event){
        $rs.page.bottomSlider.template = $rs.p + 'calendar/eventattend.html';
        $rs.page.bottomSlider.wide = true;
        $rs.page.bottomSlider.display = true;
        $rs.tempevent = angular.copy(event);
        $rs.$broadcast('requestEventDetails');
    }

    // NDJ - used to edit existing locations
    $rs.editLocationById = function(locationid){
        $rs.page.bottomSlider.template = $rs.p + 'locations/locationedit.html';
        $rs.page.bottomSlider.wide = true;
        $rs.page.bottomSlider.display = true;
        $rs.templocation = locationid;
        //$rs.templocation = angular.copy(location);
        $rs.$broadcast('requestLocationDetails');
    }

    $rs.$on('requestLocationDetails', function(){
        $rs.$broadcast('returnLocationDetails', $rs.templocation);
    });

    //$rs.showRule = function(id, date, time){
    $rs.showRule = function(id){
        $rs.page.bottomSlider.template = $rs.p + 'calendar/calendarrule.html';
        $rs.page.bottomSlider.wide = true;
        $rs.page.bottomSlider.display = true;
        $rs.temprule = {id: id};
        $rs.$broadcast('requestRuleDetails');
    }

    $rs.$on('requestRuleDetails', function(){
        $rs.$broadcast('returnRuleDetails', $rs.temprule);
    });

    // NDJ - used to show options when clicking on a blank spot
    $rs.openAddOptions = function(date, time) {
        $rs.page.bottomSlider.template = $rs.p + 'calendar/addoptions.html';
        $rs.page.bottomSlider.wide = true;
        $rs.page.bottomSlider.display = true;
        $rs.clickedTime = {date: date, time: time};
    }

    //NDJ - used to pay for payments
    $rs.makePayment = function(data){
        $rs.page.topSlider.display = false;
        $rs.page.bottomSlider.template = $rs.p + 'payments/payform.html';
        $rs.page.bottomSlider.wide = true;
        $rs.page.bottomSlider.display = true;
        $rs.tempPayment = {data: data};
        $rs.$broadcast('requestPaymentDetails');
    }

    $rs.$on('requestPaymentDetails', function(){
        $rs.$broadcast('returnPaymentDetails', $rs.tempPayment);
    });

    $rs.$on('needRegisterID', function(data){
        $rs.$broadcast('giveRegisterID', $rs.register);
    });
    
    $rs.checkNotifications = function(){
        $http.get('/notifications/get')
        .then(function(r){
            $rs.notifications = r.data;   
        });
    }
    
    $rs.checkNotifications();
    
    $interval(function(){
        if (!$rs.user.id) {
            return false;
        }
        $rs.checkNotifications();
    }, 10000);
    
    $rs.amIFave = function(id){
		if (!$rs.user.id) {
            return false;
        }
        
        filtered = $filter('filter')($rs.user.favourites, {Favourite: {trainer_id: id}});
        return (filtered.length>0) ? true : false;
	}
    
    $rs.updateFave = function(id, add){
		if (!$rs.user.id) {
            alert('You must create an account to save profiles');
            $rs.signupForm(true);
            return false;
        }
        $http.get('/Favourites/updateFave/' + id + '/' + add)
        .then(function(r){
            $rs.getUser();
        });
	}
    
	
});

app.controller('bottomSlider', function($scope) {

        
        
});

app.filter('dateNth', function() {
  return function(d) {
    if(d>3 && d<21) return d + 'th'; // thanks kennebec
    switch (d % 10) {
          case 1:  return d + "st";
          case 2:  return d + "nd";
          case 3:  return d + "rd";
          default: return d + "th";
    }
  };
});


app.animation('.slide', ['$timeout' ,function($timeout) {
	return {
	  // make note that other events (like addClass/removeClass)
	  // have different function input parameters
	  enter: function(element, doneFn) {
		  height = $(element).innerHeight();
		  padding = $(element).css('padding');
		  TweenMax.set(element,{height:0, paddingTop:0, paddingBottom: 0, opacity: 0, position: 'absolute'});
		  $timeout(function(){
			  TweenMax.set(element,{position: 'relative'});
			  TweenMax.to(element,0.4,{height:height, padding: padding, opacity: 1, onComplete: doneFn});
		  }, 400);
  
		// remember to call doneFn so that angular
		// knows that the animation has concluded
	  },
  
	  leave: function(element, doneFn) {
		TweenMax.to(element,0.4,{height:0, paddingTop:0, paddingBottom:0, opacity: 0, onComplete: doneFn});
	  }
	}
  }]
);

app.animation('.slide2', ['$timeout' ,function($timeout) {
	return {
	  // make note that other events (like addClass/removeClass)
	  // have different function input parameters
	  enter: function(element, doneFn) {
		  height = $(element).innerHeight();
		  padding = $(element).css('padding');
		  TweenMax.set(element,{height:0, paddingTop:0, paddingBottom: 0, opacity: 0, overflow: 'hidden'});
		  $timeout(function(){
			  TweenMax.to(element,0.4,{height:height, padding: padding, opacity: 1, onComplete: doneFn});
		  }, 400);
  
		// remember to call doneFn so that angular
		// knows that the animation has concluded
	  },
  
	  leave: function(element, doneFn) {
		TweenMax.to(element,0.4,{height:0, paddingTop:0, paddingBottom:0, opacity: 0, onComplete: doneFn});
	  }
	}
  }]
);

app.animation('.notificationSlide', ['$timeout' ,function($timeout) {
	return {
	  // make note that other events (like addClass/removeClass)
	  // have different function input parameters
	  enter: function(element, doneFn) {
        TweenMax.set(element,{xPercent:-100, opacity:0, onComplete: doneFn});
        TweenMax.to(element,0.4,{xPercent:0, opacity:1, onComplete: doneFn});
  
		// remember to call doneFn so that angular
		// knows that the animation has concluded
	  },
  
	  leave: function(element, doneFn) {
		TweenMax.to(element,0.4,{xPercent: 100, opacity:0, onComplete: doneFn});
	  }
	}
  }]
);


String.prototype.fromCamel = function(){
	return this.replace(/([A-Z])/g, function($1){return " "+$1;});
};
